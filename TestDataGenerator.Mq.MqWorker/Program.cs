﻿using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;
using TestDataGenerator.Mq.MqWorker.Models;
using TestDataGenerator.Core.Columns;
using TestDataGenerator.Core;
using TestDataGenerator.Abstractions.Interfaces.Rows;
using System.Linq;
using Serilog;
using System.Threading;

namespace TestDataGenerator.Mq.MqWorker
{
    class Program
    {
        private static ManualResetEvent _resetEvent = new ManualResetEvent(false);
        private static TestDataSetMqWorker _datasetMqWorker;
        private static int _connTestSleepMs = 15000;

        static void Main(string[] args)
        {
            ConfigurationSetup configSetup = new ConfigurationSetup();
            ServiceSettings settings = configSetup.ReadSettings("ServiceSettings");
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.Console()
                .WriteTo.File(settings.LogFilePath, rollingInterval: RollingInterval.Day,
                    rollOnFileSizeLimit: true, fileSizeLimitBytes: 21000000, retainedFileCountLimit: 10)
                .CreateLogger();

            Log.Information("Starting application with hostname={hostname} and input queue={queuename}...", 
                settings.RabbitMqHostname, settings.InputQueueName);

            AppDomain.CurrentDomain.ProcessExit += CurrentDomain_ProcessExit;
            Console.CancelKeyPress += Console_CancelKeyPress;

            using (_datasetMqWorker = new TestDataSetMqWorker(settings.RabbitMqHostname, settings.InputQueueName))
            {
                while (!_datasetMqWorker.MqServerConnectionReady())
                {
                    Log.Information($"Restarting connection test in {_connTestSleepMs} ms.");
                    Thread.Sleep(_connTestSleepMs);
                }
                _datasetMqWorker.StartSworker();
                _resetEvent.WaitOne();
            }

            Log.Information("Application stopped.");
        }

        private static void Console_CancelKeyPress(object sender, ConsoleCancelEventArgs e)
        {
            Log.Information("Application stop command received.");
            _datasetMqWorker.Dispose();
            _resetEvent.Set();
        }

        private static void CurrentDomain_ProcessExit(object sender, EventArgs e)
        {
            _datasetMqWorker.Dispose();
        }
    }
}
