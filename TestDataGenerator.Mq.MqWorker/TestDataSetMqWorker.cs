﻿using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using TestDataGenerator.Abstractions.Interfaces.Rows;
using TestDataGenerator.Core;
using TestDataGenerator.Core.Columns;
using TestDataGenerator.IO.DatasetWriters;
using TestDataGenerator.Mq.MqWorker.Models;

namespace TestDataGenerator.Mq.MqWorker
{
    public class TestDataSetMqWorker: IDisposable
    {
        private IConnection _connection;
        private IModel _channel;
        private readonly ConnectionFactory _connFactory;
        private readonly string _inputQueueName;

        public TestDataSetMqWorker(string hostName, string inputQueueName)
        {
            _connFactory = new ConnectionFactory() {
                HostName = hostName
            };
            _inputQueueName = inputQueueName;
        }

        public bool MqServerConnectionReady()
        {
            try
            {
                Log.Information("Testing connection to host: '{hostname}'...", _connFactory.HostName);
                using IConnection conn = _connFactory.CreateConnection();
                conn.Close();
                Log.Information("Connection test successful!");
                return true;
            }
            catch (Exception)
            {
                Log.Warning("Connection test to host: '{hostname}' failed!", _connFactory.HostName);
                return false;
            }
        }

        public void StartSworker()
        {
            try
            {
                Log.Information("Connecting to server '{hostname}'...", _connFactory.HostName);
                _connection = _connFactory.CreateConnection();
                _channel = _connection.CreateModel();
                Log.Information("Connection established.");
                _channel.QueueDeclare(_inputQueueName, true, false);
                Log.Information("Queue '{inputQueueName}' declared. Ready to work!", _inputQueueName);
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Failed to establish connection to RabbitMq!");
                throw;
            }


            EventingBasicConsumer consumer = new EventingBasicConsumer(_channel);
            consumer.Received += async (model, eventArgs) =>
            {
                string response = null;
                byte[] body = eventArgs.Body.ToArray();
                string message = Encoding.UTF8.GetString(body);
                IBasicProperties incomingProps = eventArgs.BasicProperties;

                IBasicProperties replyProps = _channel.CreateBasicProperties();
                replyProps.CorrelationId = eventArgs.BasicProperties.CorrelationId;
                replyProps.DeliveryMode = 2;

                Log.Information("Received dataset generation request with CorrelationId={CorrelationId}.", eventArgs.BasicProperties.CorrelationId);

                long incomingRequestId = long.Parse(eventArgs.BasicProperties.Headers["requestId"].ToString());
                TestDataSetResultMqDto resultDataSet = null;
                try
                {
                    TestDataSetCreateMqDto dataSetCreationDto = JsonConvert.DeserializeObject<TestDataSetCreateMqDto>(message);

                    Log.Information("Processing dataset generation request with requestId={requestId}.", incomingRequestId);

                    ColumnSet colSet = ColumnSet.DeserializeFromJson(dataSetCreationDto.ColumnSetAsJson);
                    TestDataSet dataSet = new TestDataSet(new Guid().ToString(), colSet);

                    if (dataSetCreationDto.FileExtension == null)
                    {
                        Log.Information("Generating {rowsCount} rows for requestId={requestId}.", dataSetCreationDto.RowsToGenerate, incomingRequestId);

                        IRow[] generatedRows = dataSet.GenerateNewDataSet(dataSetCreationDto.RowsToGenerate);
                        SingleRowMqDto[] resultRows = generatedRows.Select(r => new SingleRowMqDto()
                        {
                            Values = r.Select((v, i) => r.GetStringRepresentation(i)).ToArray()
                        }).ToArray();
                        resultDataSet = new TestDataSetResultMqDto()
                        {
                            RequestId = incomingRequestId,
                            GeneratedRows = resultRows
                        };
                    }
                    else if (dataSetCreationDto.FileExtension == "csv")
                    {
                        Log.Information("Generating CSV file with {rowsCount} rows for requestId={requestId}",
                            dataSetCreationDto.RowsToGenerate, incomingRequestId);

                        CsvDataSetWriter csvWriter = new CsvDataSetWriter();
                        using (MemoryStream memStream = new MemoryStream())
                        {
                            csvWriter.WriteToStreamUsingInPlaceRowGenerationAsync(memStream, dataSet, dataSetCreationDto.RowsToGenerate).Wait();
                            resultDataSet = new TestDataSetResultMqDto()
                            {
                                RequestId = incomingRequestId,
                                GeneratedFile = memStream.ToArray()
                            };
                        }
                    }
                    else if (dataSetCreationDto.FileExtension == "json")
                    {
                        Log.Information("Generating JSON file with {rowsCount} rows for requestId={requestId}",
                            dataSetCreationDto.RowsToGenerate, incomingRequestId);

                        JsonDataSetWriter jsonWriter = new JsonDataSetWriter();
                        using (MemoryStream memStream = new MemoryStream())
                        {
                            await jsonWriter.WriteToStreamUsingInPlaceRowGenerationAsync(memStream, dataSet, dataSetCreationDto.RowsToGenerate);
                            resultDataSet = new TestDataSetResultMqDto()
                            {
                                RequestId = incomingRequestId,
                                GeneratedFile = memStream.ToArray()
                            };
                        }
                    }

                    response = JsonConvert.SerializeObject(resultDataSet);

                    Log.Information("Request with requestId={requestId} successfully processed!", incomingRequestId);
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "Failed to generate dataset for requestId={requestId} and correlationId={correlationId}!",
                        incomingRequestId, eventArgs.BasicProperties.CorrelationId);

                    resultDataSet = new TestDataSetResultMqDto()
                    {
                        RequestId = incomingRequestId,
                        ErrorText = ex.Message,
                        SystemExceptionText = ex.ToString()
                    };
                    response = JsonConvert.SerializeObject(resultDataSet);
                }
                finally
                {
                    var responseBytes = Encoding.UTF8.GetBytes(response);
                    _channel.BasicPublish(exchange: "", routingKey: eventArgs.BasicProperties.ReplyTo,
                      basicProperties: replyProps, body: responseBytes);
                    _channel.BasicAck(deliveryTag: eventArgs.DeliveryTag,
                      multiple: false);
                    Log.Information("Response message for requestId={requestId} and correlationId={correlationId}" +
                        " was successfully enqueued to response queue.",
                        incomingRequestId, eventArgs.BasicProperties.CorrelationId);
                }
            };

            _channel.BasicConsume(queue: _inputQueueName, autoAck: false, consumer: consumer);
        }

        public void Dispose()
        {
            Log.Information("Disposing MqWorker...");
            this._channel?.Close();
            this._channel?.Dispose();
            this._connection?.Close();
            this._connection?.Dispose();
        }
    }
}
