﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestDataGenerator.Mq.MqWorker
{
    public class ServiceSettings
    {
        public string InputQueueName { get; set; }

        public string RabbitMqHostname { get; set; }

        public string LogFilePath { get; set; }
    }
}
