﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace TestDataGenerator.Mq.MqWorker
{
    public class ConfigurationSetup
    {
        public IConfiguration Configuration { get; private set; }

        public ConfigurationSetup()
        {
            string aspNetCoreEnvName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            IConfigurationBuilder configBuilder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.{aspNetCoreEnvName}.json", true);

            Configuration = configBuilder.Build();
        }

        public ServiceSettings ReadSettings(string sectionName)
        {
            IConfigurationSection section = Configuration.GetSection(sectionName);
            ServiceSettings options = new ServiceSettings();
            section.Bind(options);
            return options;
        }
    }
}
