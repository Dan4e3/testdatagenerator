﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestDataGenerator.Mq.MqWorker.Models
{
    public class TestDataSetResultMqDto
    {
        public long RequestId { get; set; }

        public SingleRowMqDto[] GeneratedRows { get; set; }

        public byte[] GeneratedFile { get; set; }

        public string ErrorText { get; set; }

        public string SystemExceptionText { get; set; }
    }
}
