﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestDataGenerator.Mq.MqWorker.Models
{
    public class TestDataSetCreateMqDto
    {
        public long RequestId { get; set; }

        public string ColumnSetAsJson { get; set; }

        public int RowsToGenerate { get; set; }

        public string FileExtension { get; set; }
    }
}
