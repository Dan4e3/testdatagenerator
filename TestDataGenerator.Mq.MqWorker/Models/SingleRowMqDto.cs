﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestDataGenerator.Mq.MqWorker.Models
{
    public class SingleRowMqDto
    {
        public string[] Values { get; set; }
    }
}
