﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.Abstractions.Settings
{
    /// <summary>
    /// Instance of null values generation settings
    /// </summary>
    public class NullValuesSettings
    {
        private int _averageRowsBeforeNullAppearance = 10;

        /// <summary>
        /// Flag which indicates whether null values should be generated
        /// </summary>
        public bool GenerateNullValues { get; set; }

        /// <summary>
        /// Average number of rows before null value will be generated. Must be greater or equal to 1.
        /// </summary>
        public int AverageRowsBeforeNullAppearance
        {
            get => _averageRowsBeforeNullAppearance;
            set
            {
                if (value < 1)
                    throw new ArgumentException($"{nameof(AverageRowsBeforeNullAppearance)} property must be greater or equal to 1.");

                _averageRowsBeforeNullAppearance = value;
            }
        }

        /// <summary>
        /// Decide using random whether next generated value should be null or not.
        /// </summary>
        /// <param name="randInstance">Instance of Random</param>
        /// <returns>True if next value supposed to be null. False, otherwise.</returns>
        public bool ShouldNextValueBeNull(Random randInstance)
        {
            if (!GenerateNullValues)
                return false;

            int randValue = randInstance.Next(0, AverageRowsBeforeNullAppearance);

            if (randValue == AverageRowsBeforeNullAppearance - 1)
                return true;

            return false;
        }
    }
}
