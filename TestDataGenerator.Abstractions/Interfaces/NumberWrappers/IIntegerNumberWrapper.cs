﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.Abstractions.Interfaces.NumberWrappers
{
    public interface IIntegerNumberWrapper<T> where T: struct
    {
        long Value { get; }

        T Add(T anotherValue);
        T Subtract(T anotherValue);
        T Multiply(T anotherValue);
        T Divide(T anotherValue);
    }
}
