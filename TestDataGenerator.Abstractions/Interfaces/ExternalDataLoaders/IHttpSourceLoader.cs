﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.Abstractions.Interfaces.ExternalDataLoaders
{
    /// <summary>
    /// Provides methods for loading data via HTTP
    /// </summary>
    public interface IHttpSourceLoader
    {
        /// <summary>
        /// Load array of data from external http source
        /// </summary>
        /// <param name="requestMessage">Http request message to send</param>
        /// <returns>Data array extracted from server response</returns>
        Task<object[]> LoadDataFromHttpSourceAsync(HttpRequestMessage requestMessage);
    }
}
