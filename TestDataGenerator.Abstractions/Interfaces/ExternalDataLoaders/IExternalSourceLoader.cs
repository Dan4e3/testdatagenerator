﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.Abstractions.Interfaces.ExternalDataLoaders
{
    public interface IExternalSourceLoader: IHttpSourceLoader, IFileSourceLoader, IDatabaseSourceLoader
    {

    }
}
