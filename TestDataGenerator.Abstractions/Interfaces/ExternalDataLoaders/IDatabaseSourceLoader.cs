﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.Abstractions.Interfaces.ExternalDataLoaders
{
    /// <summary>
    /// Provides methods for loading one-dimensional arrays of data from database source
    /// </summary>
    public interface IDatabaseSourceLoader
    {
        /// <summary>
        /// Load data from single column into object array using table name and column name
        /// </summary>
        /// <param name="tableName">Name of table to query</param>
        /// <param name="columnName">Column to query</param>
        /// <returns>Column as 1D array</returns>
        object[] LoadDataFromDatabaseSource(string tableName, string columnName);

        /// <summary>
        /// Load data from single column into object array using specified query. Data is taken from the first column only
        /// </summary>
        /// <param name="sqlQuery">SQL query to execute</param>
        /// <returns>Column as 1D array</returns>
        object[] LoadDataFromDatabaseSource(string sqlQuery);
    }
}
