﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.Abstractions.Interfaces.ExternalDataLoaders
{
    /// <summary>
    /// Provides methods for loading data from text files
    /// </summary>
    public interface IFileSourceLoader
    {
        /// <summary>
        /// Reads raw file with specified path and returns array of data
        /// </summary>
        /// <param name="pathToFile">Path to raw text file</param>
        /// <returns>Read data as object array</returns>
        Task<object[]> LoadDataFromRawFileSourceAsync(string pathToFile);
    }
}
