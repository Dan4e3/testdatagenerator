﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces.ValuesHandling;

namespace TestDataGenerator.Abstractions.Interfaces.Columns
{
    public interface IColumn
    {
        /// <summary>
        /// Generate value using column settings
        /// </summary>
        /// <returns>Generated value boxed in object</returns>
        object GenerateValue();

        /// <summary>
        /// Generate multiple values using column settings
        /// </summary>
        /// <param name="valuesCount">Number of values to generate</param>
        /// <returns>Array of generated objects</returns>
        object[] GenerateValues(int valuesCount);

        /// <summary>
        /// Produces string representation of value with invariant culture. Expects that provided value is 
        /// of column value type.
        /// </summary>
        /// <param name="value">Value to get string representation of</param>
        /// <returns>String representation of value</returns>
        string GetInvariantStringValueRepresentation(object value);

        /// <summary>
        /// Produces string representation of value with column culture. Expects that provided value is 
        /// of column value type.
        /// </summary>
        /// <param name="value">Value to get string representation of</param>
        /// <returns>Culture-specific string representation of value</returns>
        string GetStringValueRepresentation(object value);

        /// <summary>
        /// Returns column name
        /// </summary>
        /// <returns>Column name</returns>
        string GetColumnName();

        /// <summary>
        /// Returns target value type of column
        /// </summary>
        /// <returns>Type of values</returns>
        Type GetColumnType();

        /// <summary>
        /// Returns value generator instance
        /// </summary>
        /// <returns>Value generator instance</returns>
        IValueGenerator GetValueGenerator();

        /// <summary>
        /// Set value generator to associate with column
        /// </summary>
        /// <param name="valueGenerator">Value generator to use</param>
        void SetValueGenerator(IValueGenerator valueGenerator);

        /// <summary>
        /// Checks if column has correct parameters set and ready to produce values. Outputs error message to 'out' parameter if any.
        /// </summary>
        /// <param name="outputErrorMessage">Validation errors if any are present</param>
        /// <returns>True if column is valid. False otherwise</returns>
        bool IsValid(out string outputErrorMessage);
    }
}
