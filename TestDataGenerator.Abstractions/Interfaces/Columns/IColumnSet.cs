﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces.Dataset;
using TestDataGenerator.Abstractions.Interfaces.Rows;

namespace TestDataGenerator.Abstractions.Interfaces.Columns
{
    public interface IColumnSet: IEnumerable<IColumn>, ISerializable
    {
        /// <summary>
        /// Adds provided column to column set
        /// </summary>
        /// <param name="col">Column to add</param>
        void AddColumn(IColumn col);

        /// <summary>
        /// Remove column from column set
        /// </summary>
        /// <param name="col">Column to remove</param>
        void RemoveColumn(IColumn col);

        /// <summary>
        /// Remove all columns from column set
        /// </summary>
        void Clear();

        /// <summary>
        /// Get number of columns in column set
        /// </summary>
        /// <returns></returns>
        int GetColumnsCount();

        /// <summary>
        /// Generate new row of data
        /// </summary>
        /// <returns>Array of object-boxed values. One value per column in column set.</returns>
        object[] GetNewRow();

        /// <summary>
        /// Get column at provided index
        /// </summary>
        /// <param name="colIndex">Index of column</param>
        /// <returns>IColumn at specified index within column set</returns>
        IColumn this[int colIndex] { get; }

        /// <summary>
        /// Get column index in column set
        /// </summary>
        /// <param name="column">IColumn to get index of</param>
        /// <returns>Integer index of column</returns>
        int GetColumnIndex(IColumn column);

        /// <summary>
        /// Generate new row of data bound to specified dataset
        /// </summary>
        /// <param name="ownerDataset">Parent dataset of new datarow</param>
        /// <returns>Generated IRow with data</returns>
        IRow GetNewRow(ITestDataSet ownerDataset);

        /// <summary>
        /// Retrieve column from column set by its name
        /// </summary>
        /// <param name="columnName">Name of column to search</param>
        /// <returns>Column instance if column with such name exists. Null otherwise</returns>
        IColumn this[string columnName] { get; }
    }
}
