﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.Abstractions.Interfaces.Columns
{
    /// <summary>
    /// Used to generate collection of columns
    /// </summary>
    public interface IColumnProducer
    {
        /// <summary>
        /// Generate column collection
        /// </summary>
        /// <returns></returns>
        IEnumerable<IColumn> GetColumnsCollection();
    }
}
