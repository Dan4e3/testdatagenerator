﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.Abstractions.Interfaces.ValuesHandling
{
    /// <summary>
    /// Describes value generator provider
    /// </summary>
    public interface IValueGeneratorProvider
    {
        /// <summary>
        /// Get value generator instance by desired value type
        /// </summary>
        /// <param name="targetValueType">Type of values which are produced by generator</param>
        /// <returns>Value generator</returns>
        IValueGenerator GetByTargetValueType(Type targetValueType);

        /// <summary>
        /// Check if value generator exists (registered) within current provider
        /// </summary>
        /// <param name="targetValueType">Type of values which are produced by generator</param>
        /// <returns>True if generator exists. False, otherwise.</returns>
        bool ValueGeneratorIsRegistered(Type targetValueType);
    }
}
