﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.Abstractions.Interfaces.ValuesHandling
{
    /// <summary>
    /// Interface describing range between two values
    /// </summary>
    public interface IValueRange
    {
        /// <summary>
        /// Get minimum value of range
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns>Minimum range value</returns>
        T GetMinRangeValue<T>();

        /// <summary>
        /// Get maximum value of range
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns>Maximum range value</returns>
        T GetMaxRangeValue<T>();

        /// <summary>
        /// Get type of values within range
        /// </summary>
        /// <returns></returns>
        Type GetValueTargetType();
    }
}
