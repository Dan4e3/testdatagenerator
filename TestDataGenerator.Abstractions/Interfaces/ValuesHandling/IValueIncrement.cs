﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.Abstractions.Interfaces.ValuesHandling
{
    /// <summary>
    /// Describes values setup for incremental generator
    /// </summary>
    public interface IValueIncrement
    {
        /// <summary>
        /// Get starting value
        /// </summary>
        /// <typeparam name="T">Type of value to return</typeparam>
        /// <returns>Starting value to begin count from</returns>
        T GetStartValue<T>();

        /// <summary>
        /// Get ending value
        /// </summary>
        /// <typeparam name="T">Type of value to return</typeparam>
        /// <returns>Ending value to finish counting at</returns>
        T GetEndValue<T>();

        /// <summary>
        /// Get value step to use as increment
        /// </summary>
        /// <typeparam name="T">Type of value to return</typeparam>
        /// <returns>Value increment</returns>
        T GetValueStep<T>();

        /// <summary>
        /// Return target type of generated values
        /// </summary>
        /// <returns>Target type of generated values</returns>
        Type GetValueTargetType();
    }
}
