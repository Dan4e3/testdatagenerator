﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.Abstractions.Interfaces.ValuesHandling
{
    /// <summary>
    /// Describes which values to generate for specific property of a specific type.
    /// </summary>
    /// <typeparam name="T">Class type to describe.</typeparam>
    public interface IClassInstanceRandomizer<T> where T: class
    {
        /// <summary>
        /// Dictionary which key refers to type of a class and value refers to generator mapping to a specific property.
        /// </summary>
        /// <returns>Dictionary with property to generator mapping.</returns>
        Dictionary<Type, Dictionary<PropertyInfo, IValueGenerator>> GetGeneratorPerPropertyDict();
    }
}
