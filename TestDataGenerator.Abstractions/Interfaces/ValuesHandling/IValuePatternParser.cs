﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.Abstractions.Interfaces.ValuesHandling
{
    public interface IValuePatternParser
    {
        /// <summary>
        /// Checks if provided pattern is valid and if it is, assigns pattern to calling instance
        /// </summary>
        /// <param name="pattern">Value pattern</param>
        /// <returns>True if pattern is valid according to rules. False, otherwise</returns>
        bool ValidateAndSetPattern(string pattern);
    }
}
