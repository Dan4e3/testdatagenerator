﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.Abstractions.Interfaces.ValuesHandling
{
    /// <summary>
    /// Allows to make generator resettable to its initial state
    /// </summary>
    public interface IResettableGenerator
    {
        /// <summary>
        /// Reset generator instance state to initial one
        /// </summary>
        void ResetGeneratorState();
    }
}
