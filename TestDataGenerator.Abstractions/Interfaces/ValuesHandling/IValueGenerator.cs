﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Settings;

namespace TestDataGenerator.Abstractions.Interfaces.ValuesHandling
{
    /// <summary>
    /// Basic interface for value generators
    /// </summary>
    public interface IValueGenerator
    {
        /// <summary>
        /// Set of settings which define null value generation behaviour
        /// </summary>
        NullValuesSettings NullValuesSettings { get; set; }

        /// <summary>
        /// Generates value and tries to convert it to target type using provided culture information.
        /// </summary>
        /// <param name="targetValueType">Desired output value type</param>
        /// <param name="culture">Cultural information to use during conversions</param>
        /// <returns>Resulting value boxed in onject</returns>
        object GenerateValue(Type targetValueType, CultureInfo culture);

        /// <summary>
        /// Returns string which describes current generator in somewhat friendly way.
        /// </summary>
        /// <param name="charsToOutput">Amount of characters to take from full name</param>
        /// <returns>Either full generator name or shortened one</returns>
        string GetFriendlyName(int charsToOutput = 0);

        /// <summary>
        /// Checks if value generator has correct parameters set and ready to produce values. Outputs error message to 'out' parameter if any.
        /// </summary>
        /// <param name="outputErrorMessage">Errors found during validation if any</param>
        /// <returns>True if generator is valid. False, otherwise</returns>
        bool IsValid(out string outputErrorMessage);
    }
}
