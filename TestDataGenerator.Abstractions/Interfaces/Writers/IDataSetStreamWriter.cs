﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces.Dataset;

namespace TestDataGenerator.Abstractions.Interfaces.Writers
{
    /// <summary>
    /// Provides methods for writing dataset into stream
    /// </summary>
    public interface IDataSetStreamWriter
    {
        /// <summary>
        /// Write dataset to specified stream
        /// </summary>
        /// <param name="stream">Stream to write data to</param>
        /// <param name="dataSet">Dataset to export</param>
        /// <returns></returns>
        Task WriteToStreamAsync(Stream stream, ITestDataSet dataSet);

        /// <summary>
        /// Use specified dataset to generate rows in place and write them to stream
        /// </summary>
        /// <param name="stream">Stream to write data to</param>
        /// <param name="dataSet">Dataset to export</param>
        /// <returns></returns>
        Task WriteToStreamUsingInPlaceRowGenerationAsync(Stream stream, ITestDataSet dataSet, int rowsToGenerate);
    }
}
