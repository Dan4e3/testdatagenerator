﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces.Columns;
using TestDataGenerator.Abstractions.Interfaces.Rows;

namespace TestDataGenerator.Abstractions.Interfaces.Writers
{
    /// <summary>
    /// Basic interface designed for transferring data from dataset to database
    /// </summary>
    public interface IDbExportExecutor
    {
        /// <summary>
        /// Connection string to database intance
        /// </summary>
        string ConnectionString { get; set; }

        /// <summary>
        /// Database table name into which values will be inserted
        /// </summary>
        string TargetTableName { get; set; }

        /// <summary>
        /// Create unopened connection to database
        /// </summary>
        /// <returns></returns>
        IDbConnection CreateConnection();

        /// <summary>
        /// Generate INSERT VALUES SQL command as string 
        /// </summary>
        /// <param name="columns">Dataset columns to use</param>
        /// <param name="rowsToInsert">Dataset rows to insert into database table</param>
        /// <returns></returns>
        string CreateInsertSqlCommand(IEnumerable<IColumn> columns, IEnumerable<IRow> rowsToInsert);
    }
}
