﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces.Dataset;

namespace TestDataGenerator.Abstractions.Interfaces.Writers
{
    /// <summary>
    /// Provides methods for exporting dataset to file
    /// </summary>
    public interface IDataSetFileWriter
    {
        /// <summary>
        /// Write specified dataset to file
        /// </summary>
        /// <param name="filePath">Path to file</param>
        /// <param name="dataSet">Dataset to export</param>
        /// <returns></returns>
        Task WriteToFileAsync(string filePath, ITestDataSet dataSet);

        /// <summary>
        /// Use specified dataset to generate rows in place and write them to file
        /// </summary>
        /// <param name="filePath">Path to file</param>
        /// <param name="dataSet">Dataset to use for row generation</param>
        /// <returns></returns>
        Task WriteToFileUsingInPlaceRowGenerationAsync(string filePath, ITestDataSet dataSet, int rowsToGenerate);
    }
}
