﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces.Dataset;

namespace TestDataGenerator.Abstractions.Interfaces.Writers
{
    /// <summary>
    /// Interface designed to provide functinality for dataset export to database
    /// </summary>
    public interface IDataSetDbWriter
    {
        /// <summary>
        /// Export dataset to database table
        /// </summary>
        /// <param name="executor">Export executor instance (SQL Server, PostgreSQL etc.)</param>
        /// <param name="dataset">Dataset to export</param>
        /// <param name="commandTimeout">Maximum command execution time (may be changed to a larger value if dataset is large)</param>
        /// <returns></returns>
        int WriteToDatabase(IDbExportExecutor executor, ITestDataSet dataset, int commandTimeout = 30);

        /// <summary>
        /// Use specified dataset to generate rows in place and write them to database table
        /// </summary>
        /// <param name="executor">Export executor instance (SQL Server, PostgreSQL etc.)</param>
        /// <param name="dataset">Dataset to export</param>
        /// <param name="commandTimeout">Maximum command execution time (may be changed to a larger value if dataset is large)</param>
        /// <returns></returns>
        int WriteToDatabaseUsingInPlaceRowGeneration(IDbExportExecutor executor, ITestDataSet dataset, int rowsToGenerate, int commandTimeout = 30);
    }
}
