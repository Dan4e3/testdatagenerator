﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces.Columns;

namespace TestDataGenerator.Abstractions.Interfaces.Rows
{
    /// <summary>
    /// Row with some data
    /// </summary>
    public interface IRow: IEnumerable<object>
    {
        /// <summary>
        /// Get data array associated with row
        /// </summary>
        /// <returns>Array of object items</returns>
        object[] GetRowData();

        /// <summary>
        /// Set row data
        /// </summary>
        /// <param name="rowData">Array to associate with current row</param>
        void SetRowData(object[] rowData);

        /// <summary>
        /// Get row item at specified index
        /// </summary>
        /// <param name="columnIndex">Index of column</param>
        /// <returns>Row item at specified index</returns>
        object this[int columnIndex] { get; }

        /// <summary>
        /// Get row item by specified column
        /// </summary>
        /// <param name="column">Column instance</param>
        /// <returns>Row item associated with provided column</returns>
        object this[IColumn column] { get; }

        /// <summary>
        /// Get value at specified column index of specified type
        /// </summary>
        /// <typeparam name="TValue">Type of value</typeparam>
        /// <param name="columnIndex">Index of column to get value at</param>
        /// <returns></returns>
        TValue GetValue<TValue>(int columnIndex);

        /// <summary>
        /// Get value at specified column of specified type
        /// </summary>
        /// <typeparam name="TValue">Type of value</typeparam>
        /// <param name="column">Column to get value at</param>
        /// <returns></returns>
        TValue GetValue<TValue>(IColumn column);

        /// <summary>
        /// Get column culture-dependent value at specified index
        /// </summary>
        /// <param name="columnIndex">Index of column</param>
        /// <returns>String representation of value at provided index with respect to column culture</returns>
        string GetStringRepresentation(int columnIndex);

        /// <summary>
        /// Get column culture-dependent value at specified column
        /// </summary>
        /// <param name="column">Column instance</param>
        /// <returns>String representation of value at provided column with respect to column culture</returns>
        string GetStringRepresentation(IColumn column);

        /// <summary>
        /// Returns culture-dependent array of string representation of each value in row
        /// </summary>
        /// <returns>Array of culture-dependent string representations of values</returns>
        string[] GetStringRepresentation();

        /// <summary>
        /// Get column culture invariant value at specified index
        /// </summary>
        /// <param name="columnIndex">Index of column</param>
        /// <returns>String representation of value at provided index with invariant culture</returns>
        string GetInvariantStringRepresentation(int columnIndex);

        /// <summary>
        /// Get column culture invariant value at specified column
        /// </summary>
        /// <param name="column">Column instance</param>
        /// <returns>String representation of value at provided column with invariant culture</returns>
        string GetInvariantStringRepresentation(IColumn column);

        /// <summary>
        /// Returns culture invariant array of string representation of each value in row
        /// </summary>
        /// <returns>Array of culture invariant string representations of values</returns>
        string[] GetInvariantStringRepresentation();
    }
}
