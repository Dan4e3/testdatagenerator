﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.Abstractions.Interfaces
{
    /// <summary>
    /// Marks class as one capable of serializing into JSON-formatted text
    /// </summary>
    public interface ISerializable
    {
        /// <summary>
        /// Serialize current instance as JSON string
        /// </summary>
        /// <returns></returns>
        string SerializeAsJson();
    }
}
