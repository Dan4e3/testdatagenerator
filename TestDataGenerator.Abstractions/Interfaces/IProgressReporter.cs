﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.Abstractions.Interfaces
{
    public interface IProgressReporter
    {
        void ReportProgress(string text, int percentage);
        void ReportProgress(int percentage);
    }
}
