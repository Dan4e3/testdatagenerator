﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces.Columns;
using TestDataGenerator.Abstractions.Interfaces.Rows;

namespace TestDataGenerator.Abstractions.Interfaces.Dataset
{
    /// <summary>
    /// Basic interface which describes dataset with test data
    /// </summary>
    public interface ITestDataSet
    {
        /// <summary>
        /// Generate new data with required number of rows.
        /// </summary>
        /// <param name="rowsToGenerate">Number of rows to generate</param>
        /// <returns>Array of generated rows represented with IRow interface</returns>
        IRow[] GenerateNewDataSet(int rowsToGenerate);

        /// <summary>
        /// Generate single row of data
        /// </summary>
        /// <returns>Single row of generated data</returns>
        IRow GenerateSingleRow();

        /// <summary>
        /// Returns existing dataset if one was previously generated.
        /// </summary>
        /// <returns>Array of existing rows or null if no dataset was previously generated</returns>
        IRow[] GetExistingDataSet();

        /// <summary>
        /// Returns column set bound to current instance.
        /// </summary>
        /// <returns>Column set</returns>
        IColumnSet GetColumns();

        /// <summary>
        /// Returns dataset name.
        /// </summary>
        /// <returns>Dataset name</returns>
        string GetDataSetName();

        /// <summary>
        /// Checks if dataset is correctly set for producing rows of values. Outputs errors to 'out' parameter if any.
        /// </summary>
        /// <returns>True if dataset can be generated. False, otherwise</returns>
        bool CanGenerateDataSet(out string errorMessage);
    }
}
