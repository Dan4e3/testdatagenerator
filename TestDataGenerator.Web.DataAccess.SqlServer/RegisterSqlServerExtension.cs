﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Web.DataAccess.Interfaces.Repositories;
using TestDataGenerator.Web.DataAccess.SqlServer.Repositories;

namespace TestDataGenerator.Web.DataAccess.SqlServer
{
    public static class RegisterSqlServerExtension
    {
        public static IServiceCollection RegisterSqlServerImplementation(this IServiceCollection services)
        {
            services.AddSingleton<IUserRepository, UserRepository>();
            services.AddSingleton<IPredefinedValueRepository, PredefinedValueRepository>();
            services.AddSingleton<IPredefinedCategoryRepository, PredefinedCategoryRepository>();
            services.AddSingleton<IColumnSetRepository, ColumnSetRepository>();
            services.AddSingleton<IDatasetGenerationRequestRepository, DatasetGenerationRequestRepository>();

            return services;
        }
    }
}
