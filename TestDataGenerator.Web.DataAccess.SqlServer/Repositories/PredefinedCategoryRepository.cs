﻿using Dapper;
using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Web.DataAccess.Interfaces;
using TestDataGenerator.Web.DataAccess.Interfaces.Repositories;
using TestDataGenerator.Web.Models.DomainModels;

namespace TestDataGenerator.Web.DataAccess.SqlServer.Repositories
{
    public class PredefinedCategoryRepository : IPredefinedCategoryRepository
    {
        private readonly IDbConnectionFactory _connFactory;
        private readonly string _predefinedCategoriesTableName;
        private readonly string _usersTableName;

        public PredefinedCategoryRepository(
            IDbConnectionFactory connFactory,
            IDapperCustomSettings dapperCustomSettings)
        {
            _connFactory = connFactory;
            _predefinedCategoriesTableName = dapperCustomSettings.GetTableNameFromAttr(typeof(PredefinedCategoryDm));
            _usersTableName = dapperCustomSettings.GetTableNameFromAttr(typeof(UserDm));
        }

        public async Task<int> ClearTableAsync()
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"DELETE FROM {_predefinedCategoriesTableName}";
            int affectedRows = await conn.ExecuteAsync(sql);
            return affectedRows;
        }

        public async Task<long> CreateSingleAsync(PredefinedCategoryDm model)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            int result = await conn.InsertAsync(model);
            return model.Id;
        }

        public async Task<int> DeleteSingleByIdAsync(long categoryId)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"DELETE FROM {_predefinedCategoriesTableName} WHERE [Id]=@valueId";
            int affectedRows = await conn.ExecuteAsync(sql, new { categoryId });
            return affectedRows;
        }

        public async Task<int> DeleteManyByIdsAsync(long[] categoriesIds)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"DELETE FROM {_predefinedCategoriesTableName} WHERE [Id] IN @categoriesIds";
            int affectedRows = await conn.ExecuteAsync(sql, new { categoriesIds });
            return affectedRows;
        }

        public async Task<PredefinedCategoryDm[]> GetAllAsync()
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"
                SELECT * FROM {_predefinedCategoriesTableName} pCat
                INNER JOIN {_usersTableName} usr ON pCat.[ContributorUserId]=usr.[Id]
            ";

            PredefinedCategoryDm[] result = (await conn
                .QueryAsync<PredefinedCategoryDm, UserDm, PredefinedCategoryDm>(sql, (pCat, usr) => {
                    pCat.ContributorUser = usr;
                    return pCat;
                })).ToArray();

            return result;
        }

        public async Task<PredefinedCategoryDm> GetSingleByIdAsync(long categoryId)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"
                SELECT * FROM {_predefinedCategoriesTableName} pCat
                INNER JOIN {_usersTableName} usr ON pCat.[ContributorUserId]=usr.[Id]
                WHERE pCat.[Id]=@categoryId
            ";

            PredefinedCategoryDm result = (await conn
                .QueryAsync<PredefinedCategoryDm, UserDm, PredefinedCategoryDm>(sql, (pCat, usr) => {
                    pCat.ContributorUser = usr;
                    return pCat;
                }, new { categoryId })).FirstOrDefault();

            return result;
        }

        public async Task<PredefinedCategoryDm> GetSingleByNameAsync(string categoryName)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"
                SELECT * FROM {_predefinedCategoriesTableName} pCat
                INNER JOIN {_usersTableName} usr ON pCat.[ContributorUserId]=usr.[Id]
                WHERE pCat.[Name]=@categoryName
            ";

            PredefinedCategoryDm result = (await conn
                .QueryAsync<PredefinedCategoryDm, UserDm, PredefinedCategoryDm>(sql, (pCat, usr) => {
                    pCat.ContributorUser = usr;
                    return pCat;
                }, new { categoryName })).FirstOrDefault();

            return result;
        }

        public async Task<PredefinedCategoryDm[]> GetManyByContributorUserNameAsync(string contributorUserName, bool includePublic)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"
                SELECT * FROM {_predefinedCategoriesTableName} pCat
                INNER JOIN {_usersTableName} usr ON pCat.[ContributorUserId]=usr.[Id]
                WHERE usr.[Name]=@contributorUserName {(includePublic == true ? "OR pCat.[IsPublic]=1" : "")}
            ";

            PredefinedCategoryDm[] result = (await conn
                .QueryAsync<PredefinedCategoryDm, UserDm, PredefinedCategoryDm>(sql, (pCat, usr) => {
                    pCat.ContributorUser = usr;
                    return pCat;
                }, new { contributorUserName })).ToArray();

            return result;
        }

        public async Task<PredefinedCategoryDm[]> GetPublicCategoriesAsync()
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"
                SELECT * FROM {_predefinedCategoriesTableName}
                WHERE [IsPublic]=1
            ";
            PredefinedCategoryDm[] result = (await conn.QueryAsync<PredefinedCategoryDm>(sql)).ToArray();
            return result;
        }

        public async Task<bool> UpdateSingleAsync(PredefinedCategoryDm model)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            bool result = await conn.UpdateAsync(model);
            return result;
        }
    }
}
