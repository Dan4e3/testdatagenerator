﻿using Dapper;
using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Web.DataAccess.Interfaces;
using TestDataGenerator.Web.DataAccess.Interfaces.Repositories;
using TestDataGenerator.Web.Models.DomainModels;

namespace TestDataGenerator.Web.DataAccess.SqlServer.Repositories
{
    public class DatasetGenerationRequestRepository: IDatasetGenerationRequestRepository
    {
        private readonly IDbConnectionFactory _connFactory;
        private readonly string _dsetGenRequestsTableName;
        private readonly string _usersTableName;
        private readonly string _columnSetsTableName;

        public DatasetGenerationRequestRepository(
            IDbConnectionFactory connFactory,
            IDapperCustomSettings dapperCustomSettings)
        {
            _connFactory = connFactory;
            _dsetGenRequestsTableName = dapperCustomSettings.GetTableNameFromAttr(typeof(DatasetGenerationRequestDm));
            _usersTableName = dapperCustomSettings.GetTableNameFromAttr(typeof(UserDm));
            _columnSetsTableName = dapperCustomSettings.GetTableNameFromAttr(typeof(ColumnSetDm));
        }

        public async Task<long> CreateSingleAsync(DatasetGenerationRequestDm model)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            int insertedRows = await conn.InsertAsync(model);
            return model.Id;
        }

        public async Task<DatasetGenerationRequestDm> GetSingleByIdAsync(long id)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"
                SELECT * FROM {_dsetGenRequestsTableName} dsetReq
                INNER JOIN {_usersTableName} usr ON dsetReq.[RequestingUserId]=usr.[Id]
                LEFT JOIN {_columnSetsTableName} colSet ON colSet.[Id]=dsetReq.[ColumnSetId]
                WHERE dsetReq.[Id]=@id
            ";

            DatasetGenerationRequestDm result = (await conn.QueryAsync<DatasetGenerationRequestDm, 
                UserDm, ColumnSetDm, DatasetGenerationRequestDm>(sql,
                (dsetReq, usr, colSet) => {
                    dsetReq.RequestingUser = usr;
                    dsetReq.ColumnSet = colSet;
                    return dsetReq;
                },
                new { id })).FirstOrDefault();

            return result;
        }

        public async Task<DatasetGenerationRequestDm> GetSinglePartialStateByIdAsync(long id)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"
                SELECT dsetReq.[Id], dsetReq.[WorkerResponseReceived], dsetReq.[WorkerResponseReceivedDate], dsetReq.[ErrorText],
                    usr.*
                FROM {_dsetGenRequestsTableName} dsetReq
                INNER JOIN {_usersTableName} usr ON dsetReq.[RequestingUserId]=usr.[Id]
                WHERE dsetReq.[Id]=@id
            ";

            DatasetGenerationRequestDm result = 
                (await conn.QueryAsync<DatasetGenerationRequestDm, UserDm, DatasetGenerationRequestDm>(sql, 
                (dsetReq, usr) => {
                    dsetReq.RequestingUser = usr;
                    return dsetReq;
                }, new { id })).FirstOrDefault();
            return result;
        }

        public async Task<DatasetGenerationRequestDm[]> GetManyByUserNameAsync(string userName)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"
                SELECT dsetReq.[Id], dsetReq.[RequestingUserId], dsetReq.[ColumnSetId], dsetReq.[RowsToGenerate],
                    dsetReq.[FileExtension], dsetReq.[CreationDate], dsetReq.[FetchedAtLeastOnce],
                    dsetReq.[WorkerResponseReceived], dsetReq.[WorkerResponseReceivedDate], dsetReq.[ErrorText],
                    usr.*, colSet.*
                FROM {_dsetGenRequestsTableName} dsetReq
                INNER JOIN {_usersTableName} usr ON dsetReq.[RequestingUserId]=usr.[Id]
                LEFT JOIN {_columnSetsTableName} colSet ON colSet.[Id]=dsetReq.[ColumnSetId]
                WHERE usr.[Name]=@userName
            ";

            DatasetGenerationRequestDm[] result = (await conn.QueryAsync<DatasetGenerationRequestDm,
                UserDm, ColumnSetDm, DatasetGenerationRequestDm>(sql,
                (dsetReq, usr, colSet) => {
                    dsetReq.RequestingUser = usr;
                    dsetReq.ColumnSet = colSet;
                    return dsetReq;
                },
                new { userName })).ToArray();

            return result;
        }

        public async Task<bool> CheckIfWorkerResponseReceivedByRequestIdAsync(long id)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"
                SELECT WorkerResponseReceived FROM {_dsetGenRequestsTableName} dsetReq
                WHERE dsetReq.[Id]=@id
            ";

            bool result = await conn.QueryFirstOrDefaultAsync<bool>(sql, new { id });
            return result;
        }

        public async Task<bool> UpdateSingleAsync(DatasetGenerationRequestDm model)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            bool wasUpdated = await conn.UpdateAsync(model);
            return wasUpdated;
        }

        public async Task<long[]> DeleteFetchedRawJsonDatasetRequestsAsync()
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"
                DELETE FROM {_dsetGenRequestsTableName} 
                WHERE [FetchedAtLeastOnce]=1 AND [RowsJson] IS NOT NULL
                OUTPUT DELETED.[Id]
            ";
            long[] deletedRowsIds = (await conn.QueryAsync<long>(sql)).ToArray();
            return deletedRowsIds;
        }

        public async Task<long[]> DeleteExpiredFileDatasetReuqestsAsync(TimeSpan requestLifeTime)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"
                DELETE FROM {_dsetGenRequestsTableName} 
                WHERE ([FileContents] IS NOT NULL 
                    AND [WorkerResponseReceivedDate] + @requestLifeTime < GETDATE())
                    OR ([WorkerResponseReceivedDate] IS NULL 
                        AND [CreationDate] + @requestLifeTime < GETDATE())
                OUTPUT DELETED.[Id]
            ";
            long[] deletedRowsIds = (await conn.QueryAsync<long>(sql, new { requestLifeTime })).ToArray();
            return deletedRowsIds;
        }

        public async Task<int> SetGenerationRequestAsFetchedAsync(long id)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"
                UPDATE {_dsetGenRequestsTableName} SET [FetchedAtLeastOnce]=1
                WHERE [Id]=@id
            ";
            int affectedRows = await conn.ExecuteAsync(sql, new { id });
            return affectedRows;
        }

        public async Task<DatasetGenerationRequestDm[]> GetManyByUserNamePaginatedAsync(string userName, int valuesToSkip, int valuesToFetch)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"
                SELECT dsetReq.[Id], dsetReq.[RequestingUserId], dsetReq.[ColumnSetId], dsetReq.[RowsToGenerate],
                    dsetReq.[FileExtension], dsetReq.[CreationDate], dsetReq.[FetchedAtLeastOnce],
                    dsetReq.[WorkerResponseReceived], dsetReq.[WorkerResponseReceivedDate], dsetReq.[ErrorText],
                    usr.*, colSet.*
                FROM {_dsetGenRequestsTableName} dsetReq
                INNER JOIN {_usersTableName} usr ON dsetReq.[RequestingUserId]=usr.[Id]
                LEFT JOIN {_columnSetsTableName} colSet ON colSet.[Id]=dsetReq.[ColumnSetId]
                WHERE usr.[Name]=@userName
                ORDER BY dsetReq.[Id]
                OFFSET @valuesToSkip ROWS 
                FETCH NEXT @valuesToFetch ROWS ONLY                
            ";

            DatasetGenerationRequestDm[] result = (await conn.QueryAsync<DatasetGenerationRequestDm,
                UserDm, ColumnSetDm, DatasetGenerationRequestDm>(sql,
                (dsetReq, usr, colSet) => {
                    dsetReq.RequestingUser = usr;
                    dsetReq.ColumnSet = colSet;
                    return dsetReq;
                },
                new { userName, valuesToSkip, valuesToFetch })).ToArray();

            return result;
        }

        public async Task<int> GetUserRequestsCountAsync(string userName)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"SELECT COUNT(1) FROM {_dsetGenRequestsTableName}";
            int result = await conn.ExecuteScalarAsync<int>(sql);
            return result;
        }
    }
}
