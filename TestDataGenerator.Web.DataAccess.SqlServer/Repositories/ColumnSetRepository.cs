﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Web.DataAccess.Interfaces.Repositories;
using TestDataGenerator.Web.Models.DomainModels;
using Dapper;
using TestDataGenerator.Web.DataAccess.Interfaces;
using System.Data;
using Dapper.Contrib.Extensions;

namespace TestDataGenerator.Web.DataAccess.SqlServer.Repositories
{
    public class ColumnSetRepository : IColumnSetRepository
    {
        private readonly IDbConnectionFactory _connFactory;
        private readonly string _columnSetTableName;
        private readonly string _usersTableName;

        public ColumnSetRepository(
            IDbConnectionFactory connFactory,
            IDapperCustomSettings dapperCustomSettings)
        {
            _connFactory = connFactory;
            _columnSetTableName = dapperCustomSettings.GetTableNameFromAttr(typeof(ColumnSetDm));
            _usersTableName = dapperCustomSettings.GetTableNameFromAttr(typeof(UserDm));
        }

        public async Task<int> DeleteSingleByIdAsync(long columnSetId)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"DELETE FROM {_columnSetTableName} WHERE [Id]=@columnSetId";
            int affectedRows = await conn.ExecuteAsync(sql, new { columnSetId });
            return affectedRows;
        }

        public async Task<ColumnSetDm> GetSingleByIdAsync(long columnSetId)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"
                SELECT * FROM {_columnSetTableName} colSet
                INNER JOIN {_usersTableName} usr ON colSet.[OwnerUserId]=usr.[Id]
                WHERE colSet.[Id]=@columnSetId
            ";

            ColumnSetDm result = (await conn.QueryAsync<ColumnSetDm, UserDm, ColumnSetDm>(sql, 
                (colSet, usr) => {
                    colSet.OwnerUser = usr;
                    return colSet;
                }, 
                new { columnSetId })).FirstOrDefault();

            return result;
        }

        public async Task<ColumnSetDm[]> GetManyByOwnerUserIdAsync(long ownerUserId)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"
                SELECT * FROM {_columnSetTableName} colSet
                INNER JOIN {_usersTableName} usr ON colSet.[OwnerUserId]=usr.[Id]
                WHERE usr.[Id]=@ownerUserId
            ";

            ColumnSetDm[] result = (await conn.QueryAsync<ColumnSetDm, UserDm, ColumnSetDm>(sql,
                (colSet, usr) => {
                    colSet.OwnerUser = usr;
                    return colSet;
                },
                new { ownerUserId })).ToArray();

            return result;
        }

        public async Task<ColumnSetDm[]> GetManyByOwnerUserNameAsync(string ownerUserName)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"
                SELECT * FROM {_columnSetTableName} colSet
                INNER JOIN {_usersTableName} usr ON colSet.[OwnerUserId]=usr.[Id]
                WHERE usr.[Name]=@ownerUserName
            ";

            ColumnSetDm[] result = (await conn.QueryAsync<ColumnSetDm, UserDm, ColumnSetDm>(sql,
                (colSet, usr) => {
                    colSet.OwnerUser = usr;
                    return colSet;
                },
                new { ownerUserName })).ToArray();

            return result;
        }

        public async Task<long> CreateSingleAsync(ColumnSetDm model)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            int result = await conn.InsertAsync(model);
            return model.Id;
        }

        public async Task<bool> UpdateSingleAsync(ColumnSetDm model)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            bool result = await conn.UpdateAsync(model);
            return result;
        }
    }
}
