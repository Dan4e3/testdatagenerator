﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces.ValuesHandling;
using TestDataGenerator.Abstractions.Settings;
using TestDataGenerator.Core.ValueGenerators.IncrementalGenerator;
using TestDataGenerator.Core.ValueGenerators.Providers.Common;
using TestDataGenerator.Core.ValueGenerators.ValueRangeGenerator;

namespace TestDataGenerator.Core.ValueGenerators.Providers
{
    public class DefaultValueGeneratorProvider: IValueGeneratorProvider
    {
        public static HashSet<ValueGeneratorInfo> RegisteredGenerators = new HashSet<ValueGeneratorInfo>() {
            new ValueGeneratorInfo(ValueGeneratorTypeEnum.Pattern, new Type[] { typeof(string) }, 
                () => new ValuePatternParser.ValuePatternParser("[A-z]{4-12}")),
            new ValueGeneratorInfo(ValueGeneratorTypeEnum.Pattern, new Type[] { typeof(sbyte), typeof(byte), typeof(ushort),
                typeof(short), typeof(uint), typeof(int), typeof(long), typeof(float), typeof(double), typeof(decimal)},
                () => new ValuePatternParser.ValuePatternParser("[0-9]{1-2}")),
            new ValueGeneratorInfo(ValueGeneratorTypeEnum.SelectFromList, new Type[] { typeof(string) },
                () => new SelectFromListValueGenerator(new object[] { "test", "some string", "another one" })),
            new ValueGeneratorInfo(ValueGeneratorTypeEnum.SelectFromList, new Type[] { typeof(sbyte), typeof(byte), typeof(ushort),
                typeof(short), typeof(uint), typeof(int), typeof(long) },
                () => new SelectFromListValueGenerator(new object[] { 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89 })),
            new ValueGeneratorInfo(ValueGeneratorTypeEnum.SelectFromList, new Type[] { typeof(float), typeof(double), typeof(decimal) },
                () => new SelectFromListValueGenerator(new object[] { 1.37f, 2.653f, 9.8237f })),
            new ValueGeneratorInfo(ValueGeneratorTypeEnum.SelectFromList, new Type[] { typeof(bool) },
                () => new SelectFromListValueGenerator(new object[] { true, false })),
            new ValueGeneratorInfo(ValueGeneratorTypeEnum.SelectFromList, new Type[] { typeof(DateTime) },
                () => new SelectFromListValueGenerator(new object[] { new DateTime(2000, 1, 1), new DateTime(2000, 11, 21),
                new DateTime(2003, 4, 7), new DateTime(2008, 2, 1), new DateTime(2012, 12, 31) })),
            new ValueGeneratorInfo(ValueGeneratorTypeEnum.Incremental, new Type[] { typeof(sbyte), typeof(byte), typeof(ushort),
                typeof(short), typeof(uint), typeof(int), typeof(long) },
                () => new IncrementalValueGenerator(new ValueIncrement(0, 100, 1))),
            new ValueGeneratorInfo(ValueGeneratorTypeEnum.Incremental, new Type[] { typeof(float), typeof(double), typeof(decimal) },
                () => new IncrementalValueGenerator(new ValueIncrement(0f, 100f, 0.5f))),
            new ValueGeneratorInfo(ValueGeneratorTypeEnum.Incremental, new Type[] { typeof(DateTime) },
                () => new IncrementalValueGenerator(new ValueIncrement(new DateTime(2000, 1, 1), new DateTime(2001, 1, 1), new TimeSpan(24, 0, 0)))),
            new ValueGeneratorInfo(ValueGeneratorTypeEnum.Range, new Type[] { typeof(sbyte), typeof(byte), typeof(ushort),
                typeof(short), typeof(uint), typeof(int), typeof(long), typeof(float), typeof(double), typeof(decimal) },
                () => new ValueRangeValueGenerator(new ValueRange(0, 100))),
            new ValueGeneratorInfo(ValueGeneratorTypeEnum.Range, new Type[] { typeof(DateTime) },
                () => new ValueRangeValueGenerator(new ValueRange(new DateTime(2000, 1, 1), new DateTime(2001, 1, 1)))),
        };

        public DefaultValueGeneratorProvider()
        {

        }

        public IValueGenerator GetByTargetValueType(Type targetValueType)
        {
            // handle nullable<>
            Type underlyingType = targetValueType;
            bool isNullable = false;
            if (targetValueType.IsGenericType && targetValueType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
            {
                underlyingType = Nullable.GetUnderlyingType(targetValueType);
                isNullable = true;
            } 

            ValueGeneratorInfo infoResult = RegisteredGenerators.FirstOrDefault(info => info.CompatibleTargetValueTypes.Contains(underlyingType));
            if (infoResult == null)
                throw new ArgumentException($"No default value generator was found for target value type ({targetValueType.Name})!");

            IValueGenerator generator = infoResult.ValueGeneratorGetter();
            if (isNullable)
            {
                generator.NullValuesSettings = new NullValuesSettings() {
                    GenerateNullValues = true,
                    AverageRowsBeforeNullAppearance = 10
                };
            }

            return generator;
        }

        public IValueGenerator GetByTargetValueTypeAndGeneratorType(Type targetValueType, ValueGeneratorTypeEnum generatorType)
        {
            // handle nullable<>
            Type underlyingType = targetValueType;
            bool isNullable = false;
            if (targetValueType.IsGenericType && targetValueType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
            {
                underlyingType = Nullable.GetUnderlyingType(targetValueType);
                isNullable = true;
            }

            ValueGeneratorInfo infoResult = RegisteredGenerators.FirstOrDefault(info => info.GeneratorType == generatorType &&
                info.CompatibleTargetValueTypes.Contains(underlyingType));
            if (infoResult == null)
                throw new ArgumentException($"No default value generator was found for target value type ({targetValueType.Name}) " +
                    $"and value generator type ({Enum.GetName(typeof(ValueGeneratorTypeEnum), generatorType)})!");

            IValueGenerator generator = infoResult.ValueGeneratorGetter();
            if (isNullable)
            {
                generator.NullValuesSettings = new NullValuesSettings()
                {
                    GenerateNullValues = true,
                    AverageRowsBeforeNullAppearance = 10
                };
            }

            return generator;
        }

        public bool ValueGeneratorIsRegistered(Type targetValueType)
        {
            // handle nullable<>
            Type underlyingType = targetValueType;
            if (targetValueType.IsGenericType && targetValueType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
                underlyingType = Nullable.GetUnderlyingType(targetValueType);

            ValueGeneratorInfo infoResult = RegisteredGenerators.FirstOrDefault(info => info.CompatibleTargetValueTypes.Contains(underlyingType));
            if (infoResult == null)
                return false;

            return true;
        }
    }
}
