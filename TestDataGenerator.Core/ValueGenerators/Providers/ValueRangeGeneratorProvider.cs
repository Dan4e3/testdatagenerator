﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces;
using TestDataGenerator.Abstractions.Interfaces.ValuesHandling;
using TestDataGenerator.Core.ValueGenerators.ValueRangeGenerator;

namespace TestDataGenerator.Core.ValueGenerators.Providers
{
    /// <summary>
    /// Provider of value range generators
    /// </summary>
    public class ValueRangeGeneratorProvider : IValueGeneratorProvider
    {
        private IValueRange _valueRange;

        /// <summary>
        /// Registered value generators
        /// </summary>
        public static Dictionary<Type, Func<IValueRange, IValueGenerator>> RegisteredValueRangeGenerators = 
            new Dictionary<Type, Func<IValueRange, IValueGenerator>>() {
                {typeof(DateTime), valueRange => new DateTimeRangeValueGenerator(valueRange) },
                {typeof(sbyte), valueRange => new IntegerRangeValueGenerator<sbyte>(valueRange) },
                {typeof(byte), valueRange => new IntegerRangeValueGenerator<byte>(valueRange) },
                {typeof(short), valueRange => new IntegerRangeValueGenerator<short>(valueRange) },
                {typeof(ushort), valueRange => new IntegerRangeValueGenerator<ushort>(valueRange) },
                {typeof(int), valueRange => new IntegerRangeValueGenerator<int>(valueRange) },
                {typeof(uint), valueRange => new IntegerRangeValueGenerator<uint>(valueRange) },
                {typeof(long), valueRange => new IntegerRangeValueGenerator<long>(valueRange) },
                {typeof(float), valueRange => new DecimalRangeValueGenerator<float>(valueRange) },
                {typeof(double), valueRange => new DecimalRangeValueGenerator<double>(valueRange) },
                {typeof(decimal), valueRange => new DecimalRangeValueGenerator<decimal>(valueRange) }
            };

        /// <summary>
        /// Create new provider instance using specified value range
        /// </summary>
        /// <param name="valueRange"></param>
        public ValueRangeGeneratorProvider(IValueRange valueRange)
        {
            _valueRange = valueRange;
        }

        /// <inheritdoc/>
        public IValueGenerator GetByTargetValueType(Type targetValueType)
        {
            if (!RegisteredValueRangeGenerators.ContainsKey(targetValueType))
                throw new ArgumentException($"Provided value type is not registered within current provider ({nameof(ValueRangeGeneratorProvider)})");

            return RegisteredValueRangeGenerators[targetValueType](_valueRange);
        }

        /// <inheritdoc/>
        public bool ValueGeneratorIsRegistered(Type targetValueType)
        {
            if (!RegisteredValueRangeGenerators.ContainsKey(targetValueType))
                return false;

            return true;
        }

        /// <summary>
        /// Check if generator exists using desired value type
        /// </summary>
        /// <param name="targetValueType">Type of values which are produced by generator</param>
        /// <returns>True if generator exists. False, otherwise.</returns>
        public static bool ValueGeneratorExists(Type targetValueType)
        {
            if (!RegisteredValueRangeGenerators.ContainsKey(targetValueType))
                return false;

            return true;
        }
    }
}
