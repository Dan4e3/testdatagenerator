﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces.ValuesHandling;

namespace TestDataGenerator.Core.ValueGenerators.Providers
{
    /// <summary>
    /// Default implementation for IClassInstanceRandomizer which recursively uses generators from
    /// <br/><c>DefaultValueGeneratorProivder</c> if such are registered.
    /// </summary>
    /// <typeparam name="T">Type of class</typeparam>
    public class DefaultClassInstanceRandomizer<T> : IClassInstanceRandomizer<T> where T : class
    {
        private Dictionary<Type, Dictionary<PropertyInfo, IValueGenerator>> _manualMapDict;

        /// <summary>
        /// Create new instance of randomizer
        /// </summary>
        public DefaultClassInstanceRandomizer()
        {
            _manualMapDict = new Dictionary<Type, Dictionary<PropertyInfo, IValueGenerator>>();
        }

        /// <summary>
        /// Sets value generator for top-level class (supposed to be generic T) specified property
        /// </summary>
        /// <param name="propertyName">Name of property to set value generator for</param>
        /// <param name="valueGenerator">Value generator to associate with property</param>
        public void SetGeneratorForProperty(string propertyName, IValueGenerator valueGenerator)
        {
            Type topLevelType = typeof(T);
            PropertyInfo foundProperty = FindPropertyInType(topLevelType, propertyName);

            if (foundProperty == null)
                throw new ArgumentException($"Type {topLevelType.Name} does not contain property with provided name ({propertyName})");

            if (!_manualMapDict.ContainsKey(topLevelType))
                _manualMapDict[topLevelType] = new Dictionary<PropertyInfo, IValueGenerator>();

            _manualMapDict[topLevelType][foundProperty] = valueGenerator;
        }

        /// <summary>
        /// Sets value generator for a specific property of specified class type
        /// </summary>
        /// <param name="classType">Type of class property</param>
        /// <param name="propertyName">Name of a property</param>
        /// <param name="valueGenerator">Value generator to use</param>
        public void SetGeneratorForProperty(Type classType, string propertyName, IValueGenerator valueGenerator)
        {
            PropertyInfo foundProperty = FindPropertyInType(classType, propertyName);

            if (foundProperty == null)
                throw new ArgumentException($"Type {classType.Name} does not contain property with provided name ({propertyName})");

            if (!_manualMapDict.ContainsKey(classType))
                _manualMapDict[classType] = new Dictionary<PropertyInfo, IValueGenerator>();

            _manualMapDict[classType][foundProperty] = valueGenerator;
        }

        private PropertyInfo FindPropertyInType(Type typeOfContainingClass, string propertyName)
        {
            PropertyInfo foundProperty = typeOfContainingClass.GetProperty(propertyName);
            return foundProperty;
        }

        /// <inheritdoc/>
        public Dictionary<Type, Dictionary<PropertyInfo, IValueGenerator>> GetGeneratorPerPropertyDict()
        {
            PropertyInfo[] topLevelProps = typeof(T).GetProperties();

            Dictionary<Type, Dictionary<PropertyInfo, IValueGenerator>> result = GetDictRecursively(typeof(T), topLevelProps);

            if (_manualMapDict != null)
            {
                foreach (Type manualMapKey in _manualMapDict.Keys)
                {
                    if (result.ContainsKey(manualMapKey))
                    {
                        foreach (var manualPropMapPair in _manualMapDict[manualMapKey])
                        {
                            if (result[manualMapKey].ContainsKey(manualPropMapPair.Key))
                                result[manualMapKey][manualPropMapPair.Key] = manualPropMapPair.Value;
                        }
                    }
                }
            }

            return result;
        }

        private Dictionary<Type, Dictionary<PropertyInfo, IValueGenerator>> GetDictRecursively(Type ownerType, PropertyInfo[] propInfos)
        {
            Dictionary<Type, Dictionary<PropertyInfo, IValueGenerator>> result = new Dictionary<Type, Dictionary<PropertyInfo, IValueGenerator>>();
            DefaultValueGeneratorProvider provider = new DefaultValueGeneratorProvider();

            foreach (PropertyInfo prop in propInfos)
            {
                if (prop.PropertyType.IsClass && prop.PropertyType != typeof(string))
                {
                    PropertyInfo[] subProps = prop.PropertyType.GetProperties();
                    var innerDict = GetDictRecursively(prop.PropertyType, subProps);
                    foreach (var pair in innerDict)
                        result.Add(pair.Key, pair.Value);
                }
                else
                {
                    if (provider.ValueGeneratorIsRegistered(prop.PropertyType))
                    {
                        if (!result.ContainsKey(ownerType))
                            result[ownerType] = new Dictionary<PropertyInfo, IValueGenerator>();
                        result[ownerType][prop] = provider.GetByTargetValueType(prop.PropertyType);
                    }
                        
                }
            }

            return result;
        }
    }
}
