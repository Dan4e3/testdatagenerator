﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces.ValuesHandling;

namespace TestDataGenerator.Core.ValueGenerators.Providers.Common
{
    public class ValueGeneratorInfo
    {
        public Type[] CompatibleTargetValueTypes { get; set; }
        public ValueGeneratorTypeEnum GeneratorType { get; set; }
        public Func<IValueGenerator> ValueGeneratorGetter { get; set; }

        public ValueGeneratorInfo(ValueGeneratorTypeEnum generatorType, Type[] compatibleTargetValueTypes,
            Func<IValueGenerator> valueGeneratorGetter)
        {
            CompatibleTargetValueTypes = compatibleTargetValueTypes;
            GeneratorType = generatorType;
            ValueGeneratorGetter = valueGeneratorGetter;
        }
    }
}
