﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.Core.ValueGenerators.Providers.Common
{
    public enum ValueGeneratorTypeEnum
    {
        Pattern = 0,
        SelectFromList = 1,
        Incremental = 2,
        Range = 3
    }
}
