﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces.ValuesHandling;
using TestDataGenerator.Core.ValueGenerators.IncrementalGenerator;

namespace TestDataGenerator.Core.ValueGenerators.Providers
{
    /// <summary>
    /// Provider of incremental value generators
    /// </summary>
    public class IncrementalValueGeneratorProvider : IValueGeneratorProvider
    {
        private IValueIncrement _valueIncrement;

        /// <summary>
        /// Registered incremental value generators
        /// </summary>
        public static Dictionary<Type, Func<IValueIncrement, IValueGenerator>> RegisteredIncrementalValueGenerators =
            new Dictionary<Type, Func<IValueIncrement, IValueGenerator>>() {
                {typeof(byte), (valueIncrement) => new IntegerIncrementalValueGenerator<byte>(valueIncrement)},
                {typeof(sbyte), (valueIncrement) => new IntegerIncrementalValueGenerator<sbyte>(valueIncrement)},
                {typeof(short), (valueIncrement) => new IntegerIncrementalValueGenerator<short>(valueIncrement)},
                {typeof(ushort), (valueIncrement) => new IntegerIncrementalValueGenerator<ushort>(valueIncrement)},
                {typeof(int), (valueIncrement) => new IntegerIncrementalValueGenerator<int>(valueIncrement)},
                {typeof(uint), (valueIncrement) => new IntegerIncrementalValueGenerator<uint>(valueIncrement)},
                {typeof(long), (valueIncrement) => new IntegerIncrementalValueGenerator<long>(valueIncrement)},
                {typeof(float), (valueIncrement) => new DecimalIncrementalValueGenerator<float>(valueIncrement)},
                {typeof(double), (valueIncrement) => new DecimalIncrementalValueGenerator<double>(valueIncrement)},
                {typeof(decimal), (valueIncrement) => new DecimalIncrementalValueGenerator<decimal>(valueIncrement)},
                {typeof(DateTime), (valueIncrement) => new DateTimeIncrementalValueGenerator(valueIncrement)},
            };

        /// <summary>
        /// Create instance of provider using value increment specified
        /// </summary>
        /// <param name="valueIncrement">Value increment</param>
        public IncrementalValueGeneratorProvider(IValueIncrement valueIncrement)
        {
            _valueIncrement = valueIncrement;
        }

        /// <inheritdoc/>
        public IValueGenerator GetByTargetValueType(Type targetValueType)
        {
            if (!RegisteredIncrementalValueGenerators.ContainsKey(targetValueType))
                throw new ArgumentException($"Provided value type is not registered within current provider ({nameof(IncrementalValueGeneratorProvider)})");

            return RegisteredIncrementalValueGenerators[targetValueType](_valueIncrement);
        }

        /// <inheritdoc/>
        public bool ValueGeneratorIsRegistered(Type targetValueType)
        {
            if (!RegisteredIncrementalValueGenerators.ContainsKey(targetValueType))
                return false;

            return true;
        }

        /// <summary>
        /// Check if generator exists using desired value type
        /// </summary>
        /// <param name="targetValueType">Type of values which are produced by generator</param>
        /// <returns>True if generator exists. False, otherwise.</returns>
        public static bool ValueGeneratorExists(Type targetValueType)
        {
            if (!RegisteredIncrementalValueGenerators.ContainsKey(targetValueType))
                return false;

            return true;
        }
    }
}
