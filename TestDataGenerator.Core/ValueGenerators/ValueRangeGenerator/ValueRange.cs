﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces;
using TestDataGenerator.Abstractions.Interfaces.ValuesHandling;

namespace TestDataGenerator.Core.ValueGenerators.ValueRangeGenerator
{
    /// <summary>
    /// Range between two values
    /// </summary>
    public class ValueRange : IValueRange
    {
        /// <summary>
        /// Minimum value of range
        /// </summary>
        public object MinValue { get; private set; }

        /// <summary>
        /// Maximum value of range
        /// </summary>
        public object MaxValue { get; private set; }

        /// <summary>
        /// Type of values in range
        /// </summary>
        public Type TargetType { get; private set; }

        /// <summary>
        /// Create new range of values
        /// </summary>
        /// <param name="minValue">Min value of range</param>
        /// <param name="maxValue">Max value of range</param>
        public ValueRange(object minValue, object maxValue)
        {
            if (!minValue.GetType().Equals(maxValue.GetType()))
                throw new ArgumentException("Types of minValue and maxValue must be the same!");

            MinValue = minValue;
            MaxValue = maxValue;
            TargetType = minValue.GetType();
        }
        
        [JsonConstructor]
        private ValueRange(object minValue, object maxValue, Type targetType)
        {
            TargetType = targetType;
            // Json.Net reads numbers as long and puts them into object
            if (targetType == typeof(int))
            {
                MinValue = unchecked((int)(long)minValue);
                MaxValue = unchecked((int)(long)maxValue);
            }
            else
            {
                MinValue = minValue;
                MaxValue = maxValue;
            }
        }

        /// <inheritdoc/>
        public T GetMaxRangeValue<T>()
        {
            return (T)MaxValue;
        }

        /// <inheritdoc/>
        public T GetMinRangeValue<T>()
        {
            return (T)MinValue;
        }

        /// <inheritdoc/>
        public Type GetValueTargetType()
        {
            return TargetType;
        }
    }
}
