﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces;
using TestDataGenerator.Abstractions.Interfaces.ValuesHandling;
using TestDataGenerator.Abstractions.Settings;

namespace TestDataGenerator.Core.ValueGenerators.ValueRangeGenerator
{
    /// <summary>
    /// Generator which randomly selects DateTime values from defined range
    /// </summary>
    public class DateTimeRangeValueGenerator : IValueGenerator
    {
        /// <inheritdoc/>
        public NullValuesSettings NullValuesSettings { get; set; }

        /// <summary>
        /// Minimum DateTime value
        /// </summary>
        public DateTime MinDate { get; private set; }

        /// <summary>
        /// Maximum DateTime value
        /// </summary>
        public DateTime MaxDate { get; private set; }

        /// <summary>
        /// Create new instance of DateTime range generator
        /// </summary>
        /// <param name="valueRange">Range of DateTime values to select from</param>
        public DateTimeRangeValueGenerator(IValueRange valueRange)
        {
            MinDate = valueRange.GetMinRangeValue<DateTime>();
            MaxDate = valueRange.GetMaxRangeValue<DateTime>();
        }

        [JsonConstructor]
        private DateTimeRangeValueGenerator(DateTime minDate, DateTime maxDate)
        {
            MinDate = minDate;
            MaxDate = maxDate;
        }

        /// <inheritdoc/>
        public object GenerateValue(Type targetValueType, CultureInfo culture)
        {
            if (MinDate >= MaxDate)
                throw new ArgumentException("Minimum datetime value can't be greater or equal than maximum datetime value!");

            if (NullValuesSettings?.ShouldNextValueBeNull(Randomizer.Rand.Value) == true)
                return null;

            TimeSpan wholeTimeSpan = MaxDate - MinDate;
            TimeSpan randIncrementSpan = new TimeSpan(0, Randomizer.Rand.Value.Next(0, (int)wholeTimeSpan.TotalMinutes), 0);
            DateTime newDate = MinDate + randIncrementSpan;

            if (targetValueType == typeof(DateTime))
                return newDate;

            // handle nullable<> conversions
            Type underlyingType = targetValueType;
            if (targetValueType.IsGenericType && targetValueType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
                underlyingType = Nullable.GetUnderlyingType(targetValueType);

            return Convert.ChangeType(newDate, underlyingType, culture);
        }

        /// <inheritdoc/>
        public string GetFriendlyName(int charsToOutput = 0)
        {
            string res = $"Datetime range generator ({MinDate.ToShortDateString()} : {MaxDate.ToShortDateString()})";
            if (charsToOutput == 0)
                return res;
            else
                return string.Concat(res.Take(charsToOutput));
        }

        /// <inheritdoc/>
        public bool IsValid(out string outputErrorMessage)
        {
            outputErrorMessage = null;
            if (MinDate < MaxDate)
                return true;

            outputErrorMessage = "Minimum datetime value can't be greater or equal than maximum datetime value!";
            return false;
        }
    }
}
