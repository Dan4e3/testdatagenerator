﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces;
using TestDataGenerator.Abstractions.Interfaces.ValuesHandling;
using TestDataGenerator.Abstractions.Settings;
using TestDataGenerator.Core.NumericWrappers;

namespace TestDataGenerator.Core.ValueGenerators.ValueRangeGenerator
{
    /// <summary>
    /// Generator which randomly selects double values from defined range
    /// </summary>
    public class DecimalRangeValueGenerator<T> : IValueGenerator where T: struct
    {
        private FloatingPointNumberWrapper<T> _minValueWrap;
        private FloatingPointNumberWrapper<T> _maxValueWrap;

        /// <inheritdoc/>
        public NullValuesSettings NullValuesSettings { get; set; }

        /// <summary>
        /// Minimum double value
        /// </summary>
        public T MinValue { get; private set; }

        /// <summary>
        /// Maximum double value
        /// </summary>
        public T MaxValue { get; private set; }

        /// <summary>
        /// Create new instance of double range generator
        /// </summary>
        /// <param name="valueRange">Range of double values to select from</param>
        public DecimalRangeValueGenerator(IValueRange valueRange)
        {
            MinValue = valueRange.GetMinRangeValue<T>();
            MaxValue = valueRange.GetMaxRangeValue<T>();
            _minValueWrap = new FloatingPointNumberWrapper<T>(MinValue);
            _maxValueWrap = new FloatingPointNumberWrapper<T>(MaxValue);
        }

        [JsonConstructor]
        private DecimalRangeValueGenerator(T minValue, T maxValue)
        {
            MinValue = minValue;
            MaxValue = maxValue;
            _minValueWrap = new FloatingPointNumberWrapper<T>(MinValue);
            _maxValueWrap = new FloatingPointNumberWrapper<T>(MaxValue);
        }

        /// <inheritdoc/>
        public object GenerateValue(Type targetValueType, CultureInfo culture)
        {
            if (_minValueWrap >= _maxValueWrap)
                throw new ArgumentException("Minimum double value can't be greater or equal than maximum double value!");

            if (NullValuesSettings?.ShouldNextValueBeNull(Randomizer.Rand.Value) == true)
                return null;

            double randDecrement = Randomizer.Rand.Value.NextDouble();
            FloatingPointNumberWrapper<T> increase = (_maxValueWrap - _minValueWrap) * new FloatingPointNumberWrapper<T>((T)Convert.ChangeType(randDecrement, typeof(T)));

            FloatingPointNumberWrapper<T> newRandomValue = _minValueWrap + increase;

            // handle nullable<> conversions
            Type underlyingType = targetValueType;
            if (targetValueType.IsGenericType && targetValueType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
                underlyingType = Nullable.GetUnderlyingType(targetValueType);

            return Convert.ChangeType(newRandomValue.Value, underlyingType, culture);
        }

        /// <inheritdoc/>
        public string GetFriendlyName(int charsToOutput = 0)
        {
            string result = $"Decimal range value ({MinValue} : {MaxValue})";
            if (charsToOutput == 0)
                return result;
            else
                return string.Concat(result.Take(charsToOutput));
        }

        /// <inheritdoc/>
        public bool IsValid(out string outputErrorMessage)
        {
            outputErrorMessage = null;

            if (_minValueWrap < _maxValueWrap)
                return true;

            outputErrorMessage = "Minimum decimal value can't be greater or equal than maximum decimal value!";
            return false;
        }
    }
}
