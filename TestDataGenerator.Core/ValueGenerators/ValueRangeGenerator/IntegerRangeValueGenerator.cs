﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces;
using TestDataGenerator.Abstractions.Interfaces.NumberWrappers;
using TestDataGenerator.Abstractions.Interfaces.ValuesHandling;
using TestDataGenerator.Abstractions.Settings;
using TestDataGenerator.Core.NumericWrappers;

namespace TestDataGenerator.Core.ValueGenerators.ValueRangeGenerator
{
    /// <summary>
    /// Generator which randomly selects integer values from defined range
    /// </summary>
    public class IntegerRangeValueGenerator<T> : IValueGenerator where T: struct
    {
        private IntegerNumberWrapper<T> _minValueWrap;
        private IntegerNumberWrapper<T> _maxValueWrap;

        /// <inheritdoc/>
        public NullValuesSettings NullValuesSettings { get; set; }

        /// <summary>
        /// Minimum integer value
        /// </summary>
        public T MinValue { get; private set; }

        /// <summary>
        /// Maximum integer value
        /// </summary>
        public T MaxValue { get; private set; }

        /// <summary>
        /// Create new instance of int range generator
        /// </summary>
        /// <param name="valueRange">Range of int values to select from</param>
        public IntegerRangeValueGenerator(IValueRange valueRange)
        {
            MinValue = valueRange.GetMinRangeValue<T>();
            MaxValue = valueRange.GetMaxRangeValue<T>();
            _minValueWrap = new IntegerNumberWrapper<T>(MinValue);
            _maxValueWrap = new IntegerNumberWrapper<T>(MaxValue);
        }

        [JsonConstructor]
        private IntegerRangeValueGenerator(T minValue, T maxValue)
        {
            MinValue = minValue;
            MaxValue = maxValue;
            _minValueWrap = new IntegerNumberWrapper<T>(MinValue);
            _maxValueWrap = new IntegerNumberWrapper<T>(MaxValue);
        }

        /// <inheritdoc/>
        public object GenerateValue(Type targetValueType, CultureInfo culture)
        {
            if (_minValueWrap >= _maxValueWrap)
                throw new ArgumentException("Minimum integer value can't be greater or equal than maximum integer value!");

            if (NullValuesSettings?.ShouldNextValueBeNull(Randomizer.Rand.Value) == true)
                return null;

            long generatedLong = _minValueWrap + ((long)(Randomizer.Rand.Value.NextDouble() * (_maxValueWrap - _minValueWrap)));
            T newValue = (T)Convert.ChangeType(generatedLong, typeof(T));

            // handle nullable<> conversions
            Type underlyingType = targetValueType;
            if (targetValueType.IsGenericType && targetValueType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
                underlyingType = Nullable.GetUnderlyingType(targetValueType);

            return Convert.ChangeType(newValue, underlyingType, culture);
        }

        /// <inheritdoc/>
        public string GetFriendlyName(int charsToOutput = 0)
        {
            string result = $"Integer range value ({MinValue} : {MaxValue})";
            if (charsToOutput == 0)
                return result;
            else
                return string.Concat(result.Take(charsToOutput));
        }

        /// <inheritdoc/>
        public bool IsValid(out string outputErrorMessage)
        {
            outputErrorMessage = null;

            if (_minValueWrap < _maxValueWrap)
                return true;

            outputErrorMessage = "Minimum integer value can't be greater or equal than maximum integer value!";
            return false;
        }
    }
}
