﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces;
using TestDataGenerator.Abstractions.Interfaces.ValuesHandling;
using TestDataGenerator.Abstractions.Settings;
using TestDataGenerator.Core.ValueGenerators.Providers;

namespace TestDataGenerator.Core.ValueGenerators.ValueRangeGenerator
{
    /// <summary>
    /// Generator of values which stand between provided range. Autodiscovers type of range.
    /// </summary>
    public class ValueRangeValueGenerator : IValueGenerator
    {
        private NullValuesSettings _nullValuesSettings;

        /// <inheritdoc/>
        public NullValuesSettings NullValuesSettings
        {
            get => _nullValuesSettings;
            set
            {
                _nullValuesSettings = value;
                UnderlyingValueGenerator.NullValuesSettings = value;
            }
        }

        /// <summary>
        /// Real value generator which will be used for value generation
        /// </summary>
        public IValueGenerator UnderlyingValueGenerator { get; private set; }

        /// <summary>
        /// Provided range of values to select from
        /// </summary>
        public IValueRange ValuesRange { get; private set; }

        /// <summary>
        /// Create new value range generator. Underlying generator will be assigned based on type of values in IValueRange
        /// </summary>
        /// <param name="range">Range of values to select from</param>
        /// <param name="nullValuesSettings">Null values handling setting</param>
        public ValueRangeValueGenerator(IValueRange range, NullValuesSettings nullValuesSettings = null)
        {
            ValueRangeGeneratorProvider provider = new ValueRangeGeneratorProvider(range);
            UnderlyingValueGenerator = provider.GetByTargetValueType(range.GetValueTargetType());
            ValuesRange = range;
            NullValuesSettings = nullValuesSettings ?? new NullValuesSettings();
        }

        [JsonConstructor]
        private ValueRangeValueGenerator(IValueRange valuesRange, IValueGenerator underlyingValueGenerator, NullValuesSettings nullValuesSettings)
        {
            UnderlyingValueGenerator = underlyingValueGenerator;
            ValuesRange = valuesRange;
            NullValuesSettings = nullValuesSettings;
        }

        /// <inheritdoc/>
        public object GenerateValue(Type targetValueType, CultureInfo culture)
        {
            return UnderlyingValueGenerator.GenerateValue(targetValueType, culture);
        }

        /// <inheritdoc/>
        public string GetFriendlyName(int charsToOutput = 0)
        {
            return UnderlyingValueGenerator.GetFriendlyName(charsToOutput);
        }

        /// <inheritdoc/>
        public bool IsValid(out string outputErrorMessage)
        {
            return UnderlyingValueGenerator.IsValid(out outputErrorMessage);
        }
    }
}
