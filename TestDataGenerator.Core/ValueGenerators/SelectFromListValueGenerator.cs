﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces;
using TestDataGenerator.Abstractions.Interfaces.ValuesHandling;
using TestDataGenerator.Abstractions.Settings;

namespace TestDataGenerator.Core.ValueGenerators
{
    /// <summary>
    /// Randomly selects values from array of items
    /// </summary>
    public class SelectFromListValueGenerator : IValueGenerator
    {
        private NullValuesSettings _nullValuesSettings;
        private object[] _arrayOfValuesToSelectFrom;

        /// <inheritdoc/>
        public NullValuesSettings NullValuesSettings
        {
            get => _nullValuesSettings;
            set => _nullValuesSettings = value;
        }

        /// <summary>
        /// Object array out of which values will be selected
        /// </summary>
        public object[] ArrayOfValuesToSelectFrom
        {
            get => _arrayOfValuesToSelectFrom;
            set => _arrayOfValuesToSelectFrom = value;
        }

        /// <summary>
        /// Create new instance using array of items to select from
        /// </summary>
        /// <param name="valuesToSelectFrom">Assortment of objects to select from</param>
        /// <param name="nullValuesSettings">Null values handling setting</param>
        public SelectFromListValueGenerator(IEnumerable<object> valuesToSelectFrom, NullValuesSettings nullValuesSettings = null)
        {
            ArrayOfValuesToSelectFrom = valuesToSelectFrom.ToArray();
            NullValuesSettings = nullValuesSettings ?? new NullValuesSettings();
        }

        [JsonConstructor]
        private SelectFromListValueGenerator(object[] valuesToSelectFrom, NullValuesSettings nullValuesSettings)
        {
            ArrayOfValuesToSelectFrom = valuesToSelectFrom;
            NullValuesSettings = nullValuesSettings;
        }

        /// <inheritdoc/>
        public object GenerateValue(Type targetValueType, CultureInfo culture)
        {
            var typeConverter = TypeDescriptor.GetConverter(targetValueType);

            if (NullValuesSettings?.ShouldNextValueBeNull(Randomizer.Rand.Value) == true)
                return null;

            object randValueFromList = Randomizer.GetRandomValueFromArray(ArrayOfValuesToSelectFrom);

            // handle nullable<> conversions
            Type underlyingType = targetValueType;
            if (targetValueType.IsGenericType && targetValueType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
                underlyingType = Nullable.GetUnderlyingType(targetValueType);

            try
            {
                return typeConverter.ConvertFromString(null, culture, randValueFromList.ToString());
            }
            catch
            {
                try
                {
                    return typeConverter.ConvertTo(null, culture, randValueFromList, underlyingType);
                }
                catch
                {
                    throw;
                }
            }
        }

        /// <inheritdoc/>
        public string GetFriendlyName(int charsToOutput = 0)
        {
            string res = $"Select from list ({ArrayOfValuesToSelectFrom.Length})";
            if (charsToOutput == 0)
                return res;
            else
                return string.Concat(res.Take(charsToOutput));
        }

        /// <inheritdoc/>
        public bool IsValid(out string outputErrorMessage)
        {
            outputErrorMessage = null;
            if (ArrayOfValuesToSelectFrom.Length > 0)
                return true;

            outputErrorMessage = "Selection list can't be empty.";
            return false;
        }
    }
}
