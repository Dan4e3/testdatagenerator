﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.Core.ValueGenerators.ValuePatternParser
{
    /// <summary>
    /// Value patter parameters which help to store parsed pattern data
    /// </summary>
    internal class ValuePatternParams
    {
        /// <summary>
        /// Minimum amount of chars to generate
        /// </summary>
        public int MinLength { get; set; }

        /// <summary>
        /// Maximum amount of chars to generate
        /// </summary>
        public int MaxLength { get; set; }

        /// <summary>
        /// Array of chars to select from
        /// </summary>
        public char[] AvailableChars { get; set; }
    }
}
