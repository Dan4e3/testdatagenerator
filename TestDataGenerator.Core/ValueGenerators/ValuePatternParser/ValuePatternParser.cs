﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces;
using TestDataGenerator.Abstractions.Interfaces.ValuesHandling;
using TestDataGenerator.Abstractions.Settings;

namespace TestDataGenerator.Core.ValueGenerators.ValuePatternParser
{
    /// <summary>
    /// Provides ability to generate values based on regex-like string pattern
    /// </summary>
    public class ValuePatternParser : IValuePatternParser, IValueGenerator
    {
        private Dictionary<int, ValuePatternParams> _patternParamsPerPredicateMap = null;

        /// <inheritdoc/>
        public NullValuesSettings NullValuesSettings { get; set; }

        /// <summary>
        /// String pattern on which value generation is based
        /// </summary>
        public string ValuePattern { get; private set; }

        /// <summary>
        /// Whether supplied pattern is valid or not
        /// </summary>
        public bool PatternIsValid { get; set; }

        /// <summary>
        /// If pattern is invalid, contains error message with problems occured during pattern parsing
        /// </summary>
        public string PatternErrorText { get; set; }

        /// <summary>
        /// Creates new instance of pattern parser. Value pattern can be set later, but is required to generate values.
        /// </summary>
        /// <param name="valuePattern">Value pattern</param>
        public ValuePatternParser(string valuePattern = null, NullValuesSettings nullValuesSettings = null)
        {
            if(valuePattern != null)
                this.ValidateAndSetPattern(valuePattern);

            NullValuesSettings = nullValuesSettings ?? new NullValuesSettings();
        }

        /// <inheritdoc/>
        public object GenerateValue(Type targetValueType, CultureInfo culture)
        {
            if (!PatternIsValid)
                throw new ArgumentException("Invalid pattern supplied. Can't generate value.");

            if (ValuePattern == null)
                throw new ArgumentNullException("ValuePattern", "Value pattern was not set!");

            if (NullValuesSettings?.ShouldNextValueBeNull(Randomizer.Rand.Value) == true)
                return null;

            StringBuilder result = new StringBuilder();

            var orderedPairs = _patternParamsPerPredicateMap.OrderBy(p => p.Key);
            foreach (var pair in orderedPairs)
            {
                ValuePatternParams currentValuePattern = pair.Value;
                if (currentValuePattern.MinLength == currentValuePattern.MaxLength)
                {
                    int counter = 0;
                    while (counter++ < currentValuePattern.MinLength)
                        result.Append(Randomizer.GetRandomValueFromArray(currentValuePattern.AvailableChars));
                }
                else
                {
                    int valuesToProduce = Randomizer.Rand.Value.Next(currentValuePattern.MinLength, currentValuePattern.MaxLength + 1);
                    int counter = 0;
                    while (counter++ < valuesToProduce)
                        result.Append(Randomizer.GetRandomValueFromArray(currentValuePattern.AvailableChars));
                }
            }

            string resultAsString = result.ToString();
            var typeConverter = TypeDescriptor.GetConverter(targetValueType);
            return typeConverter.ConvertFromString(null, culture, resultAsString);
        }

        /// <inheritdoc/>
        public bool ValidateAndSetPattern(string pattern)
        {
            int predicateCounter = -1;
            StringBuilder predicateBody = new StringBuilder();
            StringBuilder predicateLength = new StringBuilder();
            bool predicateOpened = false;
            bool predicateClosed = false;
            ValuePattern = pattern;
            _patternParamsPerPredicateMap = new Dictionary<int, ValuePatternParams>();
            for (int i = 0; i < pattern.Length; i++)
            {
                char ch = pattern[i];
                if (ch == '[')
                {
                    predicateOpened = true;
                    int j = i;
                    char tempCh = pattern[j];
                    while(tempCh != ']')
                    {
                        j++;
                        if (j == pattern.Length)
                            break;

                        tempCh = pattern[j];
                        if (tempCh != ']')
                            predicateBody.Append(tempCh);
                    }

                    if (tempCh != ']')
                    {
                        predicateClosed = false;
                        break;
                    }
                    else
                    {
                        // getting predicate params
                        predicateClosed = true;
                        predicateCounter++;
                        ValuePatternParams patternParams = new ValuePatternParams();
                        _patternParamsPerPredicateMap[predicateCounter] = patternParams;
                        try
                        {
                            char[] availableChars = GetAvailableCharsFromPredicate(predicateBody.ToString());
                            patternParams.AvailableChars = availableChars;
                        }
                        catch (Exception ex)
                        {
                            PatternIsValid = false;
                            PatternErrorText = $"There was a problem parsing predicate #{predicateCounter}, between positions {i}-{j}. Error text: {ex.Message}";
                            return false;
                        }

                        // defining predicate lengths
                        if (j == pattern.Length - 1 || pattern[j+1] == '[')
                        {
                            patternParams.MaxLength = 1;
                            patternParams.MinLength = 1;
                        }
                        else if (pattern[j+1] == '{')
                        {
                            j = j + 1;
                            tempCh = pattern[j];
                            while(tempCh != '}')
                            {
                                j++;
                                if (j == pattern.Length)
                                    break;

                                tempCh = pattern[j];
                                if (tempCh != '}')
                                    predicateLength.Append(tempCh);
                            }

                            if (tempCh != '}')
                            {
                                PatternIsValid = false;
                                PatternErrorText = $"There was a problem with defining predicate length. No closing curly brace was found '}}'!";
                                return false;
                            }
                            else
                            {
                                try
                                {
                                    Tuple<int, int> minMaxPredicateLengths = GetMinAndMaxLengthsOfPredicate(predicateLength.ToString());
                                    patternParams.MinLength = minMaxPredicateLengths.Item1;
                                    patternParams.MaxLength = minMaxPredicateLengths.Item2;
                                }
                                catch (Exception ex)
                                {
                                    PatternIsValid = false;
                                    PatternErrorText = $"There was a problem parsing predicate #{predicateCounter}, between positions {i}-{j}. Error text: {ex.Message}";
                                    return false;
                                }
                            }
                        }
                        else
                        {
                            PatternIsValid = false;
                            PatternErrorText = $"There was a problem with defining predicate length. No opening curly brace was found '{{' or it is used incorrectly!";
                            return false;
                        }

                        i = j;
                        predicateBody = new StringBuilder();
                        predicateLength = new StringBuilder();
                    }
                }

                predicateClosed = false;
                predicateOpened = false;
            }

            if (predicateCounter < 0)
            {
                PatternIsValid = false;
                PatternErrorText = "No any opening '[' was found or supplied pattern is empty!";
                return false;
            }
                
            if (predicateOpened && !predicateClosed)
            {
                PatternIsValid = false;
                PatternErrorText = $"No closing ']' was found for predicate #{predicateCounter}!";
                return false;
            }

            PatternIsValid = true;
            return true;
        }

        /// <summary>
        /// Parses range of characters to select from
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        private char[] GetAvailableCharsFromPredicate(string predicate)
        {
            List<char> result = new List<char>();

            if (predicate.Length == 1 &&
                (predicate[0] == ',' || predicate[0] == '-'))
                return new char[] { predicate[0] };

            string[] splittedByComma = predicate.Split(',');
            foreach (var singleValueOrRange in splittedByComma)
            {
                if (singleValueOrRange.Contains('-'))
                {
                    string[] rangeSplit = singleValueOrRange.Split('-');
                    if (rangeSplit.Length == 2)
                    {
                        int leftCharCode = 0;
                        int rightCharCode = 0;

                        if (rangeSplit[0].Length == 1 && rangeSplit[1].Length == 1)
                        {
                            leftCharCode = (int)rangeSplit[0][0];
                            rightCharCode = (int)rangeSplit[1][0];

                            if (leftCharCode <= rightCharCode)
                            {
                                for (int i = leftCharCode; i <= rightCharCode; i++)
                                {
                                    char charToAddToResult = (char)i;
                                    if (char.IsLetterOrDigit(charToAddToResult))
                                        result.Add(charToAddToResult);
                                }
                            }
                            else
                                throw new Exception("Left character must be 'smaller' than right one so they can represent range of chars!");
                        }
                        else
                            throw new Exception("There must be only one char on each side of hyphen '-'!");
                    }
                    else
                        throw new Exception("There must be only one hyphen '-' in character range!");
                }
                else if (singleValueOrRange.Length == 1)
                    result.Add(singleValueOrRange[0]);
                else
                    throw new Exception("Range does not contain hyphen '-' or only one character must be provided as range or a single value!");
            }

            return result.Distinct().ToArray();
        }

        /// <summary>
        /// Parses predicate length pattern and gets min/max number of chars to generate
        /// </summary>
        /// <param name="predicateLengthPattern"></param>
        /// <returns></returns>
        private Tuple<int, int> GetMinAndMaxLengthsOfPredicate(string predicateLengthPattern)
        {
            if (predicateLengthPattern.Contains('-'))
            {
                string[] rangeSplit = predicateLengthPattern.Split('-');

                if (rangeSplit.Length == 2)
                {
                    if (int.TryParse(rangeSplit[0], out int minValue) &&
                        int.TryParse(rangeSplit[1], out int maxValue))
                    {
                        return new Tuple<int, int>(minValue, maxValue);
                    }
                    else
                        throw new Exception($"Can not convert '{rangeSplit[0]}' or '{rangeSplit[1]}' to integer value!");
                }
                else
                    throw new Exception("There is too many or no hyphens '-' defining range!");
            }
            else
            {
                if (int.TryParse(predicateLengthPattern.ToString(), out int value))
                    return new Tuple<int, int>(value, value);
                else
                    throw new Exception($"Can not convert '{predicateLengthPattern}' to integer value!");
            }
        }

        /// <inheritdoc/>
        public string GetFriendlyName(int charsToOutput = 0)
        {
            string res = $"Value pattern: {ValuePattern}";
            if (charsToOutput == 0)
                return res;
            else
                return string.Concat(res.Take(charsToOutput));
        }

        ///<inheritdoc/>
        public bool IsValid(out string outputErrorMessage)
        {
            outputErrorMessage = null;

            if (PatternIsValid)
                return true;

            outputErrorMessage = $"Pattern error: {PatternErrorText}";
            return false;
        }
    }
}
