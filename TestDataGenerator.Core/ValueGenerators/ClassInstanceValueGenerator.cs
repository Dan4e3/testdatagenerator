﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces.ValuesHandling;
using TestDataGenerator.Abstractions.Settings;

namespace TestDataGenerator.Core.ValueGenerators
{
    /// <summary>
    /// Used to generate randomized specified class instances
    /// </summary>
    /// <typeparam name="T">Type of class to randomize</typeparam>
    public class ClassInstanceValueGenerator<T> : IValueGenerator where T: class
    {
        private IClassInstanceRandomizer<T> _instanceRandomizer;

        /// <inheritdoc/>
        public NullValuesSettings NullValuesSettings { get; set; }

        /// <summary>
        /// Create new instance of random class generator
        /// </summary>
        /// <param name="instanceRandomizer">Class randomizer to use with current generator</param>
        public ClassInstanceValueGenerator(IClassInstanceRandomizer<T> instanceRandomizer, NullValuesSettings nullValueSettings = null)
        {
            _instanceRandomizer = instanceRandomizer;
            NullValuesSettings = nullValueSettings ?? new NullValuesSettings();
        }

        /// <inheritdoc/>
        public object GenerateValue(Type targetValueType, CultureInfo culture)
        {
            if (NullValuesSettings?.ShouldNextValueBeNull(Randomizer.Rand.Value) == true)
                return null;

            Dictionary<Type, Dictionary<PropertyInfo, IValueGenerator>> mappingDict = _instanceRandomizer.GetGeneratorPerPropertyDict();

            T generatedInstance = (T)Activator.CreateInstance(typeof(T));
            InstantiatePropertiesRecursively(generatedInstance, culture);

            return generatedInstance;
        }

        private void InstantiatePropertiesRecursively(object instance, CultureInfo culture)
        {
            Dictionary<Type, Dictionary<PropertyInfo, IValueGenerator>> mappingDict = _instanceRandomizer.GetGeneratorPerPropertyDict();
            Type instanceType = instance.GetType();

            foreach (PropertyInfo prop in instanceType.GetProperties())
            {
                if (mappingDict.ContainsKey(instanceType) && mappingDict[instanceType].ContainsKey(prop))
                {
                    prop.SetValue(instance, mappingDict[instanceType][prop].GenerateValue(prop.PropertyType, culture));
                }
                else if (mappingDict.ContainsKey(prop.PropertyType))
                {
                    object innerInstance = Activator.CreateInstance(prop.PropertyType);
                    InstantiatePropertiesRecursively(innerInstance, culture);
                    prop.SetValue(instance, innerInstance);
                }
                else
                    prop.SetValue(instance, Activator.CreateInstance(prop.PropertyType));
            }
        }

        /// <inheritdoc/>
        public string GetFriendlyName(int charsToOutput = 0)
        {
            string fullName = $"Class instance value generator of type {typeof(T).Name}";
            if (charsToOutput <= 0)
                return fullName;
            else
                return string.Concat(fullName.Take(charsToOutput));
        }

        /// <inheritdoc/>
        public bool IsValid(out string outputErrorMessage)
        {
            StringBuilder strBuilder = new StringBuilder();

            if (_instanceRandomizer == null)
            {
                outputErrorMessage = "Provided ClassInstanceRandomizer is null";
                return false;
            }

            outputErrorMessage = strBuilder.ToString();
            return true;
        }
    }
}
