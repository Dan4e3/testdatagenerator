﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces.ValuesHandling;
using TestDataGenerator.Abstractions.Settings;
using TestDataGenerator.Core.ValueGenerators.Providers;

namespace TestDataGenerator.Core.ValueGenerators.IncrementalGenerator
{
    /// <summary>
    /// Generates values based on increment setup
    /// </summary>
    public class IncrementalValueGenerator: IValueGenerator, IResettableGenerator
    {
        private NullValuesSettings _nullValuesSettings;

        /// <inheritdoc/>
        public NullValuesSettings NullValuesSettings
        {
            get => _nullValuesSettings;
            set
            {
                _nullValuesSettings = value;
                UnderlyingValueGenerator.NullValuesSettings = value;
            }
        }

        /// <summary>
        /// Value increment instance
        /// </summary>
        public IValueIncrement ValueIncrement { get; private set; }

        /// <summary>
        /// Underlying value generator which generates value
        /// </summary>
        public IValueGenerator UnderlyingValueGenerator { get; private set; }

        /// <summary>
        /// Create instance of incremental value generator
        /// </summary>
        /// <param name="valueIncrement">Value increment to consume by generator</param>
        /// <param name="nullValuesSettings">Null values handling setting</param>
        public IncrementalValueGenerator(IValueIncrement valueIncrement, NullValuesSettings nullValuesSettings = null)
        {
            IncrementalValueGeneratorProvider provider = new IncrementalValueGeneratorProvider(valueIncrement);
            UnderlyingValueGenerator = provider.GetByTargetValueType(valueIncrement.GetValueTargetType());
            ValueIncrement = valueIncrement;
            NullValuesSettings = nullValuesSettings ?? new NullValuesSettings();
        }

        [JsonConstructor]
        private IncrementalValueGenerator(IValueIncrement valueIncrement, IValueGenerator underlyingValueGenerator, NullValuesSettings nullValuesSettings)
        {
            UnderlyingValueGenerator = underlyingValueGenerator;
            ValueIncrement = valueIncrement;
            NullValuesSettings = nullValuesSettings;
        }

        /// <inheritdoc/>
        public object GenerateValue(Type targetValueType, CultureInfo culture)
        {
            return UnderlyingValueGenerator.GenerateValue(targetValueType, culture);
        }

        /// <inheritdoc/>
        public string GetFriendlyName(int charsToOutput = 0)
        {
            return UnderlyingValueGenerator.GetFriendlyName(charsToOutput);
        }

        /// <inheritdoc/>
        public bool IsValid(out string outputErrorMessage)
        {
            return UnderlyingValueGenerator.IsValid(out outputErrorMessage);
        }

        /// <inheritdoc/>
        public void ResetGeneratorState()
        {
            if (UnderlyingValueGenerator is IResettableGenerator resettable)
                resettable.ResetGeneratorState();
        }
    }
}
