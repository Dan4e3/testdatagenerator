﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces.ValuesHandling;
using TestDataGenerator.Abstractions.Settings;
using TestDataGenerator.Core.NumericWrappers;

namespace TestDataGenerator.Core.ValueGenerators.IncrementalGenerator
{
    /// <summary>
    /// Generates integer values based on provided increment
    /// </summary>
    public class IntegerIncrementalValueGenerator<T> : IValueGenerator, IResettableGenerator where T: struct
    {
        private long _currentValue;
        private IntegerNumberWrapper<T> _startValueWrap;
        private IntegerNumberWrapper<T> _endValueWrap;
        private IntegerNumberWrapper<T> _valueStepWrap;
        private object _currentValueLock = new object();

        /// <inheritdoc/>
        public NullValuesSettings NullValuesSettings { get; set; }

        /// <summary>
        /// Starting value
        /// </summary>
        public T StartValue { get; private set; }

        /// <summary>
        /// Maximum valid value
        /// </summary>
        public T EndValue { get; private set; }

        /// <summary>
        /// Increase per generated value
        /// </summary>
        public T ValueStep { get; private set; }

        /// <summary>
        /// Create new instance of incremental generator of integer type
        /// </summary>
        /// <param name="valueIncrement">Value increment instance</param>
        public IntegerIncrementalValueGenerator(IValueIncrement valueIncrement)
        {
            StartValue = valueIncrement.GetStartValue<T>();
            EndValue = valueIncrement.GetEndValue<T>();
            ValueStep = valueIncrement.GetValueStep<T>();
            _startValueWrap = new IntegerNumberWrapper<T>(StartValue);
            _endValueWrap = new IntegerNumberWrapper<T>(EndValue);
            _valueStepWrap = new IntegerNumberWrapper<T>(ValueStep);
            _currentValue = _startValueWrap - _valueStepWrap;
        }

        [JsonConstructor]
        private IntegerIncrementalValueGenerator(T startValue, T endValue, T valueStep)
        {
            StartValue = startValue;
            EndValue = endValue;
            ValueStep = valueStep;
            _startValueWrap = new IntegerNumberWrapper<T>(StartValue);
            _endValueWrap = new IntegerNumberWrapper<T>(EndValue);
            _valueStepWrap = new IntegerNumberWrapper<T>(ValueStep);
            _currentValue = _startValueWrap - _valueStepWrap;
        }


        /// <inheritdoc/>
        public object GenerateValue(Type targetValueType, CultureInfo culture)
        {
            if (_startValueWrap >= _endValueWrap)
                throw new ArgumentException("Starting value must be less than ending value!");

            if (_valueStepWrap > _endValueWrap - _startValueWrap)
                throw new ArgumentException("Value step can't be grater than difference between starting and ending value");

            if (NullValuesSettings?.ShouldNextValueBeNull(Randomizer.Rand.Value) == true)
                return null;

            lock (_currentValueLock)
            {
                _currentValue += _valueStepWrap.Value;
                if (_currentValue > _endValueWrap)
                    throw new ArithmeticException($"New value ({_currentValue}) turns to be greater than ending value ({EndValue}). " +
                        $"Please, adjust the number of rows to generate.");

                // handle nullable<> conversions
                Type underlyingType = targetValueType;
                if (targetValueType.IsGenericType && targetValueType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
                    underlyingType = Nullable.GetUnderlyingType(targetValueType);

                return Convert.ChangeType(_currentValue, underlyingType, culture);

                //var typeConverter = TypeDescriptor.GetConverter(targetValueType);
                //return typeConverter.ConvertTo(null, culture, _currentValue, targetValueType);
            }

        }

        /// <inheritdoc/>
        public string GetFriendlyName(int charsToOutput = 0)
        {
            string res = $"Integer incremental generator ({StartValue} : {EndValue}, {ValueStep})";
            if (charsToOutput == 0)
                return res;
            else
                return string.Concat(res.Take(charsToOutput));
        }

        /// <inheritdoc/>
        public bool IsValid(out string outputErrorMessage)
        {
            StringBuilder sb = new StringBuilder();
            outputErrorMessage = null;

            if (_startValueWrap >= _endValueWrap)
                sb.AppendLine("Starting value can't be greater or equal than ending value!");

            if (_valueStepWrap > _endValueWrap - _startValueWrap)
                sb.AppendLine("Value step can't be greater than difference between ending value and starting value!");

            string err = sb.ToString();
            if (string.IsNullOrEmpty(err))
                return true;
            else
            {
                outputErrorMessage = err;
                return false;
            }
        }

        /// <inheritdoc/>
        public void ResetGeneratorState()
        {
            _currentValue = _startValueWrap - _valueStepWrap;
        }
    }
}
