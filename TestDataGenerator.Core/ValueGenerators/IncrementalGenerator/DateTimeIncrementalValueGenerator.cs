﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces.ValuesHandling;
using TestDataGenerator.Abstractions.Settings;

namespace TestDataGenerator.Core.ValueGenerators.IncrementalGenerator
{
    public class DateTimeIncrementalValueGenerator : IValueGenerator, IResettableGenerator
    {
        private DateTime _currentValue;
        private object _currentValueLock = new object();

        /// <inheritdoc/>
        public NullValuesSettings NullValuesSettings { get; set; }

        /// <summary>
        /// Starting value
        /// </summary>
        public DateTime StartValue { get; private set; }

        /// <summary>
        /// Maximum valid value
        /// </summary>
        public DateTime EndValue { get; private set; }

        /// <summary>
        /// Increase per generated value
        /// </summary>
        public TimeSpan ValueStep { get; private set; }

        /// <summary>
        /// Create new instance of incremental generator of integer type
        /// </summary>
        /// <param name="valueIncrement">Value increment instance</param>
        public DateTimeIncrementalValueGenerator(IValueIncrement valueIncrement)
        {
            StartValue = valueIncrement.GetStartValue<DateTime>();
            EndValue = valueIncrement.GetEndValue<DateTime>();
            ValueStep = valueIncrement.GetValueStep<TimeSpan>();
            _currentValue = StartValue - ValueStep;
        }

        [JsonConstructor]
        private DateTimeIncrementalValueGenerator(DateTime startValue, DateTime endValue, TimeSpan valueStep)
        {
            StartValue = startValue;
            EndValue = endValue;
            ValueStep = valueStep;
        }


        /// <inheritdoc/>
        public object GenerateValue(Type targetValueType, CultureInfo culture)
        {
            if (StartValue >= EndValue)
                throw new ArgumentException("Starting value must be less than ending value!");

            if (ValueStep > EndValue - StartValue)
                throw new ArgumentException("Value step can't be grater than difference between starting and ending value");

            if (NullValuesSettings?.ShouldNextValueBeNull(Randomizer.Rand.Value) == true)
                return null;

            lock (_currentValueLock)
            {
                _currentValue += ValueStep;
                if (_currentValue > EndValue)
                    throw new ArithmeticException($"New value ({_currentValue}) turns to be greater than ending value ({EndValue}). " +
                        $"Please, adjust the number of rows to generate.");

                if (targetValueType == typeof(DateTime))
                    return _currentValue;

                // handle nullable<> conversions
                Type underlyingType = targetValueType;
                if (targetValueType.IsGenericType && targetValueType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
                    underlyingType = Nullable.GetUnderlyingType(targetValueType);

                return Convert.ChangeType(_currentValue, underlyingType, culture);

                //var typeConverter = TypeDescriptor.GetConverter(targetValueType);
                //return typeConverter.ConvertTo(null, culture, _currentValue, targetValueType);
            }
        }

        /// <inheritdoc/>
        public string GetFriendlyName(int charsToOutput = 0)
        {
            string res = $"Datetime incremental generator ({StartValue} : {EndValue}, {ValueStep})";
            if (charsToOutput == 0)
                return res;
            else
                return string.Concat(res.Take(charsToOutput));
        }

        /// <inheritdoc/>
        public bool IsValid(out string outputErrorMessage)
        {
            StringBuilder sb = new StringBuilder();
            outputErrorMessage = null;

            if (StartValue >= EndValue)
                sb.AppendLine("Starting value can't be greater or equal than ending value!");

            if (ValueStep > EndValue - StartValue)
                sb.AppendLine("Value step can't be greater than difference between ending value and starting value!");

            string err = sb.ToString();
            if (string.IsNullOrEmpty(err))
                return true;
            else
            {
                outputErrorMessage = err;
                return false;
            }
        }

        /// <inheritdoc/>
        public void ResetGeneratorState()
        {
            _currentValue = StartValue - ValueStep;
        }
    }
}
