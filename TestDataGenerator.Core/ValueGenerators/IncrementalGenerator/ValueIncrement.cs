﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces.ValuesHandling;

namespace TestDataGenerator.Core.ValueGenerators.IncrementalGenerator
{
    /// <summary>
    /// Increment value from one value to another with specified step
    /// </summary>
    public class ValueIncrement : IValueIncrement
    {
        /// <summary>
        /// Value to start from
        /// </summary>
        public object StartValue { get; private set; }

        /// <summary>
        /// Upper bound value
        /// </summary>
        public object EndValue { get; private set; }

        /// <summary>
        /// Increment step
        /// </summary>
        public object ValueStep { get; private set; }

        /// <summary>
        /// Desired type of values to generate
        /// </summary>
        public Type TargetType { get; private set; }

        /// <summary>
        /// Create instance of value increment
        /// </summary>
        /// <param name="startValue">Value to start count from</param>
        /// <param name="endValue">Maximum eligible value</param>
        /// <param name="valueStep">Increase per each generation loop</param>
        public ValueIncrement(object startValue, object endValue, object valueStep)
        {
            if (startValue.GetType() != endValue.GetType())
                throw new ArgumentException("Types of start value and end value must be the same!");

            StartValue = startValue;
            EndValue = endValue;
            ValueStep = valueStep;
            TargetType = startValue.GetType();
        }

        [JsonConstructor]
        private ValueIncrement(object startValue, object endValue, object valueStep, Type targetType)
        {
            TargetType = targetType;
            // Json.Net reads numbers as long and puts them into object
            if (targetType == typeof(int))
            {
                StartValue = unchecked((int)(long)startValue);
                EndValue = unchecked((int)(long)endValue);
                ValueStep = unchecked((int)(long)valueStep);
            }
            else if (targetType == typeof(DateTime))
            {
                StartValue = startValue;
                EndValue = endValue;
                ValueStep = TimeSpan.Parse(valueStep.ToString());
            }
            else
            {
                StartValue = startValue;
                EndValue = endValue;
                ValueStep = valueStep;
            }
        }

        /// <inheritdoc/>
        public T GetEndValue<T>()
        {
            return (T)EndValue;
        }

        /// <inheritdoc/>
        public T GetStartValue<T>()
        {
            return (T)StartValue;
        }

        /// <inheritdoc/>
        public T GetValueStep<T>()
        {
            return (T)ValueStep;
        }

        /// <inheritdoc/>
        public Type GetValueTargetType()
        {
            return TargetType;
        }
    }
}
