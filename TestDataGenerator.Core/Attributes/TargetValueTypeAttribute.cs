﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class TargetValueTypeAttribute: Attribute
    {
        public Type TargetValueType { get; set; }

        public TargetValueTypeAttribute(Type targetValueType)
        {
            TargetValueType = targetValueType;
        }
    }
}
