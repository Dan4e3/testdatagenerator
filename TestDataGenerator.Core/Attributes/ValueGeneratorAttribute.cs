﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces.ValuesHandling;
using TestDataGenerator.Core.ValueGenerators.Providers.Common;

namespace TestDataGenerator.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class ValueGeneratorAttribute: Attribute
    {
        public ValueGeneratorTypeEnum ValueGeneratorType { get; set; }

        public ValueGeneratorAttribute(ValueGeneratorTypeEnum valueGeneratorType)
        {
            ValueGeneratorType = valueGeneratorType;
        }
    }
}
