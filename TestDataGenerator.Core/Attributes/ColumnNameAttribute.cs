﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class ColumnNameAttribute: Attribute
    {
        public string Name { get; set; }

        public ColumnNameAttribute(string columnName)
        {
            Name = columnName;
        }
    }
}
