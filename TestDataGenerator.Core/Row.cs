﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces.Columns;
using TestDataGenerator.Abstractions.Interfaces.Dataset;
using TestDataGenerator.Abstractions.Interfaces.Rows;

namespace TestDataGenerator.Core
{
    /// <summary>
    /// Row containing data
    /// </summary>
    public class Row : IRow
    {
        private ITestDataSet _ownerDataset;
        private object[] _rowData;

        /// <inheritdoc/>
        public object this[int columnIndex] { get => _rowData[columnIndex]; }

        /// <inheritdoc/>
        public object this[IColumn column] { get => _rowData[_ownerDataset.GetColumns().GetColumnIndex(column)]; }

        /// <summary>
        /// Create new row with data
        /// </summary>
        /// <param name="rowData">Data to associate with row</param>
        /// <param name="ownerDataset">Parent dataset which rows belongs to</param>
        public Row(object[] rowData, ITestDataSet ownerDataset)
        {
            _rowData = rowData;
            _ownerDataset = ownerDataset;
        }

        /// <inheritdoc/>
        public string GetInvariantStringRepresentation(int columnIndex)
        {
            IColumn column = _ownerDataset.GetColumns()[columnIndex];
            string result = column.GetInvariantStringValueRepresentation(_rowData[columnIndex]);
            return result;
        }

        /// <inheritdoc/>
        public string GetInvariantStringRepresentation(IColumn column)
        {
            int columnIndex = _ownerDataset.GetColumns().GetColumnIndex(column);
            string result = column.GetInvariantStringValueRepresentation(_rowData[columnIndex]);
            return result;
        }

        /// <inheritdoc/>
        public object[] GetRowData()
        {
            return _rowData;
        }

        /// <inheritdoc/>
        public string GetStringRepresentation(int columnIndex)
        {
            IColumn column = _ownerDataset.GetColumns()[columnIndex];
            string result = column.GetStringValueRepresentation(_rowData[columnIndex]);
            return result;
        }

        /// <inheritdoc/>
        public string GetStringRepresentation(IColumn column)
        {
            int columnIndex = _ownerDataset.GetColumns().GetColumnIndex(column);
            string result = column.GetStringValueRepresentation(_rowData[columnIndex]);
            return result;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _rowData.GetEnumerator();
        }

        /// <inheritdoc/>
        public void SetRowData(object[] rowData)
        {
            _rowData = rowData;
        }

        
        public IEnumerator<object> GetEnumerator()
        {
            return ((IEnumerable<object>)_rowData).GetEnumerator();
        }

        /// <inheritdoc/>
        public TValue GetValue<TValue>(int columnIndex)
        {
            return (TValue)this[columnIndex];
        }

        /// <inheritdoc/>
        public TValue GetValue<TValue>(IColumn column)
        {
            return (TValue)this[column];
        }

        /// <inheritdoc/>
        public string[] GetStringRepresentation()
        {
            IColumnSet colset = _ownerDataset.GetColumns();
            return _rowData.Select((val, i) => colset[i].GetStringValueRepresentation(val)).ToArray();
        }

        /// <inheritdoc/>
        public string[] GetInvariantStringRepresentation()
        {
            IColumnSet colset = _ownerDataset.GetColumns();
            return _rowData.Select((val, i) => colset[i].GetInvariantStringValueRepresentation(val)).ToArray();
        }
    }
}
