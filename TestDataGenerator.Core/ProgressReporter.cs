﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces;

namespace TestDataGenerator.Core
{
    public class ProgressReporter : IProgressReporter
    {
        private Action<string, int> _progressAndStatusSetter;
        private Action<int> _progressSetter;

        public ProgressReporter(Action<int> progressSetter, Action<string, int> progressAndStatusSetter)
        {
            _progressSetter = progressSetter;
            _progressAndStatusSetter = progressAndStatusSetter;
        }

        public void ReportProgress(string text, int percentage)
        {
            _progressAndStatusSetter(text, percentage);
        }

        public void ReportProgress(int percentage)
        {
            _progressSetter(percentage);
        }
    }
}
