﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.Core.NumericWrappers
{
    /// <summary>
    /// Wrapper for floating point numbers for arithmetics
    /// </summary>
    /// <typeparam name="T">Type of floating point number</typeparam>
    public class FloatingPointNumberWrapper<T> where T: struct
    {
        /// <summary>
        /// Value that the wrapper instance holds
        /// </summary>
        public T Value { get; private set; }

        /// <summary>
        /// Create floating point wrapper instance
        /// </summary>
        /// <param name="value">Value to wrap</param>
        public FloatingPointNumberWrapper(T value)
        {
            Value = value;
        }

        public static bool operator >=(FloatingPointNumberWrapper<T> l, FloatingPointNumberWrapper<T> r)
        {
            if (l.Value is float lf && r.Value is float rf)
                return lf >= rf;
            else if (l.Value is double ld && r.Value is double rd)
                return ld >= rd;
            else 
                return (decimal)Convert.ChangeType(l.Value, typeof(decimal)) >= (decimal)Convert.ChangeType(r.Value, typeof(decimal));
        }

        public static bool operator <=(FloatingPointNumberWrapper<T> l, FloatingPointNumberWrapper<T> r)
        {
            if (l.Value is float lf && r.Value is float rf)
                return lf <= rf;
            else if (l.Value is double ld && r.Value is double rd)
                return ld <= rd;
            else
                return (decimal)Convert.ChangeType(l.Value, typeof(decimal)) <= (decimal)Convert.ChangeType(r.Value, typeof(decimal));
        }

        public static bool operator >(FloatingPointNumberWrapper<T> l, FloatingPointNumberWrapper<T> r)
        {
            if (l.Value is float lf && r.Value is float rf)
                return lf > rf;
            else if (l.Value is double ld && r.Value is double rd)
                return ld > rd;
            else
                return (decimal)Convert.ChangeType(l.Value, typeof(decimal)) > (decimal)Convert.ChangeType(r.Value, typeof(decimal));
        }

        public static bool operator <(FloatingPointNumberWrapper<T> l, FloatingPointNumberWrapper<T> r)
        {
            if (l.Value is float lf && r.Value is float rf)
                return lf < rf;
            else if (l.Value is double ld && r.Value is double rd)
                return ld < rd;
            else
                return (decimal)Convert.ChangeType(l.Value, typeof(decimal)) < (decimal)Convert.ChangeType(r.Value, typeof(decimal));
        }

        public static FloatingPointNumberWrapper<T> operator +(FloatingPointNumberWrapper<T> l, FloatingPointNumberWrapper<T> r)
        {
            if (l.Value is float lf && r.Value is float rf)
                return new FloatingPointNumberWrapper<T>((T)Convert.ChangeType(lf + rf, typeof(T)));
            else if (l.Value is double ld && r.Value is double rd)
                return new FloatingPointNumberWrapper<T>((T)Convert.ChangeType(ld + rd, typeof(T)));
            else
                return new FloatingPointNumberWrapper<T>((T)Convert.ChangeType((decimal)(object)l.Value + (decimal)(object)r.Value, typeof(T)));
        }

        public static FloatingPointNumberWrapper<T> operator -(FloatingPointNumberWrapper<T> l, FloatingPointNumberWrapper<T> r)
        {
            if (l.Value is float lf && r.Value is float rf)
                return new FloatingPointNumberWrapper<T>((T)Convert.ChangeType(lf - rf, typeof(T)));
            else if (l.Value is double ld && r.Value is double rd)
                return new FloatingPointNumberWrapper<T>((T)Convert.ChangeType(ld - rd, typeof(T)));
            else
                return new FloatingPointNumberWrapper<T>((T)Convert.ChangeType((decimal)(object)l.Value - (decimal)(object)r.Value, typeof(T)));
        }

        public static FloatingPointNumberWrapper<T> operator *(FloatingPointNumberWrapper<T> l, FloatingPointNumberWrapper<T> r)
        {
            if (l.Value is float lf && r.Value is float rf)
                return new FloatingPointNumberWrapper<T>((T)Convert.ChangeType(lf * rf, typeof(T)));
            else if (l.Value is double ld && r.Value is double rd)
                return new FloatingPointNumberWrapper<T>((T)Convert.ChangeType(ld * rd, typeof(T)));
            else
                return new FloatingPointNumberWrapper<T>((T)Convert.ChangeType((decimal)(object)l.Value * (decimal)(object)r.Value, typeof(T)));
        }

        public override string ToString()
        {
            return Value.ToString();
        }
    }
}
