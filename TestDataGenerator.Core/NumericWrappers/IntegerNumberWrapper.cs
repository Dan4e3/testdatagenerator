﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces.NumberWrappers;

namespace TestDataGenerator.Core.NumericWrappers
{
    /// <summary>
    /// Integer number wrapper to handle some arithmetic actions with integer types
    /// </summary>
    /// <typeparam name="T">Integer type</typeparam>
    public class IntegerNumberWrapper<T> where T: struct
    {
        /// <summary>
        /// Long value container
        /// </summary>
        public long Value { get; private set; }

        /// <summary>
        /// Create new integer number wrapper
        /// </summary>
        /// <param name="value">Value to wrap</param>
        public IntegerNumberWrapper(T value)
        {
            Value = Convert.ToInt64(value);
        }

        public static bool operator >=(IntegerNumberWrapper<T> l, IntegerNumberWrapper<T> r)
        {
            return l.Value >= r.Value;
        }

        public static bool operator <=(IntegerNumberWrapper<T> l, IntegerNumberWrapper<T> r)
        {
            return l.Value <= r.Value;
        }

        public static bool operator >(IntegerNumberWrapper<T> l, IntegerNumberWrapper<T> r)
        {
            return l.Value > r.Value;
        }

        public static bool operator <(IntegerNumberWrapper<T> l, IntegerNumberWrapper<T> r)
        {
            return l.Value < r.Value;
        }

        public static bool operator >(IntegerNumberWrapper<T> l, long r)
        {
            return l.Value > r;
        }

        public static bool operator <(IntegerNumberWrapper<T> l, long r)
        {
            return l.Value < r;
        }

        public static bool operator >(long l, IntegerNumberWrapper<T> r)
        {
            return l > r.Value;
        }

        public static bool operator <(long l, IntegerNumberWrapper<T> r)
        {
            return l < r.Value;
        }

        public static long operator +(IntegerNumberWrapper<T> l, IntegerNumberWrapper<T> r)
        {
            return l.Value + r.Value;
        }

        public static long operator +(IntegerNumberWrapper<T> l, long r)
        {
            return l.Value + r;
        }

        public static long operator -(IntegerNumberWrapper<T> l, IntegerNumberWrapper<T> r)
        {
            return l.Value - r.Value;
        }

        public static long operator -(IntegerNumberWrapper<T> l, long r)
        {
            return l.Value - r;
        }

        public override string ToString()
        {
            return Value.ToString();
        }
    }
}
