﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.Core.JsonSerialization
{
    public class CultureInfoJsonConverter : JsonConverter<CultureInfo>
    {
        public static readonly string customCultureName = "";

        public override CultureInfo ReadJson(JsonReader reader, Type objectType, CultureInfo existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            try
            {
                return new CultureInfo((string)reader.Value);
            }
            catch
            {
                CultureInfoRepresentation repr = (CultureInfoRepresentation)serializer.Deserialize(reader, typeof(CultureInfoRepresentation));
                return repr.GetCultureInfo();
            }
        }

        public override void WriteJson(JsonWriter writer, CultureInfo value, JsonSerializer serializer)
        {
            if (value.Name == customCultureName)
            {
                serializer.Serialize(writer, new CultureInfoRepresentation(value));
            }
            else
                writer.WriteValue(value.Name);
        }

        public class CultureInfoRepresentation
        {
            public string NumberDecimalSeparator { get; set; }
            public string ListSeparator { get; set; }
            public string FullDateTimePattern { get; set; }
            public string ShortDatePattern { get; set; }
            public string DateSeparator { get; set; }
            public string Name { get; set; }

            public CultureInfoRepresentation(CultureInfo culture)
            {
                // numbers
                NumberDecimalSeparator = culture.NumberFormat.NumberDecimalSeparator;

                //text
                ListSeparator = culture.TextInfo.ListSeparator;

                // datetime
                FullDateTimePattern = culture.DateTimeFormat.FullDateTimePattern;
                DateSeparator = culture.DateTimeFormat.DateSeparator;
                ShortDatePattern = culture.DateTimeFormat.ShortDatePattern;

                Name = culture.Name;
            }

            [JsonConstructor]
            private CultureInfoRepresentation(string numberDecimalSeparator, string listSeparator,
                string fullDateTimePattern, string name, string dateSeparator, string shortDatePattern)
            {
                // numbers
                NumberDecimalSeparator = numberDecimalSeparator;

                // text
                ListSeparator = listSeparator;

                // datetime
                FullDateTimePattern = fullDateTimePattern;
                DateSeparator = dateSeparator;
                ShortDatePattern = shortDatePattern;

                Name = name;
            }

            public CultureInfo GetCultureInfo()
            {
                CultureInfo result = new CultureInfo(customCultureName, true);

                // numbers
                result.NumberFormat.NumberDecimalSeparator = this.NumberDecimalSeparator;

                // text
                result.TextInfo.ListSeparator = this.ListSeparator;

                // datetime
                result.DateTimeFormat.FullDateTimePattern = this.FullDateTimePattern;
                result.DateTimeFormat.DateSeparator = this.DateSeparator;
                result.DateTimeFormat.ShortDatePattern = this.ShortDatePattern;

                return result;
            }
        }
    }
}
