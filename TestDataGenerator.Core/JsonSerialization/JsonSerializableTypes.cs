﻿using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Settings;
using TestDataGenerator.Core.Columns;
using TestDataGenerator.Core.NumericWrappers;
using TestDataGenerator.Core.ValueGenerators;
using TestDataGenerator.Core.ValueGenerators.IncrementalGenerator;
using TestDataGenerator.Core.ValueGenerators.ValuePatternParser;
using TestDataGenerator.Core.ValueGenerators.ValueRangeGenerator;

namespace TestDataGenerator.Core.JsonSerialization
{
    /// <summary>
    /// Describes available type for serialization (used by Newtonsoft.Json serialziation methods)
    /// </summary>
    public class JsonSerializableTypes: ISerializationBinder
    {
        public List<Type> KnownTypes { get; set; }

        public JsonSerializableTypes()
        {
            KnownTypes = new List<Type>() { 
                typeof(ColumnSet), typeof(Column), typeof(ColumnSettings),
                typeof(ValuePatternParser), typeof(SelectFromListValueGenerator), 
                typeof(ValueRange),  typeof(ValueRangeValueGenerator),
                typeof(IntegerRangeValueGenerator<sbyte>),
                typeof(IntegerRangeValueGenerator<byte>),
                typeof(IntegerRangeValueGenerator<ushort>),
                typeof(IntegerRangeValueGenerator<short>),
                typeof(IntegerRangeValueGenerator<uint>),
                typeof(IntegerRangeValueGenerator<int>),
                typeof(IntegerRangeValueGenerator<long>),
                typeof(DecimalRangeValueGenerator<float>),
                typeof(DecimalRangeValueGenerator<double>),
                typeof(DecimalRangeValueGenerator<decimal>),
                typeof(DateTimeRangeValueGenerator),
                typeof(ValueIncrement), typeof(IncrementalValueGenerator),
                typeof(IntegerIncrementalValueGenerator<sbyte>),
                typeof(IntegerIncrementalValueGenerator<byte>),
                typeof(IntegerIncrementalValueGenerator<ushort>),
                typeof(IntegerIncrementalValueGenerator<short>),
                typeof(IntegerIncrementalValueGenerator<uint>),
                typeof(IntegerIncrementalValueGenerator<int>),
                typeof(IntegerIncrementalValueGenerator<long>),
                typeof(DecimalIncrementalValueGenerator<float>),
                typeof(DecimalIncrementalValueGenerator<double>),
                typeof(DecimalIncrementalValueGenerator<decimal>),
                typeof(DateTimeIncrementalValueGenerator), 
                typeof(NullValuesSettings),
                typeof(CultureInfoJsonConverter.CultureInfoRepresentation)
            };
        }

        public void BindToName(Type serializedType, out string assemblyName, out string typeName)
        {
            assemblyName = null;
            if (serializedType.GenericTypeArguments.Length == 0)
                typeName = serializedType.Name;
            else
                typeName = $"{serializedType.Name}[{string.Join(",", serializedType.GenericTypeArguments.Select(g => g.Name))}]";
        }

        public Type BindToType(string assemblyName, string typeName)
        {
            return KnownTypes.SingleOrDefault(t => {
                if (t.GenericTypeArguments.Length == 0)
                    return t.Name == typeName;
                else
                    return $"{t.Name}[{string.Join(",", t.GenericTypeArguments.Select(g => g.Name))}]" == typeName;
            });
        }
    }
}
