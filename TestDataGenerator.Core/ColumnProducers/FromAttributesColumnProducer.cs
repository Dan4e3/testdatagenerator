﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces.Columns;
using TestDataGenerator.Abstractions.Interfaces.ValuesHandling;
using TestDataGenerator.Core.Attributes;
using TestDataGenerator.Core.Columns;
using TestDataGenerator.Core.ValueGenerators.Providers;

namespace TestDataGenerator.Core.ColumnProducers
{
    public class FromAttributesColumnProducer<T> : IColumnProducer where T : class
    {
        public IEnumerable<IColumn> GetColumnsCollection()
        {
            List<IColumn> resultColumns = new List<IColumn>();
            Type inputClassType = typeof(T);
            PropertyInfo[] classProps = inputClassType.GetProperties();

            foreach (PropertyInfo prop in classProps)
            {
                Type propType = prop.PropertyType;

                if (!propType.IsPrimitive && propType != typeof(decimal) &&
                    propType != typeof(string) && propType != typeof(DateTime))
                    continue;

                string colName = DefineColumnName(prop);
                Type targetValueType = DefineTargetValueType(prop);
                IValueGenerator valueGenerator = DefineValueGenerator(prop, targetValueType);

                Column col = new Column(new ColumnSettings() { 
                    ColumnName = colName,
                    TargetValueType = targetValueType,
                    ValueSupplier = valueGenerator
                });

                resultColumns.Add(col);
            }

            return resultColumns;
        }

        private string DefineColumnName(PropertyInfo property)
        {
            ColumnNameAttribute colNameAttr = property.GetCustomAttribute<ColumnNameAttribute>();
            return colNameAttr == null ? property.Name : colNameAttr.Name;
        }

        private Type DefineTargetValueType(PropertyInfo property)
        {
            TargetValueTypeAttribute targetValueTypeAttr = property.GetCustomAttribute<TargetValueTypeAttribute>();
            return targetValueTypeAttr == null ? property.PropertyType : targetValueTypeAttr.TargetValueType;
        }

        private IValueGenerator DefineValueGenerator(PropertyInfo property, Type targetValueType)
        {
            DefaultValueGeneratorProvider provider = new DefaultValueGeneratorProvider();
            ValueGeneratorAttribute valueGeneratorAttr = property.GetCustomAttribute<ValueGeneratorAttribute>();
            if (valueGeneratorAttr == null)
                return provider.GetByTargetValueType(targetValueType);
            else
                return provider.GetByTargetValueTypeAndGeneratorType(targetValueType, valueGeneratorAttr.ValueGeneratorType);
        }
    }
}
