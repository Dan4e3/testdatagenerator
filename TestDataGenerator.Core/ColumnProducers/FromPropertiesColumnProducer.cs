﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces;
using TestDataGenerator.Abstractions.Interfaces.Columns;
using TestDataGenerator.Core.Columns;
using TestDataGenerator.Core.ValueGenerators;
using TestDataGenerator.Core.ValueGenerators.ValuePatternParser;
using TestDataGenerator.Core.ValueGenerators.ValueRangeGenerator;

namespace TestDataGenerator.Core.ColumnProducers
{
    /// <summary>
    /// Generates collection of columns based on specified class properties types
    /// </summary>
    /// <typeparam name="T">Class to be used for column generation</typeparam>
    public class FromPropertiesColumnProducer<T> : IColumnProducer where T : class
    {
        /// <inheritdoc/>
        public IEnumerable<IColumn> GetColumnsCollection()
        {
            List<IColumn> columnsResult = new List<IColumn>();
            Type t = typeof(T);
            PropertyInfo[] typeProps = t.GetProperties();
            foreach (PropertyInfo prop in typeProps)
            {
                Column colToAdd = new Column(new ColumnSettings() { 
                    ColumnName = prop.Name,
                    TargetValueType = prop.PropertyType,
                    Culture = CultureInfo.InvariantCulture
                });

                if (prop.PropertyType == typeof(string))
                    colToAdd.ColumnSettings.ValueSupplier = new ValuePatternParser("[A-z]{3}");
                else if (prop.PropertyType == typeof(bool))
                    colToAdd.ColumnSettings.ValueSupplier = new SelectFromListValueGenerator(new object[] { true, false });
                else if (prop.PropertyType == typeof(DateTime))
                    colToAdd.ColumnSettings.ValueSupplier = new ValueRangeValueGenerator(
                        new ValueRange(new DateTime(1990, 01, 01), DateTime.Now));
                else if (prop.PropertyType == typeof(float) ||
                        prop.PropertyType == typeof(double) ||
                        prop.PropertyType == typeof(decimal))
                    colToAdd.ColumnSettings.ValueSupplier = new ValuePatternParser("[0-1][.][0-9]{2}");
                else if (prop.PropertyType == typeof(sbyte) ||
                        prop.PropertyType == typeof(byte) ||
                        prop.PropertyType == typeof(ushort) ||
                        prop.PropertyType == typeof(short) ||
                        prop.PropertyType == typeof(int) ||
                        prop.PropertyType == typeof(uint) ||
                        prop.PropertyType == typeof(long))
                    colToAdd.ColumnSettings.ValueSupplier = new ValuePatternParser("[1-9][0-9]{1}");
                else
                    continue;

                columnsResult.Add(colToAdd);
            }

            return columnsResult;
        }
    }
}
