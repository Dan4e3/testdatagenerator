﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces;
using TestDataGenerator.Abstractions.Interfaces.Columns;
using TestDataGenerator.Abstractions.Interfaces.Dataset;
using TestDataGenerator.Abstractions.Interfaces.Rows;
using TestDataGenerator.Abstractions.Interfaces.ValuesHandling;

namespace TestDataGenerator.Core
{
    /// <summary>
    /// Dataset containing set of columns, capable of generating rows of data
    /// </summary>
    public class TestDataSet : ITestDataSet
    {
        private IRow[] _resultRows;
        private IProgressReporter _progressReporter;

        /// <summary>
        /// Dataset name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Set of columns, bound to current dataset instance
        /// </summary>
        public IColumnSet ColumnSet { get; private set; }

        /// <summary>
        /// Create new dataset instance with dataset name and column set
        /// </summary>
        /// <param name="setName">Dataset name</param>
        /// <param name="columnSet">Column set to use with dataset</param>
        public TestDataSet(string setName, IColumnSet columnSet, IProgressReporter progressReporter = null)
        {
            Name = setName ?? Guid.NewGuid().ToString("N");
            ColumnSet = columnSet;
            _progressReporter = progressReporter;
        }

        /// <inheritdoc/>
        public IRow[] GenerateNewDataSet(int rowsToGenerate)
        {
            // reset generators to theirs initial states if they support reset
            ResetGeneratorsIfTheySupportIt();

            Stopwatch watch = new Stopwatch();
            _progressReporter?.ReportProgress($"Generating dataset with {rowsToGenerate} rows", 0);
            int counter = 0;
            object locker = new object();

            _resultRows = new IRow[rowsToGenerate];
            watch.Start();
            Parallel.For(0, rowsToGenerate, new ParallelOptions() { MaxDegreeOfParallelism = 6 }, (iter) => {
                _resultRows[iter] = ColumnSet.GetNewRow(this);
                Interlocked.Increment(ref counter);
                _progressReporter?.ReportProgress((int)((counter / (double)rowsToGenerate) * 100));
            });

            watch.Stop();
            _progressReporter?.ReportProgress($"Task completed in {watch.Elapsed.TotalSeconds.ToString("F2")} seconds", 0);
            return _resultRows;
        }

        /// <inheritdoc/>
        public IRow GenerateSingleRow()
        {
            ResetGeneratorsIfTheySupportIt();

            return ColumnSet.GetNewRow(this);
        }

        private void ResetGeneratorsIfTheySupportIt()
        {
            foreach (IColumn col in ColumnSet)
            {
                if (col.GetValueGenerator() is IResettableGenerator resettable)
                    resettable.ResetGeneratorState();
            }
        }

        /// <inheritdoc/>
        public IColumnSet GetColumns()
        {
            return ColumnSet;
        }

        /// <inheritdoc/>
        public IRow[] GetExistingDataSet()
        {
            return _resultRows;
        }

        /// <inheritdoc/>
        public string GetDataSetName()
        {
            return Name;
        }

        /// <inheritdoc/>
        public bool CanGenerateDataSet(out string errorMessage)
        {
            errorMessage = null;

            StringBuilder strBuilder = new StringBuilder();

            if (ColumnSet.Count() == 0)
                strBuilder.Append("Can't generate dataset with zero columns count in columnset. Add at least one column");

            foreach(var col in ColumnSet)
            {
                if (!col.IsValid(out string msg))
                    strBuilder.AppendLine($"{col.GetColumnName()} error: {msg}");
            }

            errorMessage = strBuilder.ToString();

            if (errorMessage.Length == 0)
                return true;
            else
                return false;
        }
    }
}
