﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TestDataGenerator.Core
{
    /// <summary>
    /// Creates Random instance(-s) for value generation
    /// </summary>
    public static class Randomizer
    {
        public static readonly ThreadLocal<Random> Rand = new ThreadLocal<Random>(() => new Random());

        /// <summary>
        /// Gets randomly selected value from array
        /// </summary>
        /// <typeparam name="T">Type of array items</typeparam>
        /// <param name="arr">Array of items to select from</param>
        /// <returns>Random value from array</returns>
        public static T GetRandomValueFromArray<T>(T[] arr)
        {
                int index = Rand.Value.Next(arr.Length);
                return arr[index];
        }
    }
}
