﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces;
using System.IO;
using TestDataGenerator.Core.JsonSerialization;
using TestDataGenerator.Abstractions.Interfaces.Columns;
using TestDataGenerator.Abstractions.Interfaces.Rows;
using TestDataGenerator.Abstractions.Interfaces.Dataset;

namespace TestDataGenerator.Core.Columns
{
    /// <summary>
    /// A container for several columns
    /// </summary>
    public class ColumnSet : IColumnSet
    {
        private List<IColumn> _colsList;
        private Dictionary<IColumn, int> _columnToItsIndexMapper;

        /// <summary>
        /// Creates new instance of column set with provided columns
        /// </summary>
        /// <param name="columns">List of columns to use. If none specified, creates empty list</param>
        public ColumnSet(IEnumerable<IColumn> columns = null)
        {
            _colsList = columns?.ToList() ?? new List<IColumn>();
            _columnToItsIndexMapper = new Dictionary<IColumn, int>();
            for (int i = 0; i < _colsList.Count; i++)
                _columnToItsIndexMapper.Add(_colsList[i], i);
        }

        /// <summary>
        /// Creates new instance of column set using specified column producer
        /// </summary>
        /// <param name="columnProducer">Implementation of <c>IColumnProducer</c> interface which generates columns</param>
        public ColumnSet(IColumnProducer columnProducer)
        {
            _colsList = columnProducer.GetColumnsCollection().ToList();
            _columnToItsIndexMapper = new Dictionary<IColumn, int>();
            for (int i = 0; i < _colsList.Count; i++)
                _columnToItsIndexMapper.Add(_colsList[i], i);
        }

        /// <inheritdoc/>
        public IColumn this[int colIndex]
        {
            get => _colsList[colIndex];
        }

        /// <inheritdoc/>
        public IColumn this[string columnName]
        {
            get => _colsList.FirstOrDefault(col => col.GetColumnName() == columnName);
        }

        /// <inheritdoc/>
        public void AddColumn(IColumn col)
        {
            _colsList.Add(col);
            RefreshColumnMapperDict();
        }

        /// <inheritdoc/>
        public void Clear()
        {
            _colsList.Clear();
        }

        /// <inheritdoc/>
        public int GetColumnsCount()
        {
            return _colsList.Count;
        }

        public IEnumerator<IColumn> GetEnumerator()
        {
            return _colsList.GetEnumerator();
        }

        /// <inheritdoc/>
        public object[] GetNewRow()
        {
            object[] rowData = _colsList.Select(col => col.GenerateValue()).ToArray();
            return rowData;
        }

        /// <inheritdoc/>
        public IRow GetNewRow(ITestDataSet ownerDataset)
        {
            object[] rowData = _colsList.Select(col => col.GenerateValue()).ToArray();
            Row row = new Row(rowData, ownerDataset);
            return row;
        }

        /// <inheritdoc/>
        public void RemoveColumn(IColumn col)
        {
            _colsList.Remove(col);
            RefreshColumnMapperDict();
        }

        /// <summary>
        /// Serializes instance of ColumnSet into JSON format
        /// </summary>
        /// <returns>JSON-serialized string</returns>
        public string SerializeAsJson()
        {
            JsonSerializer serializer = new JsonSerializer();
            serializer.TypeNameHandling = TypeNameHandling.Objects;
            serializer.Formatting = Formatting.Indented;
            serializer.SerializationBinder = new JsonSerializableTypes();
            serializer.Converters.Add(new CultureInfoJsonConverter());

            using (MemoryStream memStream = new MemoryStream())
            using (StreamWriter streamWriter = new StreamWriter(memStream))
            using (JsonWriter jsonWriter = new JsonTextWriter(streamWriter))
            {
                serializer.Serialize(jsonWriter, this);
                jsonWriter.Flush();
                return Encoding.UTF8.GetString(memStream.ToArray());
            }
        }

        /// <summary>
        /// Deserializes ColumnSet from JSON string
        /// </summary>
        /// <param name="inputJson">JSON string with serialized ColumnSet instance</param>
        /// <returns></returns>
        public static ColumnSet DeserializeFromJson(string inputJson)
        {
            JsonSerializer serializer = new JsonSerializer();
            serializer.TypeNameHandling = TypeNameHandling.Objects;
            serializer.SerializationBinder = new JsonSerializableTypes();
            serializer.Converters.Add(new CultureInfoJsonConverter());

            using (MemoryStream memStream = new MemoryStream(Encoding.UTF8.GetBytes(inputJson)))
            using (StreamReader streamReader = new StreamReader(memStream))
            using (JsonReader jsonReader = new JsonTextReader(streamReader))
            {
                ColumnSet restoredObject = serializer.Deserialize<ColumnSet>(jsonReader);
                return restoredObject;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _colsList.GetEnumerator();
        }

        /// <inheritdoc/>
        public int GetColumnIndex(IColumn column)
        {
            return _columnToItsIndexMapper[column];
        }

        private void RefreshColumnMapperDict()
        {
            _columnToItsIndexMapper.Clear();
            for (int i = 0; i < _colsList.Count; i++)
                _columnToItsIndexMapper.Add(_colsList[i], i);
        }
    }
}
