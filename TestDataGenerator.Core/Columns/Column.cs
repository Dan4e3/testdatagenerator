﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces;
using TestDataGenerator.Abstractions.Interfaces.Columns;
using TestDataGenerator.Abstractions.Interfaces.ValuesHandling;

namespace TestDataGenerator.Core.Columns
{
    /// <summary>
    /// A single data column which produces values of certain type based on value supplier
    /// </summary>
    public class Column : IColumn
    {
        /// <summary>
        /// Set of column-specific settings
        /// </summary>
        public ColumnSettings ColumnSettings { get; set; }

        /// <summary>
        /// Creates new column instance with provided settings
        /// </summary>
        /// <param name="settings">Column settings instance</param>
        public Column(ColumnSettings settings)
        {
            ColumnSettings = settings;
        }

        /// <summary>
        /// Generate value based on value type and value supplier settings in <c>ColumnSettings</c> property of column
        /// </summary>
        /// <returns>Boxed object containing value of target type</returns>
        public object GenerateValue()
        {
            if (ColumnSettings.ValueSupplier == null)
                throw new ArgumentNullException("ValueSupplier", "Value generator is not set!");

            var generatedValue = ColumnSettings.ValueSupplier.GenerateValue(ColumnSettings.TargetValueType, ColumnSettings.Culture);
            return generatedValue;
        }

        /// <summary>
        /// Generate multiple values based on value type and value supplier settings in <c>ColumnSettings</c> property of column
        /// </summary>
        /// <param name="valuesCount">Values to generate</param>
        /// <returns>Array of generated objects</returns>
        public object[] GenerateValues(int valuesCount)
        {
            if (ColumnSettings.ValueSupplier == null)
                throw new ArgumentNullException("ValueSupplier", "Value generator is not set!");

            object[] result = new object[valuesCount];
            for (int i = 0; i < valuesCount; i++)
            {
                result[i] = ColumnSettings.ValueSupplier.GenerateValue(ColumnSettings.TargetValueType, ColumnSettings.Culture);
            }
            return result;
        }

        /// <inheritdoc/>
        public string GetColumnName()
        {
            return ColumnSettings.ColumnName;
        }

        /// <inheritdoc/>
        public Type GetColumnType()
        {
            return ColumnSettings.TargetValueType;
        }

        /// <inheritdoc/>
        public string GetInvariantStringValueRepresentation(object value)
        {
            Type expectedValueType = ColumnSettings.TargetValueType;

            TypeConverter typeConverter = TypeDescriptor.GetConverter(expectedValueType);
            return typeConverter.ConvertToInvariantString(value);
        }

        /// <inheritdoc/>
        public string GetStringValueRepresentation(object value)
        {
            Type expectedValueType = ColumnSettings.TargetValueType;

            if (expectedValueType == typeof(DateTime) && value.GetType() == typeof(DateTime))
                return ((DateTime)value).ToString(this.ColumnSettings.Culture);

            TypeConverter typeConverter = TypeDescriptor.GetConverter(expectedValueType);
            return typeConverter.ConvertToString(null, this.ColumnSettings.Culture, value);
        }

        /// <inheritdoc/>
        public IValueGenerator GetValueGenerator()
        {
            return ColumnSettings.ValueSupplier;
        }

        /// <inheritdoc/>
        public bool IsValid(out string outputErrorMessage)
        {
            outputErrorMessage = null;
            StringBuilder strBuilder = new StringBuilder();

            if (ColumnSettings == null)
            {
                strBuilder.Append("ColumnSettings were not provided (null).");
                outputErrorMessage = strBuilder.ToString();
                return false;
            }
                
            if (ColumnSettings.ValueSupplier == null)
                strBuilder.Append("Value generator was not set.");

            if (ColumnSettings.ValueSupplier != null && 
                !ColumnSettings.ValueSupplier.IsValid(out string valueGeneratorError))
                strBuilder.Append(valueGeneratorError);

            if (ColumnSettings.ColumnName == null)
                strBuilder.Append("Column name was not set.");

            if (ColumnSettings.Culture == null)
                strBuilder.Append("Column culture was not set.");

            if (ColumnSettings.TargetValueType == null)
                strBuilder.Append("Target value type for column was not set");

            outputErrorMessage = strBuilder.ToString();

            if (outputErrorMessage.Length == 0)
                return true;
            else
                return false;
        }

        /// <inheritdoc/>
        public void SetValueGenerator(IValueGenerator valueGenerator)
        {
            ColumnSettings.ValueSupplier = valueGenerator;
        }
    }
}
