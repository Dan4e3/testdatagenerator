﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces;
using TestDataGenerator.Abstractions.Interfaces.ValuesHandling;

namespace TestDataGenerator.Core.Columns
{
    /// <summary>
    /// Set of column-specific settings
    /// </summary>
    public class ColumnSettings
    {
        /// <summary>
        /// Desired type of values to be produced by column
        /// </summary>
        public Type TargetValueType { get; set; }

        /// <summary>
        /// Column name
        /// </summary>
        public string ColumnName { get; set; }

        /// <summary>
        /// Generator of values which implements <c>IValueGenerator</c> interface
        /// </summary>
        public IValueGenerator ValueSupplier { get; set; }

        /// <summary>
        /// Input and output culture of values. Defaults to <c>CultureInfo.CurrentCulture</c>
        /// </summary>
        public CultureInfo Culture { get; set; } = CultureInfo.CurrentCulture;
    }
}
