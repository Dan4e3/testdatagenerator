﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.Core.ExtensionMethods
{
    public static class ObjectExtensions
    {
        /// <summary>
        /// Check if value belongs to any numeric data type (from byte to decimal)
        /// </summary>
        /// <param name="value">Value to check</param>
        /// <returns>True if value of numeric type. False, otherwise</returns>
        public static bool IsNumber(this object value)
        {
            return value is sbyte
                    || value is byte
                    || value is short
                    || value is ushort
                    || value is int
                    || value is uint
                    || value is long
                    || value is ulong
                    || value is float
                    || value is double
                    || value is decimal;
        }
    }
}
