﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Web.DataAccess.Interfaces;
using TestDataGenerator.Web.DataAccess.Interfaces.Repositories;
using TestDataGenerator.Web.Models.DomainModels;
using TestDataGenerator.Web.Mq.Interfaces;
using TestDataGenerator.Web.Mq.Models;

namespace TestDataGenerator.Web.Mq
{
    public class MqClient: IMqClient, IDisposable
    {
        private readonly IDatasetGenerationRequestRepository _generationRequestRepo;
        private readonly IBasicServiceOptions _servceOptions;
        private readonly ILogger<MqClient> _logger;

        private readonly IConnection _connection;
        private readonly IModel _channel;
        private readonly string _replyQueueName;
        private readonly EventingBasicConsumer _consumer;
        private readonly IBasicProperties _props;

        public MqClient(
            ILogger<MqClient> logger,
            IDatasetGenerationRequestRepository generationRequestRepo,
            IBasicServiceOptions servceOptions)
        {
            _logger = logger;
            _generationRequestRepo = generationRequestRepo;
            _servceOptions = servceOptions;

            ConnectionFactory factory = new ConnectionFactory() { 
                HostName = _servceOptions.MqOptions.RabbitMqHostname
            };

            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.QueueDeclare(_servceOptions.MqOptions.DatasetInputQueueName, true, false);
            _replyQueueName = _channel.QueueDeclare().QueueName;
            _consumer = new EventingBasicConsumer(_channel);

            _props = _channel.CreateBasicProperties();
            string correlationId = Guid.NewGuid().ToString();
            _props.CorrelationId = correlationId;
            _props.ReplyTo = _replyQueueName;
            _props.Headers = new Dictionary<string, object>() { };
            _props.DeliveryMode = 2;

            _consumer.Received += HandleNewReceivedMessage;

            _channel.BasicConsume(
                consumer: _consumer,
                queue: _replyQueueName,
                autoAck: true);

            _channel.BasicNacks += (sender, eArgs) => {
                _logger.LogWarning("MESSAGE NOT ACKNOWLEDGED!");
            };
        }

        private void HandleNewReceivedMessage(object model, BasicDeliverEventArgs ea)
        {
            byte[] body = ea.Body.ToArray();
            string response = Encoding.UTF8.GetString(body);
            if (ea.BasicProperties.CorrelationId == _props.CorrelationId)
            {
                DatasetResultMqDto generatedData = JsonConvert.DeserializeObject<DatasetResultMqDto>(response);

                DatasetGenerationRequestDm requestDm = _generationRequestRepo.GetSingleByIdAsync(generatedData.RequestId).Result;
                if (requestDm == null)
                {
                    _logger.LogWarning("Received response from MQ Worker cannot be matched with DatasetGenerationRequest. " +
                        "Probably request entity was removed from database. RequestId={requestId}", generatedData.RequestId);
                    return;
                }

                if (requestDm.FileExtension == null)
                {
                    if (generatedData.GeneratedRows != null)
                        requestDm.RowsJson = JsonConvert.SerializeObject(generatedData.GeneratedRows);
                }
                else
                {
                    requestDm.FileContents = generatedData.GeneratedFile;
                }

                requestDm.WorkerResponseReceived = true;
                requestDm.WorkerResponseReceivedDate = DateTime.Now;
                requestDm.ErrorText = generatedData.ErrorText;
                requestDm.SystemExceptionText = generatedData.SystemExceptionText;

                bool entityUpdated = _generationRequestRepo.UpdateSingleAsync(requestDm).Result;
            }
        }

        public void Dispose()
        {
            _channel.Close();
            _connection.Close();
        }

        public void SendMessage(string message, long requestId)
        {
            _props.Headers["requestId"] = requestId;

            var messageBytes = Encoding.UTF8.GetBytes(message);
            _channel.BasicPublish(
                exchange: "",
                routingKey: _servceOptions.MqOptions.DatasetInputQueueName,
                basicProperties: _props,
                body: messageBytes);
        }
    }
}
