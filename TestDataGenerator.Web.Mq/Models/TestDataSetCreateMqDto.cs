﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.Web.Mq.Models
{
    public class TestDataSetCreateMqDto
    {
        public long RequestId { get; set; }

        public string ColumnSetAsJson { get; set; }

        public int RowsToGenerate { get; set; }

        public string FileExtension { get; set; }
    }
}
