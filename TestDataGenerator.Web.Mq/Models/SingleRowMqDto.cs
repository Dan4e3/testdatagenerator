﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.Web.Mq.Models
{
    public class SingleRowMqDto
    {
        public string[] Values { get; set; }
    }
}
