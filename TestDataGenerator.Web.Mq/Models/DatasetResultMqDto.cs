﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.Web.Mq.Models
{
    public class DatasetResultMqDto
    {
        public long RequestId { get; set; }

        public SingleRowMqDto[] GeneratedRows { get; set; }

        public byte[] GeneratedFile { get; set; }

        public string ErrorText { get; set; }

        public string SystemExceptionText { get; set; }
    }
}
