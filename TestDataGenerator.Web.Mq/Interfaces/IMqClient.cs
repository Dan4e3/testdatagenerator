﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.Web.Mq.Interfaces
{
    public interface IMqClient
    {
        void SendMessage(string message, long requestId);
    }
}
