﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.Web.Models.AdditionalColumnParams
{
    public class SelectFromListAdditionalColumnParams
    {
        public long[] PredefinedCategoriesIds { get; set; }
    }
}
