﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.Web.Models.AdditionalColumnParams
{
    public class AdditionalColumnParameters
    {
        public string ColumnName { get; set; }

        public string ParamsJson { get; set; }
    }
}
