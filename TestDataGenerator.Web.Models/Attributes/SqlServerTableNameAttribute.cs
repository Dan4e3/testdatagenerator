﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.Web.Models.Attributes
{
    public class SqlServerTableNameAttribute: Attribute
    {
        public string Name { get; set; }

        public SqlServerTableNameAttribute(string name)
        {
            Name = name;
        }
    }
}
