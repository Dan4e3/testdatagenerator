﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.Web.Models.Attributes
{
    public class PostgreSqlTableNameAttribute: Attribute
    {
        public string Name { get; set; }

        public PostgreSqlTableNameAttribute(string name)
        {
            Name = name;
        }
    }
}
