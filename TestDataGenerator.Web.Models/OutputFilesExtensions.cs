﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.Web.Models
{
    public static class OutputFilesExtensions
    {
        public const string CSV = "csv";

        public const string JSON = "json";
    }
}
