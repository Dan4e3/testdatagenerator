﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Web.Models.Attributes;
using TestDataGenerator.Web.Models.DomainModels.Enums;

namespace TestDataGenerator.Web.Models.DomainModels
{
    [SqlServerTableName("[tdg].[PredefinedValues]")]
    [PostgreSqlTableName("tdg.predefined_values")]
    public class PredefinedValueDm
    {
        [Key]
        public long Id { get; set; }

        public string Value { get; set; }

        public ValueDataTypeEnum DataType { get; set; }

        public long CategoryId { get; set; }
        [Computed]
        public PredefinedCategoryDm Category { get; set; }

        public long ContributorUserId { get; set; }
        [Computed]
        public UserDm ContributorUser { get; set; }

        public DateTime CreationDate { get; set; }
    }
}
