﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.Web.Models.DomainModels.Enums
{
    public enum ValueDataTypeEnum
    {
        String = 0,
        IntNumber = 1,
        FloatPointNumber = 2,
        Boolean = 3,
        DateTime = 4
    }
}
