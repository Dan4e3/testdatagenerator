﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Web.Models.Attributes;

namespace TestDataGenerator.Web.Models.DomainModels
{
    [SqlServerTableName("[tdg].[Users]")]
    [PostgreSqlTableName("tdg.users")]
    public class UserDm
    {
        [Key]
        public long Id { get; set; }

        public string Name { get; set; }
        
        public string Password { get; set; }

        public string AccessToken { get; set; }

        public DateTime RegistrationDate { get; set; }
    }
}
