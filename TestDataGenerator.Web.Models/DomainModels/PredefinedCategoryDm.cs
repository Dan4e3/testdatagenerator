﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Web.Models.Attributes;

namespace TestDataGenerator.Web.Models.DomainModels
{
    [SqlServerTableName("[tdg].[PredefinedCategories]")]
    [PostgreSqlTableName("tdg.predefined_categories")]
    public class PredefinedCategoryDm
    {
        [Key]
        public long Id { get; set; }

        public string Name { get; set; }

        public long ContributorUserId { get; set; }
        [Computed]
        public UserDm ContributorUser { get; set; }

        public bool IsPublic { get; set; }

        public DateTime CreationDate { get; set; }
    }
}
