﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Web.Models.Attributes;

namespace TestDataGenerator.Web.Models.DomainModels
{
    [SqlServerTableName("[tdg].[DatasetGenerationRequests]")]
    [PostgreSqlTableName("tdg.dataset_generation_requests")]
    public class DatasetGenerationRequestDm
    {
        [Key]
        public long Id { get; set; }

        public long RequestingUserId { get; set; }
        [Computed]
        public UserDm RequestingUser { get; set; }

        public long? ColumnSetId { get; set; }
        [Computed]
        public ColumnSetDm ColumnSet { get; set; }

        public int RowsToGenerate { get; set; }

        public string RowsJson { get; set; }

        public byte[] FileContents { get; set; }

        public string FileExtension { get; set; }

        public bool FetchedAtLeastOnce { get; set; }

        public bool WorkerResponseReceived { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime? WorkerResponseReceivedDate { get; set; }

        public string ErrorText { get; set; }

        public string SystemExceptionText { get; set; }
    }
}
