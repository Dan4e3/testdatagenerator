﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Web.Models.Attributes;

namespace TestDataGenerator.Web.Models.DomainModels
{
    [SqlServerTableName("[tdg].[ColumnSets]")]
    [PostgreSqlTableName("tdg.column_sets")]
    public class ColumnSetDm
    {
        [Key]
        public long Id { get; set; }

        public string Name { get; set; }

        public string ColumnSetJson { get; set; }

        public string AdditionalColumnParamsJson { get; set; } = "[]";

        public long OwnerUserId { get; set; }
        [Computed]
        public UserDm OwnerUser { get; set; }

        public DateTime CreationDate { get; set; }
    }
}
