## Upcoming
- Implemented methods to provide value generators for specific properties in DefaultClassInstanceRandomizer

## v.0.4.0
- Generation of null values option is now added in code API and windows app
- Fixed bug with incorrect formatting/culture info data json serialization procedure
- Generate preview option in windows app now shows values with respect to columns' cultures
- Added JsonExportStyle parameter to DataSetJsonWriter. Implemented this feature in windows app in 'Export to JSON' section
- Added method for generation of single row in ITestDataSet interface and its base implementation in TestDataSet
- Added generic method to get value of type to IRow interface and its base implementation in Row
- Added method to IRow to get string representation of whole row at once
- Added IClassInstanceRandomizer interface for class instances generation help. Added its implementation as DefaultInstanceClassRandomizer
- Added ClassInstanceValueGenerator which works with IClassInstanceRandomizer to generate randomized class instances

## v0.3.1
- Strengthened validation for column IsValid() method
- Fixed bug with incremental generator when lock worked incorrectly and produced several identical values

## v0.3.0
- Extended IColumn interface with SetValueGenerator method
- Added indexer to get column by name to IColumnSet interface
- Changed ValueRangeValueGenerator to be compatible with any numeric basic type (integer and floating point numbers)
- Changed IncrementalValueGenerator to be compatible with any numeric basic type (integer and floating point numbers)
- Renamed DoubleIncrementalValueGenerator to DecimalIncrementalValueGenerator
- Renamed DoubleRangeValueGenerator to DecimalRangeValueGenerator
- Added more basic value types in windows app
- Fixed bug with incremental value generator when some of generated values were repeating (added internal lock so that multi-
thread processing work correctly)
- Added json export parameters to JsonDataSetWriter and to windows app
- Added FromAttributesColumnProducer for column set generation with attribute markup approach
- Added method for generation of multiple values to IColumn interface and Column implementation
- Last column set is now restored in windows app on its restart. Some of the fields are also remembered

## v0.2.3
- Added on-fly dataset generation for csv, json and database exporters
- Implemented on fly generation within windows app, thus app RAM usage is greatly reduced for large datasets
- Added "About" tab in windows app
- Added external data loaders interfaces (IDatabaseSourceLoader, IFileSourceLoader, IHttpSourceLoader) and their implementations
- Implemented external data loading functionality in windows app
- Added "New column set" toolstrip item to windows appS
= Added "Clear all" option in List select value generator panel

## v0.2.2
- Added IProgressReporter for time-consuming tasks like dataset generation and dataset export
- Added progress and status bar for windows app
- Dataset generation and export are now being carried in background tasks in windows app

## v0.2.1
- Added netcore3.1 and netstandard2.0 targets for API projects (+ NuGet packages)
- Readme file included in NuGet packages spec

## v0.2.0
- Added incremental generators for int, double and DateTime types
- Added IDbExportExecutor interface to provide better experience with dataset export to databases
- Added IRow interface to replace object[] array when generating rows using TestDataSet
- Added SQL Server and PostgreSQL providers for database export tool in Winforms app
- Fixed bug when column datatype change did not actually happen despite visual effect

## v0.1.1.1
- Added IDbExportExecutor interface to provide better experience with dataset export to databases
- Added abstraction and base implementation for row containing generated data
- Added SQL Server and PostgreSQL providers for database export tool in Winforms app
- Fixed column datatype combobox bug

## v0.1.1
- Supported value generators: by pattern, from list of values, from range between values
- Data can be exported to: JSON, CSV files and MS SQL Server database
- Column sets can be saved as json and loaded back into program on demand
- Windows-built .exe app included
