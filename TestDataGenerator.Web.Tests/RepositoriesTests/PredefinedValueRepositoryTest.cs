﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using TestDataGenerator.Web.DataAccess.Interfaces.Repositories;
using TestDataGenerator.Web.Models.DomainModels;
using TestDataGenerator.Web.Models.DomainModels.Enums;

namespace TestDataGenerator.Web.Tests.RepositoriesTests
{
    public class PredefinedValueRepositoryTest
    {
        private IPredefinedValueRepository _predefinedValueRepo;
        private IPredefinedCategoryRepository _predefinedCategoryRepo;
        private IUserRepository _userRepo;

        private PredefinedCategoryDm _testCategory;
        private UserDm _testUser;
        private PredefinedValueDm _testPredefinedValue;
        private static int _totalEntitiesInBase = 0;

        [OneTimeSetUp]
        public async Task Setup()
        {
            _predefinedValueRepo = DependencyInjection.Provider.GetService<IPredefinedValueRepository>();
            _predefinedCategoryRepo = DependencyInjection.Provider.GetService<IPredefinedCategoryRepository>();
            _userRepo = DependencyInjection.Provider.GetService<IUserRepository>();

            await _predefinedValueRepo.ClearTableAsync();
            await _predefinedCategoryRepo.ClearTableAsync();
            await _userRepo.ClearTableAsync();

            _testUser = new UserDm() {
                Name = "testUser",
                Password = "12345"
            };
            await _userRepo.CreateSingleAsync(_testUser);

            _testCategory = new PredefinedCategoryDm() { 
                Name = "testCategory",
                ContributorUserId = _testUser.Id
            };
            await _predefinedCategoryRepo.CreateSingleAsync(_testCategory);

            _testPredefinedValue = new PredefinedValueDm() {
                CategoryId = _testCategory.Id,
                ContributorUserId = _testUser.Id,
                DataType = ValueDataTypeEnum.IntNumber,
                Value = "1002"
            };
        }

        [Test, Order(1)]
        public async Task PredefinedValueRepostiory_CreatesNewValueAndGetsItFromDatabase()
        {
            long insertedEntityId = await _predefinedValueRepo.CreateSingleAsync(_testPredefinedValue);
            PredefinedValueDm queriedEntity = await _predefinedValueRepo.GetSingleByIdAsync(insertedEntityId);

            Assert.NotZero(insertedEntityId);
            Assert.AreEqual(insertedEntityId, queriedEntity.Id);
            Assert.AreEqual(_testPredefinedValue.Value, queriedEntity.Value);

            _totalEntitiesInBase++;
        }

        [Test, Order(2)]
        public async Task PredefinedValueRepostiory_CreatesManyValues()
        {
            PredefinedValueDm[] valuesToInsert = new PredefinedValueDm[] {
                new PredefinedValueDm(){
                    CategoryId = _testCategory.Id,
                    ContributorUserId = _testUser.Id,
                    DataType = ValueDataTypeEnum.IntNumber,
                    Value = "15"
                },
                new PredefinedValueDm(){
                    CategoryId = _testCategory.Id,
                    ContributorUserId = _testUser.Id,
                    DataType = ValueDataTypeEnum.String,
                    Value = "booboo"
                },
            };

            int insertedRows = await _predefinedValueRepo.CreateManyAsync(valuesToInsert);

            Assert.AreEqual(valuesToInsert.Length, insertedRows);

            _totalEntitiesInBase += insertedRows;
        }

        [Test, Order(3)]
        public async Task PredefinedValueRepostiory_GetsManyByCategoryNameOrCategoryId()
        {
            PredefinedValueDm[] valuesByCategoryId = await _predefinedValueRepo.GetManyByCategoryIdAsync(_testCategory.Id);
            PredefinedValueDm[] valuesByCategoryName = await _predefinedValueRepo.GetManyByCategoryNameAsync(_testCategory.Name);

            Assert.AreEqual(_totalEntitiesInBase, valuesByCategoryId.Length);
            Assert.AreEqual(_totalEntitiesInBase, valuesByCategoryName.Length);

            foreach (var pair in valuesByCategoryId.Zip(valuesByCategoryName))
            {
                Assert.AreEqual(pair.First.Id, pair.Second.Id);
            }
        }
        
        [Test, Order(4)]
        public async Task PredefinedValueRepostiory_UpdatesEntity()
        {
            _testPredefinedValue.Value = "99999";
            bool isEntityUpdated = await _predefinedValueRepo.UpdateSingleAsync(_testPredefinedValue);
            PredefinedValueDm queriedEntity = await _predefinedValueRepo.GetSingleByIdAsync(_testPredefinedValue.Id);

            Assert.True(isEntityUpdated);
            Assert.AreEqual(_testPredefinedValue.Value, queriedEntity.Value);
        }

        [Test, Order(5)]
        public async Task PredefinedValueRepostiory_DeletesOneAndManyEntities()
        {
            int deletedRows = await _predefinedValueRepo.DeleteSingleByIdAsync(_testPredefinedValue.Id);

            Assert.AreEqual(1, deletedRows);

            PredefinedValueDm[] valuesByCategoryId = await _predefinedValueRepo.GetManyByCategoryIdAsync(_testCategory.Id);
            int deletedManyRows = await _predefinedValueRepo.DeleteManyByIdsAsync(valuesByCategoryId.Select(x => x.Id));

            Assert.AreEqual(valuesByCategoryId.Length, deletedManyRows);
        }
    }
}
