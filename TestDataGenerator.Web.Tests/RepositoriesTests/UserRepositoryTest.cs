﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Web.DataAccess.Interfaces.Repositories;
using TestDataGenerator.Web.Models.DomainModels;
using TestDataGenerator.WebApp;

namespace TestDataGenerator.Web.Tests.RepositoriesTests
{
    public class UserRepositoryTest
    {
        private IUserRepository _userRepo;
        private static long _insertedUserId;
        private static UserDm _testUserEntity;

        [OneTimeSetUp]
        public async Task Setup()
        {
            _userRepo = DependencyInjection.Provider.GetService<IUserRepository>();
            await _userRepo.ClearTableAsync();
        }

        [Test, Order(1)]
        public async Task UserRepository_CreatesNewUserAndGetsItFromDatabase()
        {
            _testUserEntity = new UserDm() { 
                Name = "TestUserName",
                Password = "TestPassword"
            };

            _insertedUserId = await _userRepo.CreateSingleAsync(_testUserEntity);

            UserDm queriedUserById = await _userRepo.GetSingleByIdAsync(_insertedUserId);
            UserDm queriedUserByName = await _userRepo.GetSingleByNameAsync(_testUserEntity.Name);

            AssertEx.PropertyValuesAreEquals(_testUserEntity, queriedUserById);
            AssertEx.PropertyValuesAreEquals(_testUserEntity, queriedUserByName);
            Assert.NotZero(_insertedUserId);
        }

        [Test, Order(2)]
        public async Task UserRepository_UpdatesExistingEntryInDatabase()
        {
            _testUserEntity.Name = "AnotherName";

            bool wasUpdated = await _userRepo.UpdateSingleAsync(_testUserEntity);

            UserDm queriedUserById = await _userRepo.GetSingleByIdAsync(_insertedUserId);

            AssertEx.PropertyValuesAreEquals(_testUserEntity, queriedUserById);
            Assert.True(wasUpdated);
        }

        [Test, Order(3)]
        public async Task UserRepository_DeletesSpeicifiedEntryFromDatabase()
        {
            int affectedRows= await _userRepo.DeleteSingleByIdAsync(_testUserEntity.Id);
            UserDm queriedUserByName = await _userRepo.GetSingleByIdAsync(_insertedUserId);

            Assert.AreEqual(1, affectedRows);
            Assert.Null(queriedUserByName);
        }
    }
}
