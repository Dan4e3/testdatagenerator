﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Web.DataAccess.Interfaces.Repositories;
using TestDataGenerator.Web.Models.DTO;
using TestDataGenerator.WebApp;
using Microsoft.Extensions.DependencyInjection;
using TestDataGenerator.Web.Models.DomainModels;
using TestDataGenerator.Web.Models.DTO.ActionResults;
using Microsoft.Net.Http.Headers;
using System.Net;
using TestDataGenerator.Web.Models.DTO.User;

namespace TestDataGenerator.Web.Tests.ControllersTests
{
    public class UserControllerTest
    {
        private static TestServer _server;
        private static HttpClient _client;
        private static IUserRepository _userRepo;

        private const string TEST_USERNAME = "testname";
        private const string TEST_USER_PASS = "testpassword";
        private static string _loginCookie;

        [OneTimeSetUp]
        public async Task Setup()
        {
            _server = new TestServer(new WebHostBuilder()
                .UseConfiguration(DependencyInjection.Configuration)
                .UseStartup<Startup>());
            _client = _server.CreateClient();
            _userRepo = DependencyInjection.Provider.GetService<IUserRepository>();

            await _userRepo.ClearTableAsync();
        }

        [Test, Order(1)]
        public async Task UserController_RegistersUser()
        {
            UserRegistrationDto userRegisterDto = new UserRegistrationDto() {
                Name = TEST_USERNAME,
                Password = TEST_USER_PASS
            };
            StringContent content = new StringContent(JsonConvert.SerializeObject(userRegisterDto), Encoding.UTF8, "application/json");

            HttpResponseMessage response = await _client.PostAsync("/user/register", content);
            _loginCookie = response.Headers.First(x => x.Key == "Set-Cookie").Value.First();
            string responseString = await response.Content.ReadAsStringAsync();

            Assert.True(response.IsSuccessStatusCode);

            UserDm registeredUser = await _userRepo.GetSingleByNameAsync(userRegisterDto.Name);

            Assert.NotNull(registeredUser);
        }

        [Test, Order(2)]
        public async Task UserController_LoginsAlreadyLoggedUser()
        {
            UserLoginDto userLoginDto = new UserLoginDto()
            {
                Name = TEST_USERNAME,
                Password = TEST_USER_PASS
            };
            StringContent content = new StringContent(JsonConvert.SerializeObject(userLoginDto), Encoding.UTF8, "application/json");

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/user/login");
            request.Content = content;
            AssignCookieToHttpRequest(request);

            HttpResponseMessage response = await _client.SendAsync(request);

            string responseString = await response.Content.ReadAsStringAsync();
            UserLoginResult loginResult = JsonConvert.DeserializeObject<UserLoginResult>(responseString);

            Assert.True(response.IsSuccessStatusCode);
            Assert.AreEqual(TEST_USERNAME, loginResult.Name);
        }

        [Test, Order(3)]
        public async Task UserController_LogsOutUser()
        {
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/user/logout");
            AssignCookieToHttpRequest(request);

            HttpResponseMessage logoutResponse = await _client.SendAsync(request);

            Assert.True(logoutResponse.IsSuccessStatusCode);
        }

        [Test, Order(4)]
        public async Task UserController_DoesNotLogUserWithInvalidCredentials()
        {
            const string invalidUserName = "invalidName";
            UserLoginDto userLoginDto = new UserLoginDto()
            {
                Name = invalidUserName,
                Password = "invalidPassword"
            };
            StringContent stringPayload = new StringContent(JsonConvert.SerializeObject(userLoginDto), Encoding.UTF8, "application/json");

            HttpResponseMessage response = await _client.PostAsync("/user/login", stringPayload);
            string responseString = await response.Content.ReadAsStringAsync();
            UserLoginResult loginResult = JsonConvert.DeserializeObject<UserLoginResult>(responseString);

            Assert.False(response.IsSuccessStatusCode);
            Assert.True(response.StatusCode == HttpStatusCode.Unauthorized);
            Assert.NotNull(loginResult.ResultMessage);
            Assert.AreEqual(invalidUserName, loginResult.Name);
        }

        [Test, Order(5)]
        public async Task UserController_LoginsRegisteredUserWithCookies()
        {
            UserLoginDto userLoginDto = new UserLoginDto() { 
                Name = TEST_USERNAME,
                Password = TEST_USER_PASS
            };
            StringContent stringPayload = new StringContent(JsonConvert.SerializeObject(userLoginDto), Encoding.UTF8, "application/json");
            
            HttpResponseMessage response = await _client.PostAsync("/user/login", stringPayload);
            string responseString = await response.Content.ReadAsStringAsync();
            UserLoginResult loginResult = JsonConvert.DeserializeObject<UserLoginResult>(responseString);

            Assert.True(response.IsSuccessStatusCode);
            Assert.AreEqual(TEST_USERNAME, loginResult.Name);
            Assert.Null(loginResult.JwtToken);
        }

        [Test, Order(6)]
        public async Task UserController_GeneratesJwtIfUserIsRegistered()
        {
            UserLoginDto userLoginDto = new UserLoginDto()
            {
                Name = TEST_USERNAME,
                Password = TEST_USER_PASS
            };
            StringContent stringPayload = new StringContent(JsonConvert.SerializeObject(userLoginDto), Encoding.UTF8, "application/json");

            HttpResponseMessage response = await _client.PostAsync("/user/get-access-token", stringPayload);
            string responseString = await response.Content.ReadAsStringAsync();
            UserLoginResult loginResult = JsonConvert.DeserializeObject<UserLoginResult>(responseString);

            Assert.True(response.IsSuccessStatusCode);
            Assert.AreEqual(TEST_USERNAME, loginResult.Name);
            Assert.NotNull(loginResult.JwtToken);
        }

        [Test, Order(7)]
        public async Task UserController_DoesNotGenerateJwtIfUserCredentialsAreIncorrect()
        {
            UserLoginDto userLoginDto = new UserLoginDto()
            {
                Name = "blahblah",
                Password = "blahblah"
            };
            StringContent stringPayload = new StringContent(JsonConvert.SerializeObject(userLoginDto), Encoding.UTF8, "application/json");

            HttpResponseMessage response = await _client.PostAsync("/user/get-access-token", stringPayload);
            string responseString = await response.Content.ReadAsStringAsync();
            UserLoginResult loginResult = JsonConvert.DeserializeObject<UserLoginResult>(responseString);

            Assert.False(response.IsSuccessStatusCode);
            Assert.True(response.StatusCode == HttpStatusCode.Unauthorized);
            Assert.AreEqual("blahblah", loginResult.Name);
            Assert.Null(loginResult.JwtToken);
        }

        private void AssignCookieToHttpRequest(HttpRequestMessage requestMessage)
        {
            requestMessage.Headers.Add("Cookie", new string[] { _loginCookie });
        }
    }
}
