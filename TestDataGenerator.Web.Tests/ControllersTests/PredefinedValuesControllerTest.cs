﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using TestDataGenerator.Web.DataAccess.Interfaces.Repositories;
using TestDataGenerator.WebApp;
using Newtonsoft.Json;
using TestDataGenerator.Web.Models.DTO;
using TestDataGenerator.Web.Models.DTO.ActionResults;
using TestDataGenerator.WebApp.Controllers;
using Moq;
using TestDataGenerator.Web.BusinessLogic.Services;
using Microsoft.AspNetCore.Mvc;
using TestDataGenerator.Web.Models.DomainModels.Enums;
using TestDataGenerator.Web.BusinessLogic.Interfaces;
using TestDataGenerator.Web.BusinessLogic;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.Mvc.Abstractions;
using TestDataGenerator.Web.Models.DTO.PredefinedValues;
using TestDataGenerator.Web.Models.DTO.User;

namespace TestDataGenerator.Web.Tests.ControllersTests
{
    public class PredefinedValuesControllerTest
    {
        private static Mock<IPredefinedValuesService> _predefinedValuesServiceMock;
        private static PredefinedDataController _predefinedValuesController;

        private static AddPredefinedValuesCollectionDto _testData;

        [OneTimeSetUp]
        public void Setup()
        {
            _testData = new AddPredefinedValuesCollectionDto() {
                CategoryName = "testCat",
                Values = new GetPredefinedValueDto[] {
                    new GetPredefinedValueDto() {
                        DataType = ValueDataTypeEnum.Boolean,
                        Value = true
                    },
                    new GetPredefinedValueDto() {
                        DataType = ValueDataTypeEnum.IntNumber,
                        Value = 14L
                    },
                }
            };

            _predefinedValuesServiceMock = new Mock<IPredefinedValuesService>();
            _predefinedValuesServiceMock
                .Setup(x => x.GetPredefinedValuesAsync(It.IsAny<string>(), It.IsAny<int?>()))
                .Returns<string, int?>((category, valuesToFetch) => {
                    _testData.CategoryName = category;
                    if (category == null)
                        _testData.Values = new GetPredefinedValueDto[] { };
                    else
                        _testData.Values = _testData.Values.Take(valuesToFetch.Value).ToArray();
                    return Task.FromResult(_testData);
                });
            _predefinedValuesServiceMock
                .Setup(x => x.AddPredefinedValuesAsync(It.IsAny<AddPredefinedValuesCollectionDto>(), It.IsAny<string>()))
                .Returns<AddPredefinedValuesCollectionDto, string>((inputData, username) => {
                    if (username == "nonexistent")
                        throw new CustomBusinessLogicException("Invalid user");

                    return Task.FromResult(inputData.Values.Length);
                });
            _predefinedValuesServiceMock
                .Setup(x => x.DeletePredefinedValuesAsync(It.IsAny<long[]>()))
                .Returns<long[]>(input => Task.FromResult(input.Length));

            Mock<IUserHandlingService> userHandlingMock = new Mock<IUserHandlingService>();
            userHandlingMock
                .Setup(x => x.UserCredentialsAreCorrectAsync(It.IsAny<UserLoginDto>()))
                .Returns(Task.FromResult(true));

            _predefinedValuesController = new PredefinedDataController(null, null, _predefinedValuesServiceMock.Object, 
                new Mock<IPredefinedCategoriesService>().Object);
        }

        [Test]
        public async Task ValuesController_ReturnsPredefinedData()
        {
            const int valuesToFetch = 1;
            ActionResult<GetPredefinedValuesResult> actionResult = await _predefinedValuesController.GetPredefinedData("testCat", valuesToFetch);
            GetPredefinedValuesResult result = (GetPredefinedValuesResult)((OkObjectResult)actionResult.Result).Value;

            Assert.AreEqual(valuesToFetch, result.Data.Values.Length);
            Assert.AreEqual(_testData.Values[0].Value, result.Data.Values[0].Value);
        }

        [Test]
        public async Task ValuesControllerOnGetPredefinedData_ReturnsNotFoundIfCategoryDoesNotExist()
        {
            ActionResult<GetPredefinedValuesResult> actionResult = await _predefinedValuesController.GetPredefinedData(null, 1);
            GetPredefinedValuesResult result = (GetPredefinedValuesResult)((NotFoundObjectResult)actionResult.Result).Value;

            Assert.NotNull(result.ResultMessage);
            Assert.Null(result.Data.CategoryName);
            Assert.AreEqual(0, result.Data.Values.Length);
        }

        [Test]
        public async Task ValuesController_AddsNewValues()
        {
            _predefinedValuesController.ControllerContext = new ControllerContext() { 
                HttpContext = new DefaultHttpContext()
            };
            _predefinedValuesController.HttpContext.User = new ClaimsPrincipal(new ClaimsIdentity(new Claim[] { new Claim("Name", "testUser") }));

            ActionResult<AddPredefinedValuesResult> actionResult = await _predefinedValuesController.AddNewData(_testData);
            AddPredefinedValuesResult result = (AddPredefinedValuesResult)((OkObjectResult)actionResult.Result).Value;

            Assert.AreEqual(_testData.Values.Length, result.ValuesAdded);
        }

        [Test]
        public async Task ValuesController_DeletesData()
        {
            long[] testIdsToDelete = new long[] { 1, 2, 3, 4 };
            ActionResult<DeletePredefinedValuesResult> actionResult = await _predefinedValuesController.DeleteData(new DeletePredefinedValuesDto() { 
                ValuesIds = testIdsToDelete
            });
            DeletePredefinedValuesResult result = (DeletePredefinedValuesResult)((OkObjectResult)actionResult.Result).Value;

            Assert.AreEqual(testIdsToDelete.Length, result.DeletedValuesCount);
        }
    }
}
