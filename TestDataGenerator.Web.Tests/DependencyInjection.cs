﻿using Microsoft.AspNetCore.Connections;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Web.BusinessLogic;
using TestDataGenerator.Web.BusinessLogic.Interfaces;
using TestDataGenerator.Web.BusinessLogic.Services;
using TestDataGenerator.Web.DataAccess;
using TestDataGenerator.Web.DataAccess.Interfaces;
using TestDataGenerator.Web.DataAccess.Interfaces.Repositories;
using TestDataGenerator.Web.DataAccess.SqlServer.Repositories;

namespace TestDataGenerator.Web.Tests
{
    internal static class DependencyInjection
    {
        internal static IServiceProvider Provider;
        internal static IConfigurationRoot Configuration;

        static DependencyInjection()
        {
            ServiceCollection collection = new ServiceCollection();
            Configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile("appsettings.Development.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables()
                .Build();

            DbConnectionFactory connFactory = new DbConnectionFactory(Configuration);

            collection.AddAutoMapper(typeof(MappingProfile));

            collection.AddSingleton<IDbConnectionFactory>(connFactory);
            collection.AddSingleton<DapperCustomSettings>(new DapperCustomSettings());

            collection.AddSingleton<IUserHandlingService, UserHandlingService>();
            collection.AddSingleton<IPredefinedValuesService, PredefinedValuesService>();

            collection.AddSingleton<IUserRepository, UserRepository>();
            collection.AddSingleton<IPredefinedValueRepository, PredefinedValueRepository>();
            collection.AddSingleton<IPredefinedCategoryRepository, PredefinedCategoryRepository>();

            Provider = collection.BuildServiceProvider();
        }
    }
}
