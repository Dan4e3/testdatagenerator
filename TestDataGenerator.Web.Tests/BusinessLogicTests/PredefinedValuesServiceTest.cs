﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using TestDataGenerator.Web.BusinessLogic.Services;
using TestDataGenerator.Web.DataAccess.Interfaces.Repositories;
using TestDataGenerator.Web.Models.DomainModels;
using TestDataGenerator.Web.Models.DomainModels.Enums;
using TestDataGenerator.Web.BusinessLogic.Interfaces;
using TestDataGenerator.WebApp.Models.DTO.PredefinedValues;

namespace TestDataGenerator.Web.Tests.BusinessLogicTests
{
    public class PredefinedValuesServiceTest
    {
        private IPredefinedValuesService _predefinedValuesService;
        private IPredefinedValueRepository _predefinedValueRepo;
        private IPredefinedCategoryRepository _predefinedCategoryRepo;
        private IUserRepository _userRepo;

        private PredefinedCategoryDm _testCategory;
        private UserDm _testUser;
        private AddPredefinedValuesCollectionDto _testData;

        [OneTimeSetUp]
        public async Task Setup()
        {
            _predefinedValuesService = DependencyInjection.Provider.GetService<IPredefinedValuesService>();
            _predefinedValueRepo = DependencyInjection.Provider.GetService<IPredefinedValueRepository>();
            _predefinedCategoryRepo = DependencyInjection.Provider.GetService<IPredefinedCategoryRepository>();
            _userRepo = DependencyInjection.Provider.GetService<IUserRepository>();

            await _predefinedValueRepo.ClearTableAsync();
            await _predefinedCategoryRepo.ClearTableAsync();
            await _userRepo.ClearTableAsync();

            _testUser = new UserDm()
            {
                Name = "testUser",
                Password = "12345"
            };
            await _userRepo.CreateSingleAsync(_testUser);

            _testCategory = new PredefinedCategoryDm()
            {
                Name = "testCategory",
                ContributorUserId = _testUser.Id
            };
            await _predefinedCategoryRepo.CreateSingleAsync(_testCategory);
        }

        [Test, Order(1)]
        public async Task ValuesService_AddsNewValuesToDatabase()
        {
            _testData = new AddPredefinedValuesCollectionDto() { 
                CategoryName = _testCategory.Name,
                Values = new GetPredefinedValueDto[] { 
                    new GetPredefinedValueDto() { 
                        Value = "test",
                        DataType = ValueDataTypeEnum.String
                    },
                    new GetPredefinedValueDto() {
                        Value = 15L,
                        DataType = ValueDataTypeEnum.IntNumber
                    },
                    new GetPredefinedValueDto() {
                        Value = 25.093,
                        DataType = ValueDataTypeEnum.FloatPointNumber
                    },
                }
            };

            int insertedRows = await _predefinedValuesService.AddPredefinedValuesAsync(_testData, _testUser.Name);

            Assert.AreEqual(_testData.Values.Length, insertedRows);
        }

        [Test, Order(2)]
        public async Task ValuesService_GetsValuesFromDatabase()
        {
            AddPredefinedValuesCollectionDto result = await _predefinedValuesService.GetPredefinedValuesAsync(_testCategory.Name);

            Assert.NotNull(result);
            Assert.AreEqual(_testData.Values.Length, result.Values.Length);
            Assert.AreEqual(_testData.CategoryName, result.CategoryName);
        }

        [Test, Order(3)]
        public async Task ValuesService_DeletesValuesByIdsFromDataBase()
        {
            AddPredefinedValuesCollectionDto dataToDelete = await _predefinedValuesService.GetPredefinedValuesAsync(_testCategory.Name);

            int deletedRows = await _predefinedValuesService.DeletePredefinedValuesAsync(dataToDelete.Values.Select(x => x.Id));

            Assert.AreEqual(dataToDelete.Values.Length, deletedRows);
        }
    }
}
