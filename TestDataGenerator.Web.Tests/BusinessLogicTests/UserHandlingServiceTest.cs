﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Web.DataAccess.Interfaces.Repositories;
using Microsoft.Extensions.DependencyInjection;
using TestDataGenerator.Web.BusinessLogic.Services;
using TestDataGenerator.Web.Models.DTO;
using TestDataGenerator.Web.Models.DomainModels;
using TestDataGenerator.Web.BusinessLogic.Interfaces;
using TestDataGenerator.Web.Models.DTO.User;

namespace TestDataGenerator.Web.Tests.BusinessLogicTests
{
    public class UserHandlingServiceTest
    {
        private IUserRepository _userRepo;
        private IUserHandlingService _userHandlingService;
        private const string TEST_USERNAME = "testUser";
        private const string TEST_USER_PASS = "testPass";

        [OneTimeSetUp]
        public async Task Setup()
        {
            _userRepo = DependencyInjection.Provider.GetService<IUserRepository>();
            _userHandlingService = DependencyInjection.Provider.GetService<IUserHandlingService>();
            await _userRepo.ClearTableAsync();
        }

        [Test, Order(1)]
        public async Task Service_SuccessfullyRegistersUser()
        {
            UserRegistrationDto registrationDto = new UserRegistrationDto() { 
                Name = TEST_USERNAME,
                Password = TEST_USER_PASS
            };

            UserDm user = await _userHandlingService.RegisterUserAsync(registrationDto);

            Assert.NotNull(user);
            Assert.AreEqual(registrationDto.Name, user.Name);
            Assert.AreNotEqual(registrationDto.Password, user.Password); // password supposed to be hashed
            Assert.NotNull(user.Password);
        }

        [Test, Order(2)]
        public async Task ServiceChecksUserCredentials_ReturnsTrueIfCredentailsCorrectAndFalseOtherwise()
        {
            UserLoginDto loginDto = new UserLoginDto() { 
                Name = TEST_USERNAME,
                Password = TEST_USER_PASS
            };

            bool credentialsCorrect = await _userHandlingService.UserCredentialsAreCorrectAsync(loginDto);

            Assert.True(credentialsCorrect);

            loginDto.Name = "invalidUser";
            bool credentialsCorrect2 = await _userHandlingService.UserCredentialsAreCorrectAsync(loginDto);

            Assert.False(credentialsCorrect2);

            loginDto.Name = TEST_USERNAME;
            loginDto.Password = "invalidPassword";
            bool credentialsCorrect3 = await _userHandlingService.UserCredentialsAreCorrectAsync(loginDto);

            Assert.False(credentialsCorrect3);
        }
    }
}
