﻿using Dapper;
using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Web.DataAccess.Interfaces;
using TestDataGenerator.Web.DataAccess.Interfaces.Repositories;
using TestDataGenerator.Web.Models.DomainModels;

namespace TestDataGenerator.Web.DataAccess.PgSql.Repositories
{
    public class DatasetGenerationRequestRepository : IDatasetGenerationRequestRepository
    {
        private readonly IDbConnectionFactory _connFactory;
        private readonly string _dsetGenRequestsTableName;
        private readonly string _usersTableName;
        private readonly string _columnSetsTableName;

        public DatasetGenerationRequestRepository(
            IDbConnectionFactory connFactory,
            IDapperCustomSettings dapperCustomSettings)
        {
            _connFactory = connFactory;
            _dsetGenRequestsTableName = dapperCustomSettings.GetTableNameFromAttr(typeof(DatasetGenerationRequestDm));
            _usersTableName = dapperCustomSettings.GetTableNameFromAttr(typeof(UserDm));
            _columnSetsTableName = dapperCustomSettings.GetTableNameFromAttr(typeof(ColumnSetDm));
        }

        public async Task<long> CreateSingleAsync(DatasetGenerationRequestDm model)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            //int insertedRows = await conn.InsertAsync(model);
            string sql = $@"
                INSERT INTO {_dsetGenRequestsTableName} (requesting_user_id, column_set_id, rows_to_generate, 
                    rows_json, file_contents, file_extension, creation_date, worker_response_received_date,
                    error_text, system_exception_text, fetched_at_least_once, worker_response_received)
                VALUES (@RequestingUserId, @ColumnSetId, @RowsToGenerate, @RowsJson, @FileContents, @FileExtension, 
                    @CreationDate, @WorkerResponseReceivedDate, @ErrorText, @SystemExceptionText, @FetchedAtLeastOnce,
                    @WorkerResponseReceived)
                RETURNING id
            ";
            long insertedId = await conn.ExecuteScalarAsync<long>(sql, model);
            model.Id = insertedId;
            return insertedId;
        }

        public async Task<bool> UpdateSingleAsync(DatasetGenerationRequestDm model)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            //bool wasUpdated = await conn.UpdateAsync(model);
            string sql = $@"
                UPDATE {_dsetGenRequestsTableName} SET
                    requesting_user_id=@RequestingUserId,
                    column_set_id=@ColumnSetId,
                    rows_to_generate=@RowsToGenerate,
                    rows_json=@RowsJson,
                    file_contents=@FileContents,
                    file_extension=@FileExtension,
                    creation_date=@CreationDate,
                    worker_response_received_date=@WorkerResponseReceivedDate,
                    error_text=@ErrorText,
                    system_exception_text=@SystemExceptionText,
                    fetched_at_least_once=@FetchedAtLeastOnce,
                    worker_response_received=@WorkerResponseReceived
                WHERE id=@Id
                RETURNING TRUE
            ";
            bool updateResult = await conn.ExecuteScalarAsync<bool>(sql, model);
            return updateResult;
        }

        public async Task<DatasetGenerationRequestDm> GetSingleByIdAsync(long id)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"
                SELECT * FROM {_dsetGenRequestsTableName} dsetReq
                INNER JOIN {_usersTableName} usr ON dsetReq.requesting_user_id=usr.id
                LEFT JOIN {_columnSetsTableName} colSet ON colSet.id=dsetReq.column_set_id
                WHERE dsetReq.id=@id
            ";

            DatasetGenerationRequestDm result = (await conn.QueryAsync<DatasetGenerationRequestDm,
                UserDm, ColumnSetDm, DatasetGenerationRequestDm>(sql,
                (dsetReq, usr, colSet) => {
                    dsetReq.RequestingUser = usr;
                    dsetReq.ColumnSet = colSet;
                    return dsetReq;
                },
                new { id })).FirstOrDefault();

            return result;
        }

        public async Task<DatasetGenerationRequestDm> GetSinglePartialStateByIdAsync(long id)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"
                SELECT dsetReq.id, dsetReq.worker_response_received, dsetReq.worker_response_received_date, dsetReq.error_text,
                    usr.*
                FROM {_dsetGenRequestsTableName} dsetReq
                INNER JOIN {_usersTableName} usr ON dsetReq.requesting_user_id=usr.id
                WHERE dsetReq.id=@id
            ";

            DatasetGenerationRequestDm result = (await conn.QueryAsync<DatasetGenerationRequestDm,
                UserDm, DatasetGenerationRequestDm>(sql,
                (dsetReq, usr) => {
                    dsetReq.RequestingUser = usr;
                    return dsetReq;
                },
                new { id })).FirstOrDefault();
            return result;
        }

        public async Task<DatasetGenerationRequestDm[]> GetManyByUserNameAsync(string userName)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"
                SELECT dsetReq.id, dsetReq.requesting_user_id, dsetReq.column_set_id, dsetReq.rows_to_generate,
                    dsetReq.file_extension, dsetReq.creation_date, dsetReq.fetched_at_least_once,
                    dsetReq.worker_response_received, dsetReq.worker_response_received_date, dsetReq.error_text,
                    usr.*, colSet.*
                FROM {_dsetGenRequestsTableName} dsetReq
                INNER JOIN {_usersTableName} usr ON dsetReq.requesting_user_id=usr.id
                LEFT JOIN {_columnSetsTableName} colSet ON colSet.id=dsetReq.column_set_id
                WHERE usr.name=@userName
            ";

            DatasetGenerationRequestDm[] result = (await conn.QueryAsync<DatasetGenerationRequestDm,
                UserDm, ColumnSetDm, DatasetGenerationRequestDm>(sql,
                (dsetReq, usr, colSet) => {
                    dsetReq.RequestingUser = usr;
                    dsetReq.ColumnSet = colSet;
                    return dsetReq;
                },
                new { userName })).ToArray();

            return result;
        }

        public async Task<bool> CheckIfWorkerResponseReceivedByRequestIdAsync(long id)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"
                SELECT worker_response_received FROM {_dsetGenRequestsTableName} dsetReq
                WHERE dsetReq.id=@id
            ";

            bool result = await conn.QueryFirstOrDefaultAsync<bool>(sql, new { id });
            return result;
        }

        public async Task<long[]> DeleteFetchedRawJsonDatasetRequestsAsync()
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"
                DELETE FROM {_dsetGenRequestsTableName} 
                WHERE fetched_at_least_once=TRUE AND rows_json IS NOT NULL
                RETURNING id
            ";
            long[] deletedRowsIds = (await conn.QueryAsync<long>(sql)).ToArray();
            return deletedRowsIds;
        }

        public async Task<long[]> DeleteExpiredFileDatasetReuqestsAsync(TimeSpan requestLifeTime)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"
                DELETE FROM {_dsetGenRequestsTableName} 
                WHERE (file_contents IS NOT NULL 
                    AND worker_response_received_date + @requestLifeTime < NOW())
                    OR (worker_response_received_date IS NULL 
                        AND creation_date + @requestLifeTime < NOW())
                RETURNING id
            ";
            long[] deletedRowsIds = (await conn.QueryAsync<long>(sql, new { requestLifeTime })).ToArray();
            return deletedRowsIds;
        }

        public async Task<int> SetGenerationRequestAsFetchedAsync(long id)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"
                UPDATE {_dsetGenRequestsTableName} SET fetched_at_least_once=TRUE
                WHERE id=@id
            ";
            int affectedRows = await conn.ExecuteAsync(sql, new { id });
            return affectedRows;
        }

        public async Task<DatasetGenerationRequestDm[]> GetManyByUserNamePaginatedAsync(string userName, int valuesToSkip, int valuesToFetch)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"
                SELECT dsetReq.id, dsetReq.requesting_user_id, dsetReq.column_set_id, dsetReq.rows_to_generate,
                    dsetReq.file_extension, dsetReq.creation_date, dsetReq.fetched_at_least_once,
                    dsetReq.worker_response_received, dsetReq.worker_response_received_date, dsetReq.error_text,
                    usr.*, colSet.*
                FROM {_dsetGenRequestsTableName} dsetReq
                INNER JOIN {_usersTableName} usr ON dsetReq.requesting_user_id=usr.id
                LEFT JOIN {_columnSetsTableName} colSet ON colSet.id=dsetReq.column_set_id
                WHERE usr.name=@userName
                ORDER BY dsetReq.id
                OFFSET @valuesToSkip ROWS 
                FETCH NEXT @valuesToFetch ROWS ONLY                
            ";

            DatasetGenerationRequestDm[] result = (await conn.QueryAsync<DatasetGenerationRequestDm,
                UserDm, ColumnSetDm, DatasetGenerationRequestDm>(sql,
                (dsetReq, usr, colSet) => {
                    dsetReq.RequestingUser = usr;
                    dsetReq.ColumnSet = colSet;
                    return dsetReq;
                },
                new { userName, valuesToSkip, valuesToFetch })).ToArray();

            return result;
        }

        public async Task<int> GetUserRequestsCountAsync(string userName)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"SELECT COUNT(1) FROM {_dsetGenRequestsTableName}";
            int result = await conn.ExecuteScalarAsync<int>(sql);
            return result;
        }
    }
}
