﻿using Dapper;
using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Web.DataAccess.Interfaces;
using TestDataGenerator.Web.DataAccess.Interfaces.Repositories;
using TestDataGenerator.Web.Models.DomainModels;

namespace TestDataGenerator.Web.DataAccess.PgSql.Repositories
{
    public class PredefinedCategoryRepository : IPredefinedCategoryRepository
    {
        private readonly IDbConnectionFactory _connFactory;
        private readonly string _predefinedCategoriesTableName;
        private readonly string _usersTableName;

        public PredefinedCategoryRepository(
            IDbConnectionFactory connFactory,
            IDapperCustomSettings dapperCustomSettings)
        {
            _connFactory = connFactory;
            _predefinedCategoriesTableName = dapperCustomSettings.GetTableNameFromAttr(typeof(PredefinedCategoryDm));
            _usersTableName = dapperCustomSettings.GetTableNameFromAttr(typeof(UserDm));
        }

        public async Task<int> ClearTableAsync()
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"DELETE FROM {_predefinedCategoriesTableName}";
            int affectedRows = await conn.ExecuteAsync(sql);
            return affectedRows;
        }

        public async Task<long> CreateSingleAsync(PredefinedCategoryDm model)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"
                INSERT INTO {_predefinedCategoriesTableName} (name, contributor_user_id, is_public, creation_date)
                VALUES (@Name, @ContributorUserId, @IsPublic, @CreationDate)
                RETURNING id
            ";
            //int result = await conn.InsertAsync(model);
            long insertedId = await conn.ExecuteScalarAsync<long>(sql, model);
            model.Id = insertedId;
            return insertedId;
        }

        public async Task<bool> UpdateSingleAsync(PredefinedCategoryDm model)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            //bool result = await conn.UpdateAsync(model);
            string sql = $@"
                UPDATE {_predefinedCategoriesTableName} SET 
                    name=@Name,
                    contributor_user_id=@ContributorUserId,
                    is_public=@IsPublic,
                    creation_date=@CreationDate
                WHERE id=@Id
                RETURNING TRUE
            ";
            bool updateResult = await conn.ExecuteScalarAsync<bool>(sql, model);
            return updateResult;
        }

        public async Task<int> DeleteSingleByIdAsync(long categoryId)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"DELETE FROM {_predefinedCategoriesTableName} WHERE id=@valueId";
            int affectedRows = await conn.ExecuteAsync(sql, new { categoryId });
            return affectedRows;
        }

        public async Task<int> DeleteManyByIdsAsync(long[] categoriesIds)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"DELETE FROM {_predefinedCategoriesTableName} WHERE id=ANY(@categoriesIds)";
            int affectedRows = await conn.ExecuteAsync(sql, new { categoriesIds });
            return affectedRows;
        }

        public async Task<PredefinedCategoryDm[]> GetAllAsync()
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"
                SELECT * FROM {_predefinedCategoriesTableName} pCat
                INNER JOIN {_usersTableName} usr ON pCat.contributor_user_id=usr.id
            ";

            PredefinedCategoryDm[] result = (await conn
                .QueryAsync<PredefinedCategoryDm, UserDm, PredefinedCategoryDm>(sql, (pCat, usr) => {
                    pCat.ContributorUser = usr;
                    return pCat;
                })).ToArray();

            return result;
        }

        public async Task<PredefinedCategoryDm> GetSingleByIdAsync(long categoryId)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"
                SELECT * FROM {_predefinedCategoriesTableName} pCat
                INNER JOIN {_usersTableName} usr ON pCat.contributor_user_id=usr.id
                WHERE pCat.id=@categoryId
            ";

            PredefinedCategoryDm result = (await conn
                .QueryAsync<PredefinedCategoryDm, UserDm, PredefinedCategoryDm>(sql, (pCat, usr) => {
                    pCat.ContributorUser = usr;
                    return pCat;
                }, new { categoryId })).FirstOrDefault();

            return result;
        }

        public async Task<PredefinedCategoryDm> GetSingleByNameAsync(string categoryName)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"
                SELECT * FROM {_predefinedCategoriesTableName} pCat
                INNER JOIN {_usersTableName} usr ON pCat.contributor_user_id=usr.id
                WHERE pCat.name=@categoryName
            ";

            PredefinedCategoryDm result = (await conn
                .QueryAsync<PredefinedCategoryDm, UserDm, PredefinedCategoryDm>(sql, (pCat, usr) => {
                    pCat.ContributorUser = usr;
                    return pCat;
                }, new { categoryName })).FirstOrDefault();

            return result;
        }

        public async Task<PredefinedCategoryDm[]> GetManyByContributorUserNameAsync(string contributorUserName, bool includePublic)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"
                SELECT * FROM {_predefinedCategoriesTableName} pCat
                INNER JOIN {_usersTableName} usr ON pCat.contributor_user_id=usr.id
                WHERE usr.name=@contributorUserName {(includePublic == true ? "OR pCat.is_public=TRUE" : "")}
            ";

            PredefinedCategoryDm[] result = (await conn
                .QueryAsync<PredefinedCategoryDm, UserDm, PredefinedCategoryDm>(sql, (pCat, usr) => {
                    pCat.ContributorUser = usr;
                    return pCat;
                }, new { contributorUserName })).ToArray();

            return result;
        }

        public async Task<PredefinedCategoryDm[]> GetPublicCategoriesAsync()
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"
                SELECT * FROM {_predefinedCategoriesTableName}
                WHERE is_public=TRUE
            ";
            PredefinedCategoryDm[] result = (await conn.QueryAsync<PredefinedCategoryDm>(sql)).ToArray();
            return result;
        }
    }
}
