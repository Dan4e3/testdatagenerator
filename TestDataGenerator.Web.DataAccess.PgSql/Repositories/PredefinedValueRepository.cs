﻿using Dapper;
using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Web.DataAccess.Interfaces;
using TestDataGenerator.Web.DataAccess.Interfaces.Repositories;
using TestDataGenerator.Web.Models.DomainModels;
using TestDataGenerator.Web.Models.DomainModels.Enums;

namespace TestDataGenerator.Web.DataAccess.PgSql.Repositories
{
    public class PredefinedValueRepository : IPredefinedValueRepository
    {
        private readonly IDbConnectionFactory _connFactory;
        private readonly string _predefinedValuesTableName;
        private readonly string _predefinedCategoriesTableName;
        private readonly string _usersTableName;

        public PredefinedValueRepository(
            IDbConnectionFactory connFactory,
            IDapperCustomSettings dapperCustomSettings)
        {
            _connFactory = connFactory;
            _predefinedValuesTableName = dapperCustomSettings.GetTableNameFromAttr(typeof(PredefinedValueDm));
            _predefinedCategoriesTableName = dapperCustomSettings.GetTableNameFromAttr(typeof(PredefinedCategoryDm));
            _usersTableName = dapperCustomSettings.GetTableNameFromAttr(typeof(UserDm));
        }

        public async Task<int> ClearTableAsync()
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"DELETE FROM {_predefinedValuesTableName}";
            int affectedRows = await conn.ExecuteAsync(sql);
            return affectedRows;
        }

        public async Task<long> CreateSingleAsync(PredefinedValueDm model)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            //int result = await conn.InsertAsync(model);
            string sql = $@"
                INSERT INTO {_predefinedValuesTableName} (value, data_type, category_id, contributor_user_id, creation_date)
                VALUES (@Value, @DataType, @CategoryId, @ContributorUserId, @CreationDate)
                RETURNING id
            ";
            long insertedId = await conn.ExecuteScalarAsync<long>(sql, model);
            model.Id = insertedId;
            return insertedId;
        }

        public async Task<int> CreateManyAsync(PredefinedValueDm[] models)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            //int result = await conn.InsertAsync(models);
            string sql = $@"
                INSERT INTO {_predefinedValuesTableName} (value, data_type, category_id, contributor_user_id, creation_date)
                VALUES (@Value, @DataType, @CategoryId, @ContributorUserId, @CreationDate)
            ";
            int insertedCount = await conn.ExecuteScalarAsync<int>(sql, models);
            return insertedCount;
        }

        public async Task<bool> UpdateSingleAsync(PredefinedValueDm model)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            //bool result = await conn.UpdateAsync(model);
            string sql = $@"
                UPDATE {_predefinedValuesTableName} SET 
                    value=@Value,
                    data_type=@DataType,
                    category_id=@CategoryId,
                    contributor_user_id=@ContributorUserId,
                    creation_date=@CreationDate
                WHERE id=@Id
                RETURNING TRUE
            ";
            bool updateResult = await conn.ExecuteScalarAsync<bool>(sql, model);
            return updateResult;
        }

        public async Task<int> DeleteSingleByIdAsync(long valueId)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"DELETE FROM {_predefinedValuesTableName} WHERE id=@valueId";
            int affectedRows = await conn.ExecuteAsync(sql, new { valueId });
            return affectedRows;
        }

        public async Task<int> DeleteManyByIdsAsync(long[] idsToRemove)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"DELETE FROM {_predefinedValuesTableName} WHERE id = ANY(@idsToRemove)";
            int affectedRows = await conn.ExecuteAsync(sql, new { idsToRemove });
            return affectedRows;
        }

        public async Task<PredefinedValueDm[]> GetManyByCategoryIdAsync(long categoryId)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"
                SELECT * FROM {_predefinedValuesTableName} pVal 
                INNER JOIN {_predefinedCategoriesTableName} pCat ON pVal.category_id=pCat.id
                INNER JOIN {_usersTableName} usr ON pVal.contributor_user_id=usr.id
                WHERE pVal.category_id=@categoryId
            ";

            IEnumerable<PredefinedValueDm> result = await conn
                .QueryAsync<PredefinedValueDm, PredefinedCategoryDm, UserDm, PredefinedValueDm>(sql, (pVal, pCat, usr) => {
                    pVal.Category = pCat;
                    pVal.ContributorUser = usr;
                    return pVal;
                }, new { categoryId });

            return result.ToArray();
        }

        public async Task<PredefinedValueDm[]> GetManyByCategoryNameAsync(string categoryName)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"
                SELECT * FROM {_predefinedValuesTableName} pVal 
                INNER JOIN {_predefinedCategoriesTableName} pCat ON pVal.category_id=pCat.id
                INNER JOIN {_usersTableName} usr ON pVal.contributor_user_id=usr.id
                WHERE pCat.name=@categoryName
            ";

            IEnumerable<PredefinedValueDm> result = await conn
                .QueryAsync<PredefinedValueDm, PredefinedCategoryDm, UserDm, PredefinedValueDm>(sql, (pVal, pCat, usr) => {
                    pVal.Category = pCat;
                    pVal.ContributorUser = usr;
                    return pVal;
                }, new { categoryName });

            return result.ToArray();
        }

        public async Task<PredefinedValueDm> GetSingleByValueAndDataTypeAsync(string value, ValueDataTypeEnum dataType)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"
                SELECT * FROM {_predefinedValuesTableName} pVal 
                INNER JOIN {_predefinedCategoriesTableName} pCat ON pVal.category_id=pCat.id
                INNER JOIN {_usersTableName} usr ON pVal.contributor_user_id=usr.id
                WHERE pVal.value=@value AND pVal.data_type=@dataType
            ";

            PredefinedValueDm result = (await conn
                .QueryAsync<PredefinedValueDm, PredefinedCategoryDm, UserDm, PredefinedValueDm>(sql, (pVal, pCat, usr) => {
                    pVal.Category = pCat;
                    pVal.ContributorUser = usr;
                    return pVal;
                }, new { value, dataType })).FirstOrDefault();
            return result;
        }

        public async Task<PredefinedValueDm> GetSingleByIdAsync(long valueId)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"
                SELECT * FROM {_predefinedValuesTableName} pVal 
                INNER JOIN {_predefinedCategoriesTableName} pCat ON pVal.category_id=pCat.id
                INNER JOIN {_usersTableName} usr ON pVal.contributor_user_id=usr.id
                WHERE pVal.id=@valueId
            ";

            PredefinedValueDm result = (await conn
                .QueryAsync<PredefinedValueDm, PredefinedCategoryDm, UserDm, PredefinedValueDm>(sql, (pVal, pCat, usr) => {
                    pVal.Category = pCat;
                    pVal.ContributorUser = usr;
                    return pVal;
                }, new { valueId })).FirstOrDefault();
            return result;
        }

        public async Task<PredefinedValueDm[]> GetManyByCategoryNamePaginatedAsync(string categoryName, int valuesToSkip,
            int valuesToFetch)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"
                SELECT * FROM {_predefinedValuesTableName} pVal
                INNER JOIN {_predefinedCategoriesTableName} pCat ON pVal.category_id=pCat.id
                INNER JOIN {_usersTableName} usr ON pVal.contributor_user_id=usr.id
                WHERE pCat.name=@categoryName
                ORDER BY pVal.id
                OFFSET @valuesToSkip ROWS 
                FETCH NEXT @valuesToFetch ROWS ONLY
            ";

            PredefinedValueDm[] result = (await conn
                .QueryAsync<PredefinedValueDm, PredefinedCategoryDm, UserDm, PredefinedValueDm>(sql, (pVal, pCat, usr) => {
                    pVal.Category = pCat;
                    pVal.ContributorUser = usr;
                    return pVal;
                }, new { categoryName, valuesToSkip, valuesToFetch })).ToArray();
            return result;
        }

        public async Task<PredefinedValueDm[]> GetManyByCategoriesIdsAsync(long[] categoriesIds)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"
                SELECT * FROM {_predefinedValuesTableName} pVal
                WHERE pVal.category_id=ANY(@categoriesIds)
            ";

            PredefinedValueDm[] result = (await conn.QueryAsync<PredefinedValueDm>(sql, new { categoriesIds })).ToArray();
            return result;
        }

        public async Task<long> GetValuesCountByCategoryNameAsync(string categoryName)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"
                SELECT COUNT(1) FROM {_predefinedValuesTableName} pVal
                INNER JOIN {_predefinedCategoriesTableName} pCat ON pVal.category_id=pCat.id
                WHERE pCat.name=@categoryName
            ";
            long result = await conn.ExecuteScalarAsync<long>(sql, new { categoryName });
            return result;
        }
    }
}
