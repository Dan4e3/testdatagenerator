﻿using Dapper;
using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Web.DataAccess.Interfaces;
using TestDataGenerator.Web.DataAccess.Interfaces.Repositories;
using TestDataGenerator.Web.Models.DomainModels;

namespace TestDataGenerator.Web.DataAccess.PgSql.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly IDbConnectionFactory _connFactory;
        private readonly string _usersTableName;

        public UserRepository(
            IDbConnectionFactory connFactory,
            IDapperCustomSettings dapperCustomSettings)
        {
            _connFactory = connFactory;
            _usersTableName = dapperCustomSettings.GetTableNameFromAttr(typeof(UserDm));
        }

        public async Task<long> CreateSingleAsync(UserDm model)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            //int result = await conn.InsertAsync(model);
            string sql = $@"
                INSERT INTO {_usersTableName} (name, password, access_token, registration_date) 
                VALUES (@Name, @Password, @AccessToken, @RegistrationDate)
                RETURNING id
            ";
            long insertedId = await conn.ExecuteScalarAsync<long>(sql, model);
            model.Id = insertedId;
            return insertedId;
        }

        public async Task<bool> UpdateSingleAsync(UserDm model)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            //bool result = await conn.UpdateAsync(model);
            string sql = $@"
                UPDATE {_usersTableName} SET 
                    name=@Name,
                    password=@Password,
                    access_token=@AccessToken,
                    registration_date=@RegistrationDate
                WHERE id=@Id
                RETURNING TRUE
            ";
            bool updateResult = await conn.ExecuteScalarAsync<bool>(sql, model);
            return updateResult;
        }

        public async Task<int> DeleteSingleByIdAsync(long userId)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"DELETE FROM {_usersTableName} WHERE id=@userId";
            int affectedRows = await conn.ExecuteAsync(sql, new { userId });
            return affectedRows;
        }

        public async Task<UserDm> GetSingleByIdAsync(long userId)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"SELECT * FROM {_usersTableName} WHERE id=@userId";
            UserDm result = await conn.QuerySingleOrDefaultAsync<UserDm>(sql, new { userId });
            return result;
        }

        public async Task<UserDm> GetSingleByNameAsync(string userName)
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"SELECT * FROM {_usersTableName} WHERE name=@userName";
            UserDm result = await conn.QuerySingleOrDefaultAsync<UserDm>(sql, new { userName });
            return result;
        }

        public async Task<int> ClearTableAsync()
        {
            using IDbConnection conn = _connFactory.GetConnectionByName();
            conn.Open();
            string sql = $@"DELETE FROM {_usersTableName}";
            int affectedRows = await conn.ExecuteAsync(sql);
            return affectedRows;
        }
    }
}
