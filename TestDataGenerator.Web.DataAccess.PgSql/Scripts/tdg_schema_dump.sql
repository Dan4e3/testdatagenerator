--
-- PostgreSQL database dump
--

-- Dumped from database version 13.4
-- Dumped by pg_dump version 13.4

-- Started on 2022-11-13 18:28:08

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 4 (class 2615 OID 18301)
-- Name: tdg; Type: SCHEMA; Schema: -; Owner: tdg_admin
--

CREATE SCHEMA tdg;


ALTER SCHEMA tdg OWNER TO tdg_admin;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 204 (class 1259 OID 18316)
-- Name: column_sets; Type: TABLE; Schema: tdg; Owner: tdg_admin
--

CREATE TABLE tdg.column_sets (
    id bigint NOT NULL,
    name character varying(256),
    column_set_json text NOT NULL,
    additional_column_params_json text,
    owner_user_id bigint NOT NULL,
    creation_date timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE tdg.column_sets OWNER TO tdg_admin;

--
-- TOC entry 203 (class 1259 OID 18314)
-- Name: column_sets_id_seq; Type: SEQUENCE; Schema: tdg; Owner: tdg_admin
--

ALTER TABLE tdg.column_sets ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME tdg.column_sets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 210 (class 1259 OID 18368)
-- Name: dataset_generation_requests; Type: TABLE; Schema: tdg; Owner: tdg_admin
--

CREATE TABLE tdg.dataset_generation_requests (
    id bigint NOT NULL,
    requesting_user_id bigint NOT NULL,
    column_set_id bigint,
    rows_to_generate integer NOT NULL,
    rows_json text,
    file_contents bytea,
    file_extension character varying(10),
    creation_date timestamp without time zone DEFAULT now() NOT NULL,
    worker_response_received_date timestamp without time zone,
    error_text text,
    system_exception_text text,
    fetched_at_least_once boolean NOT NULL,
    worker_response_received boolean NOT NULL
);


ALTER TABLE tdg.dataset_generation_requests OWNER TO tdg_admin;

--
-- TOC entry 209 (class 1259 OID 18366)
-- Name: dataset_generation_requests_id_seq; Type: SEQUENCE; Schema: tdg; Owner: tdg_admin
--

ALTER TABLE tdg.dataset_generation_requests ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME tdg.dataset_generation_requests_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 206 (class 1259 OID 18334)
-- Name: predefined_categories; Type: TABLE; Schema: tdg; Owner: tdg_admin
--

CREATE TABLE tdg.predefined_categories (
    id bigint NOT NULL,
    name character varying(200) NOT NULL,
    contributor_user_id bigint NOT NULL,
    creation_date timestamp without time zone DEFAULT now() NOT NULL,
    is_public boolean NOT NULL
);


ALTER TABLE tdg.predefined_categories OWNER TO tdg_admin;

--
-- TOC entry 205 (class 1259 OID 18332)
-- Name: predefined_categories_id_seq; Type: SEQUENCE; Schema: tdg; Owner: tdg_admin
--

ALTER TABLE tdg.predefined_categories ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME tdg.predefined_categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 208 (class 1259 OID 18349)
-- Name: predefined_values; Type: TABLE; Schema: tdg; Owner: tdg_admin
--

CREATE TABLE tdg.predefined_values (
    id bigint NOT NULL,
    value character varying(300) NOT NULL,
    data_type smallint NOT NULL,
    category_id bigint NOT NULL,
    contributor_user_id bigint NOT NULL,
    creation_date timestamp without time zone NOT NULL
);


ALTER TABLE tdg.predefined_values OWNER TO tdg_admin;

--
-- TOC entry 207 (class 1259 OID 18347)
-- Name: predefined_values_id_seq; Type: SEQUENCE; Schema: tdg; Owner: tdg_admin
--

ALTER TABLE tdg.predefined_values ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME tdg.predefined_values_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 202 (class 1259 OID 18304)
-- Name: users; Type: TABLE; Schema: tdg; Owner: tdg_admin
--

CREATE TABLE tdg.users (
    id bigint NOT NULL,
    name character varying(150) NOT NULL,
    password character varying(256) NOT NULL,
    access_token character varying(256),
    registration_date timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE tdg.users OWNER TO tdg_admin;

--
-- TOC entry 201 (class 1259 OID 18302)
-- Name: users_id_seq; Type: SEQUENCE; Schema: tdg; Owner: tdg_admin
--

ALTER TABLE tdg.users ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME tdg.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 2886 (class 2606 OID 18324)
-- Name: column_sets column_sets_pkey; Type: CONSTRAINT; Schema: tdg; Owner: tdg_admin
--

ALTER TABLE ONLY tdg.column_sets
    ADD CONSTRAINT column_sets_pkey PRIMARY KEY (id);


--
-- TOC entry 2899 (class 2606 OID 18376)
-- Name: dataset_generation_requests dataset_generation_requests_pkey; Type: CONSTRAINT; Schema: tdg; Owner: tdg_admin
--

ALTER TABLE ONLY tdg.dataset_generation_requests
    ADD CONSTRAINT dataset_generation_requests_pkey PRIMARY KEY (id);


--
-- TOC entry 2892 (class 2606 OID 18339)
-- Name: predefined_categories predefined_categories_pkey; Type: CONSTRAINT; Schema: tdg; Owner: tdg_admin
--

ALTER TABLE ONLY tdg.predefined_categories
    ADD CONSTRAINT predefined_categories_pkey PRIMARY KEY (id);


--
-- TOC entry 2897 (class 2606 OID 18353)
-- Name: predefined_values predefined_values_pkey; Type: CONSTRAINT; Schema: tdg; Owner: tdg_admin
--

ALTER TABLE ONLY tdg.predefined_values
    ADD CONSTRAINT predefined_values_pkey PRIMARY KEY (id);


--
-- TOC entry 2884 (class 2606 OID 18312)
-- Name: users users_pkey; Type: CONSTRAINT; Schema: tdg; Owner: tdg_admin
--

ALTER TABLE ONLY tdg.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- TOC entry 2887 (class 1259 OID 18330)
-- Name: ix_column_sets_name; Type: INDEX; Schema: tdg; Owner: tdg_admin
--

CREATE INDEX ix_column_sets_name ON tdg.column_sets USING btree (name);


--
-- TOC entry 2888 (class 1259 OID 18331)
-- Name: ix_column_sets_owner_user_id; Type: INDEX; Schema: tdg; Owner: tdg_admin
--

CREATE INDEX ix_column_sets_owner_user_id ON tdg.column_sets USING btree (owner_user_id);


--
-- TOC entry 2900 (class 1259 OID 18382)
-- Name: ix_dataset_generation_requests_requesting_user_id; Type: INDEX; Schema: tdg; Owner: tdg_admin
--

CREATE INDEX ix_dataset_generation_requests_requesting_user_id ON tdg.dataset_generation_requests USING btree (requesting_user_id);


--
-- TOC entry 2889 (class 1259 OID 18346)
-- Name: ix_predefined_categories_contributor_user_id; Type: INDEX; Schema: tdg; Owner: tdg_admin
--

CREATE INDEX ix_predefined_categories_contributor_user_id ON tdg.predefined_categories USING btree (contributor_user_id);


--
-- TOC entry 2890 (class 1259 OID 18386)
-- Name: ix_predefined_categories_name; Type: INDEX; Schema: tdg; Owner: tdg_admin
--

CREATE UNIQUE INDEX ix_predefined_categories_name ON tdg.predefined_categories USING btree (name);


--
-- TOC entry 2893 (class 1259 OID 18364)
-- Name: ix_predefined_values_category_id; Type: INDEX; Schema: tdg; Owner: tdg_admin
--

CREATE INDEX ix_predefined_values_category_id ON tdg.predefined_values USING btree (category_id);


--
-- TOC entry 2894 (class 1259 OID 18365)
-- Name: ix_predefined_values_contributor_user_id; Type: INDEX; Schema: tdg; Owner: tdg_admin
--

CREATE INDEX ix_predefined_values_contributor_user_id ON tdg.predefined_values USING btree (contributor_user_id);


--
-- TOC entry 2895 (class 1259 OID 18387)
-- Name: ix_predefined_values_value_and_data_type; Type: INDEX; Schema: tdg; Owner: tdg_admin
--

CREATE UNIQUE INDEX ix_predefined_values_value_and_data_type ON tdg.predefined_values USING btree (value, data_type);


--
-- TOC entry 2882 (class 1259 OID 18313)
-- Name: ix_users_name; Type: INDEX; Schema: tdg; Owner: tdg_admin
--

CREATE INDEX ix_users_name ON tdg.users USING btree (name);


--
-- TOC entry 2904 (class 2606 OID 18359)
-- Name: predefined_values fk_predefined_categories; Type: FK CONSTRAINT; Schema: tdg; Owner: tdg_admin
--

ALTER TABLE ONLY tdg.predefined_values
    ADD CONSTRAINT fk_predefined_categories FOREIGN KEY (category_id) REFERENCES tdg.predefined_categories(id);


--
-- TOC entry 2901 (class 2606 OID 18325)
-- Name: column_sets fk_users; Type: FK CONSTRAINT; Schema: tdg; Owner: tdg_admin
--

ALTER TABLE ONLY tdg.column_sets
    ADD CONSTRAINT fk_users FOREIGN KEY (owner_user_id) REFERENCES tdg.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2902 (class 2606 OID 18340)
-- Name: predefined_categories fk_users; Type: FK CONSTRAINT; Schema: tdg; Owner: tdg_admin
--

ALTER TABLE ONLY tdg.predefined_categories
    ADD CONSTRAINT fk_users FOREIGN KEY (contributor_user_id) REFERENCES tdg.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2903 (class 2606 OID 18354)
-- Name: predefined_values fk_users; Type: FK CONSTRAINT; Schema: tdg; Owner: tdg_admin
--

ALTER TABLE ONLY tdg.predefined_values
    ADD CONSTRAINT fk_users FOREIGN KEY (contributor_user_id) REFERENCES tdg.users(id);


--
-- TOC entry 2905 (class 2606 OID 18377)
-- Name: dataset_generation_requests fk_users; Type: FK CONSTRAINT; Schema: tdg; Owner: tdg_admin
--

ALTER TABLE ONLY tdg.dataset_generation_requests
    ADD CONSTRAINT fk_users FOREIGN KEY (requesting_user_id) REFERENCES tdg.users(id);


-- Completed on 2022-11-13 18:28:10

--
-- PostgreSQL database dump complete
--

