﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Web.DataAccess.Interfaces.Repositories;
using TestDataGenerator.Web.DataAccess.PgSql.Repositories;

namespace TestDataGenerator.Web.DataAccess.PgSql
{
    public static class RegisterPgSqlExtension
    {
        public static IServiceCollection RegisterPgSqlImplementation (this IServiceCollection services)
        {
            Dapper.DefaultTypeMap.MatchNamesWithUnderscores = true;
            AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);

            services.AddSingleton<IUserRepository, UserRepository>();
            services.AddSingleton<IPredefinedValueRepository, PredefinedValueRepository>();
            services.AddSingleton<IPredefinedCategoryRepository, PredefinedCategoryRepository>();
            services.AddSingleton<IColumnSetRepository, ColumnSetRepository>();
            services.AddSingleton<IDatasetGenerationRequestRepository, DatasetGenerationRequestRepository>();

            return services;
        }
    }
}
