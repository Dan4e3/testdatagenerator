﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using TestDataGenerator.Web.BusinessLogic;
using TestDataGenerator.WebApp.Models.DTO.ValueGenerator;

namespace TestDataGenerator.WebApp.Validation
{
    public static class BoxedObjectValidationHelper
    {
        public static object ConvertGeneratorParamsObjectToConcreteParamsDto(ValueGeneratorDto generatorDto)
        {
            JObject generatorParamsAsJobject = (JObject)generatorDto.GeneratorParameters;
            object result = null;

            switch (generatorDto.GeneratorType)
            {
                case ValueGeneratorTypeEnum.PatternParser:
                    PatternParserGeneratorParametersDto patternParserParams =
                        JsonConvert.DeserializeObject<PatternParserGeneratorParametersDto>(generatorParamsAsJobject.ToString());
                    result = patternParserParams;
                    break;

                case ValueGeneratorTypeEnum.SelectFromList:
                    SelectFromListGeneratorParametersDto selectFromListParams =
                        JsonConvert.DeserializeObject<SelectFromListGeneratorParametersDto>(generatorParamsAsJobject.ToString());
                    result = selectFromListParams;
                    break;

                case ValueGeneratorTypeEnum.ValueRange:
                    ValueRangeGeneratorParametersDto valueRangeParams =
                        JsonConvert.DeserializeObject<ValueRangeGeneratorParametersDto>(generatorParamsAsJobject.ToString());
                    result = valueRangeParams;
                    break;

                case ValueGeneratorTypeEnum.Incremental:
                    IncrementalGeneratorParametersDto incrementalParams =
                        JsonConvert.DeserializeObject<IncrementalGeneratorParametersDto>(generatorParamsAsJobject.ToString());
                    result = incrementalParams;
                    break;

                default:
                    throw new CustomBusinessLogicException($"Unrecognized generator type " +
                        $"provided ({Enum.GetName(typeof(ValueGeneratorTypeEnum), generatorDto.GeneratorType)})!");
            }

            return result;
        }
    }
}
