﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using TestDataGenerator.WebApp.Models.DTO.PredefinedValues;
using TestDataGenerator.WebApp.Models.DTO.ValueGenerator;

namespace TestDataGenerator.WebApp.Swagger
{
    public static class SwaggerSettingsServiceExtension
    {
        public static IServiceCollection RegisterSwaggerGen(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "TestDataGenerator", Version = "v1" });

                // Filters
                c.SchemaFilter<CustomSchemaFilter>();
                c.DocumentFilter<CustomDocumentFilter>();

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);

                // Bearer token authentication
                c.AddSecurityDefinition("jwt_auth", new OpenApiSecurityScheme()
                {
                    Name = "Bearer",
                    BearerFormat = "JWT",
                    Scheme = "bearer",
                    Description = "Specify the authorization token.",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.Http,
                });

                // Make sure swagger UI requires a Bearer token specified
                OpenApiSecurityScheme securityScheme = new OpenApiSecurityScheme()
                {
                    Reference = new OpenApiReference()
                    {
                        Id = "jwt_auth",
                        Type = ReferenceType.SecurityScheme
                    }
                };
                OpenApiSecurityRequirement securityRequirements = new OpenApiSecurityRequirement()
                {
                    {securityScheme, new string[] { }},
                };
                c.AddSecurityRequirement(securityRequirements);

            })
            .AddSwaggerGenNewtonsoftSupport();

            return services;
        }
    }

    public class CustomSchemaFilter : ISchemaFilter
    {
        public void Apply(OpenApiSchema schema, SchemaFilterContext context)
        {
            if (context.Type == typeof(GetPredefinedValueDto))
            {
                schema.Properties[nameof(GetPredefinedValueDto.Value).ToLower()] = new OpenApiSchema() {
                    OneOf = new OpenApiSchema[] {
                        new OpenApiSchema(){
                            Type = "string"
                        },
                        new OpenApiSchema(){
                            Type = "number"
                        },
                        new OpenApiSchema(){
                            Type = "boolean"
                        }
                    },
                    Example = new OpenApiString("string, number or bool")
                };
            }

            if (context.Type == typeof(ValueGeneratorDto))
            {
                string nameOf = nameof(ValueGeneratorDto.GeneratorParameters);
                string targetPropName = char.ToLowerInvariant(nameOf[0]) + nameOf.Substring(1);
                schema.Properties[targetPropName] = new OpenApiSchema() {
                    Type = "object",
                    OneOf = new OpenApiSchema[] {
                        new OpenApiSchema(){
                            Type = "object",
                            Reference = new OpenApiReference() {
                                Type = ReferenceType.Schema,
                                Id = nameof(PatternParserGeneratorParametersDto)
                            }
                        },
                        new OpenApiSchema(){
                            Type = "object",
                            Reference = new OpenApiReference() {
                                Type = ReferenceType.Schema,
                                Id = nameof(SelectFromListGeneratorParametersDto)
                            }
                        },
                        new OpenApiSchema(){
                            Type = "object",
                            Reference = new OpenApiReference() {
                                Type = ReferenceType.Schema,
                                Id = nameof(ValueRangeGeneratorParametersDto)
                            }
                        },
                        new OpenApiSchema(){
                            Type = "object",
                            Reference = new OpenApiReference() {
                                Type = ReferenceType.Schema,
                                Id = nameof(IncrementalGeneratorParametersDto)
                            }
                        },
                    },
                    Example = new OpenApiObject(),
                };
            }
        }
    }

    public class CustomDocumentFilter : IDocumentFilter
    {
        public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
        {
            context.SchemaGenerator.GenerateSchema(typeof(PatternParserGeneratorParametersDto), context.SchemaRepository);
            context.SchemaGenerator.GenerateSchema(typeof(SelectFromListGeneratorParametersDto), context.SchemaRepository);
            context.SchemaGenerator.GenerateSchema(typeof(ValueRangeGeneratorParametersDto), context.SchemaRepository);
            context.SchemaGenerator.GenerateSchema(typeof(IncrementalGeneratorParametersDto), context.SchemaRepository);
        }
    }
}
