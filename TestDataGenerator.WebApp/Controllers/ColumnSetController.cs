﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestDataGenerator.Core.Columns;
using TestDataGenerator.Web.BusinessLogic;
using TestDataGenerator.Web.BusinessLogic.Interfaces;
using TestDataGenerator.Web.Models.DomainModels;
using TestDataGenerator.WebApp.Models.ActionResults;
using TestDataGenerator.WebApp.Models.DTO.ColumnSet;
using TestDataGenerator.WebApp.Models.DTO.ValueGenerator;
using TestDataGenerator.WebApp.Validation;

namespace TestDataGenerator.WebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ColumnSetController : ControllerBase
    {
        private readonly IColumnSetService _colSetService;
        private readonly IMapper _mapper;
        private readonly ILogger<ColumnSetController> _logger;

        public ColumnSetController(
            IColumnSetService colSetService,
            IMapper mapper,
            ILogger<ColumnSetController> logger)
        {
            _colSetService = colSetService;
            _mapper = mapper;
            _logger = logger;
        }

        /// <summary>
        /// Get single column set owned by logged user.
        /// </summary>
        /// <param name="columnSetId">Numeric identifier of column set to get.</param>
        /// <returns></returns>
        [HttpGet]
        [Route("get")]
        public async Task<ActionResult<GetColumnSetResult>> GetColumnSet(long columnSetId)
        {
            _logger.LogInformation("Received get column set request. User={userName}. ColumnSetId={columnSetId}.",
                User.Identity.Name, columnSetId);
            GetColumnSetResult result = new GetColumnSetResult();

            try
            {
                ColumnSetDm colsetDm = await _colSetService.GetColumnSetFromDataBaseAsync(columnSetId, HttpContext.User.Identity.Name);
                _logger.LogInformation("Column set obtained from database. User={userName}. ColumnSetId={columnSetId}.",
                    User.Identity.Name, columnSetId);
                GetColumnSetDto colsetDto = _mapper.Map<GetColumnSetDto>(colsetDm);
                result.ColumnSet = colsetDto;

                _logger.LogInformation("Column set mapped to DTO and will be sent to client. User={userName}. ColumnSetId={columnSetId}.",
                    User.Identity.Name, columnSetId);
                return Ok(result);
            }
            catch(CustomBusinessLogicException ex)
            {
                _logger.LogInformation("Handled exception occurred. User={userName}. ColumnSetId={columnSetId}. Exception message={exMessage}.",
                    User.Identity.Name, columnSetId, ex.Message);
                result.ResultMessage = ex.Message;
                return BadRequest(result);
            }
        }

        /// <summary>
        /// Get all column sets owned by logged user.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("get-all")]
        public async Task<ActionResult<GetAllColumnSetsResult>> GetAllColumnSetsBelongingToUser()
        {
            _logger.LogInformation("Received get all column sets request. User={userName}.",
                User.Identity.Name);
            GetAllColumnSetsResult result = new GetAllColumnSetsResult();

            try
            {
                ColumnSetDm[] colsetDms = await _colSetService.GetAllColumnSetsOfUser(HttpContext.User.Identity.Name);
                _logger.LogInformation("Obtained {colsetCount} column sets from database. User={userName}.",
                    colsetDms.Length, User.Identity.Name);
                GetColumnSetDto[] colsetDtos = _mapper.Map<GetColumnSetDto[]>(colsetDms);

                _logger.LogInformation("Column sets are mapped to DTOs and will be sent to client. User={userName}.",User.Identity.Name);
                result.ColumnSets = colsetDtos;
                return Ok(result);
            }
            catch(CustomBusinessLogicException ex)
            {
                _logger.LogInformation("Handled exception occurred. User={userName}. Exception message={exMessage}.",
                    User.Identity.Name, ex.Message);
                result.ResultMessage = ex.Message;
                return BadRequest(result);
            }
        }

        /// <summary>
        /// Delete column set from logged user's column set list.
        /// </summary>
        /// <param name="colsetToDelete">Column set to be deleted.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("delete")]
        public async Task<ActionResult<DeleteColumnSetResult>> DeleteColumnSet(DeleteColumnSetDto colsetToDelete)
        {
            _logger.LogInformation("Received delete column set request. User={userName}. ColumnSetId={columnSetId}.",
                User.Identity.Name, colsetToDelete.ColumnSetId);
            DeleteColumnSetResult result = new DeleteColumnSetResult();

            try
            {
                int deletedRowsCount = await _colSetService.RemoveColumnSetFromDatabaseAsync(colsetToDelete.ColumnSetId, HttpContext.User.Identity.Name);
                _logger.LogInformation("Deleted {deletedRowsCount} column sets. User={userName}. ColumnSetId={columnSetId}.",
                    deletedRowsCount, User.Identity.Name, colsetToDelete.ColumnSetId);

                result.DeletedColumnSetsCount = deletedRowsCount;
                return Ok(result);
            }
            catch(CustomBusinessLogicException ex)
            {
                _logger.LogInformation("Handled exception occurred. User={userName}. Exception message={exMessage}. ColumnSetId={columnSetId}.",
                    User.Identity.Name, ex.Message, colsetToDelete.ColumnSetId);
                result.ResultMessage = ex.Message;
                return BadRequest(result);
            }
        }

        /// <summary>
        /// Create and save new column set for the logged user.
        /// </summary>
        /// <param name="columnSetDto">Column set to save.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("create")]
        public async Task<ActionResult<AddColumnSetResult>> CreateNewColumnSet(CreateNewColumnSetDto columnSetDto)
        {
            _logger.LogInformation("Received create new column set request. User={userName}. ColumnSetName={columnSetName}.",
                User.Identity.Name, columnSetDto.ColumnSetName);

            bool anyOfGeneratorParamsIsInvalid = false;
            foreach (ColumnDto colDto in columnSetDto.Columns)
            {
                object valueGeneratorParams = BoxedObjectValidationHelper.ConvertGeneratorParamsObjectToConcreteParamsDto(colDto.ColumnSettings.ValueSupplier);
                if (!TryValidateModel(valueGeneratorParams))
                    anyOfGeneratorParamsIsInvalid = true;
            }
            
            if (anyOfGeneratorParamsIsInvalid)
            {
                _logger.LogInformation("Validation failed for new column set creation model. User={userName}. ColumnSetName={columnSetName}.",
                    User.Identity.Name, columnSetDto.ColumnSetName);
                return BadRequest(ModelState);
            }

            AddColumnSetResult result = new AddColumnSetResult();
            try
            {
                ColumnSetDm colsetDm = _mapper.Map<ColumnSetDm>(columnSetDto);
                _logger.LogInformation("Mapped create column set DTO to domain model. User={userName}. ColumnSetName={columnSetName}.",
                    User.Identity.Name, columnSetDto.ColumnSetName);
                await _colSetService.SaveColumnSetInDataBaseAsync(colsetDm, HttpContext.User.Identity.Name);
                _logger.LogInformation("Saved new column set in database Id={columnSetId}. Returning result to client." +
                    " User={userName}. ColumnSetName={columnSetName}.", colsetDm.Id, User.Identity.Name, columnSetDto.ColumnSetName);

                result.ColumnSetId = colsetDm.Id;
                return Ok(result);
            }
            catch(CustomBusinessLogicException ex)
            {
                _logger.LogInformation("Handled exception occurred. User={userName}. Exception message={exMessage}. ColumnSetName={columnSetName}.",
                    User.Identity.Name, ex.Message, columnSetDto.ColumnSetName);
                result.ResultMessage = ex.Message;
                return BadRequest(result);
            }
        }

        /// <summary>
        /// Update specific column set parameters.
        /// </summary>
        /// <param name="columnSetDto">Values to update column set with.</param>
        /// <returns></returns>
        [HttpPut]
        [Route("update")]
        public async Task<ActionResult<UpdateColumnSetResult>> UpdateColumnSet(UpdateColumnSetDto columnSetDto)
        {
            _logger.LogInformation("Received update column set request. User={userName}. ColumnSetId={columnSetId}.",
                User.Identity.Name, columnSetDto.Id);
            UpdateColumnSetResult result = new UpdateColumnSetResult();

            try
            {
                ColumnSetDm columnSetDm = _mapper.Map<ColumnSetDm>(columnSetDto);
                result.ColumnSetId = columnSetDto.Id;
                _logger.LogInformation("Mapped update column set DTO to domain model. User={userName}. ColumnSetId={columnSetId}.",
                    User.Identity.Name, columnSetDto.Id);

                bool wasUpdated = await _colSetService.UpdateColumnSetAsync(columnSetDm, HttpContext.User.Identity.Name);

                if (wasUpdated)
                {
                    _logger.LogInformation("Column set was successfully updated. Returning result to client. " +
                        "User={userName}. ColumnSetId={columnSetId}.", User.Identity.Name, columnSetDto.Id);
                    result.UpdatedColumns = columnSetDto.Columns;
                    return Ok(result);
                }
                else
                {
                    _logger.LogWarning("Column set was NOT successfully updated. Returning result to client. " +
                        "User={userName}. ColumnSetId={columnSetId}.", User.Identity.Name, columnSetDto.Id);
                    result.ResultMessage = "Failed to update column set.";
                    return BadRequest(result);
                }
            }
            catch(CustomBusinessLogicException ex)
            {
                _logger.LogInformation("Handled exception occurred. User={userName}. Exception message={exMessage}. ColumnSetId={columnSetId}.",
                    User.Identity.Name, ex.Message, columnSetDto.Id);
                result.ResultMessage = ex.Message;
                return BadRequest(result);
            }
        }

        /// <summary>
        /// Perform column setup validation on a single column.
        /// </summary>
        /// <param name="columnToValidate">Column to validate.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("validate-single-column")]
        public ActionResult<SingleColumnValidationResult> ValidateSingleColumn(ColumnDto columnToValidate)
        {
            _logger.LogInformation("Received validate single column request. User={userName}. ColumnName={columnName}.",
                User.Identity.Name, columnToValidate.ColumnSettings.ColumnName);
            SingleColumnValidationResult result = new SingleColumnValidationResult();
            result.CheckedColumn = columnToValidate;

            try
            {
                Column column = _mapper.Map<Column>(columnToValidate);
                _logger.LogInformation("Mapped column DTO to column core model. User={userName}. ColumnName={columnName}.",
                    User.Identity.Name, columnToValidate.ColumnSettings.ColumnName);

                if (!column.IsValid(out string validationError))
                {
                    // additional custom validation for SelectFromList generator type
                    if (columnToValidate.ColumnSettings.ValueSupplier.GeneratorType == ValueGeneratorTypeEnum.SelectFromList)
                    {
                        string paramsAsString = ((JObject)columnToValidate.ColumnSettings.ValueSupplier.GeneratorParameters).ToString();
                        SelectFromListGeneratorParametersDto generatorParams =
                            JsonConvert.DeserializeObject<SelectFromListGeneratorParametersDto>(paramsAsString);
                        if (generatorParams.PredefinedCategoriesIds == null || generatorParams.PredefinedCategoriesIds.Length == 0)
                        {
                            result.IsValid = false;
                            result.ValidationError = validationError;
                        }
                        else
                            result.IsValid = true;
                    }
                    //
                    else
                    {
                        result.IsValid = false;
                        result.ValidationError = validationError;
                    }
                }
                else
                {
                    try
                    {
                        column.GenerateValue();
                        result.IsValid = true;
                    }
                    catch
                    {
                        result.ValidationError = "Please check target value type with input value compatibility.";
                        result.IsValid = false;
                    }
                }

                _logger.LogInformation("Column validation procedure was successful. User={userName}. ColumnName={columnName}.",
                    User.Identity.Name, columnToValidate.ColumnSettings.ColumnName);

                return Ok(result);
            }
            catch (AutoMapperMappingException mappingEx)
            {
                if (mappingEx.InnerException is CustomBusinessLogicException blEx)
                {
                    _logger.LogInformation("Handled exception occurred. User={userName}. Exception message={exMessage}. ColumnName={columnName}",
                        User.Identity.Name, blEx?.Message, columnToValidate.ColumnSettings.ColumnName);
                    result.ResultMessage = blEx?.Message;
                }
                else
                {
                    _logger.LogWarning("Mapping exception was thrown during column validation. " +
                        "User={userName}. Exception message={exMessage}. ColumnName={columnName}.",
                        User.Identity.Name, mappingEx.Message, columnToValidate.ColumnSettings.ColumnName);
                }
                return BadRequest(result);
            }
        }
    }
}
