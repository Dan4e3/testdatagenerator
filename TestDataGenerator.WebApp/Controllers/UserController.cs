﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using TestDataGenerator.Web.BusinessLogic;
using TestDataGenerator.Web.BusinessLogic.Interfaces;
using TestDataGenerator.Web.BusinessLogic.Services;
using TestDataGenerator.Web.Models.DomainModels;
using TestDataGenerator.WebApp.Authorization;
using TestDataGenerator.WebApp.Models.ActionResults;
using TestDataGenerator.WebApp.Models.DTO.User;

namespace TestDataGenerator.WebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserHandlingService _userService;
        private readonly IMapper _mapper;
        private readonly IRecaptchaService _recaptchaService;
        private readonly ILogger<UserController> _logger;

        public UserController(
            IUserHandlingService userService,
            IMapper mapper,
            IRecaptchaService recaptchaService,
            ILogger<UserController> logger)
        {
            _userService = userService;
            _mapper = mapper;
            _recaptchaService = recaptchaService;
            _logger = logger;
        }

        [HttpPost]
        [Route("login")]
        public async Task<ActionResult<UserLoginResult>> Login(UserLoginDto userLoginDto)
        {
            _logger.LogInformation("Received login request. User={userName}.", userLoginDto.Name);
            UserLoginResult result = new UserLoginResult() {
                Name = userLoginDto.Name
            };

            if (HttpContext.User.Identity.IsAuthenticated)
            {
                _logger.LogInformation("User was already authenticated. User={userName}.", userLoginDto.Name);
                return Ok(result);
            }
               

            bool credentialsCorrect = await _userService.UserCredentialsAreCorrectAsync(userLoginDto.Name, userLoginDto.Password);

            if (credentialsCorrect)
            {
                _logger.LogInformation("User credentials correct. Authenticating... User={userName}.", userLoginDto.Name);
                await AuthenticateWithCookies(userLoginDto.Name);
                return Ok(result);
            }

            _logger.LogInformation("User provided incorrect credentials. Authentication rejected. User={userName}.", userLoginDto.Name);
            result.ResultMessage = "Invalid username or password provided.";
            return Unauthorized(result);
        }

        [HttpGet]
        [Authorize]
        [Route("logout")]
        public async Task<IActionResult> Logout()
        {
            _logger.LogInformation("Received logout request. User={userName}.", User.Identity.Name);
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

            return Ok();
        }

        [HttpPost]
        [Route("register")]
        public async Task<ActionResult<AddNewUserResult>> Register(UserRegistrationDto userRegistrationDto)
        {
            _logger.LogInformation("Received new user registration request. User={userName}.", userRegistrationDto.Name);
            AddNewUserResult result = new AddNewUserResult();

            try
            {
                bool captchaPassed = await _recaptchaService.IsCaptchaPassedAsync(userRegistrationDto.CaptchaToken);
                if (!captchaPassed)
                {
                    _logger.LogInformation("Captcha validation failed. User={userName}.", userRegistrationDto.Name);
                    result.ResultMessage = "Captcha validation has failed. Try again!";
                    return BadRequest(result);
                }

                _logger.LogInformation("Captcha validation succeeded. Mapping input DTO to domain user model. " +
                    "User={userName}.", userRegistrationDto.Name);
                UserDm userToRegister = _mapper.Map<UserDm>(userRegistrationDto);
                UserDm registeredUser = await _userService.RegisterUserAsync(userToRegister);
                _logger.LogInformation("New user successfully registered. Returning result to client. " +
                    "User={userName}.", userRegistrationDto.Name);


                return Ok(result);

            }
            catch (CustomBusinessLogicException ex)
            {
                result.ResultMessage = ex.Message;
                return BadRequest(result);
            }
        }

        [HttpPost]
        [Route("get-access-token")]
        public async Task<ActionResult<UserLoginResult>> GetJwtToken(UserLoginDto userLoginDto)
        {
            _logger.LogInformation("Received new JWT token request for user. User={userName}.", userLoginDto.Name);
            UserLoginResult result = new UserLoginResult() {
                Name = userLoginDto.Name
            };

            bool credentialsCorrect = await _userService.UserCredentialsAreCorrectAsync(userLoginDto.Name, userLoginDto.Password);
            if (!credentialsCorrect)
            {
                result.ResultMessage = "Invalid username or password provided.";
                return Unauthorized(result);
            }

            string encodedJwt = GenerateJwtAsString(userLoginDto.Name);
            _logger.LogInformation("Credentials are correct, JWT generated. Sending result to client. User={userName}.", userLoginDto.Name);

            result.JwtToken = encodedJwt;
            return Ok(result);
        }

        [HttpGet]
        [Authorize]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("ping")]
        public IActionResult PingAuthTest()
        {
            return Ok();
        }

        private async Task AuthenticateWithCookies(string userName)
        {
            List<Claim> claims = new List<Claim>(){
                new Claim(ClaimsIdentity.DefaultNameClaimType, userName)
            };
            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
        }

        private string GenerateJwtAsString(string userName)
        {
            List<Claim> claims = new List<Claim> { new Claim(ClaimTypes.Name, userName) };

            JwtSecurityToken jwt = new JwtSecurityToken(
                    issuer: JwtAuthOptions.ISSUER,
                    audience: JwtAuthOptions.AUDIENCE,
                    claims: claims,
                    expires: DateTime.UtcNow.Add(JwtAuthOptions.DefaultTokenLifespan),
                    signingCredentials: new SigningCredentials(JwtAuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));

            string encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            return encodedJwt;
        }
    }
}
