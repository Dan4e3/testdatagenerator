﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using TestDataGenerator.Web.BusinessLogic;
using TestDataGenerator.Web.BusinessLogic.Interfaces;
using TestDataGenerator.Web.BusinessLogic.Services;
using TestDataGenerator.Web.Models.DomainModels;
using TestDataGenerator.WebApp.Models.ActionResults;
using TestDataGenerator.WebApp.Models.DTO.PredefinedCategories;
using TestDataGenerator.WebApp.Models.DTO.PredefinedValues;

namespace TestDataGenerator.WebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class PredefinedDataController : ControllerBase
    {
        private readonly ILogger<PredefinedDataController> _logger;
        private readonly IPredefinedValuesService _predefinedValuesService;
        private readonly IPredefinedCategoriesService _predefinedCategoriesService;
        private readonly IMapper _mapper;

        public PredefinedDataController(ILogger<PredefinedDataController> logger,
            IMapper mapper,
            IPredefinedValuesService predefinedValuesService,
            IPredefinedCategoriesService predefinedCategoriesService)
        {
            _mapper = mapper;
            _logger = logger;
            _predefinedValuesService = predefinedValuesService;
            _predefinedCategoriesService = predefinedCategoriesService;
        }

        /// <summary>
        /// Get list of predefined values by category name.
        /// </summary>
        /// <param name="categoryName">Name of category to take values from.</param>
        /// <param name="valuesToFetch">Amount of values to fetch. If not specified method returns all values.</param>
        /// <returns></returns>
        [HttpGet]
        [Route("values/get")]
        public async Task<ActionResult<GetPredefinedValuesResult>> GetPredefinedValues([Required]string categoryName, int? valuesToFetch = 100)
        {
            _logger.LogInformation("Received get values request. User={userName}. CategoryName={categoryName}. ValuesToFetch={valuesToFetch}.",
                User.Identity.Name, categoryName, valuesToFetch);
            GetPredefinedValuesResult result = new GetPredefinedValuesResult();

            try
            {
                PredefinedValueDm[] predefinedValuesDm = await _predefinedValuesService.GetPredefinedValuesAsync(categoryName,
                    HttpContext.User.Identity.Name, valuesToFetch.Value);
                _logger.LogInformation("Retrieved predefined values from database. User={userName}. " +
                    "CategoryName={categoryName}. ValuesToFetch={valuesToFetch}.", User.Identity.Name, categoryName, valuesToFetch);

                GetPredefinedValuesCollectionDto valuesCollectionDto = new GetPredefinedValuesCollectionDto()
                {
                    CategoryName = categoryName,
                    Values = _mapper.Map<GetPredefinedValueDto[]>(predefinedValuesDm)
                };
                _logger.LogInformation("Mapped domain predefined values to DTO. Sending result to client. User={userName}. " +
                    "CategoryName={categoryName}. ValuesToFetch={valuesToFetch}.", User.Identity.Name, categoryName, valuesToFetch);

                result.Data = valuesCollectionDto;
                return Ok(result);
            }
            catch (CustomBusinessLogicException ex)
            {
                _logger.LogInformation("Handled exception occurred." +
                    " User={userName}. CategoryName={categoryName}. ValuesToFetch={valuesToFetch}. Exception message={exMessage}.",
                    User.Identity.Name, categoryName, valuesToFetch, ex.Message);
                result.ResultMessage = ex.Message;
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Get paginated portion of prdefined values. Should be used with large values lists 
        /// for reduced resources usage.
        /// </summary>
        /// <param name="categoryName">Category name to get values from.</param>
        /// <param name="pageNumber">Page number to fetch.</param>
        /// <param name="valuesPerPage">Amount of values per page (request).</param>
        /// <returns></returns>
        [HttpGet]
        [Route("values/get-paginated")]
        public async Task<ActionResult<GetPrdefinedValuesPaginatedResult>> GetPredefinedValuesPaginated([Required] string categoryName, 
            [Required, Range(0, int.MaxValue)] int pageNumber, [Required, Range(1, 10000)] int valuesPerPage)
        {
            _logger.LogInformation("Received get values paginated request. " +
                "User={userName}. PageNumber={pageNumber}. ValuesPerPage={valuesPerPage}.",
                User.Identity.Name, pageNumber, valuesPerPage);
            GetPrdefinedValuesPaginatedResult result = new GetPrdefinedValuesPaginatedResult();

            try
            {
                PredefinedValueDm[] predefinedValuesDm = await _predefinedValuesService.GetPredefinedValuesPaginatedAsync(categoryName, 
                    HttpContext.User.Identity.Name, pageNumber, valuesPerPage);
                _logger.LogInformation("Retrieved predefined values from database. " +
                    "User={userName}. PageNumber={pageNumber}. ValuesPerPage={valuesPerPage}.",
                    User.Identity.Name, pageNumber, valuesPerPage);
                long totalValuesCount = await _predefinedValuesService.GetPredefinedValuesCountInCategoryAsync(categoryName,
                    HttpContext.User.Identity.Name);
                _logger.LogInformation("Received total count of values in category={valuesTotalCount}. " +
                    "User={userName}. PageNumber={pageNumber}. ValuesPerPage={valuesPerPage}.",
                    totalValuesCount, User.Identity.Name, pageNumber, valuesPerPage);

                GetPredefinedValuesCollectionDto valuesCollectionDto = new GetPredefinedValuesCollectionDto()
                {
                    CategoryName = categoryName,
                    Values = _mapper.Map<GetPredefinedValueDto[]>(predefinedValuesDm)
                };
                _logger.LogInformation("Mapped domain models to DTO. Sending result to client. " +
                    "User={userName}. PageNumber={pageNumber}. ValuesPerPage={valuesPerPage}.",
                    User.Identity.Name, pageNumber, valuesPerPage);

                result.ValuesPerPage = valuesPerPage;
                result.ValuesCollection = valuesCollectionDto;
                result.PageNumber = pageNumber;
                result.TotalValuesCount = totalValuesCount;

                return Ok(result);
            }
            catch (CustomBusinessLogicException ex)
            {
                _logger.LogInformation("Handled exception occurred." +
                    " User={userName}. PageNumber={pageNumber}. ValuesPerPage={valuesPerPage}. Exception message={exMessage}.",
                    User.Identity.Name, pageNumber, valuesPerPage, ex.Message);
                result.ResultMessage = ex.Message;
                return BadRequest(result);
            }
        }

        /// <summary>
        /// Add many predefined values to specified category at once.
        /// </summary>
        /// <param name="dataToAdd">Values to add.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("values/add-many")]
        public async Task<ActionResult<AddPredefinedValuesResult>> AddNewMultipleValues(AddPredefinedValuesCollectionDto dataToAdd)
        {
            _logger.LogInformation("Received add many predefined values request. " +
                "User={userName}. CategoryName={categoryName}.",
                User.Identity.Name, dataToAdd.CategoryName);
            AddPredefinedValuesResult result = new AddPredefinedValuesResult();

            try
            {
                PredefinedValueDm[] valuesDm = _mapper.Map<PredefinedValueDm[]>(dataToAdd.Values);
                _logger.LogInformation("Mapped input values DTO to domain models. " +
                    "User={userName}. CategoryName={categoryName}.",
                    User.Identity.Name, dataToAdd.CategoryName);
                int insertedRows = await _predefinedValuesService.AddPredefinedValuesAsync(valuesDm, dataToAdd.CategoryName,
                    HttpContext.User.Identity.Name);
                _logger.LogInformation("Inserted {insertedValuesCount} rows into predefined values table. Sending result to client. " +
                    "User={userName}. CategoryName={categoryName}.",
                    insertedRows, User.Identity.Name, dataToAdd.CategoryName);

                result.ValuesAdded = insertedRows;
                return Ok(result);
            }
            catch (CustomBusinessLogicException ex)
            {
                _logger.LogInformation("Handled exception occurred." +
                    " User={userName}. CategoryName={categoryName}. Exception message={exMessage}.",
                    User.Identity.Name, dataToAdd.CategoryName, ex.Message);
                result.ResultMessage = ex.Message;
                result.ValuesAdded = 0;
                return BadRequest(result);
            }
        }

        /// <summary>
        /// Add single predefined value to specified category.
        /// </summary>
        /// <param name="valueToAdd">Value to add.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("values/add")]
        public async Task<ActionResult<AddSinglePredefinedValueResult>> AddNewValue(AddSinglePredefinedValueDto valueToAdd)
        {
            _logger.LogInformation("Received add single predefined value request. " +
                "User={userName}. CategoryName={categoryName}.", User.Identity.Name, valueToAdd.CategoryName);
            AddSinglePredefinedValueResult result = new AddSinglePredefinedValueResult();

            try
            {
                PredefinedValueDm valueToAddDm = _mapper.Map<PredefinedValueDm>(valueToAdd);
                _logger.LogInformation("Mapped input value DTO to domain model. " +
                    "User={userName}. CategoryName={categoryName}.",
                    User.Identity.Name, valueToAdd.CategoryName);
                PredefinedValueDm insertedValue = await _predefinedValuesService.AddPredefinedValueAsync(valueToAddDm, HttpContext.User.Identity.Name);
                _logger.LogInformation("Inserted new predefined value with Id={predefinedValueId} into datatable.. " +
                    "User={userName}. CategoryName={categoryName}.",
                    insertedValue.Id, User.Identity.Name, valueToAdd.CategoryName);
                GetPredefinedValueDto insertedValueDto = _mapper.Map<GetPredefinedValueDto>(insertedValue);
                _logger.LogInformation("Mapped inserted value domain model to output DTO. Sending result to client. " +
                    "User={userName}. CategoryName={categoryName}.",
                    User.Identity.Name, valueToAdd.CategoryName);

                result.CreatedValue = insertedValueDto;

                return Ok(result);
            }
            catch (CustomBusinessLogicException ex)
            {
                _logger.LogInformation("Handled exception occurred." +
                    " User={userName}. CategoryName={categoryName}. Exception message={exMessage}.",
                    User.Identity.Name, valueToAdd.CategoryName, ex.Message);
                result.ResultMessage = ex.Message;
                return BadRequest(result);
            }
        }

        /// <summary>
        /// Delete predefined values from database.
        /// </summary>
        /// <param name="dataToDelete">Values to delete.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("values/delete")]
        public async Task<ActionResult<DeletePredefinedValuesResult>> DeleteValues(DeletePredefinedValuesDto dataToDelete)
        {
            _logger.LogInformation("Received delete prdefined values request. User={userName}.", User.Identity.Name);
            int deletedRows = await _predefinedValuesService.DeletePredefinedValuesAsync(dataToDelete.ValuesIds);
            _logger.LogInformation("Deleted {deletedRowsCount} rows from predefined values table. User={userName}.",
                deletedRows, User.Identity.Name);

            return Ok(new DeletePredefinedValuesResult() { 
                DeletedValuesCount = deletedRows
            });
        }

        /// <summary>
        /// Get predefined categories list.
        /// </summary>
        /// <param name="includePublic">Include publicly visible categories or not.</param>
        /// <returns></returns>
        [HttpGet]
        [Route("categories/get")]
        public async Task<ActionResult<GetPredefinedCategoriesResult>> GetCategories(bool includePublic = true)
        {
            _logger.LogInformation("Received get many categories request. " +
                "User={userName}. IncludePublic={includePublic}.", User.Identity.Name, includePublic);
            GetPredefinedCategoriesResult result = new GetPredefinedCategoriesResult();

            try
            {
                string requestorUserName = HttpContext.User.Identity.Name;
                PredefinedCategoryDm[] categoriesDm = 
                    await _predefinedCategoriesService.GetPredefinedCategoriesFromDatabaseAsync(requestorUserName, includePublic);
                _logger.LogInformation("Retrieved {predefinedCategoryCount} categories from database. " +
                    "User={userName}. IncludePublic={includePublic}.", categoriesDm.Length, User.Identity.Name, includePublic);
                PredefinedCategoriesCollectionDto categoriesCollectionDto = new PredefinedCategoriesCollectionDto()
                {
                    Categories = _mapper.Map<GetPredefinedCategoryDto[]>(categoriesDm)
                };
                _logger.LogInformation("Mapped predefined category domain models to DTO. Sending result to client. " +
                    "User={userName}. IncludePublic={includePublic}.", User.Identity.Name, includePublic);

                result.Data = categoriesCollectionDto;
                return Ok(result);
            }
            catch (CustomBusinessLogicException ex)
            {
                _logger.LogInformation("Handled exception occurred." +
                    " User={userName}. IncludePublic={includePublic}. Exception message={exMessage}.",
                    User.Identity.Name, includePublic, ex.Message);
                result.ResultMessage = ex.Message;
                return BadRequest(result);
            }
        }

        /// <summary>
        /// Get public categoires list only.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("categories/get-public")]
        public async Task<ActionResult<GetPredefinedCategoriesResult>> GetPublicCategories()
        {
            _logger.LogInformation("Received get only public categories request. " +
                "User={userName}.", User.Identity.Name);
            GetPredefinedCategoriesResult result = new GetPredefinedCategoriesResult();

            PredefinedCategoryDm[] categoriesDm = await _predefinedCategoriesService.GetPublicPredefinedCategoriesFromDatabaseAsync();
            _logger.LogInformation("Retrieved {predefinedCategoryCount} categories from database. " +
                "User={userName}.", categoriesDm.Length, User.Identity.Name);
            PredefinedCategoriesCollectionDto categoriesDto = new PredefinedCategoriesCollectionDto()
            {
                Categories = _mapper.Map<GetPredefinedCategoryDto[]>(categoriesDm)
            };
            _logger.LogInformation("Mapped categories domain models to DTO. Sending result to client. " +
                "User={userName}.", User.Identity.Name);

            result.Data = categoriesDto;
            return Ok(result);
        }

        /// <summary>
        /// Add new category to database.
        /// </summary>
        /// <param name="addCategoryData">Category to add.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("categories/add")]
        public async Task<ActionResult<AddPredefinedCategoryResult>> AddNewCategory(AddPredefinedCategoryDto addCategoryData)
        {
            _logger.LogInformation("Received add new predefined category request. " +
                "User={userName}. CategoryName={categoryName}.", User.Identity.Name, addCategoryData.Name);
            AddPredefinedCategoryResult result = new AddPredefinedCategoryResult();

            try
            {
                PredefinedCategoryDm inputModel = _mapper.Map<PredefinedCategoryDm>(addCategoryData);
                _logger.LogInformation("Mapped input category DTO to domain model. " +
                    "User={userName}. CategoryName={categoryName}.", User.Identity.Name, addCategoryData.Name);
                PredefinedCategoryDm savedCategory = await _predefinedCategoriesService.CreatePredefinedCategoryAsync(inputModel,
                    HttpContext.User.Identity.Name);
                _logger.LogInformation("Saved new category in database with Id={predefinedCategoryId}. " +
                    "User={userName}. CategoryName={categoryName}.", savedCategory.Id, User.Identity.Name, addCategoryData.Name);
                GetPredefinedCategoryDto categoryDto = _mapper.Map<GetPredefinedCategoryDto>(savedCategory);
                _logger.LogInformation("Mapped saved category domain model to DTO. Sending result to client" +
                    "User={userName}. CategoryName={categoryName}.", User.Identity.Name, addCategoryData.Name);

                result.Data = categoryDto;
                return Ok(result);
            }
            catch (CustomBusinessLogicException ex)
            {
                _logger.LogInformation("Handled exception occurred." +
                    " User={userName}. CategoryName={categoryName}. Exception message={exMessage}.",
                    User.Identity.Name, addCategoryData.Name, ex.Message);
                result.ResultMessage = ex.Message;
                return BadRequest(result);
            }
        }

        /// <summary>
        /// Delete specified category from database.
        /// </summary>
        /// <param name="dataToDelete">Category to remove.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("categories/delete")]
        public async Task<ActionResult<DeletePredefinedCategoriesResult>> DeleteCategory(DeletePredefinedCategoryDto[] dataToDelete)
        {
            _logger.LogInformation("Received delete categories request. " +
                "User={userName}.", User.Identity.Name);
            DeletePredefinedCategoriesResult result = new DeletePredefinedCategoriesResult();
            
            if (dataToDelete.Length > 1500)
            {
                _logger.LogInformation("Can't delete categories. Too many categories to delete at a single time ({categoriesToDeleteCount}). " +
                    "User={userName}.", dataToDelete.Length, User.Identity.Name);
                result.ResultMessage = "Only up to 1500 categories can be deleted at a single time!";
                return BadRequest(result);
            }

            try
            {
                PredefinedCategoryDm[] categoriesDm = _mapper.Map<PredefinedCategoryDm[]>(dataToDelete);
                _logger.LogInformation("Mapped input categories DTO to domain models. " +
                    "User={userName}.", User.Identity.Name);
                int deletedEntitiesCount = await _predefinedCategoriesService.DeletePredefinedCategoriesAsync(
                    categoriesDm, HttpContext.User.Identity.Name);
                _logger.LogInformation("Deleted {predefinedCategoryCount} rows from database. Sending result to client. " +
                    "User={userName}.", deletedEntitiesCount, User.Identity.Name);

                result.CategoriesDeletedCount = deletedEntitiesCount;
                return Ok(result);
            }
            catch (CustomBusinessLogicException ex)
            {
                _logger.LogInformation("Handled exception occurred." +
                    " User={userName}. Exception message={exMessage}.",
                    User.Identity.Name, ex.Message);
                result.ResultMessage = ex.Message;
                return BadRequest(result);
            }
        }
    }
}
