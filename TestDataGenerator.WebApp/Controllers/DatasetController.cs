﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using TestDataGenerator.Core;
using TestDataGenerator.Web.BusinessLogic;
using TestDataGenerator.Web.BusinessLogic.Interfaces;
using TestDataGenerator.Web.DataAccess.Interfaces;
using TestDataGenerator.Web.DataAccess.Interfaces.Repositories;
using TestDataGenerator.Web.Models;
using TestDataGenerator.Web.Models.DomainModels;
using TestDataGenerator.Web.Mq.Models;
using TestDataGenerator.WebApp.Models.ActionResults;
using TestDataGenerator.WebApp.Models.DTO.ColumnSet;
using TestDataGenerator.WebApp.Models.DTO.Dataset;
using TestDataGenerator.WebApp.Validation;

namespace TestDataGenerator.WebApp.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class DatasetController : ControllerBase
    {
        private readonly ITestDataSetService _datasetService;
        private readonly IDatasetGenerationRequestRepository _datasetReqRepo;
        private readonly IBasicServiceOptions _serviceOpts;
        private readonly IMapper _mapper;
        private readonly ILogger<DatasetController> _logger;

        public DatasetController(
            ITestDataSetService datasetService,
            IDatasetGenerationRequestRepository datasetReqRepo,
            IBasicServiceOptions serviceOpts,
            IMapper mapper,
            ILogger<DatasetController> logger)
        {
            _datasetService = datasetService;
            _datasetReqRepo = datasetReqRepo;
            _mapper = mapper;
            _serviceOpts = serviceOpts;
            _logger = logger;
        }
        
        /// <summary>
        /// Generate dataset using column set saved by user.
        /// </summary>
        /// <param name="columnSetId">Column set ID to use.</param>
        /// <param name="rowsToGenerate">Amount of rows to generate.</param>
        /// <returns></returns>
        [HttpGet]
        [Route("with-presaved-columnset/generate-sync")]
        public async Task<ActionResult<GetTestDataSetResult>> GetDataSetWithPreSavedColumnSet(long columnSetId, 
            [Range(1, int.MaxValue)] int rowsToGenerate = 25)
        {
            _logger.LogInformation("Received sync generate dataset request with presaved columnset." +
                " User={userName}. ColumnSetId={columnSetId}. RowsToGenerate={rowsToGenerate}.",
                User.Identity.Name, columnSetId, rowsToGenerate);
            GetTestDataSetResult result = new GetTestDataSetResult();

            if (rowsToGenerate > _serviceOpts.DatasetGenerationRowLimitsOptions.SynchronousRequestsRowLimit)
            {
                _logger.LogInformation("Dataset can't be generated because of rows count overlimit={rowsLimit}." +
                    " User={userName}. ColumnSetId={columnSetId}. RowsToGenerate={rowsToGenerate}.",
                    _serviceOpts.DatasetGenerationRowLimitsOptions.SynchronousRequestsRowLimit, User.Identity.Name, columnSetId, rowsToGenerate);
                result.ResultMessage = "Maximum limit for synchronous request is" +
                    $" {_serviceOpts.DatasetGenerationRowLimitsOptions.SynchronousRequestsRowLimit} rows! Please use " +
                    $"asynchronous method for larger datasets.";
                return BadRequest(result);
            }

            try
            {
                Row[] generatedRows = await _datasetService.GetDatasetWithPreSavedColumnSetAsync(columnSetId, HttpContext.User.Identity.Name, rowsToGenerate);
                _logger.LogInformation("Dataset rows successfully generated." +
                    " User={userName}. ColumnSetId={columnSetId}. RowsToGenerate={rowsToGenerate}.",
                    User.Identity.Name, columnSetId, rowsToGenerate);
                RowDto[] generatedRowsDto = _mapper.Map<RowDto[]>(generatedRows);
                _logger.LogInformation("Mapped generated rows to DTO rows. Returning result to client." +
                    " User={userName}. ColumnSetId={columnSetId}. RowsToGenerate={rowsToGenerate}.",
                    User.Identity.Name, columnSetId, rowsToGenerate);

                result.GeneratedRows = generatedRowsDto;
                return Ok(result);
            }
            catch(CustomBusinessLogicException ex)
            {
                _logger.LogInformation("Handled exception occurred. User={userName}. ColumnSetId={columnSetId}. Exception message={exMessage}.",
                    User.Identity.Name, columnSetId, ex.Message);
                result.ResultMessage = ex.Message;
                return BadRequest(result);
            }
        }

        /// <summary>
        /// Create dataset generation request using previously saved column set.
        /// </summary>
        /// <param name="columnSetId">Column set ID.</param>
        /// <param name="rowsToGenerate">Amount of rows to generate.</param>
        /// <returns></returns>
        [HttpGet]
        [Route("with-presaved-columnset/generate-async")]
        public async Task<ActionResult<CreateDatasetRequestResult>> GetLargeDataSetWithPreSavedColumnSet(long columnSetId, int rowsToGenerate = 1000)
        {
            _logger.LogInformation("Received async generate dataset request with presaved columnset." +
                " User={userName}. ColumnSetId={columnSetId}. RowsToGenerate={rowsToGenerate}.",
                User.Identity.Name, columnSetId, rowsToGenerate);
            CreateDatasetRequestResult result = new CreateDatasetRequestResult();

            if (rowsToGenerate < _serviceOpts.DatasetGenerationRowLimitsOptions.SynchronousRequestsRowLimit)
            {
                _logger.LogInformation("Dataset can't be generated because of rows count underlimit={rowsLimit}." +
                    " User={userName}. ColumnSetId={columnSetId}. RowsToGenerate={rowsToGenerate}.",
                    _serviceOpts.DatasetGenerationRowLimitsOptions.SynchronousRequestsRowLimit, User.Identity.Name, columnSetId, rowsToGenerate);
                result.ResultMessage = "For datasets smaller than" +
                    $" {_serviceOpts.DatasetGenerationRowLimitsOptions.SynchronousRequestsRowLimit} rows, please use synchronous method.";
                return BadRequest(result);
            }

            if (rowsToGenerate > _serviceOpts.DatasetGenerationRowLimitsOptions.AsyncRawJsonRequestsLimit)
            {
                _logger.LogInformation("Dataset can't be generated because of rows count overlimit={rowsLimit}." +
                    " User={userName}. ColumnSetId={columnSetId}. RowsToGenerate={rowsToGenerate}.",
                    _serviceOpts.DatasetGenerationRowLimitsOptions.AsyncRawJsonRequestsLimit, User.Identity.Name, columnSetId, rowsToGenerate);
                result.ResultMessage = "Currently only datasets with up to " +
                    $"{_serviceOpts.DatasetGenerationRowLimitsOptions.AsyncRawJsonRequestsLimit} rows can be generated as raw JSON response.";
                return BadRequest(result);
            }

            try
            {
                long requestId = await _datasetService.CreateDatasetGenerationRequestAndPostToMqAsync(columnSetId, HttpContext.User.Identity.Name, rowsToGenerate);
                _logger.LogInformation("Created raw JSON dataset generation request with Id={generationRequestId}." +
                    " User={userName}. ColumnSetId={columnSetId}. RowsToGenerate={rowsToGenerate}.",
                    requestId, User.Identity.Name, columnSetId, rowsToGenerate);

                result.RequestId = requestId;
                return Ok(result);
            }
            catch(CustomBusinessLogicException ex)
            {
                _logger.LogInformation("Handled exception occurred. User={userName}. ColumnSetId={columnSetId}. Exception message={exMessage}.",
                    User.Identity.Name, columnSetId, ex.Message);
                result.ResultMessage = ex.Message;
                return BadRequest(result);
            }
        }

        /// <summary>
        /// Get result of asynchronous request made with using presaved column set.
        /// </summary>
        /// <param name="generationRequestId">Generation request ID.</param>
        /// <returns></returns>
        [HttpGet]
        [Route("with-presaved-columnset/get-async-generated-result")]
        public async Task<ActionResult<GetTestDataSetResult>> GetLargeDatasetGenerationResult(long generationRequestId)
        {
            _logger.LogInformation("Recevied get async generated result with presaved column request." +
                " User={userName}. GenerationRequestId={generationRequestId}.", User.Identity.Name, generationRequestId);
            GetTestDataSetResult result = new GetTestDataSetResult();
            RowDto[] rowsResult = new RowDto[0];

            try
            {
                DatasetGenerationRequestDm generationRequest = await _datasetService.GetDatasetGenerationRequestAsync(generationRequestId, HttpContext.User.Identity.Name);
                _logger.LogInformation("Extracted generation request from database." +
                    " User={userName}. GenerationRequestId={generationRequestId}.", User.Identity.Name, generationRequestId);

                if (generationRequest.RowsJson != null)
                {
                    SingleRowMqDto[] rowsFromDb = JsonConvert.DeserializeObject<SingleRowMqDto[]>(generationRequest.RowsJson);
                    rowsResult = _mapper.Map<RowDto[]>(rowsFromDb);
                    _logger.LogInformation("Mapped generated rows to client DTO." +
                        " User={userName}. GenerationRequestId={generationRequestId}.", User.Identity.Name, generationRequestId);
                }

                await _datasetReqRepo.SetGenerationRequestAsFetchedAsync(generationRequestId);
                _logger.LogInformation("Set request as fetched. Returning {rowsCount} rows to client." +
                    " User={userName}. GenerationRequestId={generationRequestId}.", rowsResult.Length, User.Identity.Name, generationRequestId);

                result.GeneratedRows = rowsResult;
                result.ErrorText = generationRequest.ErrorText;

                return Ok(result);
            }
            catch(CustomBusinessLogicException ex)
            {
                _logger.LogInformation("Handled exception occurred. User={userName}. GenerationRequestId={generationRequestId}. Exception message={exMessage}.",
                    User.Identity.Name, generationRequestId, ex.Message);
                result.ResultMessage = ex.Message;
                return BadRequest(result);
            }
        }

        /// <summary>
        /// Generate simple small dataset using provided parameters.
        /// </summary>
        /// <param name="dataSetParametersDto">Dataset generation parameters.</param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("generate-sync")]
        public async Task<ActionResult<GetTestDataSetResult>> GenerateDataset(InputTestDataSetParametersDto dataSetParametersDto)
        {
            _logger.LogInformation("Received sync generate dataset request." +
                " User={userName}. RowsToGenerate={rowsToGenerate}.", User.Identity.Name, dataSetParametersDto.RowsToGenerate);
            bool anyOfGeneratorParamsIsInvalid = false;
            foreach (ColumnDto colDto in dataSetParametersDto.ColumnSet.Columns)
            {
                object valueGeneratorParams = BoxedObjectValidationHelper.ConvertGeneratorParamsObjectToConcreteParamsDto(colDto.ColumnSettings.ValueSupplier);
                if (!TryValidateModel(valueGeneratorParams))
                    anyOfGeneratorParamsIsInvalid = true;
            }

            if (anyOfGeneratorParamsIsInvalid)
            {
                _logger.LogInformation("Input parameters validation failed for sync generate request." +
                    " User={userName}. RowsToGenerate={rowsToGenerate}.", User.Identity.Name, dataSetParametersDto.RowsToGenerate);
                return BadRequest(ModelState);
            }

            GetTestDataSetResult result = new GetTestDataSetResult();

            if (dataSetParametersDto.RowsToGenerate > _serviceOpts.DatasetGenerationRowLimitsOptions.SynchronousRequestsRowLimit)
            {
                _logger.LogInformation("Dataset can't be generated because of rows count overlimit={rowsLimit}." +
                    " User={userName}. RowsToGenerate={rowsToGenerate}.",
                    _serviceOpts.DatasetGenerationRowLimitsOptions.SynchronousRequestsRowLimit, User.Identity.Name, dataSetParametersDto.RowsToGenerate);
                result.ResultMessage = "Maximum limit for synchronous request is" +
                    $" {_serviceOpts.DatasetGenerationRowLimitsOptions.SynchronousRequestsRowLimit} rows! Please use " +
                    $"asynchronous method for larger datasets.";
                return BadRequest(result);
            }

            try
            {
                ColumnSetDm colsetDm = _mapper.Map<ColumnSetDm>(dataSetParametersDto.ColumnSet);
                _logger.LogInformation("Mapped input DTO to column set domain model." +
                    " User={userName}. RowsToGenerate={rowsToGenerate}", User.Identity.Name, dataSetParametersDto.RowsToGenerate);
                Row[] generatedRows = await _datasetService.GenerateDatasetAsync(colsetDm, dataSetParametersDto.RowsToGenerate);
                _logger.LogInformation("Generated {rowsCount}." +
                    " User={userName}. RowsToGenerate={rowsToGenerate}", generatedRows.Length, User.Identity.Name, dataSetParametersDto.RowsToGenerate);
                RowDto[] generatedRowsDto = _mapper.Map<RowDto[]>(generatedRows);
                _logger.LogInformation("Mapped generated rows to DTO. Returning result to client." +
                    " User={userName}. RowsToGenerate={rowsToGenerate}", User.Identity.Name, dataSetParametersDto.RowsToGenerate);

                result.GeneratedRows = generatedRowsDto;
                return Ok(result);
            }
            catch(CustomBusinessLogicException ex)
            {
                _logger.LogInformation("Handled exception occurred. User={userName}. RowsToGenerate={rowsToGenerate}. Exception message={exMessage}.",
                    User.Identity.Name, dataSetParametersDto.RowsToGenerate, ex.Message);
                result.ResultMessage = ex.Message;
                return BadRequest(result);
            }
        }

        /// <summary>
        /// Create asynchronous request for dataset generation and further export as JSON file.
        /// </summary>
        /// <param name="exportParametersDto">Dataset generation and file export parameters.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("export/request-async-json")]
        public async Task<ActionResult<CreateDatasetRequestResult>> RegisterJsonFileExportRequest(JsonFileExportParametersDto exportParametersDto)
        {
            _logger.LogInformation("Received JSON file dataset export request." +
                " User={userName}. RowsToGenerate={rowsToGenerate}. ColumnSetId={columnSetId}", 
                User.Identity.Name, exportParametersDto.DatasetParameters.RowsToGenerate, exportParametersDto.DatasetParameters.ColumnSet.ColumnSetId);
            CreateDatasetRequestResult result = new CreateDatasetRequestResult();

            if (exportParametersDto.DatasetParameters.RowsToGenerate >
                _serviceOpts.DatasetGenerationRowLimitsOptions.AsyncFileRequestsRowLimit)
            {
                _logger.LogInformation("Can't generate dataset due to rows overlimit={rowsLimit}." +
                    " User={userName}. RowsToGenerate={rowsToGenerate}. ColumnSetId={columnSetId}",
                    _serviceOpts.DatasetGenerationRowLimitsOptions.AsyncFileRequestsRowLimit, User.Identity.Name, 
                    exportParametersDto.DatasetParameters.RowsToGenerate, exportParametersDto.DatasetParameters.ColumnSet.ColumnSetId);
                result.ResultMessage = $"A maximum of {_serviceOpts.DatasetGenerationRowLimitsOptions.AsyncFileRequestsRowLimit} " +
                    $"rows can be generated within single JSON file.";
                return BadRequest(result);
            }

            try
            {
                ColumnSetDm colsetDm = _mapper.Map<ColumnSetDm>(exportParametersDto.DatasetParameters.ColumnSet);
                _logger.LogInformation("Mapped input column set DTO to domain model." +
                    " User={userName}. RowsToGenerate={rowsToGenerate}. ColumnSetId={columnSetId}",
                    User.Identity.Name, exportParametersDto.DatasetParameters.RowsToGenerate, exportParametersDto.DatasetParameters.ColumnSet.ColumnSetId);
                long requestId = await _datasetService.CreateDatasetGenerationRequestAndPostToMqAsync(colsetDm, 
                    HttpContext.User.Identity.Name, exportParametersDto.DatasetParameters.RowsToGenerate, OutputFilesExtensions.JSON);
                _logger.LogInformation("Created JSON file generation request with Id={generationRequestId}." +
                    " User={userName}. RowsToGenerate={rowsToGenerate}. ColumnSetId={columnSetId}",
                    requestId, User.Identity.Name, exportParametersDto.DatasetParameters.RowsToGenerate, 
                    exportParametersDto.DatasetParameters.ColumnSet.ColumnSetId);

                result.RequestId = requestId;
                return Ok(result);
            }
            catch (CustomBusinessLogicException ex)
            {
                _logger.LogInformation("Handled exception occurred. User={userName}. RowsToGenerate={rowsToGenerate}. " +
                    "ColumnSetId={columnSetId}. Exception message={exMessage}.",
                    User.Identity.Name, exportParametersDto.DatasetParameters.RowsToGenerate, 
                    exportParametersDto.DatasetParameters.ColumnSet.ColumnSetId, ex.Message);
                result.ResultMessage = ex.Message;
                return BadRequest(result);
            }
        }

        /// <summary>
        /// Create asynchronous request for dataset generation and further export as CSV file.
        /// </summary>
        /// <param name="exportParametersDto">Dataset generation and file export parameters.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("export/request-async-csv")]
        public async Task<ActionResult<CreateDatasetRequestResult>> RegisterCsvFileExportRequest(CsvFileExportParametersDto exportParametersDto)
        {
            _logger.LogInformation("Received CSV file dataset export request." +
                " User={userName}. RowsToGenerate={rowsToGenerate}. ColumnSetId={columnSetId}",
                User.Identity.Name, exportParametersDto.DatasetParameters.RowsToGenerate, exportParametersDto.DatasetParameters.ColumnSet.ColumnSetId);
            CreateDatasetRequestResult result = new CreateDatasetRequestResult();

            if (exportParametersDto.DatasetParameters.RowsToGenerate >
                _serviceOpts.DatasetGenerationRowLimitsOptions.AsyncFileRequestsRowLimit)
            {
                _logger.LogInformation("Can't generate dataset due to rows overlimit={rowsLimit}." +
                    " User={userName}. RowsToGenerate={rowsToGenerate}. ColumnSetId={columnSetId}",
                    _serviceOpts.DatasetGenerationRowLimitsOptions.AsyncFileRequestsRowLimit, User.Identity.Name,
                    exportParametersDto.DatasetParameters.RowsToGenerate, exportParametersDto.DatasetParameters.ColumnSet.ColumnSetId);
                result.ResultMessage = $"A maximum of {_serviceOpts.DatasetGenerationRowLimitsOptions.AsyncFileRequestsRowLimit} " +
                    $"rows can be generated within single CSV file.";
                return BadRequest(result);
            }

            try
            {
                ColumnSetDm colsetDm = _mapper.Map<ColumnSetDm>(exportParametersDto.DatasetParameters.ColumnSet);
                _logger.LogInformation("Mapped input column set DTO to domain model." +
                    " User={userName}. RowsToGenerate={rowsToGenerate}. ColumnSetId={columnSetId}",
                    User.Identity.Name, exportParametersDto.DatasetParameters.RowsToGenerate, exportParametersDto.DatasetParameters.ColumnSet.ColumnSetId);
                long requestId = await _datasetService.CreateDatasetGenerationRequestAndPostToMqAsync(colsetDm, HttpContext.User.Identity.Name, 
                    exportParametersDto.DatasetParameters.RowsToGenerate, OutputFilesExtensions.CSV);
                _logger.LogInformation("Created JSON file generation request with Id={generationRequestId}." +
                    " User={userName}. RowsToGenerate={rowsToGenerate}. ColumnSetId={columnSetId}",
                    requestId, User.Identity.Name, exportParametersDto.DatasetParameters.RowsToGenerate,
                    exportParametersDto.DatasetParameters.ColumnSet.ColumnSetId);

                result.RequestId = requestId;
                return Ok(result);
            }
            catch (CustomBusinessLogicException ex)
            {
                _logger.LogInformation("Handled exception occurred. User={userName}. RowsToGenerate={rowsToGenerate}. " +
                    "ColumnSetId={columnSetId}. Exception message={exMessage}.",
                    User.Identity.Name, exportParametersDto.DatasetParameters.RowsToGenerate,
                    exportParametersDto.DatasetParameters.ColumnSet.ColumnSetId, ex.Message);
                result.ResultMessage = ex.Message;
                return BadRequest(result);
            }
        }

        /// <summary>
        /// Download generated dataset as file.
        /// </summary>
        /// <param name="requestId">File generation request ID.</param>
        /// <returns></returns>
        [HttpGet]
        [Route("export/get-file-result")]
        public async Task<ActionResult<GetTestDataSetResult>> GetFileGenerationRequestResult(long requestId)
        {
            _logger.LogInformation("Received get file result request." +
                " User={userName}. GenerationRequestId={generationRequestId}.", User.Identity.Name, requestId);
            GetTestDataSetResult result = new GetTestDataSetResult();

            try
            {
                DatasetGenerationRequestDm generationRequest = await _datasetService.GetDatasetGenerationRequestAsync(requestId, HttpContext.User.Identity.Name);
                _logger.LogInformation("Generation request entity retrieved from database." +
                    " User={userName}. GenerationRequestId={generationRequestId}.", User.Identity.Name, requestId);

                await _datasetReqRepo.SetGenerationRequestAsFetchedAsync(requestId);
                _logger.LogInformation("Generation request is set as fetched." +
                    " User={userName}. GenerationRequestId={generationRequestId}.", User.Identity.Name, requestId);

                _logger.LogInformation("Sending file result to client." +
                    " User={userName}. GenerationRequestId={generationRequestId}.", User.Identity.Name, requestId);
                if (generationRequest.FileExtension == OutputFilesExtensions.CSV)
                {
                    return File(generationRequest.FileContents, "text/csv",
                        $"dataset_cSetId{generationRequest.ColumnSetId}_reqId{generationRequest.Id}." + generationRequest.FileExtension);
                }
                else if (generationRequest.FileExtension == OutputFilesExtensions.JSON)
                {
                    return File(generationRequest.FileContents, "application/json",
                        $"dataset_cSetId{generationRequest.ColumnSetId}_reqId{generationRequest.Id}." + generationRequest.FileExtension);
                }

                _logger.LogError("File was failed to deliver to client!" +
                    " User={userName}. GenerationRequestId={generationRequestId}.", User.Identity.Name, requestId);
                return StatusCode(StatusCodes.Status500InternalServerError, "Unexpected server error has occurred.");
            }
            catch (CustomBusinessLogicException ex)
            {
                _logger.LogInformation("Handled exception occurred." +
                    " User={userName}. GenerationRequestId={generationRequestId}. Exception message={exMessage}.", 
                    User.Identity.Name, requestId, ex.Message);
                result.ResultMessage = ex.Message;
                return BadRequest(result);
            }
        }

        /// <summary>
        /// Get all file export requests of current user.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("export/get-all-file-requests")]
        public async Task<ActionResult<GetManyDatasetFileGenerationRequestsResult>> GetAllFileExportRequests()
        {
            _logger.LogInformation("Received get all file generation requests request." +
                " User={userName}.", User.Identity.Name);
            GetManyDatasetFileGenerationRequestsResult result = new GetManyDatasetFileGenerationRequestsResult();

            DatasetGenerationRequestDm[] generationRequestsDm = await _datasetService.GetAllDatasetGenerationRequestsByUser(HttpContext.User.Identity.Name);
            _logger.LogInformation("Retrieved {generationRequestsCount} requests from database." +
                " User={userName}.", generationRequestsDm.Length, User.Identity.Name);

            DatasetFileGenerationRequestDto[] generationRequestsDto = _mapper.Map<DatasetFileGenerationRequestDto[]>(generationRequestsDm);
            _logger.LogInformation("Mapped domain model requests to DTO. Sending result to client." +
                " User={userName}.", User.Identity.Name);

            result.DatasetRequests = generationRequestsDto;
            return Ok(result);
        }

        /// <summary>
        /// Get file export requests by page number.
        /// </summary>
        /// <param name="pageNumber">Page number.</param>
        /// <param name="requestsPerPage">Entries per page count.</param>
        /// <returns></returns>
        [HttpGet]
        [Route("export/get-file-requests-paginated")]
        public async Task<ActionResult<GetDatasetFileGenerationRequestPaginatedResult>> GetFileExportRequestsPaginated(
            [Required, Range(0, int.MaxValue)] int pageNumber, [Required, Range(1, 10000)] int requestsPerPage)
        {
            _logger.LogInformation("Received get file requests paginated request." +
                " User={userName}. PageNumber={pageNumber}. RequestsPerPage={requestsPerPage}.", User.Identity.Name, 
                pageNumber, requestsPerPage);
            GetDatasetFileGenerationRequestPaginatedResult result = new GetDatasetFileGenerationRequestPaginatedResult();

            try
            {
                DatasetGenerationRequestDm[] generationRequestsDm = await _datasetService.GetGenerationRequestsPaginatedAsync(
                    HttpContext.User.Identity.Name, pageNumber, requestsPerPage);
                _logger.LogInformation("Retrieved paginated portion of generation requests from database." +
                    " User={userName}. PageNumber={pageNumber}. RequestsPerPage={requestsPerPage}.", User.Identity.Name,
                    pageNumber, requestsPerPage);
                DatasetFileGenerationRequestDto[] generationRequestsDto = _mapper.Map<DatasetFileGenerationRequestDto[]>(generationRequestsDm);
                _logger.LogInformation("Mapped generation requests domain models to DTO." +
                    " User={userName}. PageNumber={pageNumber}. RequestsPerPage={requestsPerPage}.", User.Identity.Name,
                    pageNumber, requestsPerPage);
                int totalRequestsCount = await _datasetReqRepo.GetUserRequestsCountAsync(HttpContext.User.Identity.Name);
                _logger.LogInformation("Received total count of user's requests in database={totalRequestsCount}. Returning result to client." +
                    " User={userName}. PageNumber={pageNumber}. RequestsPerPage={requestsPerPage}.", totalRequestsCount, User.Identity.Name,
                    pageNumber, requestsPerPage);

                result.ValuesPerPage = requestsPerPage;
                result.TotalValuesCount = totalRequestsCount;
                result.DatasetRequests = generationRequestsDto;
                result.PageNumber = pageNumber;

                return Ok(result);
            }
            catch (CustomBusinessLogicException ex)
            {
                _logger.LogInformation("Handled exception occurred." +
                    " User={userName}. PageNumber={pageNumber}. RequestsPerPage={requestsPerPage}. Exception message={exMessage}.",
                    User.Identity.Name, pageNumber, requestsPerPage, ex.Message);
                result.ResultMessage = ex.Message;
                return BadRequest(result);
            }
        }

        /// <summary>
        /// Check if individual dataset generation request is completed or failed with corresponding
        /// message.
        /// </summary>
        /// <param name="requestId">Generation request ID.</param>
        /// <returns></returns>
        [HttpGet]
        [Route("check-result-readiness")]
        public async Task<ActionResult<GetDatasetGenerationReadinessResult>> CheckFileResultReadiness(long requestId)
        {
            _logger.LogInformation("Received check result readiness request." +
                " User={userName}. GenerationRequestId={generationRequestId}.", User.Identity.Name, requestId);
            GetDatasetGenerationReadinessResult result = new GetDatasetGenerationReadinessResult();

            try
            {
                DatasetGenerationRequestDm generationRequest = await _datasetService.GetGenerationRequestStateAsync(requestId, HttpContext.User.Identity.Name);
                _logger.LogInformation("Generation request retrieved from database. Sending result to client." +
                    " User={userName}. GenerationRequestId={generationRequestId}.", User.Identity.Name, requestId);

                result.ResultReady = generationRequest.WorkerResponseReceived;
                result.ErrorText = generationRequest.ErrorText;
                result.RequestProcessedDate = generationRequest.WorkerResponseReceivedDate;
                result.RequestFailedToProcess = generationRequest.ErrorText != null;
                return result;
            }
            catch (CustomBusinessLogicException ex)
            {
                _logger.LogInformation("Handled exception occurred." +
                    " User={userName}. GenerationRequestId={generationRequestId}. Exception message={exMessage}.",
                    User.Identity.Name, requestId, ex.Message);
                result.ResultMessage = ex.Message;
                return BadRequest(result);
            }
        }

    }
}