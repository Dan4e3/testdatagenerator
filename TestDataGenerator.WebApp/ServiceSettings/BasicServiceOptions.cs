﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestDataGenerator.Web.DataAccess.Interfaces;

namespace TestDataGenerator.WebApp.ServiceSettings
{
    public class BasicServiceOptions: IBasicServiceOptions
    {

        public bool UsePostgreSqlEngine { get; set; }

        public string DefaultDbConnectionStringName { get; set; } = "mainDb";

        public TimeSpan FileDatasetRequestsLifeTime { get; set; }

        public MqOptions MqOptions { get; set; }

        public ReCaptchaOptions ReCaptchaOptions { get; set; }

        public DatasetGenerationRowLimitsOptions DatasetGenerationRowLimitsOptions { get; set; }

        public QuartzJobOption[] QuartzJobs { get; set; }
    }

    public static class ServiceOptionsRegisterExtensions
    {
        public static BasicServiceOptions RegisterServiceOptions(this IServiceCollection services, IConfiguration config, string sectionName)
        {
            IConfigurationSection section = config.GetSection(sectionName);
            BasicServiceOptions options = new BasicServiceOptions();
            section.Bind(options);
            services.AddSingleton<IBasicServiceOptions>(options);
            return options;
        }
    }
}
