﻿using Quartz;
using System;
using System.Linq;
using TestDataGenerator.Web.DataAccess.Interfaces;

namespace TestDataGenerator.WebApp.Quartz
{
    public static class QuartzJobRegistrationExtensions
    {
        public static void AddJobAndTrigger<T>(
            this IServiceCollectionQuartzConfigurator quartz,
            IBasicServiceOptions serviceOpts) where T : IJob
        {
            string jobNameFromClassName = typeof(T).Name;

            QuartzJobOption optionFromAppSettings = 
                serviceOpts.QuartzJobs.FirstOrDefault(j => j.JobName == jobNameFromClassName);

            if (optionFromAppSettings == null)
                throw new Exception($"No Quartz job configuration was found for job {jobNameFromClassName}");
            
            JobKey jobKey = new JobKey(optionFromAppSettings.JobName);
            quartz.AddJob<T>(opts => opts
            .WithIdentity(jobKey));

            quartz.AddTrigger(opts => opts
                .ForJob(jobKey)
                .WithIdentity(optionFromAppSettings.JobName + "-trigger")
                .StartNow()
                .WithSimpleSchedule(x => x
                    .WithInterval(optionFromAppSettings.RepeatTimeSpan)
                    .RepeatForever()));
        }
    }
}
