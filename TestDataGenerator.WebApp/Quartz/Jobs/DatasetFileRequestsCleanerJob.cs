﻿using Microsoft.Extensions.Logging;
using Quartz;
using System;
using System.Threading.Tasks;
using TestDataGenerator.Web.DataAccess.Interfaces;
using TestDataGenerator.Web.DataAccess.Interfaces.Repositories;

namespace TestDataGenerator.WebApp.Quartz.Jobs
{
    public class DatasetFileRequestsCleanerJob : IJob
    {
        private readonly IDatasetGenerationRequestRepository _datasetRequestsRepo;
        private readonly ILogger<DatasetFileRequestsCleanerJob> _logger;
        private readonly IBasicServiceOptions _serviceOpts;

        public DatasetFileRequestsCleanerJob(
            IDatasetGenerationRequestRepository datasetRequestsRepo,
            ILogger<DatasetFileRequestsCleanerJob> logger,
            IBasicServiceOptions serviceOpts)
        {
            _datasetRequestsRepo = datasetRequestsRepo;
            _logger = logger;
            _serviceOpts = serviceOpts;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            _logger.LogInformation("{cleanerName}: gathering expired file dataset requests to clean up...", nameof(DatasetFileRequestsCleanerJob));
            long[] deletedEntriesIds = await _datasetRequestsRepo.DeleteExpiredFileDatasetReuqestsAsync(_serviceOpts.FileDatasetRequestsLifeTime);
            _logger.LogInformation("{cleanerName}: removed rows with ids: {rowsIds} from datasets generation requests table.",
                nameof(DatasetFileRequestsCleanerJob), deletedEntriesIds.Length > 0 ? string.Join(", ", deletedEntriesIds) : "none");
        }
    }
}
