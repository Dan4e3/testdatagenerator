﻿using Microsoft.Extensions.Logging;
using Quartz;
using System.Threading.Tasks;
using TestDataGenerator.Web.DataAccess.Interfaces.Repositories;

namespace TestDataGenerator.WebApp.Quartz.Jobs
{
    public class RawJsonDatasetRequestsCleanerJob : IJob
    {
        private readonly IDatasetGenerationRequestRepository _datasetRequestsRepo;
        private readonly ILogger<RawJsonDatasetRequestsCleanerJob> _logger;

        public RawJsonDatasetRequestsCleanerJob(
            IDatasetGenerationRequestRepository datasetRequestsRepo,
            ILogger<RawJsonDatasetRequestsCleanerJob> logger)
        {
            _datasetRequestsRepo = datasetRequestsRepo;
            _logger = logger;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            _logger.LogInformation("{cleanerName}: gathering fetched raw JSON requests to clean up...", nameof(RawJsonDatasetRequestsCleanerJob));
            long[] deletedEntriesIds = await _datasetRequestsRepo.DeleteFetchedRawJsonDatasetRequestsAsync();
            _logger.LogInformation("{cleanerName}: removed rows with ids: {rowsIds} from datasets generation requests table.",
                nameof(RawJsonDatasetRequestsCleanerJob), deletedEntriesIds.Length > 0 ? string.Join(", ", deletedEntriesIds) : "none");
        }
    }
}
