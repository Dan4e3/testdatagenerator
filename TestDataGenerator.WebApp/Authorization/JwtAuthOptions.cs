﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.WebApp.Authorization
{
    public class JwtAuthOptions
    {
        public const string ISSUER = "TestDataGenerator";
        public const string AUDIENCE = "Public";
        public static TimeSpan DefaultTokenLifespan = TimeSpan.FromDays(1);
        const string KEY = "909a0)F( JCS 9023r fdgxchjS@3150-a=))@_#%(*)dsf-02r3_*R(33";
        public static SymmetricSecurityKey GetSymmetricSecurityKey() =>
            new SymmetricSecurityKey(Encoding.UTF8.GetBytes(KEY));
    }
}
