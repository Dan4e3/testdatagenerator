using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Net.Http.Headers;
using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json.Converters;
using System.Threading.Tasks;
using TestDataGenerator.Web.BusinessLogic.Interfaces;
using TestDataGenerator.Web.BusinessLogic.Services;
using TestDataGenerator.Web.DataAccess;
using TestDataGenerator.Web.DataAccess.Interfaces;
using TestDataGenerator.Web.Mq;
using TestDataGenerator.Web.Mq.Interfaces;
using TestDataGenerator.WebApp.Authorization;
using TestDataGenerator.WebApp.HealthChecks;
using TestDataGenerator.WebApp.Swagger;
using TestDataGenerator.WebApp.ServiceSettings;
using System;
using TestDataGenerator.WebApp.Models.Mapping;
using TestDataGenerator.Web.DataAccess.PgSql;
using TestDataGenerator.Web.DataAccess.SqlServer;
using Quartz;
using TestDataGenerator.WebApp.Quartz;
using TestDataGenerator.WebApp.Quartz.Jobs;
using Microsoft.AspNetCore.Http;
using System.Linq;
using Microsoft.AspNetCore.HttpOverrides;

namespace TestDataGenerator.WebApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers()
                .AddNewtonsoftJson(opts => {
                    //opts.SerializerSettings.Converters.Add(new StringEnumConverter());
                });

            services.RegisterSwaggerGen();

            services.AddAutoMapper(typeof(MappingProfile));

            services
                .AddAuthentication(options => {
                    options.DefaultScheme = "JWT_OR_COOKIE";
                    options.DefaultChallengeScheme = "JWT_OR_COOKIE";
                })
                .AddCookie(CookieAuthenticationDefaults.AuthenticationScheme, options =>
                {
                    //options.LoginPath = "user/login";
                    //options.AccessDeniedPath = "user/access-denied";
                    options.Events.OnRedirectToLogin = (t) => {
                        t.Response.StatusCode = 401;
                        return Task.CompletedTask;
                    };
                })
                .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, options => {
                    options.TokenValidationParameters = new TokenValidationParameters() {
                        ValidateIssuer = true,
                        ValidIssuer = JwtAuthOptions.ISSUER,
                        ValidateAudience = true,
                        ValidAudience = JwtAuthOptions.AUDIENCE,
                        ValidateLifetime = true,
                        IssuerSigningKey = JwtAuthOptions.GetSymmetricSecurityKey(),
                        ValidateIssuerSigningKey = true,
                    };
                })
                .AddPolicyScheme("JWT_OR_COOKIE", "JWT_OR_COOKIE", options =>
                {
                    // runs on each request
                    options.ForwardDefaultSelector = context =>
                    {
                        // filter by auth type
                        string authorization = context.Request.Headers[HeaderNames.Authorization];
                        if (!string.IsNullOrEmpty(authorization) && authorization.StartsWith("Bearer "))
                            return JwtBearerDefaults.AuthenticationScheme;

                        // otherwise always check for cookie auth
                        return CookieAuthenticationDefaults.AuthenticationScheme;
                    };
                });

            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/dist";
            });

            // Config options
            BasicServiceOptions serviceOpts = services.RegisterServiceOptions(Configuration, "ServiceOptions");

            // Healthchecks
            services.AddHealthChecks()
                .AddCheck<DbConnectionHealthCheck>("Database connection check.")
                .AddCheck<MqConnectionHealthCheck>("MQ Server connection check.");

            // Quartz scheduler
            services.AddQuartz(q => {
                q.UseMicrosoftDependencyInjectionJobFactory();
                q.AddJobAndTrigger<DatasetFileRequestsCleanerJob>(serviceOpts);
                q.AddJobAndTrigger<RawJsonDatasetRequestsCleanerJob>(serviceOpts);
            });
            services.AddQuartzServer(options => {
                options.WaitForJobsToComplete = true;
            });

            // CORS
            services.AddCors(opts => {
                opts.AddDefaultPolicy(policy => {
                    policy.WithOrigins("http://localhost:5005", "http://data-gen.com", "https://data-gen.com")
                        .AllowAnyHeader()
                        .AllowAnyMethod()
                        .AllowCredentials();
                });
            });

            services.AddSingleton<IDbConnectionFactory, DbConnectionFactory>();
            services.AddSingleton<IDapperCustomSettings, DapperCustomSettings>();

            services.AddSingleton<IMqClient, MqClient>();

            services.AddSingleton<IUserHandlingService, UserHandlingService>();
            services.AddSingleton<IPredefinedValuesService, PredefinedValuesService>();
            services.AddSingleton<IPredefinedCategoriesService, PredefinedCategoriesService>();
            services.AddSingleton<IColumnSetService, ColumnSetService>();
            services.AddSingleton<ITestDataSetService, DataSetService>();
            services.AddSingleton<IAdditionalColumnParamsService, AdditionalColumnParamsService>();
            services.AddScoped<IRecaptchaService, RecaptchaService>();

            services.AddHttpClient();

            if (serviceOpts.UsePostgreSqlEngine)
                services.RegisterPgSqlImplementation();
            else
                services.RegisterSqlServerImplementation();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "TestDataGenerator v1"));

            //app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSpaStaticFiles(new StaticFileOptions()
            {
                OnPrepareResponse = (context) =>
                {
                    var headers = context.Context.Response.GetTypedHeaders();
                    headers.CacheControl = new CacheControlHeaderValue
                    {
                        Public = true,
                        MaxAge = TimeSpan.FromDays(14)
                    };
                }
            });

            app.UseRouting();

            app.UseCors();

            app.UseAuthentication();
            app.UseAuthorization();
            
            // mapping for endpoints. all of them are starting with /api
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            // in case request starts with /api, but not mapped to any controller - return 404 code without html
            app.MapWhen(x => x.Request.Path.StartsWithSegments("/api"), builder => {
                builder.Run(async ctx => {
                    ctx.Response.StatusCode = 404;
                    await ctx.Response.WriteAsync("Requested api call was not found!");
                });
            });

            // mapping for specific pseudopages/content for SPA
            app.MapWhen(x => {
                string pathAsString = x.Request.Path.Value;
                string[] legitPseudoUrlsArr = new string[] {
                    "/login", "/register", "/predefined-data", 
                    "/column-sets", "/dataset-generator", "/reference", "/index"
                };
                if (pathAsString == "/" ||
                    (pathAsString.StartsWith("/index") && (pathAsString.EndsWith(".html") || pathAsString.EndsWith(".js"))) ||
                    (pathAsString.StartsWith("/vendors~index") && pathAsString.EndsWith(".js")) ||
                    pathAsString.EndsWith(".bundle.js") ||
                    pathAsString.StartsWith("/favicon.ico") ||
                    pathAsString.StartsWith("/src/static") || 
                    legitPseudoUrlsArr.Contains(pathAsString))
                    return true;
                else
                    return false;
            }, builder => { 
                builder.UseSpa(spa =>
                {
                    spa.Options.SourcePath = "ClientApp";
                    if (env.IsDevelopment())
                    {
                        spa.UseProxyToSpaDevelopmentServer(new Uri("http://localhost:9000"));
                    }
                });
            });

            // finally, if every routing rule before failed - return 404 page.
            app.Run(async ctx => {
                ctx.Response.StatusCode = 404;
                await ctx.Response.WriteAsync(@"<html><head><title>Error 404 - Not Found</title></head><body>
                    <h1>Sorry, but requested page is not found...</h1>
                    <h4>You can navigate back to <a href=""/"">Home page</a>.</h4>
                </body></html>");
            });

            //app.UseSpa(spa =>
            //{
            //    spa.Options.SourcePath = "ClientApp";
            //    if (env.IsDevelopment())
            //    {
            //        spa.UseProxyToSpaDevelopmentServer(new Uri("http://localhost:9000"));
            //    }
            //});
        }
    }
}
