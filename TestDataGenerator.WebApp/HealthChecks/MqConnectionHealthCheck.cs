﻿using Microsoft.Extensions.Diagnostics.HealthChecks;
using RabbitMQ.Client;
using System;
using System.Threading;
using System.Threading.Tasks;
using TestDataGenerator.Web.DataAccess.Interfaces;

namespace TestDataGenerator.WebApp.HealthChecks
{
    public class MqConnectionHealthCheck : IHealthCheck
    {
        private readonly IBasicServiceOptions _servceOptions;

        public MqConnectionHealthCheck(
            IBasicServiceOptions servceOptions)
        {
            _servceOptions = servceOptions;
        }

        public Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default)
        {
            try
            {
                ConnectionFactory factory = new ConnectionFactory() {
                    HostName = _servceOptions.MqOptions.RabbitMqHostname
                };

                using IConnection conn = factory.CreateConnection();
                conn.Close();

                return Task.FromResult(HealthCheckResult.Healthy($"Rabbit MQ server at '{_servceOptions.MqOptions.RabbitMqHostname}' is reachable!"));
            }
            catch (Exception ex)
            {
                return Task.FromResult(HealthCheckResult.Unhealthy($"Rabbit MQ server at '{_servceOptions.MqOptions.RabbitMqHostname}' could not be reached!", ex));
            }
        }
    }
}
