﻿using Microsoft.Extensions.Diagnostics.HealthChecks;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TestDataGenerator.Web.DataAccess.Interfaces;

namespace TestDataGenerator.WebApp.HealthChecks
{
    public class DbConnectionHealthCheck : IHealthCheck
    {
        private readonly IDbConnectionFactory _connectionFactory;

        public DbConnectionHealthCheck(IDbConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        public Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default)
        {
            try
            {
                using IDbConnection connection = _connectionFactory.GetConnectionByName();
                connection.Open();
                using IDbCommand command = connection.CreateCommand();
                command.CommandText = "SELECT 1";
                int result = (int)command.ExecuteScalar();

                return Task.FromResult(HealthCheckResult.Healthy("Database connection is OK!"));
            }
            catch (Exception ex)
            {
                return Task.FromResult(HealthCheckResult.Unhealthy("Database connection FAILED!", ex));
            }
        }
    }
}
