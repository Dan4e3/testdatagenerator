﻿import { IColumn } from "../Column";

export interface IGenerationRequestColumnSetParamsDto {
    columnSetId?: number,
    columnSetName: string,
    columns: IColumn[]
}