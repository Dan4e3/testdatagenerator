﻿import { IGenerationRequestColumnSetParamsDto } from "./GenerationRequestColumnSetParamsDto";

export interface IFileExportDatasetParametersDrto {
    rowsToGenerate: number,
    columnSet: IGenerationRequestColumnSetParamsDto
}