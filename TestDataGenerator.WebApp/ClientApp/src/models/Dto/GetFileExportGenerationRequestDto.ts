﻿
export interface IGetFileExportGenerationRequestDto {
    generationRequestId: number,
    columnSetId?: number,
    columnSetName: string,
    rowsToGenerate: number,
    exportFileExtension: string,
    resultReady: boolean,
    requestFailedToProcess: boolean,
    errorText: string,
    requestProcessedDate: Date | null
}