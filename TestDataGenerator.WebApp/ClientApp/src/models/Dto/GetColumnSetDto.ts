﻿import { IColumn } from "../Column";

export interface IGetColumnSetDto {
    id: number,
    columns: IColumn[],
    creationDate: Date,
    name: string
}