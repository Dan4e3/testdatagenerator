﻿export interface IValueGeneratorParams {

}

export class ValuePatternGeneratorParams implements IValueGeneratorParams {
    valuePattern: string = "";
}

export class SelectFromListGeneratorParams implements IValueGeneratorParams {
    values: ISelectFromListItem[] = [];
    predefinedCategoriesIds: number[] = [];
}

export class ValueRangeGeneratorParams implements IValueGeneratorParams {
    dataType: TargetValueTypeEnum = TargetValueTypeEnum.IntegerNumber;
    minValue: string = "1";
    maxValue: string = "100";
}

export class IncrementalGeneratorParams implements IValueGeneratorParams {
    dataType: TargetValueTypeEnum = TargetValueTypeEnum.IntegerNumber;
    startValue: string = "1";
    endValue: string = "100";
    valueStep: string = "1";
}

export interface ISelectFromListItem {
    dataType: TargetValueTypeEnum,
    value: string
}

export enum ValueGeneratorTypeEnum {
    PatternParser = 0,
    SelectFromList = 1,
    ValueRange = 2,
    Incremental = 3
}

export enum TargetValueTypeEnum {
    String = 0,
    IntegerNumber = 1,
    FloatingPointNumber = 2,
    Boolean = 3,
    DateTime = 4
}

export enum CultureInfoTypeEnum {
    Ru = "ru-RU",
    EnUs = "en-US"
}