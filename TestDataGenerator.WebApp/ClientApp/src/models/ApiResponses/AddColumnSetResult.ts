﻿import { ICommonResult } from "./CommonResult";

export interface IAddColumnSetResult extends ICommonResult {
    columnSetId: number
}