﻿import { IColumn } from "../Column";
import { ICommonResult } from "./CommonResult";

export interface IValidateSingleColumnResult extends ICommonResult {
    checkedColumn: IColumn,
    isValid: boolean,
    validationError: string
}