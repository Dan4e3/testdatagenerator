﻿import { IPredefinedValueCollection } from "../PredefinedValueCollection";
import { ICommonResult } from "./CommonResult";

export interface IGetPredefinedValuesPaginatedResult extends ICommonResult {
    valuesCollection: IPredefinedValueCollection,
    pageNumber: number,
    valuesPerPage: number,
    totalValuesCount: number
}