﻿import { IColumn } from "../Column";
import { ICommonResult } from "./CommonResult";

export interface IUpdateColumnSetResult extends ICommonResult {
    columnSetId: number,
    updatedColumns: IColumn[]
}