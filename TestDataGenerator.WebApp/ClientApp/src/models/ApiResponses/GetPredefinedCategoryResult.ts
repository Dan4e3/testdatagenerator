﻿import { IPredefinedCategoryCollection } from "../PredefinedCategoryCollection";
import { ICommonResult } from "./CommonResult";

export interface IGetPredefinedCategoryResult extends ICommonResult  {
    data: IPredefinedCategoryCollection
}