﻿import { IGetDataRowDto } from "../Dto/GetDataRowDto";
import { ICommonResult } from "./CommonResult";

export interface IGetPreviewDatasetResult extends ICommonResult {
    generatedRows: IGetDataRowDto[]
}