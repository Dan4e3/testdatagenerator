﻿import { IGetFileExportGenerationRequestDto } from "../Dto/GetFileExportGenerationRequestDto";
import { ICommonResult } from "./CommonResult";

export interface IGetFileExportGenerationRequestsPaginatedResult extends ICommonResult {
    datasetRequests: IGetFileExportGenerationRequestDto[],
    pageNumber: number,
    valuesPerPage: number,
    totalValuesCount: number
}