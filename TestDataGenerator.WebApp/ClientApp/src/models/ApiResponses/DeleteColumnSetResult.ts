﻿import { ICommonResult } from "./CommonResult";

export interface IDeleteColumnSetResult extends ICommonResult {
    deletedColumnSetsCount: number
}