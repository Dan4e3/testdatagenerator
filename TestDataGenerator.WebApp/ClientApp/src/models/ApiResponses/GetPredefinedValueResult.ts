﻿import { IPredefinedValueCollection } from "../PredefinedValueCollection";
import { ICommonResult } from "./CommonResult";

export interface IGetPredefinedValueResult extends ICommonResult {
    data: IPredefinedValueCollection
}