﻿import { ICommonResult } from "./CommonResult";

export interface IDeletePredefinedCategoryResult extends ICommonResult {
    categoriesDeletedCount: number
}