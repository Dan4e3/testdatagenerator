﻿import { IPredefinedCategory } from "../PredefinedCategory";
import { ICommonResult } from "./CommonResult";

export interface IAddPredefinedCategoryResult extends ICommonResult {
    data: IPredefinedCategory
}