﻿import { ICommonResult } from "./CommonResult";

export interface IDeletePredefinedValuesResult extends ICommonResult {
    deletedValuesCount: number
}