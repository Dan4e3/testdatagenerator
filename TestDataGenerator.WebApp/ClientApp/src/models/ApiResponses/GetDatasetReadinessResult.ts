﻿import { ICommonResult } from "./CommonResult";

export interface IGetDatasetReadinessResult extends ICommonResult {
    resultReady: boolean,
    requestFailedToProcess: boolean,
    errorText: string,
    requestProcessedDate: Date | null
}