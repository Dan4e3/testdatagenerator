﻿import { IGetColumnSetDto } from "../Dto/GetColumnSetDto";
import { ICommonResult } from "./CommonResult";

export interface IGetUserColumnSetsResult extends ICommonResult {
    columnSets: IGetColumnSetDto[]
}