﻿import { IGetFileExportGenerationRequestDto } from "../Dto/GetFileExportGenerationRequestDto";
import { ICommonResult } from "./CommonResult";

export interface IGetAllFileExportGenerationRequestsResult extends ICommonResult {
    datasetRequests: IGetFileExportGenerationRequestDto[]
}