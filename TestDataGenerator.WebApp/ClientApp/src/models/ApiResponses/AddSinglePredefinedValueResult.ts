﻿import { IPredefinedValue } from "../PredefinedValue";
import { ICommonResult } from "./CommonResult";

export interface IAddSinglePredefinedValuesResult extends ICommonResult {
    createdValue: IPredefinedValue
}