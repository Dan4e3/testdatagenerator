﻿import { ICommonResult } from "./CommonResult";

export interface IAddFileExportRequestResult extends ICommonResult {
    requestId: number
}