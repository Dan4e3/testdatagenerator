﻿import { ICommonResult } from "./CommonResult";

export interface IAddPredefinedValuesResult extends ICommonResult {
    valuesAdded: number
}