﻿import { IValueGenerator } from "./ValueGenerator";
import { CultureInfoTypeEnum, TargetValueTypeEnum } from "./ValueGeneratorParams";

export interface IColumnSettings {
    id: number,
    columnName: string,
    targetValueType: TargetValueTypeEnum,
    culture: CultureInfoTypeEnum,
    valueSupplier: IValueGenerator
}