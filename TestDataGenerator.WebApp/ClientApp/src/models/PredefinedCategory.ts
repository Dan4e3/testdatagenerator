﻿export interface IPredefinedCategory {
    id: number,
    name: string,
    createdBy: string,
    isPublic: boolean,
    creationDate: Date
};