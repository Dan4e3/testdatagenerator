﻿import { TargetValueTypeEnum } from "./ValueGeneratorParams";

export interface IPredefinedValue {
    id: number,
    dataType: TargetValueTypeEnum,
    value: any,
    creationDate: Date
}