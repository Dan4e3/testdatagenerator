﻿import { IPredefinedCategory } from "./PredefinedCategory";

export interface IPredefinedCategoryCollection {
    categories: IPredefinedCategory[]
}