﻿import { IValueGeneratorParams, ValueGeneratorTypeEnum } from "./ValueGeneratorParams";

export interface IValueGenerator {
    generatorType: ValueGeneratorTypeEnum,
    generatorParameters: IValueGeneratorParams
}