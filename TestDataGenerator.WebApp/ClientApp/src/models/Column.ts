﻿import { IColumnSettings } from "./ColumnSettings";

export interface IColumn {
    columnSettings: IColumnSettings
}