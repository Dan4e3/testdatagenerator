﻿import { IColumnSet } from "./ColumnSet";
import { DatasetExportFileTypeEnum } from "./Enums/DatasetExportFileTypeEnum";

export interface IFileExportGenerationRequest {
    requestId: number,
    rowsToGenerate: number,
    columnSet: IColumnSet,
    fileExportType: DatasetExportFileTypeEnum,
    resultReady: boolean,
    requestFailedToProcess: boolean,
    errorText: string,
    requestProcessedDate: Date | null
}