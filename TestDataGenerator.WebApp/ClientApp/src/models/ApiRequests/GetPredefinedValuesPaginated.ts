﻿
export interface IGetPredefinedValuesPaginated {
    categoryName: string,
    pageNumber: number,
    valuesPerPage: number
}