﻿import { IColumn } from "../Column";

export interface IAddColumnSet {
    columns: IColumn[],
    columnSetName: string
}
