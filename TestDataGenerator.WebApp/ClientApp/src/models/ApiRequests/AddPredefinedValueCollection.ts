﻿import { IAddPredefinedValue } from "./AddPredefinedValue";

export interface IAddPredefinedValueCollection {
    categoryName: string,
    values: IAddPredefinedValue[]
}