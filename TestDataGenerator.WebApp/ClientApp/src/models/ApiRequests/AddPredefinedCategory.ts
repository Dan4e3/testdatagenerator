﻿export interface IAddPredefinedCategory {
    name: string,
    isPublic: boolean
}