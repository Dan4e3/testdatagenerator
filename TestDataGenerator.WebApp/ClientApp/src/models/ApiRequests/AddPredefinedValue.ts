﻿import { TargetValueTypeEnum } from "../ValueGeneratorParams";

export interface IAddPredefinedValue {
    dataType: TargetValueTypeEnum,
    value: any
}