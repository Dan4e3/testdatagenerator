﻿
export interface IGetDatasetFileGenerationRequestsPaginated {
    pageNumber: number,
    requestsPerPage: number
}