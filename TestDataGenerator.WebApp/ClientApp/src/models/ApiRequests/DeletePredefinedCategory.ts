﻿export interface IDeletePredefinedCategory {
    categoryId: number,
    categoryName: string
}