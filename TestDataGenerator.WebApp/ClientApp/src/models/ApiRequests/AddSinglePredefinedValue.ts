﻿import { IAddPredefinedValue } from "./AddPredefinedValue";

export interface IAddSinglePredefinedValue {
    valueInfo: IAddPredefinedValue,
    categoryName: string
}