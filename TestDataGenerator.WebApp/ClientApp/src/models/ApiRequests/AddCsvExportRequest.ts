﻿import { IFileExportDatasetParametersDrto } from "../Dto/FileExportDatasetParametersDrto";

export interface IAddCsvExportRequest {
    datasetParameters: IFileExportDatasetParametersDrto
}