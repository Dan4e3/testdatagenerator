﻿
export interface IAddNewUser {
    name: string,
    password: string,
    captchaToken: string
}