﻿import { IColumn } from "../Column";

export interface IUpdateColumnSet {
    id: number,
    name: string,
    columns: IColumn[]
}