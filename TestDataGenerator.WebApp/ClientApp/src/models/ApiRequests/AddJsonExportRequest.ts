﻿import { IFileExportDatasetParametersDrto } from "../Dto/FileExportDatasetParametersDrto";

export interface IAddJsonExportRequest {
    datasetParameters: IFileExportDatasetParametersDrto
}