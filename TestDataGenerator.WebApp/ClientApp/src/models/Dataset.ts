﻿
export interface IDataset {
    columnNames: string[],
    dataRows: string[][]
}