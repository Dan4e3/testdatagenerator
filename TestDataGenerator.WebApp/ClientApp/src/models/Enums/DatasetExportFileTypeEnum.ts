﻿
export enum DatasetExportFileTypeEnum {
    JSON = 0,
    CSV = 1
}