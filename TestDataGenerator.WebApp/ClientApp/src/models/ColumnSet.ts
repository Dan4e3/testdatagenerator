﻿import { ColumnStore } from "../stores/ColumnStore";

export interface IColumnSet {
    id: number,
    name: string,
    columnStore: ColumnStore,
    ownerUserName?: string,
    ownerUserId?: string
}