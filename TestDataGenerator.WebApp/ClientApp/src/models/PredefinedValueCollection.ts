﻿import { IPredefinedValue } from "./PredefinedValue";

export interface IPredefinedValueCollection {
    categoryName: string,
    values: IPredefinedValue[]
}