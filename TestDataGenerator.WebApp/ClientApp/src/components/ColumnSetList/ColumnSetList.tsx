﻿import React, { useEffect, useState } from "react";
import { observer } from "mobx-react";
import { useStores } from "../../use-stores";
import {
    Button, Box, Grid, Paper, FormControl,
    InputLabel, Select, MenuItem, Typography, SelectChangeEvent, Divider, Link
} from "@mui/material";
import ColumnSet from "../ColumnSet/ColumnSet";
import ModalNewColumnColumnSet from "../ModalNewColumnSet/ModalNewColumnSet";
import { IColumnSet } from "../../models/ColumnSet";
import AddIcon from '@mui/icons-material/Add';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import { useNavigate } from "react-router-dom";


const ColumnSetList = observer(() => {
    const [modalNewColumnSetIsOpen, setModalNewColumnSetOpen] = useState<boolean>(false);
    const [selectedColumnSet, setSelectedColumnSet] = useState<IColumnSet>();

    const { rootStore } = useStores();
    const navigate = useNavigate();

    useEffect(() => {
        (async () => await rootStore.columnSetStore.getAllColumnSetsOfUserAsync())();
    }, []);

    const handleColumnSetComboBoxChange = (event: SelectChangeEvent) => {
        const colSetFromId = rootStore.columnSetStore.columnSets.find(cs => cs.id === Number(event.target.value));
        setSelectedColumnSet(colSetFromId);
    };

    const handleRemoveColumnSetButtonClick = async () => {
        if (selectedColumnSet !== undefined) {
            await rootStore.columnSetStore.removeColumnSetAsync(selectedColumnSet);
            setSelectedColumnSet(undefined);
        }  
    };

    const handleNavigateLinkClick = (e: React.MouseEvent<HTMLElement>, path: string) => {
        e.preventDefault();
        navigate(path);
        return false;
    };

    return (
        <Grid spacing={3} container>
            {modalNewColumnSetIsOpen && (
                <ModalNewColumnColumnSet
                    isOpen={modalNewColumnSetIsOpen}
                    closeModal={() => setModalNewColumnSetOpen(false)}
                    getCreatedColumnSet={(createdColSet) => {
                        setSelectedColumnSet(createdColSet);
                    }}
                />
            )}
            <Grid xs={12} lg={12} item>
                <Paper elevation={6} sx={{ padding: 3 }}>
                    <Typography variant="h2" fontSize={30}>
                        Column sets
                    </Typography>
                    <Box marginTop={2} marginBottom={1}>
                        <Divider variant="fullWidth" />
                    </Box>
                    <Typography variant="body1">
                        Here you can manage your column sets and individual columns in them. Create a new column set, setup some columns (consider 
                        using values from <Link href="/predefined-data" onClick={(e) => handleNavigateLinkClick(e, "/predefined-data")}>Values Collection</Link>) and
                        go for the final step - <Link href="/dataset-generator" onClick={(e) => handleNavigateLinkClick(e, "/dataset-generator")}>Dataset Generation</Link>.
                    </Typography>
                </Paper>
            </Grid>
            <Grid xs={12} lg={12} item>
                <Grid container>
                    <Grid xs={12} lg={12} item>
                        <Paper elevation={3} sx={{ padding: 3 }}>
                            <Box marginBottom={2}>
                                <Typography variant="h5">
                                    Column set list
                                </Typography>
                                <Box marginTop={2}>
                                    <Divider variant="fullWidth" />
                                </Box>
                            </Box>
                            <FormControl fullWidth>
                                <InputLabel id="selected-columnset-label">Select column set</InputLabel>
                                <Select
                                    labelId="selected-columnset-label"
                                    value={selectedColumnSet?.id.toString() || ""}
                                    onChange={handleColumnSetComboBoxChange}
                                    label="Select column set">
                                    {
                                        rootStore.columnSetStore.columnSets.map((cs, i) => (
                                            <MenuItem key={cs.id} value={cs.id}>
                                                {cs.name} (Id={cs.id}, {cs.columnStore.columnsCount} column{cs.columnStore.columnsCount === 1 ? "" : "s"})
                                            </MenuItem>))
                                    }
                                </Select>
                            </FormControl>
                            <Grid container marginTop={1}>
                                <Grid xs={12} lg={12} item>
                                    <Box display="flex" justifyContent="flex-end">
                                        <Button
                                            color="primary"
                                            variant="contained"
                                            onClick={() => setModalNewColumnSetOpen(true)}
                                            sx={{ marginRight: 3 }}
                                            endIcon={<AddIcon />}
                                        >
                                            Add new column set
                                        </Button>
                                        <Button
                                            color="secondary"
                                            variant="contained"
                                            onClick={handleRemoveColumnSetButtonClick}
                                            disabled={selectedColumnSet === undefined || rootStore.columnSetStore.columnSetsCount === 0}
                                            endIcon={<DeleteForeverIcon />}
                                        >
                                            Remove column set
                                        </Button>
                                    </Box>
                                </Grid>
                            </Grid>
                        </Paper>
                    </Grid>
                    <Grid xs={12} lg={12} item marginTop={3}>
                        <Paper elevation={3} sx={{ padding: 3 }}>
                            <Box marginBottom={2}>
                                <Typography variant="h5">
                                    Columns in column set{" "}
                                    <Typography variant="h5" component="strong" >
                                        <b>{selectedColumnSet?.name === undefined ? "" : `"${selectedColumnSet.name}"`}</b>
                                    </Typography>
                                </Typography>
                                <Box marginTop={2}>
                                    <Divider variant="fullWidth" />
                                </Box>
                            </Box>
                            {selectedColumnSet !== undefined && <ColumnSet columnSet={selectedColumnSet} />}
                        </Paper>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
});

export default ColumnSetList;