﻿import { LoadingButton } from "@mui/lab";
import { Alert, AlertTitle,
    Box, Button, Card, Chip, CircularProgress, Divider, FormControl, Grid, IconButton, InputLabel, MenuItem, Paper, Select, SelectChangeEvent
    , Table, TableBody, TableCell, TableContainer, TableHead, TablePagination, TableRow, TextField, Tooltip, Typography
} from "@mui/material";
import { observer } from "mobx-react";
import React, { useEffect, useState } from "react";
import { IColumnSet } from "../../models/ColumnSet";
import { useStores } from "../../use-stores";
import DatasetOutlinedIcon from '@mui/icons-material/DatasetOutlined';
import FileDownloadOutlinedIcon from '@mui/icons-material/FileDownloadOutlined';
import { DatasetExportFileTypeEnum } from "../../models/Enums/DatasetExportFileTypeEnum";
import HelpOutlineIcon from '@mui/icons-material/HelpOutline';
import { grey } from "@mui/material/colors";
import RefreshOutlinedIcon from '@mui/icons-material/RefreshOutlined';

const DatasetFileExport = observer(() => {
    const [selectedColumnSet, setSelectedColumnSet] = useState<IColumnSet>();
    const [selecetedColumnSetId, setSelecetedColumnSetId] = useState<string>("");
    const [amountOfRowsToGenerate, setAmountOfRowsToGenerate] = useState<string>("100");
    const [rowsToGenerateFieldErr, setRowsToGenerateFieldErr] = useState<string>("");
    const [exportFileType, setExportFileType] = useState<DatasetExportFileTypeEnum>(DatasetExportFileTypeEnum.CSV);
    const [dataIsLoading, setDataIsLoading] = useState<boolean>(false);
    const [currentPage, setCurrentPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(5);

    const { rootStore } = useStores();
    const maxRowsToGenerate: number = Number(process.env.MAX_ROWS_IN_GENERATED_FILE);

    useEffect(() => {
        (async () => await rootStore.columnSetStore.getAllColumnSetsOfUserAsync())();

        (async () => await rootStore.datasetGeneratorStore.getFileExportRequestsPaginatedAsync({
            pageNumber: currentPage,
            requestsPerPage: rowsPerPage
        }))();

    }, []);

    useEffect(() => {
        const readinessFetcher = setInterval(() => {
            rootStore.datasetGeneratorStore.fileExportGeneratonRequests.map(async (req) => {
                if (!req.resultReady) {
                    await rootStore.datasetGeneratorStore.checkResultReadinessStateAsync(req.requestId);
                }
            });
        }, 5000);

        return () => {
            clearInterval(readinessFetcher);
        }

    }, [rootStore.datasetGeneratorStore.currentPageNum]);

    const handleSelectedColumnSetChange = (e: SelectChangeEvent) => {
        setSelecetedColumnSetId(e.target.value);
        const selectedId: number = Number(e.target.value);
        setSelectedColumnSet(rootStore.columnSetStore.columnSets.find(c => c.id === selectedId));
    };

    const handleAmountOfRowsToGenerateChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const inputAsNumber = Number(e.target.value);
        if (e.target.value.length > 0 && Number.isNaN(inputAsNumber))
            return;

        setAmountOfRowsToGenerate(e.target.value);
        if (e.target.value.length === 0) {
            setRowsToGenerateFieldErr("Field must not be empty!");
            return;
        }
        if (inputAsNumber > maxRowsToGenerate) {
            setRowsToGenerateFieldErr(`Maximum allowed rows amount is ${maxRowsToGenerate}!`);
            return;
        }
        setRowsToGenerateFieldErr("");
    };

    const handeExportFileTypeChange = (e: SelectChangeEvent) => {
        setExportFileType(parseInt(e.target.value));
    };

    const handleSubmitNewRequest = async () => {
        if (selectedColumnSet === undefined)
            return;

        setDataIsLoading(true);
        await rootStore.datasetGeneratorStore.createFileExportRequestAsync(selectedColumnSet,
            Number(amountOfRowsToGenerate), exportFileType);
        if (rootStore.datasetGeneratorStore.generationRequestsCount > rowsPerPage) {
            const lastPageNum =
                Math.floor(rootStore.datasetGeneratorStore.totalValuesCount / rootStore.datasetGeneratorStore.valuesPerPage);
            handleChangePage(null, lastPageNum);
        }
        setDataIsLoading(false);
    };

    const handleDownloadResultButtonClick = async (requestId: number) => {
        await rootStore.datasetGeneratorStore.downloadFileResultAsync(requestId);
    };

    const handleChangePage = async (event: unknown, newPage: number) => {
        try {
            setDataIsLoading(true);
            await rootStore.datasetGeneratorStore.getFileExportRequestsPaginatedAsync({
                pageNumber: newPage,
                requestsPerPage: rowsPerPage
            });
            setCurrentPage(newPage);
        }
        finally {
            setDataIsLoading(false);
        }
    };

    const handleChangeRowsPerPage = async (event: React.ChangeEvent<HTMLInputElement>) => {
        try {
            setDataIsLoading(true);
            const newRowsPerPage: number = parseInt(event.target.value, 10);
            setRowsPerPage(newRowsPerPage);
            setCurrentPage(0);

            await rootStore.datasetGeneratorStore.getFileExportRequestsPaginatedAsync({
                pageNumber: 0,
                requestsPerPage: newRowsPerPage
            });
        }
        finally {
            setDataIsLoading(false);
        }
    };

    const handleRefreshGenerationRequestsClick = async () => {
        handleChangePage(null, 0);
    };

    return (
        <Grid spacing={3} container>
            <Grid xs={12} lg={5} item>
                <Card>
                    <Grid container padding={3}>
                        <Grid xs={12} lg={12} item>
                            <Box marginBottom={2}>
                                <Typography variant="h5">
                                    Export settings
                                </Typography>
                            </Box>
                            <Divider variant="fullWidth" />
                        </Grid>
                        <Grid xs={12} lg={12} item>
                            <FormControl margin="normal" fullWidth>
                                <InputLabel id="select-columnset-label">Select column set</InputLabel>
                                <Select
                                    labelId="select-columnset-label"
                                    value={selecetedColumnSetId}
                                    onChange={handleSelectedColumnSetChange}
                                    label="Select column set">
                                    {rootStore.columnSetStore.columnSets.map(c =>
                                        <MenuItem key={c.id} value={c.id}>{c.name} (Id={c.id})</MenuItem>)}
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid xs={12} lg={12} item>
                            <TextField
                                margin="normal"
                                label="Amount of rows to generate"
                                variant="standard"
                                fullWidth
                                error={rowsToGenerateFieldErr.length > 0}
                                helperText={rowsToGenerateFieldErr}
                                value={amountOfRowsToGenerate}
                                onChange={handleAmountOfRowsToGenerateChange}
                            />
                        </Grid>
                        <Grid xs={12} lg={12} item>
                            <Alert severity="info">
                                <AlertTitle>Information</AlertTitle>
                                Due to restricted server resources current limit for rows count per dataset is <b>{maxRowsToGenerate}</b>.
                            </Alert>
                        </Grid>
                        <Grid xs={12} lg={12} item>
                            <FormControl margin="normal" fullWidth>
                                <InputLabel id="export-file-type-label">Export file type</InputLabel>
                                <Select
                                    labelId="export-file-type-label"
                                    value={exportFileType.toString()}
                                    onChange={handeExportFileTypeChange}
                                    label="Export file type">
                                    {Object.entries(DatasetExportFileTypeEnum)
                                        .filter(([key, val]) => isNaN(Number(key)))
                                        .map(([key, val], index) => (<MenuItem key={val} value={val}>{key}</MenuItem>))
                                    }
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid xs={12} lg={12} item marginTop={1}>
                            <LoadingButton
                                disabled={selectedColumnSet === undefined ||
                                    rowsToGenerateFieldErr.length > 0 || Number(amountOfRowsToGenerate) <= 0}
                                type="submit"
                                variant="contained"
                                color="primary"
                                endIcon={<DatasetOutlinedIcon />}
                                loadingPosition="end"
                                loading={dataIsLoading}
                                onClick={handleSubmitNewRequest}
                            >
                                Generate
                            </LoadingButton>
                        </Grid>
                    </Grid>
                </Card>
            </Grid>
            <Grid xs={12} lg={7} item>
                <Card>
                    <Grid container padding={3}>
                        <Grid xs={12} lg={12} item>
                            <Box marginBottom={2} display="flex" justifyContent="flex-start" alignItems="center">
                                <Typography variant="h5">
                                    Dataset generation requests
                                </Typography>
                                <Box marginLeft={1}>
                                    <Tooltip title="Refresh table">
                                        <span>
                                            <IconButton
                                                aria-label="refresh"
                                                color="primary" onClick={handleRefreshGenerationRequestsClick}
                                                disabled={rootStore.datasetGeneratorStore.generationRequestsCount === 0}>
                                                <RefreshOutlinedIcon />
                                            </IconButton>
                                        </span>
                                    </Tooltip>
                                </Box>
                            </Box>
                            <Divider variant="fullWidth" />
                        </Grid>
                        <Grid xs={12} lg={12} item>
                            <Alert severity="warning">
                                <AlertTitle>Warning</AlertTitle>
                                Be advised that all processed requests no matter successful or failed,
                                will be deleted after <strong>24 hours</strong> counted from the processing date. 
                            </Alert>
                        </Grid>
                        <Grid xs={12} lg={12} item marginTop={2}>
                            {dataIsLoading ? <Box display="flex" justifyContent="center"><CircularProgress color="primary" /></Box> : 
                                <TableContainer component={Paper}>
                                    <Table>
                                        <TableHead sx={{ backgroundColor: grey[100] }}>
                                            <TableRow>
                                                <TableCell><b>Request ID</b></TableCell>
                                                <TableCell><b>Column set</b></TableCell>
                                                <TableCell><b>Rows count</b></TableCell>
                                                <TableCell><b>Export format</b></TableCell>
                                                <TableCell><b>Processed on</b></TableCell>
                                                <TableCell><b>Ready?</b></TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {rootStore.datasetGeneratorStore.fileExportGeneratonRequests.map((r, i) => {
                                                return (
                                                    <TableRow key={i}>
                                                        <TableCell>{r.requestId}</TableCell>
                                                        <TableCell>{r.columnSet.name} (Id={r.columnSet.id})</TableCell>
                                                        <TableCell>{r.rowsToGenerate}</TableCell>
                                                        <TableCell>{DatasetExportFileTypeEnum[r.fileExportType]}</TableCell>
                                                        <TableCell>{r.requestProcessedDate === null ?
                                                            "-" : r.requestProcessedDate.toLocaleString()}</TableCell>
                                                        <TableCell>
                                                            {r.resultReady === false ? "Processing..." :
                                                                r.requestFailedToProcess === false ?
                                                                    <Button
                                                                        type="submit"
                                                                        variant="contained"
                                                                        color="primary"
                                                                        endIcon={<FileDownloadOutlinedIcon />}
                                                                        onClick={() => handleDownloadResultButtonClick(r.requestId)}>
                                                                        Download
                                                                    </Button>
                                                                    :
                                                                    <Box>
                                                                        <Tooltip title={r.errorText} arrow>
                                                                            <Chip icon={<HelpOutlineIcon />}
                                                                                label="Failed" color="error" sx={{ width: "100%" }} />
                                                                        </Tooltip>
                                                                    </Box>
                                                            }
                                                        </TableCell>
                                                    </TableRow>
                                                );
                                            })}
                                        </TableBody>
                                    </Table>
                                    <TablePagination
                                        rowsPerPageOptions={[5, 10, 25]}
                                        component="div"
                                        count={rootStore.datasetGeneratorStore.totalValuesCount}
                                        rowsPerPage={rowsPerPage}
                                        page={currentPage}
                                        onPageChange={handleChangePage}
                                        onRowsPerPageChange={handleChangeRowsPerPage}
                                        showFirstButton={true}
                                        showLastButton={true}
                                    />
                                </TableContainer>
                            }
                        </Grid>
                    </Grid>
                </Card>
            </Grid>
        </Grid>
    );
});

export default DatasetFileExport;