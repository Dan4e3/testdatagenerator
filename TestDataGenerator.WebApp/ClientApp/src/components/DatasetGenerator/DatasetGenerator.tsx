﻿import { Box, Divider, Grid, Link, Paper, Tab, Tabs, Typography } from "@mui/material";
import { observer } from "mobx-react";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import DatasetFileExport from "./DatasetFileExport";
import DatasetPreview from "./DatasetPreview";

const DatasetGenerator = observer(() => {
    const [selectedTabIndex, setSelectedTabIndex] = useState<number>(0);

    const navigate = useNavigate();

    const handleTabIndexChange = (event: React.SyntheticEvent, newValue: number) => {
        setSelectedTabIndex(newValue);
    };

    const handleNavigateLinkClick = (e: React.MouseEvent<HTMLElement>, path: string) => {
        e.preventDefault();
        navigate(path);
        return false;
    };

    return (
        <Grid spacing={3} container>
            <Grid xs={12} lg={12} item>
                <Paper elevation={6} sx={{ padding: 3 }}>
                    <Typography variant="h2" fontSize={30}>
                        Dataset generator
                    </Typography>
                    <Box marginTop={2} marginBottom={1}>
                        <Divider variant="fullWidth" />
                    </Box>
                    <Typography variant="body1">
                        Take a quick short preview of your future file dataset by selecting your previously designed column set. If you are not 
                        satisfied with the result, please go back to
                        {" "}<Link href="/column-sets" onClick={(e) => handleNavigateLinkClick(e, "/column-sets")}>Column Sets</Link> and 
                        make required edits to the column set. If everything is ok, go to dataset generation tab, fill in the generation parameters 
                        and hit "Generate" button. File will be prepared then within several seconds and available for download afterwards.
                    </Typography>
                </Paper>
            </Grid>
            <Grid xs={12} lg={12} item>
                <Paper elevation={3} sx={{ padding: 3 }}>
                    <Grid container>
                        <Grid xs={12} lg={12} item>
                            <Tabs value={selectedTabIndex} onChange={handleTabIndexChange} aria-label="basic tabs example">
                                <Tab label="Preview Dataset" />
                                <Tab label="Generate and Export to File" />
                            </Tabs>
                        </Grid>
                        <Grid xs={12} lg={12} item marginTop={3}>
                            {selectedTabIndex === 0 && (
                                <DatasetPreview />
                            )}
                            {selectedTabIndex === 1 && (
                                <DatasetFileExport />
                            )}
                        </Grid>
                    </Grid>
                </Paper>
            </Grid>
        </Grid>
    );
});

export default DatasetGenerator;