﻿import { LoadingButton } from "@mui/lab";
import {
    Box, Divider, FormControl, InputLabel, MenuItem, Paper, Select,
    SelectChangeEvent, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography
} from "@mui/material";
import { observer } from "mobx-react";
import React, { useEffect, useState } from "react";
import { IColumnSet } from "../../models/ColumnSet";
import { useStores } from "../../use-stores";
import RefreshOutlinedIcon from '@mui/icons-material/RefreshOutlined';
import { grey } from "@mui/material/colors";

const DatasetPreview = observer(() => {
    const [selectedColumnSet, setSelectedColumnSet] = useState<IColumnSet>();
    const [selecetedColumnSetId, setSelecetedColumnSetId] = useState<string>("");
    const [dataIsLoading, setDataIsLoading] = useState<boolean>(false);

    const { rootStore } = useStores();

    useEffect(() => {
        if (rootStore.columnSetStore.columnSetsCount === 0) {
            (async () => await rootStore.columnSetStore.getAllColumnSetsOfUserAsync())();
        }
    }, []);

    const handleSelectedColumnSetChange = async (e: SelectChangeEvent) => {
        setSelecetedColumnSetId(e.target.value);
        const selectedId: number = Number(e.target.value);
        const selectedColsetById: IColumnSet | undefined = rootStore.columnSetStore.columnSets.find(c => c.id === selectedId)
        setSelectedColumnSet(selectedColsetById);

        if (selectedColsetById !== undefined) {
            setDataIsLoading(true);
            await rootStore.datasetGeneratorStore.generatePreviewDatasetAsync(selectedColsetById);
            setDataIsLoading(false);
        }
    };

    const handleRefreshButtonClick = async () => {
        if (selectedColumnSet !== undefined) {
            setDataIsLoading(true);
            await rootStore.datasetGeneratorStore.generatePreviewDatasetAsync(selectedColumnSet);
            setDataIsLoading(false);
        }
    };

    return (
        <Box>
            <Box>
                <Typography variant="h5">
                    Column set
                </Typography>
            </Box>
            <Box marginBottom={2} component="form" display="flex" justifyContent="flex-start" alignItems="center">
                <FormControl margin="normal" sx={{ minWidth: 600 }} >
                    <InputLabel id="select-columnset-label">Select column set for dataset preview</InputLabel>
                    <Select
                        labelId="select-columnset-label"
                        value={selecetedColumnSetId}
                        onChange={handleSelectedColumnSetChange}
                        label="Select column set for dataset preview">
                        {rootStore.columnSetStore.columnSets.map(c =>
                            <MenuItem key={c.id} value={c.id}>{c.name} (Id={c.id}, Columns count: {c.columnStore.columnsCount})</MenuItem>)}
                    </Select>
                </FormControl>
                <Box marginLeft={3}>
                    <LoadingButton
                        disabled={selectedColumnSet === undefined}
                        variant="outlined"
                        onClick={handleRefreshButtonClick}
                        startIcon={<RefreshOutlinedIcon />}
                        loadingPosition="start"
                        size="large"
                        loading={dataIsLoading}
                        type="submit"
                    >
                        Regenerate preview
                    </LoadingButton>
                </Box>
            </Box>
            <Divider variant="fullWidth" />
            {rootStore.datasetGeneratorStore.previewDataset !== undefined && (
                <Box>
                    <Box marginY={2}>
                        <Typography variant="h5">
                            Dataset preview result
                        </Typography>
                    </Box>
                    <TableContainer component={Paper}>
                        <Table >
                            <TableHead sx={{ backgroundColor: grey[100] }}>
                                <TableRow>
                                    {
                                        rootStore.datasetGeneratorStore.previewDataset.columnNames.map(
                                            (c, i) => <TableCell key={i}><b>{c}</b></TableCell>)
                                    }
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {rootStore.datasetGeneratorStore.previewDataset.dataRows.map((r, i) => {
                                    return (
                                        <TableRow key={i}>
                                            {r.map((v, vi) => <TableCell key={vi}>{v}</TableCell>)}
                                        </TableRow>
                                    );
                                })}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Box>

            )}

        </Box>
    );
});

export default DatasetPreview;