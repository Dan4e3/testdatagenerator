﻿import React, { useState } from 'react';
import { useStores } from "../../use-stores";
import LoadingButton from '@mui/lab/LoadingButton';
import { TextField, Box, Typography, Paper, Link, Grid, Divider } from "@mui/material";
import LoginIcon from "@mui/icons-material/Login";
import { useNavigate } from 'react-router-dom';
import { Link as RouterLink } from 'react-router-dom';
import { Helmet } from "react-helmet";

interface IProps {
    nextRoutePath?: string
}

const Login = ({ nextRoutePath }: IProps) => {
    const [username, setUserName] = useState<string>("");
    const [password, setPassword] = useState<string>("");
    const [loginLoading, setLoginLoading] = useState<boolean>(false);

    const { rootStore } = useStores();
    const navigate = useNavigate();

    const handleSubmitLogin = async (e: React.FormEvent) => {
        e.preventDefault();

        setLoginLoading(true);
        let authResult: boolean = false;
        if (username !== undefined && password !== undefined)
            authResult = await rootStore.userStore.authenticateAsync(username, password);
        setLoginLoading(false);

        if (authResult)
            navigate(nextRoutePath === undefined ? "/" : nextRoutePath);
    };

    return (
        <Box
            component="form"
            noValidate
            autoComplete="off" width={400} marginX="auto">
            <Helmet>
                <title>Sign in - Test Data Generator</title>
                <meta name="description" content="Sign in into Test Data Generator application and start generating datasets now!" />
            </Helmet>
            <Paper elevation={6}>
                <Grid container padding={3}>
                    <Grid xs={12} lg={12} item marginBottom={1}>
                        <Typography variant="h5">
                            Login form
                        </Typography>
                    </Grid>
                    <Grid xs={12} lg={12} item>
                        <Divider variant="fullWidth" />
                        <TextField
                            required
                            label="Username"
                            type="text"
                            margin="normal"
                            fullWidth   
                            onChange={e => setUserName(e.target.value)}
                        />
                    </Grid>
                    <Grid xs={12} lg={12} item>
                        <TextField
                            required
                            label="Password"
                            type="password"
                            margin="normal"
                            fullWidth
                            onChange={e => setPassword(e.target.value)}
                        />
                    </Grid>
                    <Grid xs={12} lg={12} item marginTop={2}>
                        <Box display="flex" justifyContent="center">
                            <LoadingButton
                                variant="contained"
                                endIcon={<LoginIcon />}
                                onClick={handleSubmitLogin}
                                loadingPosition="end"
                                loading={loginLoading}
                                disabled={[username, password].find(f => f.length === 0) !== undefined}
                                type="submit"
                            >
                                Login
                            </LoadingButton>
                        </Box>
                    </Grid>
                    <Grid xs={12} lg={12} item textAlign="center" marginY={2}>
                        <Typography variant="caption">
                            No account? Quick registration <Link component={RouterLink} to="/register">is here</Link>.
                        </Typography>
                    </Grid>
                </Grid>
            </Paper>
        </Box>
    );
};

export default Login;