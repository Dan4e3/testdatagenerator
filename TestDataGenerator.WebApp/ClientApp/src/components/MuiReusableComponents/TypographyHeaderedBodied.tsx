﻿import { Typography } from "@mui/material";
import React from "react";

interface ITypographyHeaderedBodiedProps {
    headerText: string,
    bodyText: string
}

const TypographyHeaderedBodied = ({ headerText, bodyText }: ITypographyHeaderedBodiedProps) => {
    return (
        <Typography variant="subtitle1">
            {headerText}:{" "}
            <Typography variant="body1" component="span" color="text.secondary">
                {bodyText}
            </Typography>
        </Typography>
    );
};

export default TypographyHeaderedBodied;