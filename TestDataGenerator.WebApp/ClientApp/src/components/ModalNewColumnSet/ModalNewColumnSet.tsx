﻿import React, { useState } from "react";
import { Dialog, DialogTitle, DialogContent, DialogActions, TextField, Box } from "@mui/material";
import { ColumnStore } from "../../stores/ColumnStore";
import { useStores } from "../../use-stores";
import { IColumnSet } from "../../models/ColumnSet";
import { LoadingButton } from "@mui/lab";
import PublishIcon from '@mui/icons-material/Publish';

interface IProps {
    isOpen: boolean,
    closeModal: () => void,
    getCreatedColumnSet: (columnSet: IColumnSet) => void
}

const ModalNewColumnSet = ({ isOpen, closeModal, getCreatedColumnSet }: IProps) => {
    const [columnSetName, setColumnSetName] = useState<string>("");
    const [sendingSubmit, setSendingSubmit] = useState<boolean>(false);
    const { rootStore } = useStores();

    const handleColSetNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setColumnSetName(e.target.value);
    };

    const handleSubmit = async (e: React.FormEvent) => {
        e.preventDefault();
        setSendingSubmit(true);
        const newColumnSet: IColumnSet = {
            id: Date.now(),
            name: columnSetName,
            ownerUserName: rootStore.userStore.user.name,
            columnStore: new ColumnStore()
        };
        await rootStore.columnSetStore.addColumnSetAsync(newColumnSet);
        getCreatedColumnSet(newColumnSet);
        setSendingSubmit(false);
        closeModal();
    };

    return (
        <Dialog open={isOpen} onClose={closeModal} maxWidth="md" fullWidth={true}>
            <Box component="form"
                noValidate
                autoComplete="off">
                <DialogTitle>
                    Create new column set
                </DialogTitle>
                <DialogContent>
                    <TextField
                        margin="dense"
                        autoFocus
                        label="Column set name"
                        variant="standard"
                        fullWidth
                        onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                            handleColSetNameChange(e)
                        }
                    />
                </DialogContent>
                <DialogActions>
                    <LoadingButton
                        disabled={columnSetName.length === 0}
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        style={{ marginTop: "1rem" }}
                        onClick={handleSubmit}
                        endIcon={<PublishIcon />}
                        loadingPosition="end"
                        loading={sendingSubmit}
                    >
                        Submit
                    </LoadingButton>
                </DialogActions>
            </Box>
        </Dialog>
    );
};

export default ModalNewColumnSet;