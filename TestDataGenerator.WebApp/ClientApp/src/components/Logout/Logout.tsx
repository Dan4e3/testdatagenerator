﻿import React from 'react';
import { useNavigate } from 'react-router-dom';
import { useStores } from "../../use-stores";

const Logout = () => {

    const { rootStore } = useStores();
    const navigate = useNavigate();

    const handleLogout = async (e: React.FormEvent) => {
        e.preventDefault();
        await rootStore.userStore.logoutAsync();
        navigate("/login");
    };

    return (
        <form>
            <button type="submit" onClick={handleLogout} >Logout</button>
        </form>
    );
};

export default Logout;