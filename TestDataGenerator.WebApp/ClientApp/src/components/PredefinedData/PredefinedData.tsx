﻿import {
    Box, Button, Card, CardContent, Divider, FormControl, FormControlLabel, Grid,
    InputLabel, MenuItem, Paper, Select, SelectChangeEvent, Skeleton, Switch, Typography
} from "@mui/material";
import { observer } from "mobx-react";
import React, { useEffect, useState } from "react";
import { IDeletePredefinedCategory } from "../../models/ApiRequests/DeletePredefinedCategory";
import { IPredefinedCategory } from "../../models/PredefinedCategory";
import { useStores } from "../../use-stores";
import ModalNewPredefinedCategory from "../ModalNewPredefinedCategory/ModalNewPredefinedCategory";
import PredefinedValuesList from "./PredefinedValuesList";
import AddIcon from '@mui/icons-material/Add';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';

const PredefinedData = observer(() => {
    const [modalNewCategoryIsOpen, setModalNewCategoryIsOpen] = useState(false);
    const [showPublicCategoriesCheck, setShowPublicCategoriesCheck] = useState<boolean>(true);
    const [selectedCategory, setSelectedCategory] = useState<IPredefinedCategory>();
    const [dataLoadingState, setDataLoadingState] = useState<boolean>(true);

    const { rootStore } = useStores();

    useEffect(() => {
        try {
            setDataLoadingState(true);
            if (!rootStore.userStore.user.authenticated)
                (async () => await rootStore.predefinedCategoryStore.queryPublicCategoriesAsync())();
            else
                (async () => await rootStore.predefinedCategoryStore.queryUserCategoriesAsync(true))();
        }
        finally {
            setDataLoadingState(false);
        }
    }, [rootStore.userStore.user.authenticated]);

    const handleShowPublicCategoriesChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setShowPublicCategoriesCheck(event.target.checked);

        // Drop selected category choice if public boolean value 
        // prevents it from being persisted in categories list
        if (selectedCategory?.isPublic && !event.target.checked)
            setSelectedCategory(undefined);
    };

    const handlePredefinedCategoryChange = async (event: SelectChangeEvent) => {
        const id: number = Number(event.target.value);
        const newSelectedCategory = rootStore.predefinedCategoryStore.categories.find(cat => cat.id === id);
        setSelectedCategory(newSelectedCategory);
    };

    const handleDeleteCategoryButtonClick = async () => {
        if (selectedCategory !== undefined) {
            const categoriesToDelete: IDeletePredefinedCategory[] = [{
                categoryId: selectedCategory.id,
                categoryName: selectedCategory.name
            }];
            await rootStore.predefinedCategoryStore.deleteCategoriesAsync(categoriesToDelete);
            setSelectedCategory(undefined);
        }
    };

    return (
        <Grid spacing={3} container>
            {modalNewCategoryIsOpen && (
                <ModalNewPredefinedCategory
                    isOpen={modalNewCategoryIsOpen}
                    closeModal={() => setModalNewCategoryIsOpen(false)} />
            )}
            <Grid xs={12} lg={12} item>
                <Paper elevation={6} sx={{ padding: 3 }}>
                    <Typography variant="h2" fontSize={30}>
                        Values collection
                    </Typography>
                    <Box marginTop={2} marginBottom={1}>
                        <Divider variant="fullWidth" />
                    </Box>
                    <Typography variant="body1">
                        This is values collection page. Values are divided into thematic categories which might be public and therefore visible/editable 
                        for everyone. They might also be private, which means that you are the only one who has the control over them. Fill up existing 
                        category with values or create a new one, and use these items later during column's value generator setup.
                    </Typography>
                </Paper>
            </Grid>
            <Grid xs={12} lg={5} item>
                <Paper elevation={6} sx={{ padding: 3 }}>
                    <Grid container marginBottom={2}>
                        <Grid xs={12} lg={12} item>
                            <Typography variant="h5">
                                Data categories
                            </Typography>
                            <Box marginTop={2} marginBottom={1}>
                                <Divider variant="fullWidth" />
                            </Box>
                        </Grid>
                        <Grid xs={12} lg={12} item>
                            {rootStore.userStore.user.authenticated && (
                                <Box display="flex" justifyContent={{ xs: "flex-start", lg: "flex-end" }}>
                                    <FormControlLabel
                                        control={
                                            <Switch checked={showPublicCategoriesCheck} onChange={handleShowPublicCategoriesChange} />
                                        }
                                        label="Show publicly visible categories"
                                    />
                                </Box>
                            )}
                        </Grid>
                        <Grid xs={12} lg={12} item>
                            {dataLoadingState ? <Skeleton variant="rectangular" height={60} /> : (
                                <FormControl fullWidth margin="normal">
                                    <InputLabel id="data-categories-label">Select category</InputLabel>
                                    <Select
                                        labelId="data-categories-label"
                                        value={selectedCategory?.id.toString() || ""}
                                        onChange={handlePredefinedCategoryChange}
                                        label="Select category">
                                        {rootStore.predefinedCategoryStore.categories
                                            .filter(cat => {
                                                if (!showPublicCategoriesCheck && cat.isPublic)
                                                    return;
                                                return cat;
                                            })
                                            .map(cat => (<MenuItem key={cat.id} value={cat.id}>{cat.name}</MenuItem>))
                                        }
                                    </Select>
                                </FormControl>
                            )}
                        </Grid>
                    </Grid>
                    <Divider variant="fullWidth" />
                    {selectedCategory !== undefined && (
                        <Box marginTop={2}>
                            <Typography variant="h5" marginBottom={1}>
                                Selected category
                            </Typography>
                            <Divider variant="fullWidth" />
                            <Card variant="elevation" elevation={5}>
                                <CardContent>
                                    <Typography variant="h6">
                                        {selectedCategory.name}
                                    </Typography>
                                    <Typography variant="subtitle1">
                                        Category ID: <Typography component="span" variant="body1" color="text.secondary">{selectedCategory.id}</Typography>
                                    </Typography>
                                    <Typography variant="subtitle1">
                                        Created by: <Typography component="span" variant="body1" color="text.secondary">{selectedCategory.createdBy}</Typography>
                                    </Typography>
                                    <Typography variant="subtitle1">
                                        Creation date: <Typography component="span" variant="body1" color="text.secondary">{selectedCategory.creationDate.toLocaleString()}</Typography>
                                    </Typography>
                                    <Typography variant="subtitle1">
                                        Is public: <Typography component="span" variant="body1" color="text.secondary">{selectedCategory.isPublic ? "Yes" : "No"}</Typography>
                                    </Typography>
                                </CardContent>
                            </Card>
                        </Box>
                    )}
                    {rootStore.userStore.user.authenticated && (
                        <Grid container marginTop={2} justifyContent="space-between">
                            <Grid xs={12} lg={6} item>
                                <Box display="flex" justifyContent={{ xs: "center", lg: "flex-start" }}>
                                    <Button
                                        variant="contained"
                                        color="primary"
                                        onClick={() => setModalNewCategoryIsOpen(true)}
                                        endIcon={<AddIcon />} >
                                        Add new
                                    </Button>
                                </Box>
                            </Grid>
                            <Grid xs={12} lg={6} item>
                                <Box display="flex" justifyContent={{ xs: "center", lg: "flex-end" }}>
                                    <Button
                                        variant="contained"
                                        color="secondary"
                                        onClick={handleDeleteCategoryButtonClick}
                                        endIcon={<DeleteForeverIcon />}>
                                        Delete selected
                                    </Button>
                                </Box>
                            </Grid>
                        </Grid>
                    )}
                </Paper>
            </Grid>
            <Grid xs={12} lg={7} item>
                <Paper elevation={6} sx={{ padding: 3 }}>
                    <PredefinedValuesList
                        predefinedValues={rootStore.predefinedValueStore.predefinedValues}
                        selectedCategory={selectedCategory}
                        isDataLoading={dataLoadingState} />
                </Paper>
            </Grid>


        </Grid>
    );
});

export default PredefinedData;