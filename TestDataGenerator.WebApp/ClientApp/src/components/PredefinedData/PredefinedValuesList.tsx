﻿import {
    Box, Button, Checkbox, Divider, Grid, Paper, Skeleton, Table, TableBody,
    TableCell, TableContainer, TableHead, TablePagination, TableRow, Typography
} from "@mui/material";
import { observer } from "mobx-react";
import React, { useEffect, useState } from "react";
import { IPredefinedCategory } from "../../models/PredefinedCategory";
import { IPredefinedValue } from "../../models/PredefinedValue";
import { useStores } from "../../use-stores";
import ModalNewPredefinedValue from "../ModalNewPredefinedValue/ModalNewPredefinedValue";
import AddIcon from '@mui/icons-material/Add';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import { TargetValueTypeEnum } from "../../models/ValueGeneratorParams";
import { grey } from "@mui/material/colors";

interface IProps {
    predefinedValues: IPredefinedValue[],
    selectedCategory?: IPredefinedCategory,
    isDataLoading: boolean
}

const PredefinedValuesList = observer(({ predefinedValues, selectedCategory,
        isDataLoading }: IProps) => {
    const [newModalValueIsOpen, setModalNewValueIsOpen] = useState<boolean>(false);
    const [selectedValuesIds, setSelectedValuesIds] = useState<number[]>([]);
    const [currentPage, setCurrentPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(5);

    const { rootStore } = useStores();

    useEffect(() => {
        if (selectedCategory !== undefined) {
            handleChangePage(null, 0);
        }
        else {
            setCurrentPage(0);
            rootStore.predefinedValueStore.resetValuesList();
        }
    }, [selectedCategory]);

    const handleValueCheckboxClick = (event: React.ChangeEvent<HTMLInputElement>, valueId: number) => {
        const selectedIndex = selectedValuesIds.indexOf(valueId);
        let newSelected: number[] = [];

        if (selectedIndex === -1) {
            newSelected = newSelected.concat(selectedValuesIds, valueId);
        } else if (selectedIndex === 0) {
            newSelected = newSelected.concat(selectedValuesIds.slice(1));
        } else if (selectedIndex === selectedValuesIds.length - 1) {
            newSelected = newSelected.concat(selectedValuesIds.slice(0, -1));
        } else if (selectedIndex > 0) {
            newSelected = newSelected.concat(
                selectedValuesIds.slice(0, selectedIndex),
                selectedValuesIds.slice(selectedIndex + 1),
            );
        }

        setSelectedValuesIds(newSelected);
    };

    const handleSelectAllValuesCheckBoxClick = (event: React.ChangeEvent<HTMLInputElement>) => {
        if (event.target.checked) {
            const newSelected = predefinedValues.map(val => val.id);
            setSelectedValuesIds(newSelected);
        }
        else {
            setSelectedValuesIds([]);
        }
    };

    const handleDeleteSelectedValuesButtonClick = async () => {
        await rootStore.predefinedValueStore.deletePredefinedValuesAsync(selectedValuesIds);
        setSelectedValuesIds([]);
        if (rootStore.predefinedValueStore.valuesCount === 0 && currentPage >= 1)
            handleChangePage(null, currentPage - 1);
        else
            handleChangePage(null, currentPage);
    };

    const handleChangePage = async (event: unknown, newPage: number) => {
        if (selectedCategory !== undefined) {
            await rootStore.predefinedValueStore.getValuesByCategoryPaginatedAsync({
                categoryName: selectedCategory.name,
                pageNumber: newPage,
                valuesPerPage: rowsPerPage
            });
        }

        setCurrentPage(newPage);
    };

    const handleChangeRowsPerPage = async (event: React.ChangeEvent<HTMLInputElement>) => {
        const newRowsPerPage: number = parseInt(event.target.value, 10);
        setRowsPerPage(newRowsPerPage);
        setCurrentPage(0);

        if (selectedCategory !== undefined) {
            await rootStore.predefinedValueStore.getValuesByCategoryPaginatedAsync({
                categoryName: selectedCategory.name,
                pageNumber: 0,
                valuesPerPage: newRowsPerPage
            });
        }
    };

    const handleCloseModalNewPredefinedValue = () => {
        if (rootStore.predefinedValueStore.valuesCount > rootStore.predefinedValueStore.valuesPerPage) {
            const lastPageNum =
                Math.floor(rootStore.predefinedValueStore.totalValuesCount / rootStore.predefinedValueStore.valuesPerPage);
            handleChangePage(null, lastPageNum);
        }
        setModalNewValueIsOpen(false);
    };

    return (
        <Grid container>
            {newModalValueIsOpen && (
                <ModalNewPredefinedValue
                    isOpen={newModalValueIsOpen}
                    closeModal={handleCloseModalNewPredefinedValue}
                    currentCategory={selectedCategory}
                />
            )}
            <Grid xs={12} lg={12} item>
                <Typography variant="h5">
                    Values in category <Typography variant="h5" component="strong" ><b>{selectedCategory?.name !== undefined ?
                        `"${selectedCategory?.name}"` : ""}</b></Typography>
                </Typography>
                <Box marginY={2}>
                    <Divider variant="fullWidth" />
                </Box>
            </Grid>
            <Grid xs={12} lg={12} item marginY={2}>
                {isDataLoading ? <Skeleton variant="rounded" height={300} /> : (
                    <TableContainer component={Paper}>
                        <Table size="small" padding="normal">
                            <TableHead sx={{ backgroundColor: grey[100] }}>
                                <TableRow>
                                    <TableCell>
                                        <Checkbox
                                            color="primary"
                                            checked={predefinedValues.length > 0 && selectedValuesIds.length === predefinedValues.length}
                                            onChange={handleSelectAllValuesCheckBoxClick}
                                        />
                                    </TableCell>
                                    <TableCell><b>ID</b></TableCell>
                                    <TableCell><b>Value</b></TableCell>
                                    <TableCell><b>Type</b></TableCell>
                                    <TableCell><b>Created on</b></TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {predefinedValues.map(pv => (
                                    <TableRow key={pv.id} >
                                        <TableCell>
                                            <Checkbox
                                                color="primary"
                                                checked={selectedValuesIds.includes(pv.id)}
                                                onChange={e => handleValueCheckboxClick(e, pv.id)}
                                            />
                                        </TableCell>
                                        <TableCell>{pv.id}</TableCell>
                                        <TableCell>{pv.value.toString()}</TableCell>
                                        <TableCell>{TargetValueTypeEnum[pv.dataType]}</TableCell>
                                        <TableCell>{pv.creationDate.toLocaleString()}</TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                        <TablePagination
                            rowsPerPageOptions={[5, 10, 25]}
                            component="div"
                            count={rootStore.predefinedValueStore.totalValuesCount}
                            rowsPerPage={rowsPerPage}
                            page={currentPage}
                            onPageChange={handleChangePage}
                            onRowsPerPageChange={handleChangeRowsPerPage}
                            showFirstButton={true}
                            showLastButton={true}
                        />
                    </TableContainer>
                )}
            </Grid>
            <Grid xs={12} lg={12} item justifyContent="space-between">
                <Grid container>
                    <Grid xs={12} lg={6} item>
                        {selectedCategory !== undefined && (
                            <Box display="flex" justifyContent={{ xs: "center", lg: "flex-start" }}>
                                <Button
                                    disabled={selectedCategory === undefined}
                                    variant="contained"
                                    color="primary"
                                    endIcon={<AddIcon />}
                                    onClick={() => setModalNewValueIsOpen(true)} >
                                    Add new value
                                </Button>
                            </Box>
                        )}
                    </Grid>
                    <Grid xs={12} lg={6} item>
                        {selectedValuesIds.length > 0 && (
                            <Box display="flex" justifyContent={{ xs: "center", lg: "flex-end" }}>
                                <Button
                                    variant="contained"
                                    color="secondary"
                                    endIcon={<DeleteForeverIcon />}
                                    onClick={handleDeleteSelectedValuesButtonClick} >
                                    Delete selected values
                                </Button>
                            </Box>
                        )}
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
});

export default PredefinedValuesList;