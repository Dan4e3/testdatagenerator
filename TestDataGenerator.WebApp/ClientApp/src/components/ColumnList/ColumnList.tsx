﻿import { IconButton, List, ListItem, ListItemButton, Typography } from "@mui/material";
import { observer } from "mobx-react";
import { IColumn } from "../../models/Column";
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import { TargetValueTypeEnum } from "../../models/ValueGeneratorParams";
import React from "react";

interface IProps {
    columns: IColumn[],
    setSelectedColumnInList: (column: IColumn) => void,
    forcedSelectedColumn?: IColumn,
    removeClickedColumn: (column: IColumn) => void
}

const ColumnList = observer(({ columns, setSelectedColumnInList,
    forcedSelectedColumn, removeClickedColumn }: IProps) => {

    const handleColumnClick = (col: IColumn) => {
        setSelectedColumnInList(col);
    };

    const handleRemoveColumnButtonClick = (col: IColumn) => {
        removeClickedColumn(col);
    };

    return (
        <List sx={{ maxHeight: 500, overflow: 'auto' }}>
            {
                columns.map((column, i) => {
                    return (
                        <ListItem key={i}>
                            <ListItemButton
                                onClick={() => handleColumnClick(column)}
                                selected={column.columnSettings.id === forcedSelectedColumn?.columnSettings.id} >
                                <Typography variant="subtitle1">
                                    {i + 1}. {column.columnSettings.columnName} ({TargetValueTypeEnum[column.columnSettings.targetValueType]} type)
                                </Typography>
                            </ListItemButton>
                            <IconButton size="large" color="secondary" edge="end" onClick={() => handleRemoveColumnButtonClick(column) }>
                                <DeleteForeverIcon />
                            </IconButton>
                        </ListItem>
                    );
                })
            }
        </List>

    );
});

export default ColumnList;