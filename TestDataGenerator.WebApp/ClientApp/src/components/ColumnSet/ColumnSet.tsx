﻿import React, { useEffect, useState } from 'react';
import { observer } from 'mobx-react';
import {
    Button, Box, Grid, Card
} from "@mui/material";
import ColumnInfo from '../Column/ColumnInfo';
import ModalNewColumn from '../ModalNewColumn/ModalNewColumn';
import ColumnList from '../ColumnList/ColumnList';
import { IColumnSet } from '../../models/ColumnSet';
import { IColumn } from '../../models/Column';
import { useStores } from '../../use-stores';
import AddIcon from '@mui/icons-material/Add';
import ModeEditOutlineOutlinedIcon from '@mui/icons-material/ModeEditOutlineOutlined';

interface IProps {
    columnSet: IColumnSet
}

const ColumnSet = observer(({ columnSet }: IProps) => {
    const [modalNewColumnIsOpen, setModalNewColumnOpen] = useState(false);
    const [selectedColumnInList, setSelectedColumnInList] = useState<IColumn>();

    const { rootStore } = useStores();

    const handleColumnEditClick = (column?: IColumn) => {
        if (column !== undefined)
            setModalNewColumnOpen(true);
    };

    const handleAddNewColumnButtonClick = () => {
        setSelectedColumnInList(undefined);
        setModalNewColumnOpen(true);
    };

    const handleRemoveColumnButtonClick = async (col: IColumn) => {
        if (col !== undefined) {
            columnSet.columnStore.removeColumn(col);
            setSelectedColumnInList(columnSet.columnStore.columns[columnSet.columnStore.columnsCount - 1]);
            await rootStore.columnSetStore.updateColumnSetAsync(columnSet);
        }
    };

    const handleCloseModal = async (isSubmitted: boolean) => {
        setModalNewColumnOpen(false);
        setSelectedColumnInList(columnSet.columnStore.columns[columnSet.columnStore.columnsCount - 1]);
        if (isSubmitted) {
            await rootStore.columnSetStore.updateColumnSetAsync(columnSet);
        }
    };

    useEffect(() => {
        setSelectedColumnInList(undefined);
    }, [columnSet]);

    return (
        <Grid spacing={3} container>
            {modalNewColumnIsOpen && (
                <ModalNewColumn
                    isOpen={modalNewColumnIsOpen}
                    closeModal={(closedOnSubmit) => handleCloseModal(closedOnSubmit)}
                    columnStore={columnSet.columnStore}
                    existingColumn={columnSet.columnStore.getColumnById(selectedColumnInList?.columnSettings.id)}
                />
            )}
            <Grid xs={12} lg={6} item>
                {columnSet.columnStore.columnsCount > 0 && (
                    <Card variant="outlined">
                        <ColumnList
                            columns={columnSet.columnStore.columns}
                            setSelectedColumnInList={(col) => setSelectedColumnInList(col)}
                            removeClickedColumn={(col) => handleRemoveColumnButtonClick(col)}
                            forcedSelectedColumn={selectedColumnInList} />
                    </Card>
                )}
                <Box marginTop={2}>
                    <Button
                        color="primary"
                        variant="contained"
                        endIcon={<AddIcon />}
                        onClick={handleAddNewColumnButtonClick}
                    >
                        Add new column
                    </Button>
                </Box>
            </Grid>
            <Grid xs={12} lg={6} item>
                {columnSet.columnStore.getColumnById(selectedColumnInList?.columnSettings.id) !== undefined &&
                    <Card variant="outlined" >
                        <Box position="relative" padding={3}>
                            <ColumnInfo column={columnSet.columnStore.getColumnById(selectedColumnInList?.columnSettings.id) as IColumn} />
                            <Box position="absolute" right={5} top={5}>
                                <Button
                                    variant="text"
                                    size="large"
                                    color="primary"
                                    endIcon={<ModeEditOutlineOutlinedIcon />}
                                    onClick={() => handleColumnEditClick(selectedColumnInList)}
                                >
                                    Edit...
                                </Button>
                            </Box>
                        </Box>
                    </Card>
                }
            </Grid>
        </Grid>
    );
});

export default ColumnSet;