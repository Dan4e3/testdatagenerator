﻿import { Box, Typography } from "@mui/material";
import { observer } from "mobx-react";
import React from "react";
import { IColumnSet } from "../../models/ColumnSet";

interface IProps {
    columnSet: IColumnSet
}

const ColumnSetShortInfo = observer(({ columnSet }: IProps) => {

    return (
        <Box>
            <Typography variant="h6">
                {columnSet.name}
            </Typography>
            <Typography variant="subtitle2">
                ID: {columnSet.id}
            </Typography>
            <Typography variant="body1">
                Columns count: {columnSet.columnStore.columnsCount} columns
            </Typography>
        </Box>
    );
});

export default ColumnSetShortInfo;