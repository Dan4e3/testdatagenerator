﻿import { Box, Divider, Grid, Link, Paper, Typography } from "@mui/material";
import React from "react";
import { useNavigate } from "react-router-dom";
import { TargetValueTypeEnum, ValueGeneratorTypeEnum } from "../../models/ValueGeneratorParams";
import { Helmet } from "react-helmet";

const Reference = () => {

    const navigate = useNavigate();

    const handleNavigateLinkClick = (e: React.MouseEvent<HTMLElement>, path: string) => {
        e.preventDefault();
        navigate(path);
        return false;
    };

    return (
        <Paper elevation={6} sx={{ padding: 3 }}>
            <Helmet>
                <title>TDG Application Reference - Test Data Generator</title>
                <meta name="description" content="Having some trouble with Test Data Generator (TDG) application? This is a short reference page for Test Data Generator application with some hints on how to use TDG." />
            </Helmet>
            <Grid container>
                <Grid xs={12} lg={12} item marginBottom={2}>
                    <Typography variant="h4">
                        Test Data Generator Reference
                    </Typography>
                    <Box marginY={2}>
                        <Divider />
                    </Box>
                </Grid>
                <Grid xs={12} lg={12} item>
                    <Typography>
                        Current reference describes usage of web version of Test Data Generator app. If 
                        you are interested in C# code API reference, please visit
                        {" "}<Link href="https://gitlab.com/Dan4e3/testdatagenerator/-/wikis/home" target="_blank">project wiki page</Link> at GitLab. If you 
                        have any questions, feel free to contact
                        {" "}<Link href="mailto:support@data-gen.com">support@data-gen.com</Link>.
                    </Typography>
                </Grid>
                <Grid xs={12} lg={12} item>
                    <Box marginTop={3} marginBottom={1}>
                        <Typography variant="h5">
                            Data types
                        </Typography>
                    </Box>
                    <Typography variant="body1">
                        At moment, following value data types are usable within application:
                    </Typography>
                    <Typography variant="body1" component="div">
                        <ol>
                            <li><b>{TargetValueTypeEnum[TargetValueTypeEnum.String]}</b> - string value. For example: "test value of your choice".</li>
                            <li><b>{TargetValueTypeEnum[TargetValueTypeEnum.IntegerNumber]}</b> - integer number value. 
                                Either positive or negative. Internally Int64 is used. For example: "1402931".</li>
                            <li><b>{TargetValueTypeEnum[TargetValueTypeEnum.FloatingPointNumber]}</b> - floating-point number value.
                                Either positive or negative. For example: "15.13234".</li>
                            <li><b>{TargetValueTypeEnum[TargetValueTypeEnum.DateTime]}</b> - date with time.
                                Following DateTime pattern should be used when providing values to inputs: 
                                "MM-dd-yyyy HH-mm-ss". "MM" - month number, from 01 to 12; "dd" - day of month, from 01 to 31; 
                                "yyyy" - year, from 0001 to 4000; "HH" - 24-hour format hours, from 00 to 23; "mm" - minutes, from 00 to 59; 
                                "ss" - seconds, from 00 to 59. Time part can be omitted. 
                                For example: "12.31.2000 00:24:13", "04/15/1959", "02-05-1999 12:15:00".
                            </li>
                            <li><b>{TargetValueTypeEnum[TargetValueTypeEnum.Boolean]}</b> - either "true" or "false" and no other way.</li>
                        </ol>
                    </Typography>
                    <Typography variant="body1">
                        If you don't want to take column cultural settings into consideration, feel free to use <b>{TargetValueTypeEnum[TargetValueTypeEnum.String]}</b> 
                        {" "}data type where possible. This data type behaves indiffirently towards culture settings and will display generated values 
                        the same way you inserted them. Especially
                        useful in <Link href="/predefined-data" onClick={(e) => handleNavigateLinkClick(e, "/predefined-data")}>values collection</Link>.
                    </Typography>
                </Grid>
                <Grid xs={12} lg={12} item>
                    <Box marginTop={3} marginBottom={1}>
                        <Typography variant="h5">
                            Column settings
                        </Typography>
                    </Box>
                    <Typography variant="body1">
                        Every single column in your setup requires following parameters to be defined:
                    </Typography>
                    <Typography variant="body1" component="div">
                        <ol>
                            <li><b>Column name.</b> Any column name required for your dataset. Will be shown in headers row in CSV files and 
                            is used as object property in JSON files.</li>
                            <li><b>Target value type.</b> Desired value type for your column. Default is {TargetValueTypeEnum[TargetValueTypeEnum.String]}
                                {" "}which means that no special type-bound formatting and sanity checks would be applied to generated values, depending on 
                                culture setting. If you choose data type different from {TargetValueTypeEnum[TargetValueTypeEnum.String]}, all values 
                                within column will be casted (or attempted at least) to this type.
                            </li>
                            <li><b>Value representation culture.</b> Apply special formatting rules for non-string values. Like different 
                                decimal separators for floating-point values: "." and ",", or DateTime display formats and date separators: 
                                "12.25.2001, 13:19:29" and "8/28/3789 11:33:00 AM".
                            </li>
                            <li><b>Value generator type.</b> Value generator for your columns. Each has its own specific settings, check them out at corresponding 
                                {" "}<Link href="#value-generators-section">reference section</Link>.</li>
                        </ol>
                    </Typography>
                </Grid>
                <Grid xs={12} lg={12} item id="value-generators-section">
                    <Box marginTop={3} marginBottom={1}>
                        <Typography variant="h5">
                            Value generators
                        </Typography>
                    </Box>
                    <Typography variant="body1">
                        Value generators are specialized program units which handle the way values are "born" into your resulting dataset.
                        There are {Object.keys(ValueGeneratorTypeEnum).filter(x => Number.isNaN(x)).length} generators available:
                    </Typography>
                    <Typography variant="body1" component="div">
                        <ul>
                            <li><b>{ValueGeneratorTypeEnum[ValueGeneratorTypeEnum.PatternParser]}</b></li>
                            <li><b>{ValueGeneratorTypeEnum[ValueGeneratorTypeEnum.SelectFromList]}</b></li>
                            <li><b>{ValueGeneratorTypeEnum[ValueGeneratorTypeEnum.ValueRange]}</b></li>
                            <li><b>{ValueGeneratorTypeEnum[ValueGeneratorTypeEnum.Incremental]}</b></li>
                        </ul>
                    </Typography>
                    <Box marginTop={1} marginBottom={1}>
                        <Typography variant="subtitle1">
                            <b>{ValueGeneratorTypeEnum[ValueGeneratorTypeEnum.PatternParser]}</b>
                        </Typography>
                        <Typography variant="body1">
                            Default value generator. Uses character patterns to generate values in a corresponding way. 
                            Basic controlling elements of patterns are:
                        </Typography>
                        <Typography variant="body1" component="div">
                            <ul>
                                <li><code>[]</code> - indicates predicate with available characters</li>
                                <li><code>{"{}"}</code> - indicates amount of characters to generate</li>
                                <li><code>,</code> - seperates multiple characters or characters range within one predicate (<code>[]</code>)</li>
                                <li><code>-</code> - specifies range of values to select from or range of number of values to generate</li>
                            </ul>
                        </Typography>
                        <Typography variant="body1">
                            For example:
                        </Typography>
                        <Typography variant="body1" component="div">
                            <ul>
                                <li><code>[A-z]</code> - generates single character value between "A" and "z" according
                                    to <Link href="https://en.wikipedia.org/wiki/List_of_Unicode_characters">Unicode characters table</Link>.</li>
                                <li><code>[0-9]</code> - generates single character value between "0" and "9".</li>
                                <li><code>[b-g,5-8]</code> - will generate single char from range "b-g" or "5-8".</li>
                                <li><code>{"[A-z]{1-5}[2-8]{4-9}"}</code> - will generate from 1 to 5 chars within "A-z" range and from 4 to 9 chars within "2-8" range.</li>
                                <li><code>{"[A-z]{3}[ ][0-9]{2}"}</code> - generates strictly 3 characters within "A-z" range,
                                    then only one space and finally 2 characters in "0-9" range</li>
                            </ul>
                        </Typography>
                    </Box>
                    <Box marginTop={1} marginBottom={1}>
                        <Typography variant="subtitle1">
                            <b>{ValueGeneratorTypeEnum[ValueGeneratorTypeEnum.SelectFromList]}</b>
                        </Typography>
                        <Typography variant="body1">
                            Randomly selects values from provided pool. Pool can be defined by setting exact values in "Create column" interface 
                            or by setting categories (which is supposed to be created and filled with values first). In-place values and categories 
                            can be combined together in a single column setup.
                        </Typography>
                    </Box>
                    <Box marginTop={1} marginBottom={1}>
                        <Typography variant="subtitle1">
                            <b>{ValueGeneratorTypeEnum[ValueGeneratorTypeEnum.ValueRange]}</b>
                        </Typography>
                        <Typography variant="body1">
                            Generates random value within a defined min/max range of values.
                        </Typography>
                    </Box>
                    <Box marginTop={1} marginBottom={1}>
                        <Typography variant="subtitle1">
                            <b>{ValueGeneratorTypeEnum[ValueGeneratorTypeEnum.Incremental]}</b>
                        </Typography>
                        <Typography variant="body1">
                            Generates values by applying step (increasing or decreasing one) to starting value. Keep in touch that upper bound 
                            is not acknowledged by dataset generator yet, meaning that if you willing to generate more rows of data than 
                            incremental generator can produce - it will simply fail.
                        </Typography>
                    </Box>
                </Grid>
            </Grid>
        </Paper>
    );
};

export default Reference;