﻿import { LoadingButton } from "@mui/lab";
import {
    Box, Divider, Grid, Paper, TextField, Link, Typography
} from "@mui/material";
import React, { useState } from "react";
import { Link as RouterLink, useNavigate } from 'react-router-dom';
import { toast } from "react-toastify";
import { useStores } from "../../use-stores";
import LoginIcon from "@mui/icons-material/Login";
import { useGoogleReCaptcha } from "react-google-recaptcha-v3";
import { Helmet } from "react-helmet";

const Registration = () => {
    const [userName, setUserName] = useState<string>("");
    const [userNameErrText, setUserNameErrText] = useState<string>("");
    const [password, setPassword] = useState<string>("");
    const [passwordErrText, setPasswordErrText] = useState<string>("");
    const [repeatPassword, setRepeatPassword] = useState<string>("");
    const [repeatPasswordErrText, setRepeatPasswordErrText] = useState<string>("");
    const [registrationLoading, setRegistrationLoading] = useState<boolean>(false);

    const { rootStore } = useStores();
    const navigate = useNavigate();
    const { executeRecaptcha } = useGoogleReCaptcha();

    const maxPasswordLen: number = 16;
    const minPasswordLen: number = 6;
    const minUsernameLen: number = 5;

    const handleRegistrationButtonClick = async (e: React.FormEvent) => {
        e.preventDefault();
        if (!executeRecaptcha) {
            toast.warning("Wait for ReCaptcha, please...");
            return;
        }

        const captchaToken = await executeRecaptcha("registration");

        try {
            setRegistrationLoading(true);

            const registrationResult: boolean = await rootStore.userStore.registerAsync({
                name: userName,
                password: password,
                captchaToken: captchaToken
            });

            if (registrationResult)
                navigate("/login");
        }
        finally {
            setRegistrationLoading(false);
        }
    };

    const handlePasswordFieldChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setPassword(e.target.value);
        if (e.target.value.length < minPasswordLen) {
            setPasswordErrText(`Password must be at least ${minPasswordLen} characters long.`);
            return;
        }
        setPasswordErrText("");
    };

    const handleRepeatPasswordFieldChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setRepeatPassword(e.target.value);
        if (e.target.value !== password) {
            setRepeatPasswordErrText("Passwords mismatch.");
            return;
        }
        setRepeatPasswordErrText("");
    };

    const handleUsernameFieldChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setUserName(e.target.value);
        if (e.target.value.length < minUsernameLen) {
            setUserNameErrText(`Username must be at least ${minUsernameLen} characters long.`);
            return;
        }
        setUserNameErrText("");
    };

    return (
        <Box width={400} marginX="auto" component="form"
            noValidate
            autoComplete="off">
            <Helmet>
                <title>Sign up - Test Data Generator</title>
                <meta name="description" content="Sign up now and get unlimited access to test data generation and export to CSV/JSON files! No email or other personal data required." />
            </Helmet>
            <Paper elevation={6}>
                <Grid container padding={3}>
                    <Grid xs={12} lg={12} item marginBottom={1}>
                        <Typography variant="h5">
                            Registration form
                        </Typography>
                    </Grid>
                    <Grid xs={12} lg={12} item>
                        <Divider variant="fullWidth" />
                        <TextField
                            label="Username"
                            value={userName}
                            error={userNameErrText.length > 0}
                            helperText={userNameErrText}
                            onChange={handleUsernameFieldChange} required margin="normal" fullWidth />
                    </Grid>
                    <Grid xs={12} lg={12} item>
                        <TextField label="Password" type="password"
                            value={password}
                            inputProps={{ maxLength: maxPasswordLen }}
                            error={passwordErrText.length > 0}
                            helperText={passwordErrText}
                            onChange={handlePasswordFieldChange} required margin="normal" fullWidth />
                    </Grid>
                    <Grid xs={12} lg={12} item>
                        <TextField label="Repeat password" type="password"
                            value={repeatPassword}
                            inputProps={{ maxLength: maxPasswordLen }}
                            error={repeatPasswordErrText.length > 0}
                            helperText={repeatPasswordErrText}
                            onChange={handleRepeatPasswordFieldChange} required margin="normal" fullWidth />
                    </Grid>
                    <Grid xs={12} lg={12} item textAlign="center">
                        <small>
                            This site is protected by reCAPTCHA and the Google
                            {" "}<a href="https://policies.google.com/privacy">Privacy Policy</a> and
                            {" "}<a href="https://policies.google.com/terms">Terms of Service</a> apply.
                        </small>
                    </Grid>
                    <Grid xs={12} lg={12} item marginTop={2}>
                        <Box display="flex" justifyContent="center">
                            <LoadingButton
                                type="submit"
                                variant="contained"
                                onClick={handleRegistrationButtonClick}
                                endIcon={<LoginIcon />}
                                disabled={
                                    [userName, password, repeatPassword].find(f => f.length === 0) !== undefined ||
                                    [passwordErrText, repeatPasswordErrText, userNameErrText].find(f => f.length > 0) !== undefined
                                }
                                loadingPosition="end"
                                loading={registrationLoading}
                                color="primary">
                                Register
                            </LoadingButton>
                        </Box>
                    </Grid>
                    <Grid xs={12} lg={12} item textAlign="center" marginY={2}>
                        <Typography variant="caption">
                            Back to <Link component={RouterLink} to="/login">login</Link>.
                        </Typography>
                    </Grid>
                </Grid>
            </Paper>
        </Box>
    );
};

export default Registration;