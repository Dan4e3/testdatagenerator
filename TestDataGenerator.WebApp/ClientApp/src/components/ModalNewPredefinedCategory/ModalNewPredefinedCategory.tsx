﻿import {
    Box, Button, Checkbox, Dialog, DialogActions, DialogContent,
    DialogTitle, FormControlLabel, TextField
} from "@mui/material";
import { observer } from "mobx-react";
import React, { useState } from "react";
import { IAddPredefinedCategory } from "../../models/ApiRequests/AddPredefinedCategory";
import { useStores } from "../../use-stores";

interface IProps {
    isOpen: boolean,
    closeModal: () => void,
}

const ModalNewPredefinedCategory = observer(({ isOpen, closeModal }: IProps) => {
    const [categoryName, setCategoryName] = useState<string>("");
    const [categoryPublic, setCategoryPublic] = useState<boolean>(false);

    const { rootStore } = useStores();

    const handleCategoryNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setCategoryName(e.target.value);
    };

    const handleCategoryPublicChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setCategoryPublic(e.target.checked);
    };

    const handleSubmitButtonClick = async (e: React.FormEvent) => {
        e.preventDefault();
        const categoryToAdd: IAddPredefinedCategory = {
            name: categoryName,
            isPublic: categoryPublic
        };
        const createResult = await rootStore.predefinedCategoryStore.createNewCategoryAsync(categoryToAdd);
        if (createResult)
            closeModal();
    };

    return (
        <Dialog open={isOpen} onClose={closeModal} maxWidth="md" fullWidth={true}>
            <Box
                component="form"
                noValidate
                autoComplete="off">
                <DialogTitle>
                    Create new data category
                </DialogTitle>
                <DialogContent>
                    <TextField
                        margin="dense"
                        autoFocus
                        label="Category name"
                        variant="standard"
                        fullWidth
                        inputProps={{ maxLength: 100 }}
                        value={categoryName}
                        onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                            handleCategoryNameChange(e)
                        }
                    />
                    <FormControlLabel
                        control={<Checkbox value={categoryPublic} onChange={handleCategoryPublicChange} />}
                        label="Make public?"
                        labelPlacement="end" />
                </DialogContent>
                <DialogActions>
                    <Button
                        disabled={categoryName.length === 0}
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        style={{ marginTop: "1rem" }}
                        onClick={handleSubmitButtonClick}
                    >
                        Submit
                    </Button>
                </DialogActions>
            </Box>
        </Dialog>
    );
});

export default ModalNewPredefinedCategory;