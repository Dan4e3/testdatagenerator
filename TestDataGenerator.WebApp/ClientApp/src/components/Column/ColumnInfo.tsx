﻿import React from 'react';
import { observer } from 'mobx-react';
import { Box, Divider, Typography } from "@mui/material";
import { IColumn } from '../../models/Column';
import {
    IncrementalGeneratorParams, SelectFromListGeneratorParams, TargetValueTypeEnum,
    ValueGeneratorTypeEnum, ValuePatternGeneratorParams, ValueRangeGeneratorParams
} from '../../models/ValueGeneratorParams';
import TypographyHeaderedBodied from '../MuiReusableComponents/TypographyHeaderedBodied';

interface IProps {
    column: IColumn
}

const ColumnInfo = observer(({ column }: IProps) => {
    return (
        <Box>
            <Box marginBottom={2}>
                <Typography variant="h5">
                    Column properties
                </Typography>
                <TypographyHeaderedBodied headerText="Column name" bodyText={column.columnSettings.columnName} />
                <TypographyHeaderedBodied headerText="Target value type" bodyText={TargetValueTypeEnum[column.columnSettings.targetValueType]} />
                <TypographyHeaderedBodied headerText="Column culture" bodyText={column.columnSettings.culture} />
            </Box>
            <Divider variant="middle" />
            <Box marginTop={2}>
                <Typography variant="h5">
                    Generator properties
                </Typography>
                <TypographyHeaderedBodied headerText="Generator type" bodyText={ValueGeneratorTypeEnum[column.columnSettings.valueSupplier.generatorType]} />
                {column.columnSettings.valueSupplier.generatorType == ValueGeneratorTypeEnum.PatternParser &&
                    <TypographyHeaderedBodied headerText="Value pattern" 
                        bodyText={(column.columnSettings.valueSupplier.generatorParameters as ValuePatternGeneratorParams)?.valuePattern} />
                }
                {column.columnSettings.valueSupplier.generatorType == ValueGeneratorTypeEnum.SelectFromList &&
                    <Box>
                        <TypographyHeaderedBodied headerText="Total in-place items"
                            bodyText={(column.columnSettings.valueSupplier.generatorParameters as SelectFromListGeneratorParams)?.values.length.toString()} />
                        <TypographyHeaderedBodied headerText="First 10 in-place items"
                            bodyText={(column.columnSettings.valueSupplier.generatorParameters as SelectFromListGeneratorParams)?.values
                                .filter((v, i) => i < 10)
                                .map(v => v.value)
                                .join(", ")} />
                        <TypographyHeaderedBodied headerText="Included categories IDs"
                            bodyText={
                                (column.columnSettings.valueSupplier.generatorParameters as SelectFromListGeneratorParams)
                                    ?.predefinedCategoriesIds
                                    .join(", ")}
                        />
                    </Box>
                }
                {column.columnSettings.valueSupplier.generatorType == ValueGeneratorTypeEnum.ValueRange &&
                    <Box>
                        <TypographyHeaderedBodied headerText="Value range type"
                            bodyText={TargetValueTypeEnum[(column.columnSettings.valueSupplier.generatorParameters as ValueRangeGeneratorParams)?.dataType]} />
                        <TypographyHeaderedBodied headerText="Minimum value"
                            bodyText={(column.columnSettings.valueSupplier.generatorParameters as ValueRangeGeneratorParams)?.minValue} />
                        <TypographyHeaderedBodied headerText="Maximum value"
                            bodyText={(column.columnSettings.valueSupplier.generatorParameters as ValueRangeGeneratorParams)?.maxValue} />
                    </Box>
                }
                {column.columnSettings.valueSupplier.generatorType == ValueGeneratorTypeEnum.Incremental &&
                    <Box>
                        <TypographyHeaderedBodied headerText="Value increment type"
                            bodyText={TargetValueTypeEnum[(column.columnSettings.valueSupplier.generatorParameters as IncrementalGeneratorParams)?.dataType]} />
                        <TypographyHeaderedBodied headerText="Starting value"
                            bodyText={(column.columnSettings.valueSupplier.generatorParameters as IncrementalGeneratorParams)?.startValue} />
                        <TypographyHeaderedBodied headerText="End value"
                            bodyText={(column.columnSettings.valueSupplier.generatorParameters as IncrementalGeneratorParams)?.endValue} />
                        <TypographyHeaderedBodied headerText="Value step"
                            bodyText={(column.columnSettings.valueSupplier.generatorParameters as IncrementalGeneratorParams)?.valueStep} />
                    </Box>
                }
            </Box>
        </Box>
    );
});

export default ColumnInfo;