﻿import {
    Accordion, AccordionDetails, AccordionSummary,
    Box, Divider, Grid, Link, Paper, Typography
} from "@mui/material";
import React from "react";
import HelpOutlineRoundedIcon from '@mui/icons-material/HelpOutlineRounded';
import TocRoundedIcon from '@mui/icons-material/TocRounded';
import ArrowForwardOutlinedIcon from '@mui/icons-material/ArrowForwardOutlined';
import { useNavigate } from "react-router-dom";
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import MoreOutlinedIcon from '@mui/icons-material/MoreOutlined';

const linksStyle = {
    textDecoration: "none",
    color: "primary.main",
    "&:hover": {
        textDecoration: "underline",
        cursor: "pointer"
    }
} as const;

const Home = () => {
    const [accordionExpanded, setAccordionExpanded] = React.useState<string | false>(""); 

    const navigate = useNavigate();

    const paragraphStyle = {
        textIndent: 30
    } as const;

    const handleNavigateLinkClick = (e: React.MouseEvent<HTMLElement>, path: string) => {
        e.preventDefault();
        navigate(path);
        return false;
    };

    const handleAccordionExpandChange =
        (panel: string) =>
            (event: React.SyntheticEvent, isExpanded: boolean) => {
                setAccordionExpanded(isExpanded ? panel : false);
    };


    return (
        <Grid spacing={3} container>
            <Grid xs={12} lg={6} item>
                <Paper elevation={6} sx={{padding: 3}}>
                    <Box display="flex" alignItems="center">
                        <Box marginRight={3}>
                            <HelpOutlineRoundedIcon fontSize="large" color="secondary" />
                        </Box>
                        <Typography variant="h4">
                            What is it?
                        </Typography>
                    </Box>
                    <Box marginY={2}>
                        <Divider />
                    </Box>
                    <Box>
                        <Box>
                            <Box marginBottom={1} display="flex" alignItems="center">
                                <Box marginRight={3}>
                                    <ArrowForwardOutlinedIcon fontSize="medium" color="primary" />
                                </Box>
                                <Typography variant="h5">
                                    <Link href="/dataset-generator" onClick={(e) => handleNavigateLinkClick(e, "/dataset-generator")}>Dataset generator</Link>
                                </Typography>
                            </Box>
                            <Typography variant="body1" sx={paragraphStyle}>
                                Create column sets with desired values generation patterns and use them to generate
                                datasets. Datasets can be exported to both CSV and JSON file formats to use in your
                                job.
                            </Typography>
                        </Box>
                        <Box marginTop={4}>
                            <Box marginBottom={1} display="flex" alignItems="center">
                                <Box marginRight={3}>
                                    <ArrowForwardOutlinedIcon fontSize="medium" color="primary" />
                                </Box>
                                <Typography variant="h5" sx={linksStyle} onClick={() => navigate("/predefined-data")}>
                                    <Link href="/predefined-data" onClick={(e) => handleNavigateLinkClick(e, "/predefined-data")}>Values collection holder</Link>
                                </Typography>
                            </Box>
                            <Typography variant="body1" sx={paragraphStyle}>
                                Use predefined values collection as a source for random selection within your column sets
                                such as cities, first names, etc. Create new collection or expand existing one to get
                                desired assortment of values to select from.
                            </Typography>
                        </Box>
                        <Box marginTop={4} display="flex" alignItems="center">
                            <Box marginRight={3}>
                                <MoreOutlinedIcon fontSize="large" color="secondary" />
                            </Box>
                            <Typography variant="h4">
                                Moreover
                            </Typography>
                        </Box>
                        <Box marginTop={2}>
                            <Divider />
                        </Box>
                        <Box marginTop={2}>
                            <Box marginBottom={1} display="flex" alignItems="center">
                                <Box marginRight={3}>
                                    <ArrowForwardOutlinedIcon fontSize="medium" color="primary" />
                                </Box>
                                <Typography variant="h5">
                                    <Link href="/swagger" target="_blank">REST API</Link>
                                </Typography>
                            </Box>
                            <Typography variant="body1" sx={paragraphStyle}>
                                Test Data Generator provides a way to generate datasets for your project using RESTful API.
                                Currently under development.
                            </Typography>
                        </Box>
                        <Box marginTop={4}>
                            <Box marginBottom={1} display="flex" alignItems="center">
                                <Box marginRight={3}>
                                    <ArrowForwardOutlinedIcon fontSize="medium" color="primary" />
                                </Box>
                                <Typography variant="h5">
                                    <Link href="https://gitlab.com/Dan4e3/testdatagenerator" target="_blank">C# API library</Link>
                                </Typography>
                            </Box>
                            <Typography variant="body1" sx={paragraphStyle}>
                                Extended capabilities for random dataset generation (including random class instances generation)
                                alongside with direct export to database functionality are available in C# API library which
                                this web-service is based on. Published at GitLab repository.
                            </Typography>
                        </Box>
                        <Box marginTop={4}>
                            <Box marginBottom={1} display="flex" alignItems="center">
                                <Box marginRight={3}>
                                    <ArrowForwardOutlinedIcon fontSize="medium" color="primary" />
                                </Box>
                                <Typography variant="h5">
                                    <Link href="https://sourceforge.net/projects/testdatagenerator/" target="_blank">Windows standalone app</Link>
                                </Typography>
                            </Box>
                            <Typography variant="body1" sx={paragraphStyle}>
                                A standalone Windows Forms app providing offline access to dataset generation capabilities. Includes 
                                built-in support for export to database functionality.
                            </Typography>
                        </Box>
                    </Box>
                </Paper>
            </Grid>
            <Grid xs={12} lg={6} item>
                <Paper elevation={6} sx={{ padding: 3 }}>
                    <Box display="flex" alignItems="center">
                        <Box marginRight={3}>
                            <TocRoundedIcon fontSize="large" color="secondary" />
                        </Box>
                        <Typography variant="h4">
                            A quick how to
                        </Typography>
                    </Box>
                    <Box marginY={2}>
                        <Divider />
                    </Box>
                    <Box>
                        <Box>
                            <Typography variant="body1" sx={paragraphStyle}>
                                The basic idea behind Test Data Generator (TDG) is to provide configurable
                                {" "}<b>value generators</b> which will pick random values from designated pool of values.
                                That pool may be defined by using explicit values within <b>values collections</b> as well 
                                as by using some string patterns and numeric conditions. Those value generators configured by 
                                you reside in <b>data columns</b> which unify together in <b>column sets</b>. Formed column set 
                                with at least one column is required to generate and export <b>dataset</b> to <i>CSV</i> or <i>JSON</i> file.
                                Please see step-by-step guide below for a quick start. More information can be found
                                at <Link href="/reference" onClick={(e) => handleNavigateLinkClick(e, "/reference")}><b>Reference page</b></Link>.
                            </Typography>
                        </Box>
                        <Box marginY={2}>
                            <Typography variant="h5">
                                Step-by-step guide
                            </Typography>
                        </Box>
                        <Accordion expanded={accordionExpanded === "step1"} onChange={handleAccordionExpandChange("step1")}>
                            <AccordionSummary
                                expandIcon={<ExpandMoreIcon />}>
                                <Typography variant="h6" sx={{ width: "33%", flexShrink: 0 }}>
                                    Step 1
                                </Typography>
                                <Typography variant="h6" sx={{ color: "text.secondary" }}>Sign in/Sign up</Typography>
                            </AccordionSummary>
                            <AccordionDetails>
                                <Typography variant="body1">
                                    In order to use this application you
                                    need to <Link href="/login" onClick={(e) => handleNavigateLinkClick(e, "/login")}>Sign in</Link>
                                    {" "}or <Link href="/register" onClick={(e) => handleNavigateLinkClick(e, "/register")}>Sign up</Link> if
                                    don't have an account already.
                                </Typography>
                            </AccordionDetails>
                        </Accordion>
                        <Accordion expanded={accordionExpanded === "step2"} onChange={handleAccordionExpandChange("step2")}>
                            <AccordionSummary
                                expandIcon={<ExpandMoreIcon />}>
                                <Typography variant="h6" sx={{ width: "33%", flexShrink: 0 }}>
                                    Step 2 (optional)
                                </Typography>
                                <Typography variant="h6" sx={{ color: "text.secondary" }}>Add values to collection</Typography>
                            </AccordionSummary>
                            <AccordionDetails>
                                <Typography variant="body1" component="div">
                                    Navigate to <Link href="/predefined-data" onClick={(e) => handleNavigateLinkClick(e, "/predefined-data")}>
                                        Values Collection</Link> and then:
                                    <ul>
                                        <li>Select existing value category or create new one</li>
                                        <li>
                                            If you are creating new category, pay attention to "make public" switch. 
                                            Turning this feature on makes this category publicly visible and, therefore, 
                                            editable/usable by other people.
                                        </li>
                                        <li>Add new values to selected category</li>
                                        <li>
                                            Feel free to select <b>String</b> value type if you have problems 
                                            with exact type of value
                                        </li>
                                        <li>Or delete those ones which are not needed</li>
                                    </ul>
                                </Typography>
                            </AccordionDetails>
                        </Accordion>
                        <Accordion expanded={accordionExpanded === "step3"} onChange={handleAccordionExpandChange("step3")}>
                            <AccordionSummary
                                expandIcon={<ExpandMoreIcon />}>
                                <Typography variant="h6" sx={{ width: "33%", flexShrink: 0 }}>
                                    Step 3
                                </Typography>
                                <Typography variant="h6" sx={{ color: "text.secondary" }}>Create column set</Typography>
                            </AccordionSummary>
                            <AccordionDetails>
                                <Typography variant="body1" component="div">
                                    Go to <Link href="/column-sets" onClick={(e) => handleNavigateLinkClick(e, "/column-sets")}>Column Sets</Link>:
                                    <ul>
                                        <li>Create new column set using corresponding button</li>
                                        <li>Add some columns to newly column set</li>
                                        <li>Defines column's name, target value type and display culture</li>
                                        <li>Select value generator from provided assortment</li>
                                        <li>Configureate value generator appropriately</li>
                                        <li>Value generator reference page
                                            {" "}<Link href="/reference#value-generators-section" target="_blank">is here</Link>
                                        </li>
                                    </ul>
                                </Typography>
                            </AccordionDetails>
                        </Accordion>
                        <Accordion expanded={accordionExpanded === "step4"} onChange={handleAccordionExpandChange("step4")}>
                            <AccordionSummary
                                expandIcon={<ExpandMoreIcon />}>
                                <Typography variant="h6" sx={{ width: "33%", flexShrink: 0 }}>
                                    Step 4
                                </Typography>
                                <Typography variant="h6" sx={{ color: "text.secondary" }}>Generate and export dataset</Typography>
                            </AccordionSummary>
                            <AccordionDetails>
                                <Typography variant="body1" component="div">
                                    Finally, visit <Link href="/dataset-generator"
                                        onClick={(e) => handleNavigateLinkClick(e, "/dataset-generator")}>Generate Datasets</Link> page.
                                    <ul>
                                        <li>
                                            On "Preview Dataset" tab, select previously prepared column set 
                                            to generate a few rows based on column set setup
                                        </li>
                                        <li>
                                            If you are fine with generated example go to "Generate and Export to File" tab.
                                            Otherwise, please return to previous step and apply required changes to column set
                                        </li>
                                        <li>Select column set again, amount of rows to generate and desired file format to output</li>
                                        <li>Click "Generate" button and wait for your request to finish processing (can take a couple of seconds)</li>
                                        <li>
                                            After request is processed - click "Download" button opposite to your recent request row in requests table.
                                            Your file download will immediatly start.
                                        </li>
                                    </ul>
                                </Typography>
                            </AccordionDetails>
                        </Accordion>
                    </Box>
                </Paper>
            </Grid>
        </Grid>
    );
};

export default Home;