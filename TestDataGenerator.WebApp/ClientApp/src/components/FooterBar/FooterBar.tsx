﻿import { Box, Container, Grid, Link, Typography } from "@mui/material";
import React from "react";

const FooterBar = () => {
    return (
        <Grid justifyContent="center"
            textAlign="center"
            marginTop="auto"
            paddingY={1}
            sx={{ backgroundColor: "primary.main", color: "primary.contrastText" }} container>
            <Grid xs={12} lg={12} item>
                <Typography variant="caption">
                    Having questions or suggestions? Feel free to
                    contact: <Link href="mailto:support@data-gen.com" sx={{ color: "primary.contrastText" }}><b>support@data-gen.com</b></Link>
                    {" "}concerning your problem.
                </Typography>
            </Grid>
            <Grid xs={12} lg={12} item>
                <Typography variant="caption">
                    Copyright &copy; Daniil Chuprinko, 2022. All rights reserved.
                </Typography>
            </Grid>
        </Grid>
    );
};

export default FooterBar;