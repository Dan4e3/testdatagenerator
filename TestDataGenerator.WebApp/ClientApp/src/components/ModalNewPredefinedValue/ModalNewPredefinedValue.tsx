﻿import { LoadingButton } from "@mui/lab";
import {
    Box, Dialog, DialogActions, DialogContent, DialogTitle,
    FormControl, InputLabel, MenuItem, Select, SelectChangeEvent, TextField
} from "@mui/material";
import React, { useState } from "react";
import { IPredefinedCategory } from "../../models/PredefinedCategory";
import { TargetValueTypeEnum } from "../../models/ValueGeneratorParams";
import { useStores } from "../../use-stores";
import PublishIcon from '@mui/icons-material/Publish';

interface IProps {
    isOpen: boolean,
    closeModal: () => void,
    currentCategory?: IPredefinedCategory
}

const ModalNewPredefinedValue = ({ isOpen, closeModal, currentCategory }: IProps) => {
    const [predefinedValue, setPredefinedValue] = useState<string>("");
    const [valueType, setValueType] = useState<TargetValueTypeEnum>(TargetValueTypeEnum.String);
    const [sendingSubmit, setSendingSubmit] = useState<boolean>(false);

    const { rootStore } = useStores();

    const handlePredefinedValueChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setPredefinedValue(e.target.value);
    };

    const handleValueTypeChange = (e: SelectChangeEvent) => {
        setValueType(parseInt(e.target.value));
    };

    const handleSubmitButtonClick = async (e: React.FormEvent) => {
        e.preventDefault();
        if (currentCategory === undefined)
            return;

        try {
            setSendingSubmit(true);

            const addResult = await rootStore.predefinedValueStore.addSingleNewValueAsync({
                valueInfo: {
                    dataType: valueType,
                    value: predefinedValue
                },
                categoryName: currentCategory.name
            });
            if (!addResult)
                return;

            closeModal();
        }
        finally {
            setSendingSubmit(false);
        }
    };

    return (
        <Dialog open={isOpen} onClose={closeModal} maxWidth="md" fullWidth={true}>
            <Box
                component="form"
                noValidate
                autoComplete="off">
                <DialogTitle>
                    Add new value
                </DialogTitle>
                <DialogContent>
                    <TextField
                        margin="dense"
                        autoFocus
                        label="Value"
                        variant="standard"
                        fullWidth
                        value={predefinedValue}
                        onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                            handlePredefinedValueChange(e)
                        }
                    />
                    <FormControl margin="normal" fullWidth >
                        <InputLabel id="value-type-label">Value type</InputLabel>
                        <Select
                            labelId="value-type-label"
                            value={valueType.toString()}
                            onChange={handleValueTypeChange}
                            label="Value type">
                            {Object.entries(TargetValueTypeEnum)
                                .filter(([key, val]) => isNaN(Number(key)))
                                .map(([key, val], index) => (<MenuItem key={val} value={val}>{key}</MenuItem>))
                            }
                        </Select>
                    </FormControl>
                </DialogContent>
                <DialogActions>
                    <LoadingButton
                        disabled={predefinedValue.length === 0}
                        variant="contained"
                        onClick={handleSubmitButtonClick}
                        endIcon={<PublishIcon />}
                        loadingPosition="end"
                        loading={sendingSubmit}
                        fullWidth
                        type="submit"
                    >
                        Submit
                    </LoadingButton>
                </DialogActions>
            </Box>
        </Dialog>
    );
};

export default ModalNewPredefinedValue;