﻿import { FormControl, InputLabel, MenuItem, Select, SelectChangeEvent, TextField } from "@mui/material";
import React, { useState, useEffect } from "react";
import { TargetValueTypeEnum, ValueRangeGeneratorParams } from "../../models/ValueGeneratorParams";

interface IProps {
    setValueGeneratorParams: (generatorParams: ValueRangeGeneratorParams) => void,
    existingParams: ValueRangeGeneratorParams
}

const ValueRangeGenerator = ({ setValueGeneratorParams, existingParams }: IProps) => {
    const [localParams, setLocalParams] = useState<ValueRangeGeneratorParams>(new ValueRangeGeneratorParams());
    const eligibleDataTypes: TargetValueTypeEnum[] =
        [TargetValueTypeEnum.FloatingPointNumber, TargetValueTypeEnum.IntegerNumber, TargetValueTypeEnum.DateTime];

    const handleResultValueTypeChange = (e: SelectChangeEvent) => {
        const valueTypeAsInt = parseInt(e.target.value);
        setValueGeneratorParams({ ...localParams, dataType: valueTypeAsInt });
        setLocalParams(prevParams => {
            const newParams: ValueRangeGeneratorParams = { ...prevParams, dataType: valueTypeAsInt };
            return newParams;
        });
    };

    const handleMinValueChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const newValue = e.target.value.replace(",", ".");
        setValueGeneratorParams({ ...localParams, minValue: newValue });
        setLocalParams(prevParams => {
            const newParams: ValueRangeGeneratorParams = { ...prevParams, minValue: newValue };
            return newParams;
        });
    };

    const handleMaxValueChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const newValue = e.target.value.replace(",", ".");
        setValueGeneratorParams({ ...localParams, maxValue: newValue });
        setLocalParams(prevParams => {
            const newParams: ValueRangeGeneratorParams = { ...prevParams, maxValue: newValue };
            return newParams;
        });
    };

    useEffect(() => {
        if (existingParams?.minValue !== undefined) {
            setLocalParams(existingParams);
        }
        else {
            setValueGeneratorParams(localParams);
        }
    }, []);

    return (
        <FormControl fullWidth margin="none">
            <FormControl margin="normal" sx={{ minWidth: 300 }}>
                <InputLabel id="value-range-data-type-label">Value range data type</InputLabel>
                <Select
                    labelId="value-range-data-type-label"
                    value={localParams.dataType.toString()}
                    onChange={handleResultValueTypeChange}
                    label="Value range data type">
                    {Object.entries(TargetValueTypeEnum)
                        .filter(([key, val]) => isNaN(Number(key)) && eligibleDataTypes.indexOf(TargetValueTypeEnum[key]) > -1)
                        .map(([key, val], index) => (<MenuItem key={val} value={val}>{key}</MenuItem>))
                    }
                </Select>
            </FormControl>
            <TextField
                margin="normal"
                autoFocus
                label="Minimum value of range"
                fullWidth
                required
                variant="standard"
                value={localParams.minValue}
                onChange={handleMinValueChange}
            />
            <TextField
                margin="normal"
                autoFocus
                label="Maximum value of range"
                fullWidth
                required
                variant="standard"
                value={localParams.maxValue}
                onChange={handleMaxValueChange}
            />
        </FormControl>
    );
};

export default ValueRangeGenerator;