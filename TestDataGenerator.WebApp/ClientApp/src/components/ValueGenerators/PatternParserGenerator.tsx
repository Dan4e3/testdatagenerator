﻿import { TextField, FormControl } from "@mui/material";
import { observer } from "mobx-react";
import React, { useState, useEffect } from "react";
import { ValuePatternGeneratorParams } from "../../models/ValueGeneratorParams";

interface IProps {
    setValueGeneratorParams: (generatorParams: ValuePatternGeneratorParams) => void,
    existingParams?: ValuePatternGeneratorParams
}

const PatternParserGenerator = observer(({ setValueGeneratorParams, existingParams }: IProps) => {
    const [valuePattern, setValuePattern] = useState<string>("");

    const handleValuePatternChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setValuePattern(e.target.value);
        setValueGeneratorParams({
            valuePattern: e.target.value
        });
    };

    useEffect(() => {
        if (existingParams?.valuePattern !== undefined) {
            setValuePattern(existingParams.valuePattern);
        }
    }, []);

    return (
        <FormControl fullWidth margin="none" >
            <TextField
                margin="none"
                label="Value pattern"
                fullWidth
                required
                variant="standard"
                value={valuePattern}
                onChange={handleValuePatternChange}
            />
        </FormControl>
    );
});

export default PatternParserGenerator;