﻿import {
    Button, List, ListItem, InputLabel, Select, ListItemText,
    ListItemButton, TextField, FormControl, MenuItem, SelectChangeEvent,
    Grid, Menu, ListItemIcon, Typography, Box
} from '@mui/material';
import DeleteOutlinedIcon from '@mui/icons-material/DeleteOutlined';
import React, { useEffect, useState } from 'react';
import { ISelectFromListItem, SelectFromListGeneratorParams, TargetValueTypeEnum } from '../../models/ValueGeneratorParams';
import { IPredefinedCategory } from '../../models/PredefinedCategory';
import { useStores } from '../../use-stores';
import { observer } from 'mobx-react';
import { toast } from 'react-toastify';

interface IProps {
    setValueGeneratorParams: (params: SelectFromListGeneratorParams) => void,
    existingParams: SelectFromListGeneratorParams
}

const SelectFromListGenerator = observer(({ setValueGeneratorParams, existingParams }: IProps) => {
    const [itemsList, setItemsList] = useState<ISelectFromListItem[]>([]);
    const [newValue, setNewValue] = useState<string>("");
    const [resultValueType, setResultValueType] = useState<TargetValueTypeEnum>(TargetValueTypeEnum.String);
    const [chosenPredefinedCategoriesList, setChosenPredefinedCategoriesList] = useState<IPredefinedCategory[]>([]);
    const [selectedCategory, setSelectedCategory] = useState<IPredefinedCategory>();
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const [clickedItemList, setClickedItemList] = useState<ISelectFromListItem | IPredefinedCategory>();
    const open = Boolean(anchorEl);

    const { rootStore } = useStores();

    const handleResultValueTypeChange = (e: SelectChangeEvent) => {
        setResultValueType(parseInt(e.target.value));
    };

    const handleNewValueInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setNewValue(e.target.value);
    };

    const handleAddNewValueButtonClick = () => {
        setValueGeneratorParams({
            values: [...itemsList, { dataType: resultValueType, value: newValue }],
            predefinedCategoriesIds: chosenPredefinedCategoriesList.map(cat => cat.id)
        });
        setItemsList(prevItems => {
            const newItemsList = [...prevItems, { dataType: resultValueType, value: newValue }];
            return newItemsList;
        });
        setNewValue("");
    };

    const handleListItemClick = (event: React.MouseEvent<HTMLDivElement>, item: ISelectFromListItem | IPredefinedCategory) => {
        setClickedItemList(item);
        setAnchorEl(event.currentTarget);
    };

    const handleListItemMenuClose = () => {
        setClickedItemList(undefined);
        setAnchorEl(null);
    };

    const handleListItemDeleteClick = () => {
        setAnchorEl(null);
        const itemAsCustomNewItem = clickedItemList as ISelectFromListItem;
        const itemAsCategoryItem = clickedItemList as IPredefinedCategory;
        if (clickedItemList !== undefined) {
            if (itemAsCustomNewItem.value !== undefined) {
                setValueGeneratorParams({
                    values: itemsList.filter(val => val.value !== itemAsCustomNewItem.value),
                    predefinedCategoriesIds: chosenPredefinedCategoriesList.map(cat => cat.id)
                });
                setItemsList(prevItems => {
                    const newItemsList = prevItems.filter(val => val.value !== itemAsCustomNewItem.value);
                    return newItemsList;
                });
            }
            else if (itemAsCategoryItem.name !== undefined) {
                setValueGeneratorParams({
                    values: itemsList,
                    predefinedCategoriesIds: chosenPredefinedCategoriesList.map(cat => cat.id)
                });
                setChosenPredefinedCategoriesList(prevItems => {
                    const newPredefinedCatsList = prevItems.filter(cat => cat.id !== itemAsCategoryItem.id);
                    return newPredefinedCatsList;
                });
            }
        }
    };

    const handlePredefinedCategoryChange = async (event: SelectChangeEvent) => {
        const id: number = Number(event.target.value);
        const newSelectedCategory = rootStore.predefinedCategoryStore.categories.find(cat => cat.id === id);
        setSelectedCategory(newSelectedCategory);
    };

    const handleAddCategoryButtonClick = () => {
        if (selectedCategory !== undefined) {
            if (chosenPredefinedCategoriesList.find(cat => cat.id === selectedCategory.id) !== undefined) {
                toast.warning(`List already contains this category!`);
                return;
            }
            setValueGeneratorParams({
                values: itemsList,
                predefinedCategoriesIds: [...chosenPredefinedCategoriesList, selectedCategory].map(cat => cat.id)
            });
            setChosenPredefinedCategoriesList(prevItems => {
                const newChosenCategoriesList = [...prevItems, selectedCategory];
                return newChosenCategoriesList;
            });
        }
    };

    useEffect(() => {
        if (existingParams?.predefinedCategoriesIds !== undefined) {
            const categories = rootStore.predefinedCategoryStore.categories.filter(cat => existingParams.predefinedCategoriesIds.includes(cat.id));
            setChosenPredefinedCategoriesList(categories);
        }
    }, [rootStore.predefinedCategoryStore.categories]);

    useEffect(() => {
        (async () => await rootStore.predefinedCategoryStore.queryUserCategoriesAsync(true))();

        if (existingParams?.values !== undefined) {
            setItemsList(existingParams.values);
        }
    }, []);

    return (
        <Grid container>
            <Grid xs={12} lg={12} item>
                <Typography variant="subtitle1">
                    Add values in place:
                </Typography>
            </Grid>
            <Grid xs={12} lg={12} item>
                <FormControl margin="normal" sx={{ minWidth: 300 }}>
                    <InputLabel id="value-type-to-insert-label">Value type to insert</InputLabel>
                    <Select
                        labelId="value-type-to-insert-label"
                        value={resultValueType.toString()}
                        onChange={handleResultValueTypeChange}
                        label="Value type to insert">
                        {Object.entries(TargetValueTypeEnum)
                            .filter(([key, val]) => isNaN(Number(key)))
                            .map(([key, val], index) => (<MenuItem key={val} value={val}>{key}</MenuItem>))
                        }
                    </Select>
                </FormControl>
            </Grid>
            <Grid xs={12} lg={12} item>
                <Box display="flex" justifyContent="flex-start" alignItems="center">
                    <Box minWidth={300} marginRight={3}>
                        <TextField
                            margin="normal"
                            autoFocus
                            fullWidth
                            label="New value"
                            variant="standard"
                            value={newValue}
                            onChange={handleNewValueInputChange}
                        />
                    </Box>
                    <Button
                        onClick={handleAddNewValueButtonClick}
                        disabled={newValue.length === 0}
                        color="primary"
                        variant="contained" >
                        Add value
                    </Button>
                </Box>
            </Grid>
            <Grid xs={12} lg={12} item marginTop={2}>
                <Typography variant="subtitle1">
                    Or add values from category:
                </Typography>
            </Grid>
            <Grid xs={12} lg={12} item>
                <Box display="flex" justifyContent="flex-start" alignItems="center">
                    <Box minWidth={300} marginRight={3}>
                        <FormControl sx={{ minWidth: 300 }} margin="normal">
                            <InputLabel id="data-categories-label">Select category</InputLabel>
                            <Select
                                labelId="data-categories-label"
                                value={selectedCategory?.id.toString() || ""}
                                onChange={handlePredefinedCategoryChange}
                                label="Select category">
                                {rootStore.predefinedCategoryStore.categories
                                    .map(cat => (<MenuItem key={cat.id} value={cat.id}>{cat.name}</MenuItem>))
                                }
                            </Select>
                        </FormControl>
                    </Box>
                    <Button
                        onClick={handleAddCategoryButtonClick}
                        disabled={selectedCategory === undefined}
                        color="primary"
                        variant="contained" >
                        Add category values
                    </Button>
                </Box>
            </Grid>
            <Grid xs={12} lg={12} item>
                <List sx={{ maxHeight: 250, overflow: "auto" }}>
                    <Box display="flex" flexWrap="wrap">
                        {chosenPredefinedCategoriesList.map((cat, ind) => {
                            return (
                                <Box key={ind}>
                                    <ListItem>
                                        <ListItemButton onClick={(e) => handleListItemClick(e, cat)}
                                            sx={{ backgroundColor: "#e0f7fa" }} >
                                            <ListItemText
                                                primary={cat.name}
                                                primaryTypographyProps={{ sx: { overflow: "hidden", textOverflow: "ellipsis" } }}
                                                secondary="Category" />
                                        </ListItemButton>
                                    </ListItem>
                                </Box>
                            );
                        })}
                        {itemsList.map((val, ind) => {
                            return (
                                <Box key={ind}>
                                    <ListItem>
                                        <ListItemButton onClick={(e) => handleListItemClick(e, val)}
                                            sx={{ backgroundColor: "#e8eaf6" }} >
                                            <ListItemText
                                                primary={val.value}
                                                primaryTypographyProps={{ sx: { overflow: "hidden", textOverflow: "ellipsis" } }}
                                                secondary={TargetValueTypeEnum[val.dataType]} />
                                        </ListItemButton>
                                    </ListItem>
                                </Box>
                            );
                        })}
                    </Box>
                </List>
            </Grid>
            <Menu
                anchorEl={anchorEl}
                open={open} onClose={handleListItemMenuClose} >
                <MenuItem onClick={handleListItemDeleteClick} >
                    <ListItemIcon>
                        <DeleteOutlinedIcon fontSize="small" />
                    </ListItemIcon>
                    <ListItemText>Delete item</ListItemText>
                </MenuItem>
            </Menu>
        </Grid>
    );
});

export default SelectFromListGenerator;