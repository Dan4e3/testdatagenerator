﻿import { FormControl, InputLabel, MenuItem, Select, SelectChangeEvent, TextField } from "@mui/material";
import React, { useState, useEffect } from "react";
import { IncrementalGeneratorParams, TargetValueTypeEnum } from "../../models/ValueGeneratorParams";

interface IProps {
    setValueGeneratorParams: (generatorParams: IncrementalGeneratorParams) => void,
    existingParams: IncrementalGeneratorParams
}

const IncrementalValueGenerator = ({ setValueGeneratorParams, existingParams }: IProps) => {
    const [localParams, setLocalParams] = useState<IncrementalGeneratorParams>(new IncrementalGeneratorParams());
    const eligibleDataTypes: TargetValueTypeEnum[] =
        [TargetValueTypeEnum.FloatingPointNumber, TargetValueTypeEnum.IntegerNumber, TargetValueTypeEnum.DateTime];

    const handleResultValueTypeChange = (e: SelectChangeEvent) => {
        const valueTypeAsInt = parseInt(e.target.value);
        setValueGeneratorParams({ ...localParams, dataType: valueTypeAsInt });
        setLocalParams(prevParams => {
            const newParams: IncrementalGeneratorParams = { ...prevParams, dataType: valueTypeAsInt };
            return newParams;
        });
    };

    const handleStartValueChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const newValue = e.target.value.replace(",", ".");
        setValueGeneratorParams({ ...localParams, startValue: newValue });
        setLocalParams(prevParams => {
            const newParams: IncrementalGeneratorParams = { ...prevParams, startValue: newValue };
            return newParams;
        });
    };

    const handleEndValueChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const newValue = e.target.value.replace(",", ".");
        setValueGeneratorParams({ ...localParams, endValue: newValue });
        setLocalParams(prevParams => {
            const newParams: IncrementalGeneratorParams = { ...prevParams, endValue: newValue };
            return newParams;
        });
    };

    const handleValueStepChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const newValue = e.target.value.replace(",", ".");
        setValueGeneratorParams({ ...localParams, valueStep: newValue });
        setLocalParams(prevParams => {
            const newParams: IncrementalGeneratorParams = { ...prevParams, valueStep: newValue };
            return newParams;
        });
    };

    useEffect(() => {
        if (existingParams?.valueStep !== undefined) {
            setLocalParams(existingParams);
        }
        else {
            setValueGeneratorParams(localParams);
        }
    }, []);

    return (
        <FormControl fullWidth margin="none">
            <FormControl margin="normal" sx={{ minWidth: 300 }}>
                <InputLabel id="increment-value-data-type-label">Increment value data type</InputLabel>
                <Select
                    labelId="increment-value-data-type-label"
                    value={localParams.dataType.toString()}
                    onChange={handleResultValueTypeChange}
                    label="Increment value data type">
                    {Object.entries(TargetValueTypeEnum)
                        .filter(([key, val]) => isNaN(Number(key)) && eligibleDataTypes.indexOf(TargetValueTypeEnum[key]) > -1)
                        .map(([key, val], index) => (<MenuItem key={val} value={val}>{key}</MenuItem>))
                    }
                </Select>
            </FormControl>
            <TextField
                margin="normal"
                autoFocus
                label="Starting value"
                fullWidth
                required
                variant="standard"
                value={localParams.startValue}
                onChange={handleStartValueChange}
            />
            <TextField
                margin="normal"
                autoFocus
                label="End value"
                fullWidth
                required
                variant="standard"
                value={localParams.endValue}
                onChange={handleEndValueChange}
            />
            <TextField
                margin="normal"
                autoFocus
                label="Value step"
                fullWidth
                required
                variant="standard"
                value={localParams.valueStep}
                onChange={handleValueStepChange}
            />
        </FormControl>
    );
};

export default IncrementalValueGenerator;