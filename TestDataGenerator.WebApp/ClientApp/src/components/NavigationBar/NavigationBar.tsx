﻿import { AppBar, Grid, Toolbar, Link, Typography, Box } from "@mui/material";
import { Link as RouterLink, useNavigate } from 'react-router-dom';
import React from "react";
import logo from "../../static/logo.png";
import { observer } from "mobx-react";
import { useStores } from "../../use-stores";

const linksStyle = {
    textDecoration: "none",
    color: "white",
    fontSize: 18,
    "&:hover": {
        color: "yellow",
        borderBottom: "1px solid white",
        cursor: "pointer"
    }
} as const;

const NavigationBar = observer(() => {

    const { rootStore } = useStores();
    const navigate = useNavigate();

    const loginOrLogout = () => {
        if (rootStore.userStore.user.authenticated)
            return (<Link onClick={handleLogoutClick} sx={linksStyle}>Logout</Link>);
        else
            return (<Link component={RouterLink} to="/login" sx={linksStyle}>Sign in/Sign up</Link>);
    };

    const handleLogoutClick = async () => {
        await rootStore.userStore.logoutAsync();
        navigate("/");
    };

    return (
        <AppBar position="static" elevation={6}>
            <Toolbar disableGutters>
                <Grid alignItems="center" container>
                    <Grid xs={12} lg={3} item justifyContent="center"
                        alignItems="center" display={{ xs: "none", lg: "block" }}>
                        <Link component={RouterLink} to="/" sx={{
                            display: "flex",
                            alignItems: "center",
                            alignContents: "center",
                            justifyContent: "flex-start",
                            cursor: "pointer"
                        }}>
                            <img src={logo} alt="Test Data Generator" width={82} height={82} />
                            <Typography variant="h1" fontSize={24} sx={{
                                textDecoration: "none",
                                color: "white",
                                marginLeft: 3,
                                textAlign: "center"
                            }}> Test Data Generator (beta) </Typography>
                        </Link>
                    </Grid>
                    <Grid xs={12} lg={9} item>
                        <Grid justifyContent="flex-end" columns={20} container>
                            <Grid xs={20} item display={{ xs: "block", lg: "none" }}>
                                <Box height={{ xs: 45, lg: "100%" }} display="flex" justifyContent="center" alignItems="center">
                                    <Link component={RouterLink} to="/" sx={linksStyle}>Home</Link>
                                </Box>
                            </Grid>
                            <Grid xs={20} lg={4} height="inherit" item>
                                <Box height={{ xs: 45, lg: "100%" }} display="flex" justifyContent="center" alignItems="center">
                                    <Link component={RouterLink} to="/predefined-data" sx={linksStyle}>Values collection</Link>
                                </Box>
                            </Grid>
                            <Grid xs={20} lg={4} item textAlign="center">
                                <Box height={{ xs: 45, lg: "100%" }} display="flex" justifyContent="center" alignItems="center">
                                    <Link component={RouterLink} to="/column-sets" sx={linksStyle}>Column sets</Link>
                                </Box>
                            </Grid>
                            <Grid xs={20} lg={4} item textAlign="center">
                                <Box height={{ xs: 45, lg: "100%" }} display="flex" justifyContent="center" alignItems="center">
                                    <Link component={RouterLink} to="/dataset-generator" sx={linksStyle}>Generate datasets</Link>
                                </Box>
                            </Grid>
                            <Grid xs={20} lg={4} item textAlign="center">
                                <Box height={{ xs: 45, lg: "100%" }} display="flex" justifyContent="center" alignItems="center">
                                    <Link component={RouterLink} to="/reference" sx={linksStyle}>Reference</Link>
                                </Box>
                            </Grid>
                            <Grid xs={20} lg={4} item textAlign="center">
                                <Box height={{ xs: 45, lg: "100%" }} display="flex" justifyContent="center" alignItems="center">
                                    {loginOrLogout()}
                                </Box>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Toolbar>
        </AppBar>
    );
});

export default NavigationBar;