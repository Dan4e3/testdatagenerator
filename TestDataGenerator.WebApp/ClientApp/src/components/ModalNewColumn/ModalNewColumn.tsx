﻿import React, { useState, useEffect } from "react";
import {
    Dialog, DialogTitle, DialogContent, Select, MenuItem,
    DialogActions, TextField, SelectChangeEvent,
    Box, FormControl, InputLabel,
    Typography, Grid, Divider, Chip
} from "@mui/material";
import PatternParserGenerator from "../ValueGenerators/PatternParserGenerator";
import { observer } from "mobx-react";
import SelectFromListGenerator from "../ValueGenerators/SelectFromListGenerator";
import ValueRangeGenerator from "../ValueGenerators/ValueRangeGenerator";
import IncrementalValueGenerator from "../ValueGenerators/IncrementalValueGenerator";
import { ColumnStore } from "../../stores/ColumnStore";
import { IColumn } from "../../models/Column";
import {
    CultureInfoTypeEnum, IncrementalGeneratorParams, IValueGeneratorParams, SelectFromListGeneratorParams,
    TargetValueTypeEnum, ValueGeneratorTypeEnum, ValuePatternGeneratorParams, ValueRangeGeneratorParams
} from "../../models/ValueGeneratorParams";
import { LoadingButton } from "@mui/lab";
import { toast } from "react-toastify";
import { useStores } from "../../use-stores";


interface IProps {
    isOpen: boolean,
    closeModal: (closedOnSubmit: boolean) => void,
    columnStore: ColumnStore,
    existingColumn?: IColumn
}

const ModalNewColumn = observer(({ isOpen, closeModal, columnStore, existingColumn }: IProps) => {
    const [colName, setColName] = useState<string>("");
    const [targetValueType, setTargetValueType] = useState<TargetValueTypeEnum>(TargetValueTypeEnum.String);
    const [culture, setCulture] = useState<CultureInfoTypeEnum>(CultureInfoTypeEnum.EnUs);
    const [valueGeneratorType, setValueGeneratorType] = useState<ValueGeneratorTypeEnum>(ValueGeneratorTypeEnum.PatternParser);
    const [valueGeneratorParameters, setValueGeneratorParameters] = useState<IValueGeneratorParams>({ valuePattern: "" });
    const [isSubmitting, setIsSubmitting] = useState<boolean>(false);
    const [validationError, setValidationError] = useState<string | undefined>(undefined);

    const { rootStore } = useStores();

    useEffect(() => {
        if (existingColumn !== undefined) {
            setColName(existingColumn.columnSettings.columnName);
            setTargetValueType(existingColumn.columnSettings.targetValueType);
            setCulture(existingColumn.columnSettings.culture);
            setValueGeneratorType(existingColumn.columnSettings.valueSupplier.generatorType);
            setValueGeneratorParameters(existingColumn.columnSettings.valueSupplier.generatorParameters);
        }
    }, []);

    useEffect(() => {
        setValidationError(undefined);
    }, [valueGeneratorType]);

    const handleColNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setColName(e.target.value);
    };

    const handleTargetValueTypeChange = (e: SelectChangeEvent) => {
        const target: TargetValueTypeEnum = parseInt(e.target.value);
        setTargetValueType(target);
    };

    const handleCultureChange = (e: SelectChangeEvent) => {
        setCulture(e.target.value as CultureInfoTypeEnum)
    };

    const handleValueGeneratorTypeChange = (e: SelectChangeEvent) => {
        setValueGeneratorType(parseInt(e.target.value));
    };

    const handleSubmit = async (e: React.FormEvent) => {
        e.preventDefault();
        setIsSubmitting(true);

        try {
            const resultColumn: IColumn = {
                columnSettings: {
                    id: existingColumn === undefined ? Date.now() : existingColumn.columnSettings.id,
                    columnName: colName,
                    targetValueType: targetValueType,
                    culture: culture,
                    valueSupplier: {
                        generatorType: valueGeneratorType,
                        generatorParameters: valueGeneratorParameters ?? {}
                    }
                }
            };

            const validationResult = await rootStore.columnSetStore.validateSingleColumnAsync(resultColumn);

            if (validationResult === undefined) {
                return;
            }
            else if (!validationResult.isValid) {
                setValidationError(validationResult.validationError);
                toast.error("Column validation error occurred.");
                return;
            }

            setValidationError(undefined);

            if (existingColumn === undefined)
                columnStore.addColumn(resultColumn);
            else
                columnStore.updateColumn(resultColumn);

            closeModal(true);
        }
        finally {
            setIsSubmitting(false);
        }
    };

    return (
        <Dialog open={isOpen} onClose={() => closeModal(false)} maxWidth="md" fullWidth={true} >
            <Box component="form"
                autoComplete="on">
                <DialogTitle>
                    Create new column
                </DialogTitle>
                <DialogContent>
                    <Grid container>
                        <Grid xs={12} lg={12} item marginBottom={2}>
                            <Divider textAlign="center">
                                <Chip label="Common settings" color="primary" variant="outlined" size="small" />
                            </Divider>
                            <TextField
                                margin="none"
                                autoFocus
                                label="Column name"
                                fullWidth
                                required
                                variant="standard"
                                value={colName}
                                onChange={handleColNameChange}
                            />
                        </Grid>
                        <Grid xs={12} lg={12} item>
                            <FormControl margin="normal" sx={{ minWidth: 300 }} >
                                <InputLabel id="target-value-type-label">Target value type</InputLabel>
                                <Select
                                    labelId="target-value-type-label"
                                    value={targetValueType.toString()}
                                    onChange={handleTargetValueTypeChange}
                                    label="Target value type">
                                    {Object.entries(TargetValueTypeEnum)
                                        .filter(([key, val]) => isNaN(Number(key)))
                                        .map(([key, val], index) => (<MenuItem key={val} value={val}>{key}</MenuItem>))
                                    }
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid xs={12} lg={12} item>
                            <FormControl margin="normal" sx={{ minWidth: 300 }}>
                                <InputLabel id="culture-label">Values representation culture</InputLabel>
                                <Select
                                    labelId="culture-label"
                                    value={culture}
                                    onChange={handleCultureChange}
                                    label="Values representation culture">
                                    {Object.entries(CultureInfoTypeEnum).map(
                                        ([key, val], index) => (<MenuItem key={index} value={val}>{val}</MenuItem>))}
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid xs={12} lg={12} item>
                            <FormControl margin="normal" sx={{ minWidth: 300 }}>
                                <InputLabel id="generator-type-label">Value generator type</InputLabel>
                                <Select
                                    labelId="generator-type-label"
                                    value={valueGeneratorType.toString()}
                                    onChange={handleValueGeneratorTypeChange}
                                    label="Value generator type">
                                    {Object.entries(ValueGeneratorTypeEnum)
                                        .filter(([key, val]) => isNaN(Number(key)))
                                        .map(([key, val], index) => (<MenuItem key={val} value={val}>{key}</MenuItem>))
                                    }
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid xs={12} lg={12} item marginTop={3}>
                            <Box marginBottom={1}>
                                <Divider textAlign="center">
                                    <Chip label="Generator parameters" color="primary" variant="outlined" size="small" />
                                </Divider>
                            </Box>
                            {valueGeneratorType == ValueGeneratorTypeEnum.PatternParser && (
                                <PatternParserGenerator
                                    setValueGeneratorParams={(generatorParams) => setValueGeneratorParameters(generatorParams)}
                                    existingParams={existingColumn?.columnSettings.valueSupplier.generatorParameters as ValuePatternGeneratorParams} />
                            )}

                            {valueGeneratorType == ValueGeneratorTypeEnum.SelectFromList && (
                                <SelectFromListGenerator
                                    setValueGeneratorParams={(generatorParams) => setValueGeneratorParameters(generatorParams)}
                                    existingParams={existingColumn?.columnSettings.valueSupplier.generatorParameters as SelectFromListGeneratorParams} />
                            )}

                            {valueGeneratorType == ValueGeneratorTypeEnum.ValueRange && (
                                <ValueRangeGenerator
                                    setValueGeneratorParams={(generatorParams) => setValueGeneratorParameters(generatorParams)}
                                    existingParams={existingColumn?.columnSettings.valueSupplier.generatorParameters as ValueRangeGeneratorParams} />
                            )}

                            {valueGeneratorType == ValueGeneratorTypeEnum.Incremental && (
                                <IncrementalValueGenerator
                                    setValueGeneratorParams={(generatorParams) => setValueGeneratorParameters(generatorParams)}
                                    existingParams={existingColumn?.columnSettings.valueSupplier.generatorParameters as IncrementalGeneratorParams} />
                            )}
                        </Grid>
                        <Grid xs={12} lg={12} item>
                            {validationError !== undefined && (
                                <Box marginY={1}>
                                    <Typography variant="body1" color="error">
                                        {validationError}
                                    </Typography>
                                </Box>
                            )}
                        </Grid>
                    </Grid>
                </DialogContent>
                <DialogActions>
                    <LoadingButton
                        disabled={colName.length === 0}
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        loadingPosition="center"
                        loading={isSubmitting}
                        onClick={handleSubmit}
                    >
                        Submit
                    </LoadingButton>
                </DialogActions>
            </Box>
        </Dialog>
    );
});

export default ModalNewColumn;