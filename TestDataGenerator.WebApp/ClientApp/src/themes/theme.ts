﻿import { blueGrey, cyan, grey } from "@mui/material/colors";
import { createTheme } from "@mui/material/styles";

export const appTheme = createTheme({
    palette: {
        primary: {
            main: cyan[700],
        },
        secondary: {
            main: blueGrey[500]
        },
        error: {
            main: "#e02214"
        },
        background: {
            default: "#eceff1",
        },
    },
    components: {
        MuiCssBaseline: {
            styleOverrides: {
                code: {
                    backgroundColor: grey[200]
                }
            }
        }
    }
});