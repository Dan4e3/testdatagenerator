﻿import { makeObservable, observable, action, runInAction } from "mobx";
import { toast } from 'react-toastify';
import { IAddNewUser } from "../models/ApiRequests/AddNewUser";
import { IAddNewUserResult } from "../models/ApiResponses/AddNewUserResult";
import UserService from "../services/UserService";
import { criticalServerError, handledError, unexpectedError } from "../utils/ServerErrorHandler";
import { RootStore } from "./RootStore";

export interface IUser {
    name?: string,
    authenticated: boolean
}

export class UserStore {
    public user: IUser;
    public rootStore: RootStore;
    public userService: UserService;

    constructor(rootStore: RootStore) {
        this.user = { authenticated: false };
        this.rootStore = rootStore;
        this.userService = new UserService(rootStore.baseApiUrl);

        (async () => await this.initializeUser())();

        makeObservable(this, {
            user: observable,
            authenticateAsync: action,
            logoutAsync: action,
            initializeUser: action
        });
    }

    public initializeUser = async () => {
        const userData = localStorage.getItem("user");
        const pingResult: boolean = await this.pingAsync();
        if (userData === null) {
            runInAction(() => {
                this.user = { authenticated: false };
            });
        }
        else {
            if (pingResult) {
                runInAction(() => {
                    this.user = JSON.parse(userData) as IUser;
                });
            }
            else {
                runInAction(() => {
                    this.user = { authenticated: false };
                });
            }
        }
    };

    public authenticateAsync = async (username: string, password: string): Promise<boolean> => {
        try {
            const response = await this.userService.authenticate(username, password);
            if (response.ok) {
                runInAction(() => {
                    this.user.authenticated = true;
                    this.user.name = username;
                });
                localStorage.setItem("user", JSON.stringify(this.user));
                toast.success(`Logged in as ${username}!`);
                return true;
            }
            else if (response.status === 401) {
                const responseJson = await response.json();
                toast.error(`Login failed because: ${responseJson.resultMessage}`);
            }
            else {
                unexpectedError();
            }

            return false;
        }
        catch (error) {
            criticalServerError();
            return false;
        }
    }; 


    public logoutAsync = async () => {
        const response = await this.userService.logout();
        if (response.ok) {
            runInAction(() => {
                this.user.authenticated = false;
                this.user.name = undefined;
            });
            localStorage.removeItem("user");
            toast.success("Successfully logged out!")
        }
        else {
            unexpectedError();
        }
    };

    public pingAsync = async (): Promise<boolean> => {
        try {
            const response = await this.userService.ping();
            if (response.ok)
                return true;
            else
                return false;
        }
        catch (err) {
            return false;
        }
    };

    public registerAsync = async (registrationData: IAddNewUser): Promise<boolean> => {
        try {
            const response = await this.userService.register(registrationData);
            const responseAsJson: IAddNewUserResult = await response.json();
            if (response.ok) {
                toast.success("Succes! Please login to continue.")
                return true;
            }
            else {
                handledError(responseAsJson.resultMessage);
                return false;
            }
        }
        catch (err) {
            criticalServerError();
            return false;
        }
    };
}