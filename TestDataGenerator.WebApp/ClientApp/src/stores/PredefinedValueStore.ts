﻿import { action, computed, makeObservable, observable, runInAction } from "mobx";
import { toast } from "react-toastify";
import { IAddPredefinedValueCollection } from "../models/ApiRequests/AddPredefinedValueCollection";
import { IAddSinglePredefinedValue } from "../models/ApiRequests/AddSinglePredefinedValue";
import { IGetPredefinedValuesPaginated } from "../models/ApiRequests/GetPredefinedValuesPaginated";
import { IAddPredefinedValuesResult } from "../models/ApiResponses/AddPredefinedValuesResult";
import { IAddSinglePredefinedValuesResult } from "../models/ApiResponses/AddSinglePredefinedValueResult";
import { IDeletePredefinedValuesResult } from "../models/ApiResponses/DeletePredefinedValuesResult";
import { IGetPredefinedValueResult } from "../models/ApiResponses/GetPredefinedValueResult";
import { IGetPredefinedValuesPaginatedResult } from "../models/ApiResponses/GetPredefinedValuesPaginatedResult";
import { IPredefinedValue } from "../models/PredefinedValue";
import PredefinedValueService from "../services/PredefinedValueService";
import { criticalServerError, handledError, unexpectedError } from "../utils/ServerErrorHandler";
import { RootStore } from "./RootStore";

export default class PredefinedValueStore {
    public rootStore: RootStore;
    public valuesService: PredefinedValueService;

    public predefinedValues: IPredefinedValue[]
    public totalValuesCount: number;
    public currentPageNum: number;
    public valuesPerPage: number;

    constructor(rootStore: RootStore) {
        this.rootStore = rootStore;
        this.valuesService = new PredefinedValueService(rootStore.baseApiUrl);

        this.predefinedValues = [];
        this.totalValuesCount = 0;
        this.currentPageNum = 0;
        this.valuesPerPage = 0;

        makeObservable(this, {
            predefinedValues: observable,
            totalValuesCount: observable,
            currentPageNum: observable,
            valuesPerPage: observable,
            valuesCount: computed,
            getValuesByCategoryAsync: action,
            getValuesByCategoryPaginatedAsync: action,
            addNewValuesAsync: action,
            addSingleNewValueAsync: action,
            resetValuesList: action
        });
    }

    public getValuesByCategoryAsync = async (categoryName: string, valuesToFetch: number = 25) => {
        try {
            const response = await this.valuesService.getValuesByCategoryAsync(categoryName, valuesToFetch);
            const responseAsJson: IGetPredefinedValueResult = await response.json();
            if (response.ok) {
                responseAsJson.data.values.forEach(v => v.creationDate = new Date(v.creationDate))
                runInAction(() => {
                    this.predefinedValues = responseAsJson.data.values;
                });
            }
            else if (response.status == 400) {
                handledError(responseAsJson.resultMessage);
            }
            else
                unexpectedError();
        }
        catch (error) {
            criticalServerError();
        }
    };

    public getValuesByCategoryPaginatedAsync = async (pageData: IGetPredefinedValuesPaginated) => {
        try {
            const response = await this.valuesService.getValuesByCategoryPaginatedAsync(pageData);
            const responseAsJson: IGetPredefinedValuesPaginatedResult = await response.json();
            if (response.ok) {
                responseAsJson.valuesCollection.values.forEach(v => v.creationDate = new Date(v.creationDate));
                runInAction(() => {
                    this.predefinedValues = responseAsJson.valuesCollection.values;
                    this.totalValuesCount = responseAsJson.totalValuesCount;
                    this.currentPageNum = responseAsJson.pageNumber;
                    this.valuesPerPage = responseAsJson.valuesPerPage;
                });
            }
            else if (response.status == 400) {
                handledError(responseAsJson.resultMessage);
            }
            else
                unexpectedError();
        }
        catch (error) {
            criticalServerError();
        }
    };

    public addNewValuesAsync = async (dataToAdd: IAddPredefinedValueCollection) => {
        try {
            const response = await this.valuesService.createValuesAsync(dataToAdd);
            const responseAsJson: IAddPredefinedValuesResult = await response.json();
            if (response.ok) {
                toast.success(`Successfully added ${responseAsJson.valuesAdded} item(s)!`);
            }
            else if (response.status == 400) {
                handledError(responseAsJson.resultMessage);
            }
            else
                unexpectedError();
        }
        catch (error) {
            criticalServerError();
        }
    };

    public addSingleNewValueAsync = async (valueToAdd: IAddSinglePredefinedValue): Promise<boolean> => {
        try {
            const response = await this.valuesService.createSingleValueAsync(valueToAdd);
            const responseAsJson: IAddSinglePredefinedValuesResult = await response.json();
            if (response.ok) {
                runInAction(() => {
                    responseAsJson.createdValue.creationDate = new Date(responseAsJson.createdValue.creationDate);
                    this.predefinedValues.push(responseAsJson.createdValue);
                    this.totalValuesCount++;
                });
                toast.success(`Successfully added new value '${responseAsJson.createdValue.value}'!`);
                return true;
            }
            else if (response.status == 400) {
                handledError(responseAsJson.resultMessage);
            }
            else
                unexpectedError();
            return false;
        }
        catch (error) {
            criticalServerError();
            return false;
        }
    };

    public deletePredefinedValuesAsync = async (valuesIdsToDelete: number[]) => {
        try {
            const response = await this.valuesService.deleteValuesAsync({ valuesIds: valuesIdsToDelete });
            const responseAsJson: IDeletePredefinedValuesResult = await response.json();
            if (response.ok) {
                runInAction(() => {
                    const newValues = this.predefinedValues.filter(val => !valuesIdsToDelete.includes(val.id));
                    this.predefinedValues = newValues;
                });
                toast.success(`Successfully deleted ${responseAsJson.deletedValuesCount} values!`)
            }
            else if (response.status == 400) {
                handledError(responseAsJson.resultMessage);
            }
            else
                unexpectedError();
        }
        catch (error) {
            criticalServerError();
        }
    };

    public resetValuesList = () => {
        this.predefinedValues = [];
        this.totalValuesCount = 0;
    };

    get valuesCount(): number {
        return this.predefinedValues.length;
    };
}