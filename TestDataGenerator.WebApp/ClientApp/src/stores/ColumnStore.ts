﻿import { makeObservable, action, observable, computed } from "mobx";
import { toast } from 'react-toastify';
import { IColumn } from "../models/Column";

export class ColumnStore {
    public columns: IColumn[];

    constructor(existingColumns?: IColumn[]) {
        existingColumns?.forEach((c, i) => {
            c.columnSettings.id = i;
            //c.columnSettings.targetValueType = TargetValueTypeEnum[c.columnSettings.targetValueType.toString()];
            //c.columnSettings.valueSupplier.generatorType = ValueGeneratorTypeEnum[c.columnSettings.valueSupplier.generatorType.toString()];
        });
        this.columns = existingColumns || [];

        makeObservable(this, {
            columns: observable,
            addColumn: action,
            removeColumn: action,
            updateColumn: action,
            columnsCount: computed
        });
    }

    public addColumn = (column: IColumn): void => {
        this.columns.push(column);
        toast.success(`Column '${column.columnSettings.columnName}' was added!`);
    };

    public removeColumn = (column: IColumn): void => {
        const updatedColumns = this.columns.filter(col => col.columnSettings.id !== column.columnSettings.id);
        this.columns = updatedColumns;
        toast.info(`Column '${column.columnSettings.columnName}' was removed!`)
    };

    public updateColumn = (column: IColumn): void => {
        const updatedColumns = this.columns.map(col => {
            if (col.columnSettings.id === column.columnSettings.id)
                return { ...column };
            return col;
        });
        this.columns = updatedColumns;
    }

    public getColumnById = (id: number | undefined): IColumn | undefined => {
        if (id === undefined)
            return undefined;
        return this.columns.find(col => col.columnSettings.id === id);
    };

    get columnsCount() {
        return this.columns.length;
    }
}