﻿import { ColumnSetStore } from "./ColumnSetStore";
import { DatasetGeneratorStore } from "./DatasetGeneratorStore";
import { PredefinedCategoryStore } from "./PredefinedCategoryStore";
import PredefinedValueStore from "./PredefinedValueStore";
import { UserStore } from "./UserStore";

export class RootStore {
    public baseApiUrl: string = process.env.BASE_API_URL as string;

    public userStore: UserStore;
    public columnSetStore: ColumnSetStore;
    public predefinedCategoryStore: PredefinedCategoryStore;
    public predefinedValueStore: PredefinedValueStore;
    public datasetGeneratorStore: DatasetGeneratorStore;

    constructor() {
        this.userStore = new UserStore(this);
        this.columnSetStore = new ColumnSetStore(this);
        this.predefinedCategoryStore = new PredefinedCategoryStore(this);
        this.predefinedValueStore = new PredefinedValueStore(this);
        this.datasetGeneratorStore = new DatasetGeneratorStore(this);
    }
}