﻿import { action, computed, makeObservable, observable, runInAction } from "mobx";
import { toast } from "react-toastify";
import { IGetDatasetFileGenerationRequestsPaginated } from "../models/ApiRequests/GetDatasetFileGenerationRequestsPaginated";
import { IAddFileExportRequestResult } from "../models/ApiResponses/AddFileExportRequestResult";
import { IGetAllFileExportGenerationRequestsResult } from "../models/ApiResponses/GetAllFileExportGenerationRequestsResult";
import { IGetDatasetReadinessResult } from "../models/ApiResponses/GetDatasetReadinessResult";
import { IGetFileExportGenerationRequestsPaginatedResult } from "../models/ApiResponses/GetFileExportGenerationRequestsPaginatedResult";
import { IGetPreviewDatasetResult } from "../models/ApiResponses/GetPreviewDatasetResult";
import { IColumnSet } from "../models/ColumnSet";
import { IDataset } from "../models/Dataset";
import { DatasetExportFileTypeEnum } from "../models/Enums/DatasetExportFileTypeEnum";
import { IFileExportGenerationRequest } from "../models/FileExportGenerationRequest";
import DatasetGeneratorService from "../services/DatasetGeneratorService";
import { criticalServerError, handledError, unexpectedError } from "../utils/ServerErrorHandler";
import { RootStore } from "./RootStore";

export class DatasetGeneratorStore {
    public previewDataset?: IDataset;
    public fileExportGeneratonRequests: IFileExportGenerationRequest[];

    public rootStore: RootStore;
    public datasetGeneratorService: DatasetGeneratorService;
    public totalValuesCount: number;
    public currentPageNum: number;
    public valuesPerPage: number;

    public constructor(rootStore: RootStore) {
        this.rootStore = rootStore;
        this.datasetGeneratorService = new DatasetGeneratorService(rootStore.baseApiUrl);

        this.previewDataset = undefined;
        this.fileExportGeneratonRequests = [];
        this.totalValuesCount = 0;
        this.currentPageNum = 0;
        this.valuesPerPage = 0;

        makeObservable(this, {
            previewDataset: observable,
            fileExportGeneratonRequests: observable,
            totalValuesCount: observable,
            currentPageNum: observable,
            valuesPerPage: observable,
            generationRequestsCount: computed,
            generatePreviewDatasetAsync: action,
            createFileExportRequestAsync: action,
            checkResultReadinessStateAsync: action,
            downloadFileResultAsync: action,
            getAllFileExportRequestsAsync: action
        });
    }

    public generatePreviewDatasetAsync = async (columnSet: IColumnSet) => {
        try {
            const response = await this.datasetGeneratorService.generateDatasetPreview(columnSet.id);
            const responseAsJson: IGetPreviewDatasetResult = await response.json();
            if (response.ok) {
                runInAction(() => {
                    this.previewDataset = {
                        columnNames: columnSet.columnStore.columns.map(c => c.columnSettings.columnName),
                        dataRows: responseAsJson.generatedRows.map(r => r.values)
                    };
                });
            }
            else if (response.status === 400) {
                handledError(responseAsJson.resultMessage);
            }
            else
                unexpectedError();
        }
        catch (err) {
            criticalServerError();
        }
    };

    public createFileExportRequestAsync = async (columnSet: IColumnSet, rowsToGenerate: number,
        fileType: DatasetExportFileTypeEnum) => {
        try {
            let response: Response;

            switch (fileType) {
                case DatasetExportFileTypeEnum.CSV: {
                    response = await this.datasetGeneratorService.createCsvFileExportRequest({
                        datasetParameters: {
                            rowsToGenerate: rowsToGenerate,
                            columnSet: {
                                columnSetId: columnSet.id,
                                columnSetName: columnSet.name,
                                columns: columnSet.columnStore.columns
                            }
                        }
                    });
                    break;
                }

                case DatasetExportFileTypeEnum.JSON: {
                    response = await this.datasetGeneratorService.createJsonFileExportRequest({
                        datasetParameters: {
                            rowsToGenerate: rowsToGenerate,
                            columnSet: {
                                columnSetId: columnSet.id,
                                columnSetName: columnSet.name,
                                columns: columnSet.columnStore.columns
                            }
                        }
                    });
                    break;
                }

            }

            const responseAsJson: IAddFileExportRequestResult = await response.json();
            if (response.ok) {
                runInAction(() => {
                    const fileExportRequest: IFileExportGenerationRequest = {
                        requestId: responseAsJson.requestId,
                        rowsToGenerate: rowsToGenerate,
                        fileExportType: fileType,
                        columnSet: columnSet,
                        resultReady: false,
                        errorText: "",
                        requestFailedToProcess: false,
                        requestProcessedDate: null
                    };
                    this.fileExportGeneratonRequests.push(fileExportRequest);
                    this.totalValuesCount++;
                });
                toast.success(`Successfully created request with Id=${responseAsJson.requestId}!`)
            }
            else if (response.status === 400) {
                handledError(responseAsJson.resultMessage);
            }
            else
                unexpectedError();
        }
        catch (err) {
            criticalServerError();
        }
    };

    public checkResultReadinessStateAsync = async (requestId: number) => {
        try {
            const response = await this.datasetGeneratorService.checkResultReadinessState(requestId);
            const responseAsJson: IGetDatasetReadinessResult = await response.json();

            if (response.ok) {
                runInAction(() => {
                    const updatedRequestsList = this.fileExportGeneratonRequests.map(req => {
                        if (req.requestId === requestId) {
                            const updatedRequest = { ...req };
                            updatedRequest.resultReady = responseAsJson.resultReady;
                            updatedRequest.errorText = responseAsJson.errorText;
                            updatedRequest.requestFailedToProcess = responseAsJson.requestFailedToProcess;
                            updatedRequest.requestProcessedDate = responseAsJson.requestProcessedDate === null
                                ? null : new Date(responseAsJson.requestProcessedDate);
                            return updatedRequest;
                        } 
                        return req;
                    });
                    this.fileExportGeneratonRequests = updatedRequestsList;
                });
            }
            else if (response.status === 400) {
                handledError(responseAsJson.resultMessage);
            }
            else
                unexpectedError();
        }
        catch (err) {
            criticalServerError();
        }
    };

    public downloadFileResultAsync = async (requestId: number) => {
        try {
            const response = await this.datasetGeneratorService.downloadFileResult(requestId);
            if (response.status === 400) {
                const responseAsJson = await response.json();
                handledError(responseAsJson.resultMessage);
                return;
            }
            const fileBlob = new Blob([await response.blob()]);
            const blobUrl = URL.createObjectURL(fileBlob);
            var a = document.createElement("a");
            a.href = blobUrl;
            a.download = response.headers.get('content-disposition')!
                .split(';')
                .find(n => n.includes('filename='))!
                .replace('filename=', '')
                .trim();
            a.click();
        }
        catch (err) {
            criticalServerError();
        }
    };

    public getAllFileExportRequestsAsync = async () => {
        try {
            const response = await this.datasetGeneratorService.getAllFileExportRequests();
            const responseAsJson: IGetAllFileExportGenerationRequestsResult = await response.json();
            if (response.ok) {
                runInAction(() => {
                    this.fileExportGeneratonRequests = [];
                    responseAsJson.datasetRequests.map(r => {
                        if (r.columnSetId !== undefined) {
                            this.fileExportGeneratonRequests.push({
                                requestId: r.generationRequestId,
                                columnSet: this.rootStore.columnSetStore.columnSets.find(c => c.id === r.columnSetId) as IColumnSet,
                                fileExportType: DatasetExportFileTypeEnum[r.exportFileExtension.toUpperCase()],
                                resultReady: r.resultReady,
                                rowsToGenerate: r.rowsToGenerate,
                                errorText: r.errorText,
                                requestFailedToProcess: r.requestFailedToProcess,
                                requestProcessedDate: r.requestProcessedDate === null ?
                                    null : new Date(r.requestProcessedDate)
                            })
                        }
                    });
                });
            }
            else if (response.status === 400) {
                handledError(responseAsJson.resultMessage);
            }
            else
                unexpectedError();
        }
        catch (err) {
            criticalServerError();
        }
    };

    public getFileExportRequestsPaginatedAsync = async (paginationSetup: IGetDatasetFileGenerationRequestsPaginated) => {
        try {
            const response = await this.datasetGeneratorService.getFileExportRequestsPaginated(paginationSetup);
            const responseAsJson: IGetFileExportGenerationRequestsPaginatedResult = await response.json();
            if (response.ok) {
                runInAction(() => {
                    this.fileExportGeneratonRequests = responseAsJson.datasetRequests.map(r => {
                        return {
                            requestId: r.generationRequestId,
                            columnSet: this.rootStore.columnSetStore.columnSets.find(c => c.id === r.columnSetId) as IColumnSet,
                            fileExportType: DatasetExportFileTypeEnum[r.exportFileExtension.toUpperCase()],
                            resultReady: r.resultReady,
                            rowsToGenerate: r.rowsToGenerate,
                            errorText: r.errorText,
                            requestFailedToProcess: r.requestFailedToProcess,
                            requestProcessedDate: r.requestProcessedDate === null ?
                                null : new Date(r.requestProcessedDate)
                        }
                    });
                    this.totalValuesCount = responseAsJson.totalValuesCount;
                    this.currentPageNum = responseAsJson.pageNumber;
                    this.valuesPerPage = responseAsJson.valuesPerPage;
                });
            }
            else if (response.status == 400) {
                handledError(responseAsJson.resultMessage);
            }
            else
                unexpectedError();
        }
        catch (error) {
            criticalServerError();
        }
    };

    get generationRequestsCount() {
        return this.fileExportGeneratonRequests.length;
    }
}