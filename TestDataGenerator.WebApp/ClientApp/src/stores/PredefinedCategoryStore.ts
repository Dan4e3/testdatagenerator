﻿import { action, makeObservable, observable, runInAction } from "mobx";
import { toast } from "react-toastify";
import { IAddPredefinedCategory } from "../models/ApiRequests/AddPredefinedCategory";
import { IDeletePredefinedCategory } from "../models/ApiRequests/DeletePredefinedCategory";
import { IAddPredefinedCategoryResult } from "../models/ApiResponses/AddPredefinedCategoryResult";
import { IDeletePredefinedCategoryResult } from "../models/ApiResponses/DeletePredefinedCategoryResult";
import { IGetPredefinedCategoryResult } from "../models/ApiResponses/GetPredefinedCategoryResult";
import { IPredefinedCategory } from "../models/PredefinedCategory";
import PredefinedCategoryService from "../services/PredefinedCategoryService";
import { criticalServerError, handledError, unexpectedError } from "../utils/ServerErrorHandler";
import { RootStore } from "./RootStore";

export class PredefinedCategoryStore {
    public rootStore: RootStore;
    public categoryService: PredefinedCategoryService;

    public categories: IPredefinedCategory[];

    constructor(rootStore: RootStore) {
        this.rootStore = rootStore;
        this.categoryService = new PredefinedCategoryService(rootStore.baseApiUrl);

        this.categories = [];

        makeObservable(this, {
            categories: observable,
            queryPublicCategoriesAsync: action,
            queryUserCategoriesAsync: action, 
            createNewCategoryAsync: action,
            deleteCategoriesAsync: action
        });
    }

    public queryPublicCategoriesAsync = async () => {
        try {
            const response = await this.categoryService.getPublicCategoriesAsync();
            if (response.ok) {
                const responseAsJson: IGetPredefinedCategoryResult = await response.json();
                responseAsJson.data.categories.forEach(c => c.creationDate = new Date(c.creationDate));
                runInAction(() => {
                    this.categories = responseAsJson.data.categories;
                });
            }
            else
                unexpectedError();
        }
        catch (error) {
            criticalServerError();
        }
    };

    public queryUserCategoriesAsync = async (includePublic: boolean) => {
        try {
            const response = await this.categoryService.getUserCategoriesAsync(includePublic);
            const responseAsJson: IGetPredefinedCategoryResult = await response.json();
            if (response.ok) {
                responseAsJson.data.categories.forEach(c => c.creationDate = new Date(c.creationDate));
                runInAction(() => {
                    this.categories = responseAsJson.data.categories;
                });
            }
            else if (response.status == 400) {
                handledError(responseAsJson.resultMessage);
            }
            else
                unexpectedError();
        }
        catch (error) {
            criticalServerError();
        }
    }

    public createNewCategoryAsync = async (categoryToAdd: IAddPredefinedCategory): Promise<boolean> => {
        try {
            const response = await this.categoryService.addNewCategoryAsync(categoryToAdd);
            const responseAsJson: IAddPredefinedCategoryResult = await response.json();
            if (response.ok) {
                runInAction(() => {
                    responseAsJson.data.creationDate = new Date(responseAsJson.data.creationDate);
                    this.categories.push(responseAsJson.data);
                });
                toast.success(`Category '${responseAsJson.data.name}' has been added!`);
                return true;
            }
            else if (response.status == 400) {
                handledError(responseAsJson.resultMessage);
            }
            else
                unexpectedError();
            return false;
        }
        catch (error) {
            criticalServerError();
            return false;
        }
    };

    public deleteCategoriesAsync = async (categoriesToDelete: IDeletePredefinedCategory[]) => {
        try {
            const response = await this.categoryService.deleteCategoriesAsync(categoriesToDelete);
            const responseAsJson: IDeletePredefinedCategoryResult = await response.json();
            if (response.ok) {
                runInAction(async () => {
                    await this.queryUserCategoriesAsync(true);
                });
                toast.success(`Successfully removed ${responseAsJson.categoriesDeletedCount} categories!`);
            }
            else if (response.status == 400) {
                handledError(responseAsJson.resultMessage);
            }
            else
                unexpectedError();
        }
        catch (error) {
            criticalServerError();
        }
    };
}