﻿import { action, computed, makeObservable, observable, runInAction } from "mobx";
import { toast } from "react-toastify";
import { IAddColumnSetResult } from "../models/ApiResponses/AddColumnSetResult";
import { IDeleteColumnSetResult } from "../models/ApiResponses/DeleteColumnSetResult";
import { IGetUserColumnSetsResult } from "../models/ApiResponses/GetUserColumnSetsResult";
import { IUpdateColumnSetResult } from "../models/ApiResponses/UpdateColumnSetResult";
import { IValidateSingleColumnResult } from "../models/ApiResponses/ValidateSingleColumnResult";
import { IColumn } from "../models/Column";
import { IColumnSet } from "../models/ColumnSet";
import ColumnSetService from "../services/ColumnSetService";
import { criticalServerError, handledError, unexpectedError } from "../utils/ServerErrorHandler";
import { ColumnStore } from "./ColumnStore";
import { RootStore } from "./RootStore";

export class ColumnSetStore {
    public columnSets: IColumnSet[];

    public rootStore: RootStore;
    public columnSetService: ColumnSetService;

    public constructor(rootStore: RootStore) {
        this.columnSets = [];
        this.rootStore = rootStore;
        this.columnSetService = new ColumnSetService(rootStore.baseApiUrl);

        makeObservable(this, {
            columnSets: observable,
            columnSetsCount: computed,
            addColumnSetAsync: action,
            getAllColumnSetsOfUserAsync: action,
            removeColumnSetAsync: action
        });
    }

    public addColumnSetAsync = async (columnSet: IColumnSet) => {
        try {
            const response = await this.columnSetService.saveNewColumnSetAsync({
                columns: columnSet.columnStore.columns,
                columnSetName: columnSet.name
            })
            const responseAsJson: IAddColumnSetResult = await response.json();
            if (response.ok) {
                columnSet.id = responseAsJson.columnSetId;
                runInAction(() => {
                    this.columnSets.push(columnSet);
                });
                toast.success(`Column set '${columnSet.name}' was added!`);
            }
            else if (response.status == 400) {
                toast.error(`Error: ${responseAsJson.resultMessage}`);
            }
            else
                unexpectedError();
        }
        catch (err) {
            criticalServerError();
        }
    };

    public getAllColumnSetsOfUserAsync = async (): Promise<void> => {
        try {
            const response = await this.columnSetService.getUserColumnSetsAsync();
            const responseAsJson: IGetUserColumnSetsResult = await response.json();
            if (response.ok) {
                runInAction(() => {
                    this.columnSets = [];
                    responseAsJson.columnSets.map(colset => {
                        this.columnSets.push({
                            id: colset.id,
                            name: colset.name,
                            ownerUserName: this.rootStore.userStore.user.name,
                            columnStore: new ColumnStore(colset.columns)
                        });
                    });
                });
            }
            else if (response.status === 400) {
                toast.error(`Error: ${responseAsJson.resultMessage}`);
            }
            else
                unexpectedError();
        }
        catch (err) {
            criticalServerError();
        }
    };

    public removeColumnSetAsync = async (columnSet: IColumnSet): Promise<void> => {
        try {
            const response = await this.columnSetService.deleteColumnSetAsync({ columnSetId: columnSet.id });
            const responseAsJson: IDeleteColumnSetResult = await response.json();
            if (response.ok) {
                runInAction(() => {
                    const updatedColumnSets = this.columnSets.filter(cs => cs.id !== columnSet.id);
                    this.columnSets = updatedColumnSets;
                });
                toast.success(`Column set '${columnSet.name}' was removed! Total of ${responseAsJson.deletedColumnSetsCount} column sets.`)
            }
            else if (response.status === 400) {
                toast.error(`Error: ${responseAsJson.resultMessage}`);
            }
            else
                unexpectedError();
        }
        catch (err) {
            criticalServerError();
        }

    };

    public updateColumnSetAsync = async (columnSet: IColumnSet): Promise<void> => {
        try {
            const response = await this.columnSetService.updateColumnSetAsync({
                id: columnSet.id,
                name: columnSet.name,
                columns: columnSet.columnStore.columns
            });
            const responseAsJson: IUpdateColumnSetResult = await response.json();
            if (response.ok) {
                toast.success(`Column set '${columnSet.name}' (ID=${columnSet.id}) was successfully updated!`);
            }
            else if (response.status === 400) {
                toast.error(`Error: ${responseAsJson.resultMessage}`);
            }
            else
                unexpectedError();
        }
        catch (err) {
            criticalServerError();
        }
    };

    public validateSingleColumnAsync = async (column: IColumn): Promise<IValidateSingleColumnResult | undefined> => {
        try {
            const response = await this.columnSetService.validateSingleColumnAsync(column);
            const responseAsJson: IValidateSingleColumnResult = await response.json();
            if (response.ok)
                return responseAsJson;
            else if (response.status === 400) {
                handledError(responseAsJson.resultMessage);
                return undefined;
            }

            unexpectedError();
            return undefined
        }
        catch (err) {
            criticalServerError();
            return undefined;
        }
    }

    get columnSetsCount() {
        return this.columnSets.length;
    }
}