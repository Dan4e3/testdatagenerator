﻿import { IAddNewUser } from "../models/ApiRequests/AddNewUser";
import { JsonRequestType, sendGetRequestAsync, sendJsonPayloadRequestAsync } from "./ApiRequestOrganizer";

export default class UserService {
    private readonly controllerApiUrl: string = "User/";

    public serviceApiUrl: string;

    constructor(baseApiUrl: string) {
        this.serviceApiUrl = baseApiUrl + this.controllerApiUrl;
    }

    public authenticate = async (username: string, password: string): Promise<Response> => {
        const response = await sendJsonPayloadRequestAsync(this.serviceApiUrl + "login", JsonRequestType.POST, {
            name: username, password
        });
        return response;
    };

    public logout = async (): Promise<Response> => {
        const response = await sendGetRequestAsync(this.serviceApiUrl + "logout");
        return response;
    };

    public ping = async (): Promise<Response> => {
        const response = await sendGetRequestAsync(this.serviceApiUrl + "ping");
        return response;
    };

    public register = async (userRegData: IAddNewUser): Promise<Response> => {
        const response = await sendJsonPayloadRequestAsync(this.serviceApiUrl + "register", JsonRequestType.POST, userRegData);
        return response;
    };
}