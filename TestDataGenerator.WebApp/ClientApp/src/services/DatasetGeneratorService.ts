﻿import { IAddCsvExportRequest } from "../models/ApiRequests/AddCsvExportRequest";
import { IAddJsonExportRequest } from "../models/ApiRequests/AddJsonExportRequest";
import { IGetDatasetFileGenerationRequestsPaginated } from "../models/ApiRequests/GetDatasetFileGenerationRequestsPaginated";
import { JsonRequestType, sendGetRequestAsync, sendJsonPayloadRequestAsync } from "./ApiRequestOrganizer";


export default class DatasetGeneratorService {
    private readonly controllerApiUrl: string = "dataset/";

    public serviceApiUrl: string;

    constructor(baseApiUrl: string) {
        this.serviceApiUrl = baseApiUrl + this.controllerApiUrl;
    }

    public generateDatasetPreview = async (colsetId: number): Promise<Response> => {
        const response = sendGetRequestAsync(this.serviceApiUrl + "with-presaved-columnset/generate-sync", {
            columnSetId: colsetId.toString(),
            rowsToGenerate: "10"
        });
        return response;
    };

    public createCsvFileExportRequest = async (request: IAddCsvExportRequest): Promise<Response> => {
        const response = await sendJsonPayloadRequestAsync(this.serviceApiUrl + "export/request-async-csv",
            JsonRequestType.POST, request);
        return response;
    };

    public createJsonFileExportRequest = async (request: IAddJsonExportRequest): Promise<Response> => {
        const response = await sendJsonPayloadRequestAsync(this.serviceApiUrl + "export/request-async-json",
            JsonRequestType.POST, request);
        return response;
    };

    public checkResultReadinessState = async (requestId: number): Promise<Response> => {
        const response = await sendGetRequestAsync(this.serviceApiUrl + "check-result-readiness", {
            requestId: requestId.toString()
        });
        return response;
    };

    public downloadFileResult = async (requestId: number): Promise<Response> => {
        const response = await sendGetRequestAsync(this.serviceApiUrl + "export/get-file-result", {
            requestId: requestId.toString()
        });
        return response;
    };

    public getAllFileExportRequests = async (): Promise<Response> => {
        const response = await sendGetRequestAsync(this.serviceApiUrl + "export/get-all-file-requests");
        return response;
    };

    public getFileExportRequestsPaginated = async (paginationSetup: IGetDatasetFileGenerationRequestsPaginated): Promise<Response> => {
        const response = await sendGetRequestAsync(this.serviceApiUrl + "export/get-file-requests-paginated", {
            pageNumber: paginationSetup.pageNumber.toString(),
            requestsPerPage: paginationSetup.requestsPerPage.toString()
        });
        return response;
    };
}