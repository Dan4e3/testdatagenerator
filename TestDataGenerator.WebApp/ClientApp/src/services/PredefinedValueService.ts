﻿import { IAddPredefinedValueCollection } from "../models/ApiRequests/AddPredefinedValueCollection";
import { IAddSinglePredefinedValue } from "../models/ApiRequests/AddSinglePredefinedValue";
import { IDeletePredefinedValues } from "../models/ApiRequests/DeletePredefinedValues";
import { IGetPredefinedValuesPaginated } from "../models/ApiRequests/GetPredefinedValuesPaginated";
import { JsonRequestType, sendGetRequestAsync, sendJsonPayloadRequestAsync } from "./ApiRequestOrganizer";

export default class PredefinedValueService {
    private readonly controllerApiUrl: string = "PredefinedData/values/";

    public serviceApiUrl: string;

    constructor(baseApiUrl: string) {
        this.serviceApiUrl = baseApiUrl + this.controllerApiUrl;
    }

    public getValuesByCategoryAsync = async (categoryName: string, valuesToFetch?: number): Promise<Response> => {
        const getParams: any = {};
        getParams.categoryName = categoryName;
        if (valuesToFetch !== undefined)
            getParams.valuesToFetch = valuesToFetch.toString()

        const response = await sendGetRequestAsync(this.serviceApiUrl + "get", getParams);
        return response;
    }

    public createValuesAsync = async (dataToAdd: IAddPredefinedValueCollection): Promise<Response> => {
        const response = await sendJsonPayloadRequestAsync(this.serviceApiUrl + "add-many", JsonRequestType.POST, dataToAdd);
        return response;
    };

    public createSingleValueAsync = async (valueToAdd: IAddSinglePredefinedValue): Promise<Response> => {
        const response = await sendJsonPayloadRequestAsync(this.serviceApiUrl + "add", JsonRequestType.POST, valueToAdd);
        return response;
    };

    public deleteValuesAsync = async (valuesToDelete: IDeletePredefinedValues): Promise<Response> => {
        const response = await sendJsonPayloadRequestAsync(this.serviceApiUrl + "delete", JsonRequestType.DELETE, valuesToDelete);
        return response;
    };

    public getValuesByCategoryPaginatedAsync = async (getParams: IGetPredefinedValuesPaginated): Promise<Response> => {
        const response = await sendGetRequestAsync(this.serviceApiUrl + "get-paginated", {
            categoryName: getParams.categoryName,
            pageNumber: getParams.pageNumber.toString(),
            valuesPerPage: getParams.valuesPerPage.toString()
        });
        return response;
    };
}