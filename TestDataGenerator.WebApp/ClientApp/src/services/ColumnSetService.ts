﻿import { IAddColumnSet } from "../models/ApiRequests/AddColumnSet";
import { IDeleteColumnSet } from "../models/ApiRequests/DeleteColumnSet";
import { IUpdateColumnSet } from "../models/ApiRequests/UpdateColumnSet";
import { IColumn } from "../models/Column";
import { JsonRequestType, sendGetRequestAsync, sendJsonPayloadRequestAsync } from "./ApiRequestOrganizer";



export default class ColumnSetService {
    private readonly controllerApiUrl: string = "columnset/";

    public serviceApiUrl: string;

    constructor(baseApiUrl: string) {
        this.serviceApiUrl = baseApiUrl + this.controllerApiUrl;
    }

    public saveNewColumnSetAsync = async (columnSetToSave: IAddColumnSet): Promise<Response> => {
        const response = await sendJsonPayloadRequestAsync(this.serviceApiUrl + "create", JsonRequestType.POST, columnSetToSave);
        return response;
    };

    public getUserColumnSetsAsync = async (): Promise<Response> => {
        const response = await sendGetRequestAsync(this.serviceApiUrl + "get-all");
        return response;
    };

    public deleteColumnSetAsync = async (colSetToDelete: IDeleteColumnSet): Promise<Response> => {
        const response = await sendJsonPayloadRequestAsync(this.serviceApiUrl + "delete", JsonRequestType.DELETE, colSetToDelete);
        return response;
    };

    public updateColumnSetAsync = async (colsetToUpdate: IUpdateColumnSet): Promise<Response> => {
        const response = await sendJsonPayloadRequestAsync(this.serviceApiUrl + "update", JsonRequestType.PUT, colsetToUpdate);
        return response;
    };

    public validateSingleColumnAsync = async (column: IColumn): Promise<Response> => {
        const response = await sendJsonPayloadRequestAsync(this.serviceApiUrl + "validate-single-column", JsonRequestType.POST, column);
        return response;
    };
}