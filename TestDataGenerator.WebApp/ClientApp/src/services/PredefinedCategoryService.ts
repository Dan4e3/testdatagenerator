﻿import { IAddPredefinedCategory } from "../models/ApiRequests/AddPredefinedCategory";
import { IDeletePredefinedCategory } from "../models/ApiRequests/DeletePredefinedCategory";
import { JsonRequestType, sendGetRequestAsync, sendJsonPayloadRequestAsync } from "./ApiRequestOrganizer";

export default class PredefinedCategoryService {
    private readonly controllerApiUrl: string = "PredefinedData/categories/";

    public serviceApiUrl: string;

    constructor(baseApiUrl: string) {
        this.serviceApiUrl = baseApiUrl + this.controllerApiUrl;
    }

    public getPublicCategoriesAsync = async (): Promise<Response> => {
        const response = await sendGetRequestAsync(this.serviceApiUrl + "get-public");
        return response;
    };

    public getUserCategoriesAsync = async (includePublic: boolean): Promise<Response> => {
        const response = await sendGetRequestAsync(this.serviceApiUrl + "get", {
            includePublic: includePublic.toString()
        });
        return response;
    };

    public addNewCategoryAsync = async (categoryToAdd: IAddPredefinedCategory): Promise<Response> => {
        const response = await sendJsonPayloadRequestAsync(this.serviceApiUrl + "add", JsonRequestType.POST, categoryToAdd);
        return response;
    };

    public deleteCategoriesAsync = async (categoriesToDelete: IDeletePredefinedCategory[]): Promise<Response> => {
        const response = await sendJsonPayloadRequestAsync(this.serviceApiUrl + "delete", JsonRequestType.DELETE, categoriesToDelete);
        return response;
    };
}