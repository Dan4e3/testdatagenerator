﻿export enum JsonRequestType {
    POST = "POST",
    DELETE = "DELETE",
    PUT = "PUT"
}

export const sendGetRequestAsync = async (urlString: string, 
    getParams: { [paramName: string]: string } = {}): Promise<Response> => {

    const options: RequestInit = {
        method: "GET",
        credentials: "include"
    };
    const url = new URL(urlString);
    for (const key in getParams) {
        url.searchParams.set(key, getParams[key]);
    }
    const response = await fetch(url, options);
    return response;
};

export const sendJsonPayloadRequestAsync = async (urlString: string,
    requestType: JsonRequestType, payload: any) => {

    const headers: HeadersInit = new Headers();
    headers.append("Content-Type", "application/json");
    const options: RequestInit = {
        method: requestType.toString(),
        headers,
        body: JSON.stringify(payload),
        credentials: "include"
    };
    const response = await fetch(urlString, options);
    return response;
};