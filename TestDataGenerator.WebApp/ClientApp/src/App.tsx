﻿import React, { lazy, Suspense, useEffect } from "react";
import { toast, ToastContainer } from "react-toastify";
import { BrowserRouter, Route, Routes, Navigate } from "react-router-dom";
import { useStores } from "./use-stores";
import { observer } from "mobx-react";
import Login from "./components/Login/Login";
import "react-toastify/dist/ReactToastify.css";
import { Container, Box, CssBaseline, ThemeProvider, CircularProgress } from "@mui/material";
import { appTheme } from "./themes/theme";
import { GoogleReCaptchaProvider } from "react-google-recaptcha-v3";
import NavigationBar from "./components/NavigationBar/NavigationBar";
import FooterBar from "./components/FooterBar/FooterBar";
import { Helmet } from "react-helmet";

import Home from "./components/Home/Home";
import PredefinedData from "./components/PredefinedData/PredefinedData";
import Reference from "./components/Reference/Reference";
import ColumnSetList from "./components/ColumnSetList/ColumnSetList";
import DatasetGenerator from "./components/DatasetGenerator/DatasetGenerator";
import Registration from "./components/Registration/Registration";

//const HomeComponent = lazy(() => import("./components/Home/Home"));
//const PredefinedDataComponent = lazy(() => import("./components/PredefinedData/PredefinedData"));
//const ReferenceComponent = lazy(() => import("./components/Reference/Reference")); 
//const ColumnSetsComponent = lazy(() => import("./components/ColumnSetList/ColumnSetList"));
//const DatasetGeneratorComponent = lazy(() => import("./components/DatasetGenerator/DatasetGenerator"));
//const SignUpComponent = lazy(() => import("./components/Registration/Registration"));

const App = observer(() => {
    return (
        <ThemeProvider theme={appTheme}>
            <GoogleReCaptchaProvider
                reCaptchaKey={process.env.RECAPTCHA_PUBLIC_KEY as string}
                useEnterprise={true}
                container={{
                    element: "google-recaptcha",
                    parameters: {
                        badge: "inline",
                        theme: "light"
                    }
                }}>
                <Box id="google-recaptcha" visibility="hidden" height={0} width={0}>
                </Box>
                <Helmet>
                    <title>Test Data Generator - Generate CSV and JSON Datasets Online</title>
                    <meta name="description" content="Generate datasets online for any purpose including testing routines and export them to CSV and JSON files. Works fast, generates up to 1 million rows per request. Multiple generator types are present - at least one will suit your needs." />
                </Helmet>
                <CssBaseline enableColorScheme />
                <Container maxWidth="xl" disableGutters>
                    <BrowserRouter>
                        <Box minHeight="100vh" display="flex" flexDirection="column">
                            <NavigationBar />
                            <Box paddingY={4}>
                                {/*<Suspense fallback={<Box display="flex" justifyContent="center"><CircularProgress color="primary"/></Box>}>*/}
                                    <Routes>
                                        <Route path="/" element={<Home />} />
                                        <Route path="/index" element={<Home />} />
                                        <Route path="/login" element={<Login />} />
                                        <Route path="/register" element={<Registration />} />
                                            <Route path="/column-sets" element={
                                                <PrivateRoute nextPath="/column-sets">
                                                    <Helmet>
                                                        <title>Manage Column Sets - Test Data Generator</title>
                                                        <meta name="description" content="Manage your column sets for dataset generation here - create new, edit or delete ones no longer required." />
                                                    </Helmet>
                                                    <ColumnSetList />
                                                </PrivateRoute>
                                            } />
                                            <Route path="/predefined-data" element={
                                                <PrivateRoute nextPath="/predefined-data">
                                                    <Helmet>
                                                        <title>Values Collection - Test Data Generator</title>
                                                        <meta name="description" content="Create new catgories and fill them with values assortment of your choice. They can be used later during dataset generation." />
                                                    </Helmet>
                                                    <PredefinedData />
                                                </PrivateRoute>
                                            } />
                                            <Route path="/dataset-generator" element={
                                                <PrivateRoute nextPath="/dataset-generator">
                                                    <Helmet>
                                                        <title>Datasets - Test Data Generator</title>
                                                        <meta name="description" content="Take a short preview of your future dataset. After you are OK with it - generate big one in CSV or JSON format." />
                                                    </Helmet>
                                                    <DatasetGenerator />
                                                </PrivateRoute>
                                            } />
                                            <Route path="/reference" element={<Reference />} />
                                            <Route path="*" element={<Navigate to="/" replace />} />
                                    </Routes>
                                {/*</Suspense>*/}
                            </Box>
                            <FooterBar />
                        </Box>
                    </BrowserRouter>
                    <ToastContainer
                        position="bottom-right"
                        autoClose={3000}
                        hideProgressBar={false}
                        newestOnTop={false}
                        closeOnClick
                        rtl={false}
                        pauseOnFocusLoss
                        draggable
                        pauseOnHover
                    />
                </Container>
            </GoogleReCaptchaProvider>
        </ThemeProvider>
    );
});

const PrivateRoute = observer(({ children, nextPath }) => {
    const { rootStore } = useStores();

    const resultToRender = () => {
        if (rootStore.userStore.user.authenticated)
            return children;
        else {
            ///toast.warning("Please sign in to continue.");
            return <Login nextRoutePath={nextPath } />;/// (<Navigate to="/login" replace />);
        }
    }

    return (
        resultToRender()
    );
});

export default App;