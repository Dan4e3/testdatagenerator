﻿import { toast } from "react-toastify";

export const unexpectedError = (): void =>  {
    toast.error("Unexpected error occurred.")
};

export const criticalServerError = (): void => {
    toast.error("Unknown error occurred.");
};

export const handledError = (errText: string): void => {
    toast.error(`Error: ${errText}`);
};