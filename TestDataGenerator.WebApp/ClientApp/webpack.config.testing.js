﻿const { resolve } = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const TerserWebpackPlugin = require("terser-webpack-plugin");
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const webpack = require('webpack');

const config = {
    mode: "production",
    entry: {
        index: "./src/index.tsx",
    },
    output: {
        path: resolve(__dirname, "dist"),
        filename: "[name].[hash].bundle.js",
    },
    resolve: {
        extensions: [".js", ".jsx", ".ts", ".tsx", ".css"],
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: "babel-loader",
                exclude: /node_modules/,
            },
            {
                test: /\.css$/i,
                use: ["style-loader", "css-loader"],
            },
            {
                test: /\.(png|svg|jpg|jpeg|gif)$/i,
                loader: "file-loader?name=/src/static/[name].[ext]"
            },
        ],
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: "./src/index.html",
            filename: "index.html",
            inject: "body",
            favicon: "./src/favicon.ico"
        }),
        new webpack.DefinePlugin({
            "process.env.BASE_API_URL": JSON.stringify("http://localhost:5000/api/"),
            "process.env.RECAPTCHA_PUBLIC_KEY": JSON.stringify("6LfnJfEiAAAAANxPAwAPgQvzCMS49aB-FUUE0yFY"),
            "process.env.MAX_ROWS_IN_GENERATED_FILE": JSON.stringify(5000)
        }),
        new CleanWebpackPlugin()
    ],
    optimization: {
        minimize: true,
        minimizer: [new TerserWebpackPlugin()],
        runtimeChunk: "single",
        splitChunks: {
            chunks: "all"
        }
    }
};

module.exports = config;