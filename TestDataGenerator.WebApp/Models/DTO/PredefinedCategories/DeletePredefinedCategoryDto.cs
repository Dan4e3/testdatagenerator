﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.WebApp.Models.DTO.PredefinedCategories
{
    public class DeletePredefinedCategoryDto
    {
        public long CategoryId { get; set; }

        public string CategoryName { get; set; }
    }
}
