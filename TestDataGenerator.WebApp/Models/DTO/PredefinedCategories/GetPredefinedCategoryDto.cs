﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.WebApp.Models.DTO.PredefinedCategories
{
    public class GetPredefinedCategoryDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string CreatedBy { get; set; }
        public bool IsPublic { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
