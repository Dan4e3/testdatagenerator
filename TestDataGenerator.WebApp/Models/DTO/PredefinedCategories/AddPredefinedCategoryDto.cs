﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.WebApp.Models.DTO.PredefinedCategories
{
    public class AddPredefinedCategoryDto
    {
        public string Name { get; set; }

        public bool IsPublic { get; set; }
    }
}
