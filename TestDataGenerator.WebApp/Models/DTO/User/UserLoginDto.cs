﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.WebApp.Models.DTO.User
{
    public class UserLoginDto
    {
        public string Name { get; set; }
        public string Password { get; set; }
    }
}
