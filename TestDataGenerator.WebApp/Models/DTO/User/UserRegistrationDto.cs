﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.WebApp.Models.DTO.User
{
    public class UserRegistrationDto
    {
        [MinLength(5)]
        public string Name { get; set; }

        [MinLength(6)]
        [MaxLength(16)]
        public string Password { get; set; }

        public string CaptchaToken { get; set; }
    }
}
