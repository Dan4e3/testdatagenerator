﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.WebApp.Models.DTO.ValueGenerator
{
    public enum ValueGeneratorTypeEnum
    {
        PatternParser = 0,
        SelectFromList = 1,
        ValueRange = 2,
        Incremental = 3
    }
}
