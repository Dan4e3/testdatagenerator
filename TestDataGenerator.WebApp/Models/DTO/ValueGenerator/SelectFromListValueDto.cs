﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Web.Models.DomainModels.Enums;

namespace TestDataGenerator.WebApp.Models.DTO.ValueGenerator
{
    public class SelectFromListValueDto
    {
        [Required]
        public ValueDataTypeEnum DataType { get; set; }

        [Required]
        public string Value { get; set; }
    }
}
