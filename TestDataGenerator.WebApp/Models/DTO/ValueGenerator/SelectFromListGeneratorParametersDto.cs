﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.WebApp.Models.DTO.ValueGenerator
{
    public class SelectFromListGeneratorParametersDto
    {
        public SelectFromListValueDto[] Values { get; set; }

        public long[] PredefinedCategoriesIds { get; set; }
    }
}
