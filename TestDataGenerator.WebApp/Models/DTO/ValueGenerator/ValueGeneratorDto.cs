﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.WebApp.Models.DTO.ValueGenerator
{
    public class ValueGeneratorDto
    {
        [Required]
        public ValueGeneratorTypeEnum GeneratorType { get; set; }

        [Required]
        public object GeneratorParameters { get; set; }
    }
}
