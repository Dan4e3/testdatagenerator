﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.WebApp.Models.DTO.ValueGenerator
{
    public class PatternParserGeneratorParametersDto
    {
        [Required]
        public string ValuePattern { get; set; }
    }
}
