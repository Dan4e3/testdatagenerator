﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Web.Models.DomainModels.Enums;

namespace TestDataGenerator.WebApp.Models.DTO.PredefinedValues
{
    public class PredefinedValueDto
    {
        [Required]
        public ValueDataTypeEnum DataType { get; set; }

        [Required]
        public object Value { get; set; }
    }
}
