﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.WebApp.Models.DTO.PredefinedValues
{
    public class AddSinglePredefinedValueDto
    {
        [Required]
        public PredefinedValueDto ValueInfo { get; set; }

        [Required]
        public string CategoryName { get; set; }
    }
}
