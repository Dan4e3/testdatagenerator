﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Web.Models.DomainModels.Enums;

namespace TestDataGenerator.WebApp.Models.DTO.PredefinedValues
{
    public class GetPredefinedValueDto
    {
        public long Id { get; set; }

        public ValueDataTypeEnum DataType { get; set; }

        public object Value { get; set; }

        public DateTime CreationDate { get; set; }
    }
}
