﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.WebApp.Models.DTO.PredefinedValues
{
    public class GetPredefinedValuesCollectionDto
    {
        public string CategoryName { get; set; }
        public GetPredefinedValueDto[] Values { get; set; }
    }
}
