﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.WebApp.Models.DTO.Dataset
{
    public class DatasetFileGenerationRequestDto
    {
        public long GenerationRequestId { get; set; }

        public long? ColumnSetId { get; set; }

        public string ColumnSetName { get; set; }

        public int RowsToGenerate { get; set; }

        public string ExportFileExtension { get; set; }

        public bool ResultReady { get; set; }

        public bool RequestFailedToProcess { get; set; }

        public string ErrorText { get; set; }

        public DateTime? RequestProcessedDate { get; set; }
    }
}
