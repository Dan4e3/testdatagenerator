﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.WebApp.Models.DTO.ColumnSet;

namespace TestDataGenerator.WebApp.Models.DTO.Dataset
{
    public class InputTestDataSetParametersDto
    {
        public CreateNewColumnSetDto ColumnSet { get; set; }

        [Range(1, int.MaxValue)]
        public int RowsToGenerate { get; set; }
    }
}
