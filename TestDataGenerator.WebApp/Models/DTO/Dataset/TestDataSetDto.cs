﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Web.Models.DomainModels;

namespace TestDataGenerator.WebApp.Models.DTO.Dataset
{
    public class TestDataSetDto
    {
        public RowDto[] GeneratedRows { get; set; }
    }
}
