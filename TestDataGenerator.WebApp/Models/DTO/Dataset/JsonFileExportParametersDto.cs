﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.WebApp.Models.DTO.Dataset
{
    public class JsonFileExportParametersDto
    {
        [Required]
        public InputTestDataSetParametersDto DatasetParameters { get; set; }
    }
}
