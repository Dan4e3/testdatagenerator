﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.WebApp.Models.DTO.ColumnSet
{
    public class DeleteColumnSetDto
    {
        public long ColumnSetId { get; set; }
    }
}
