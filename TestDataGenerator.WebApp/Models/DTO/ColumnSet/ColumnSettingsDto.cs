﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.WebApp.Models.DTO.ValueGenerator;

namespace TestDataGenerator.WebApp.Models.DTO.ColumnSet
{
    public class ColumnSettingsDto
    {
        [Required]
        public TargetValueTypeEnum TargetValueType { get; set; }

        [Required]
        public string ColumnName { get; set; }

        [Required]
        public ValueGeneratorDto ValueSupplier { get; set; }

        [Required]
        public CultureInfo Culture { get; set; }
    }
}
