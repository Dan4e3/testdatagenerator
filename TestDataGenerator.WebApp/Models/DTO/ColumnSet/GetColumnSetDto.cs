﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.WebApp.Models.DTO.ColumnSet
{
    public class GetColumnSetDto
    {
        public long Id { get; set; }

        public ColumnDto[] Columns { get; set; }

        public string ColumnSetCustomJson { get; set; }

        public DateTime CreationDate { get; set; }

        public string Name { get; set; }
    }
}
