﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.WebApp.Models.DTO.ColumnSet
{
    public class CreateNewColumnSetDto
    {
        public long? ColumnSetId { get; set; }

        [Required]
        public ColumnDto[] Columns { get; set; }

        [Required]
        [StringLength(255, MinimumLength = 1)]
        public string ColumnSetName { get; set; }
    }
}
