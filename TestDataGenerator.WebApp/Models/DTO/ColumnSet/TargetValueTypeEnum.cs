﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.WebApp.Models.DTO.ColumnSet
{
    public enum TargetValueTypeEnum
    {
        String = 0,
        IntegerNumber = 1,
        FloatingPointNumber = 2, 
        Boolean = 3,
        DateTime = 4
    }
}
