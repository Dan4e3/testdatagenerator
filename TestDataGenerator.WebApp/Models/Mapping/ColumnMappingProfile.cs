﻿using AutoMapper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces.ValuesHandling;
using TestDataGenerator.Core.Columns;
using TestDataGenerator.Core.ValueGenerators;
using TestDataGenerator.Core.ValueGenerators.IncrementalGenerator;
using TestDataGenerator.Core.ValueGenerators.ValuePatternParser;
using TestDataGenerator.Core.ValueGenerators.ValueRangeGenerator;
using TestDataGenerator.Web.BusinessLogic;
using TestDataGenerator.Web.DataAccess.Interfaces.Repositories;
using TestDataGenerator.Web.Models.DomainModels;
using TestDataGenerator.Web.Models.DomainModels.Enums;
using TestDataGenerator.WebApp.Models.DTO.ColumnSet;
using TestDataGenerator.WebApp.Models.DTO.ValueGenerator;

namespace TestDataGenerator.WebApp.Models.Mapping
{
    public class ColumnMappingProfile: Profile
    {
        public ColumnMappingProfile()
        {
            CreateMap<Column, ColumnDto>()
                .ForMember(dest => dest.ColumnSettings, opts => opts.MapFrom(src => src.ColumnSettings));

            CreateMap<ColumnDto, Column>()
                .ForMember(dest => dest.ColumnSettings, opts => opts.MapFrom(src => src.ColumnSettings))
                .ForCtorParam("settings", opts => opts.MapFrom(src => src.ColumnSettings));

            CreateMap<ColumnSettings, ColumnSettingsDto>()
                .ForMember(dest => dest.ColumnName, opts => opts.MapFrom(src => src.ColumnName))
                .ForMember(dest => dest.ValueSupplier, opts => opts.MapFrom<ValueGeneratorDtoForColumnResolver>())
                .ForMember(dest => dest.Culture, opts => opts.MapFrom(src => src.Culture))
                .ForMember(dest => dest.TargetValueType,
                    opts => opts.MapFrom(src => ValuesConverterHelper.TargetTypeMapper.FirstOrDefault(p => p.Value == src.TargetValueType).Key));

            CreateMap<ColumnSettingsDto, ColumnSettings>()
                .ForMember(dest => dest.ColumnName, opts => opts.MapFrom(src => src.ColumnName))
                .BeforeMap<CheckColumnSettingsDtoParamsAction>()
                .ForMember(dest => dest.ValueSupplier, opts => opts.MapFrom<ValueGeneratorForColumnDtoResolver>())
                .ForMember(dest => dest.Culture, opts => opts.MapFrom(src => src.Culture))
                .ForMember(dest => dest.TargetValueType,
                    opts => opts.MapFrom(src => ValuesConverterHelper.TargetTypeMapper[(ValueDataTypeEnum)src.TargetValueType]));
        }
    }

    public class ValueGeneratorDtoForColumnResolver : IValueResolver<ColumnSettings, ColumnSettingsDto, ValueGeneratorDto>
    {
        public ValueGeneratorDto Resolve(ColumnSettings source, ColumnSettingsDto destination, ValueGeneratorDto destMember, ResolutionContext context)
        {
            IValueGenerator generator = source.ValueSupplier;

            ValueGeneratorDto resultDto = context.Mapper.Map<ValueGeneratorDto>(generator);

            //if (generator is ValuePatternParser patternParser)
            //{
            //    resultDto = context.Mapper.Map<ValueGeneratorDto>(patternParser);
            //}
            //else if (generator is SelectFromListValueGenerator selectFromListGenerator)
            //{
            //    resultDto = context.Mapper.Map<ValueGeneratorDto>(selectFromListGenerator);
            //}
            //else if (generator is ValueRangeValueGenerator valueRangeGenerator)
            //{
            //    resultDto = context.Mapper.Map<ValueGeneratorDto>(valueRangeGenerator);
            //}
            //else if (generator is IncrementalValueGenerator incrementalValueGenerator)
            //{
            //    resultDto = context.Mapper.Map<ValueGeneratorDto>(incrementalValueGenerator);
            //}
            //else
            //    throw new Exception("Unregistered IValueGenerator encountered during conversion to DTO.");

            return resultDto;
        }
    }

    public class CheckColumnSettingsDtoParamsAction : IMappingAction<ColumnSettingsDto, ColumnSettings>
    {
        public void Process(ColumnSettingsDto source, ColumnSettings destination, ResolutionContext context)
        {
            JObject generatorParamsAsJobject = (JObject)source.ValueSupplier.GeneratorParameters;
            string generatorParamsAsString = generatorParamsAsJobject.ToString();

            switch (source.ValueSupplier.GeneratorType)
            {
                case ValueGeneratorTypeEnum.ValueRange:
                    ValueRangeGeneratorParametersDto valueRangeParameters =
                        JsonConvert.DeserializeObject<ValueRangeGeneratorParametersDto>(generatorParamsAsString);
                    if (valueRangeParameters.MinValue.Trim().Length == 0)
                        throw new CustomBusinessLogicException("Minimum value can't be empty!");
                    if (valueRangeParameters.MaxValue.Trim().Length == 0)
                        throw new CustomBusinessLogicException("Maximum value can't be empty!");
                    break;

                case ValueGeneratorTypeEnum.Incremental:
                    IncrementalGeneratorParametersDto incrementalParameters =
                        JsonConvert.DeserializeObject<IncrementalGeneratorParametersDto>(generatorParamsAsString);
                    if (incrementalParameters.StartValue.Trim().Length == 0)
                        throw new CustomBusinessLogicException("Start value can't be empty!");
                    if (incrementalParameters.EndValue.Trim().Length == 0)
                        throw new CustomBusinessLogicException("End value can't be empty!");
                    if (incrementalParameters.ValueStep.Trim().Length == 0)
                        throw new CustomBusinessLogicException("Value step can't be empty!");
                    break;

                case ValueGeneratorTypeEnum.SelectFromList:
                    SelectFromListGeneratorParametersDto selectFromListParameters =
                        JsonConvert.DeserializeObject<SelectFromListGeneratorParametersDto>(generatorParamsAsString);

                    if (selectFromListParameters.Values == null || selectFromListParameters.Values.Length == 0)
                        return;

                    SelectFromListValueDto currentValueDto = null;
                    try
                    {
                        foreach (SelectFromListValueDto valueDto in selectFromListParameters.Values)
                        {
                            currentValueDto = valueDto;
                            ValuesConverterHelper.ConvertAndBoxStringValue(valueDto.Value, valueDto.DataType);
                        }
                    }
                    catch (FormatException)
                    {
                        throw new CustomBusinessLogicException($"Failed to convert value '{currentValueDto?.Value}' " +
                            $"to type '{Enum.GetName(typeof(ValueDataTypeEnum), currentValueDto.DataType)}'.");
                    }
                    break;
            }
        }
    }

    public class ValueGeneratorForColumnDtoResolver : IValueResolver<ColumnSettingsDto, ColumnSettings, IValueGenerator>
    {
        public IValueGenerator Resolve(ColumnSettingsDto source, ColumnSettings destination, IValueGenerator destMember, ResolutionContext context)
        {
            JObject generatorParamsAsJobject = (JObject)source.ValueSupplier.GeneratorParameters;
            string generatorParamsAsString = generatorParamsAsJobject.ToString();
            IValueGenerator generatorResult;

            switch (source.ValueSupplier.GeneratorType)
            {
                case ValueGeneratorTypeEnum.PatternParser:
                    PatternParserGeneratorParametersDto parserParameters =
                        JsonConvert.DeserializeObject<PatternParserGeneratorParametersDto>(generatorParamsAsString);
                    ValuePatternParser parserGenerator = new ValuePatternParser(parserParameters.ValuePattern);
                    generatorResult = parserGenerator;
                    break;

                case ValueGeneratorTypeEnum.SelectFromList:
                    SelectFromListGeneratorParametersDto selectFromListParameters =
                        JsonConvert.DeserializeObject<SelectFromListGeneratorParametersDto>(generatorParamsAsString);
                    SelectFromListValueGenerator selectFromListGenerator = new SelectFromListValueGenerator(
                            selectFromListParameters.Values.Select(x => ValuesConverterHelper.ConvertAndBoxStringValue(x.Value, x.DataType)
                        ));
                    generatorResult = selectFromListGenerator;
                    break;

                case ValueGeneratorTypeEnum.ValueRange:
                    ValueRangeGeneratorParametersDto valueRangeParameters =
                        JsonConvert.DeserializeObject<ValueRangeGeneratorParametersDto>(generatorParamsAsString);
                    ValueRangeValueGenerator valueRangeGenerator = new ValueRangeValueGenerator(
                        new ValueRange(
                            ValuesConverterHelper.ConvertAndBoxStringValue(valueRangeParameters.MinValue, valueRangeParameters.DataType),
                            ValuesConverterHelper.ConvertAndBoxStringValue(valueRangeParameters.MaxValue, valueRangeParameters.DataType))
                        );
                    generatorResult = valueRangeGenerator;
                    break;

                case ValueGeneratorTypeEnum.Incremental:
                    IncrementalGeneratorParametersDto incrementalParameters =
                        JsonConvert.DeserializeObject<IncrementalGeneratorParametersDto>(generatorParamsAsString);

                    object valueIncrement = null;
                    if (incrementalParameters.DataType == ValueDataTypeEnum.DateTime)
                        valueIncrement = TimeSpan.Parse(incrementalParameters.ValueStep, ValuesConverterHelper.Culture);
                    else
                        valueIncrement = ValuesConverterHelper.ConvertAndBoxStringValue(incrementalParameters.ValueStep, incrementalParameters.DataType);

                    IncrementalValueGenerator incrementalValueGenerator = new IncrementalValueGenerator(
                        new ValueIncrement(
                                ValuesConverterHelper.ConvertAndBoxStringValue(incrementalParameters.StartValue, incrementalParameters.DataType),
                                ValuesConverterHelper.ConvertAndBoxStringValue(incrementalParameters.EndValue, incrementalParameters.DataType),
                                valueIncrement
                            )
                        );
                    generatorResult = incrementalValueGenerator;
                    break;

                default:
                    throw new CustomBusinessLogicException($"Unrecognized generator type " +
                        $"provided ({Enum.GetName(typeof(ValueGeneratorTypeEnum), source.ValueSupplier.GeneratorType)})!");
            }

            return generatorResult;
        }
    }
}
