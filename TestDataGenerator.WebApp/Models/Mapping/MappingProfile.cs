﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Core.Columns;
using TestDataGenerator.Web.BusinessLogic.Services;
using TestDataGenerator.Web.Models.DomainModels;
using TestDataGenerator.Web.Models.DomainModels.Enums;
using TestDataGenerator.Web.Mq.Models;
using TestDataGenerator.WebApp.Models.DTO.ColumnSet;
using TestDataGenerator.WebApp.Models.DTO.Dataset;
using TestDataGenerator.WebApp.Models.DTO.PredefinedCategories;
using TestDataGenerator.WebApp.Models.DTO.PredefinedValues;
using TestDataGenerator.WebApp.Models.DTO.User;

namespace TestDataGenerator.WebApp.Models.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<UserRegistrationDto, UserDm>()
                .ForMember(dst => dst.Name, opts => opts.MapFrom(src => src.Name))
                .ForMember(dst => dst.Password, opts => opts.MapFrom(src => src.Password));

            CreateMap<SingleRowMqDto, RowDto>()
                .ForMember(dst => dst.Values, opts => opts.MapFrom(src => src.Values));
        }
    }
}
