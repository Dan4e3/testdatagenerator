﻿using System;
using System.Collections.Generic;
using System.Globalization;
using TestDataGenerator.Web.BusinessLogic;
using TestDataGenerator.Web.Models.DomainModels.Enums;

namespace TestDataGenerator.WebApp.Models.Mapping
{
    public static class ValuesConverterHelper
    {
        public static CultureInfo Culture = CultureInfo.InvariantCulture;

        public static Dictionary<Type, ValueDataTypeEnum> SysTypeToDataTypeEnumMapper = new Dictionary<Type, ValueDataTypeEnum>() {
            { typeof(string), ValueDataTypeEnum.String },
            { typeof(long), ValueDataTypeEnum.IntNumber },
            { typeof(double), ValueDataTypeEnum.FloatPointNumber },
            { typeof(bool), ValueDataTypeEnum.Boolean },
            { typeof(DateTime), ValueDataTypeEnum.DateTime }
        };

        public static Dictionary<ValueDataTypeEnum, Type> TargetTypeMapper = new Dictionary<ValueDataTypeEnum, Type>() {
                { ValueDataTypeEnum.String, typeof(string) },
                { ValueDataTypeEnum.IntNumber, typeof(long) },
                { ValueDataTypeEnum.FloatPointNumber, typeof(double) },
                { ValueDataTypeEnum.Boolean, typeof(bool) },
                { ValueDataTypeEnum.DateTime, typeof(DateTime) }
            };

        public static ValueDataTypeEnum GetDataTypeEnumFromSystemType(Type systemType)
        {
            if (SysTypeToDataTypeEnumMapper.ContainsKey(systemType))
                return SysTypeToDataTypeEnumMapper[systemType];
            else
                throw new Exception("Unregistered datatype encountered during system type to datatype enum conversion!");
        }

        public static string UnboxAndConvertValueToString(object valueToProcess, ValueDataTypeEnum inputValueType)
        {
            try
            {

                switch (inputValueType)
                {
                    case ValueDataTypeEnum.String:
                        return Convert.ToString(valueToProcess);

                    case ValueDataTypeEnum.IntNumber:
                        return Convert.ToInt64(valueToProcess, Culture).ToString(Culture);

                    case ValueDataTypeEnum.FloatPointNumber:
                        return Convert.ToDouble(valueToProcess, Culture).ToString(Culture);

                    case ValueDataTypeEnum.Boolean:
                        return Convert.ToBoolean(valueToProcess, Culture).ToString(Culture);

                    case ValueDataTypeEnum.DateTime:
                        return Convert.ToDateTime(valueToProcess, Culture).ToString(Culture);

                    default:
                        throw new Exception("Provided value data type is not supported!");
                }
            }
            catch
            {
                throw new CustomBusinessLogicException($"Failed to convert value '{valueToProcess}' " +
                    $"to type '{Enum.GetName(typeof(ValueDataTypeEnum), inputValueType)}'");
            }
        }

        public static object ConvertAndBoxStringValue(string valueToProcess, ValueDataTypeEnum storedValueType)
        {
            switch (storedValueType)
            {
                case ValueDataTypeEnum.String:
                    return (object)valueToProcess;

                case ValueDataTypeEnum.IntNumber:
                    return Convert.ToInt64(valueToProcess, Culture);

                case ValueDataTypeEnum.FloatPointNumber:
                    return Convert.ToDouble(valueToProcess, Culture);

                case ValueDataTypeEnum.Boolean:
                    return Convert.ToBoolean(valueToProcess, Culture);

                case ValueDataTypeEnum.DateTime:
                    return Convert.ToDateTime(valueToProcess, Culture);

                default:
                    throw new Exception("Provided value data type is not supported!");
            }
        }
    }
}
