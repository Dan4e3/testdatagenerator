﻿using AutoMapper;
using System.Linq;
using TestDataGenerator.Core;
using TestDataGenerator.Web.Models.DomainModels;
using TestDataGenerator.WebApp.Models.DTO.Dataset;

namespace TestDataGenerator.WebApp.Models.Mapping
{
    public class DatasetMappingProfile: Profile
    {
        public DatasetMappingProfile()
        {
            CreateMap<Row, RowDto>()
                .ForMember(dest => dest.Values, opts => opts.MapFrom<RowDtoValuesResolver>());

            CreateMap<DatasetGenerationRequestDm, DatasetFileGenerationRequestDto>()
                .ForMember(dst => dst.ColumnSetId, opts => opts.MapFrom(src => src.ColumnSetId))
                .ForMember(dst => dst.ColumnSetName, opts => opts.MapFrom(src => src.ColumnSet == null ? null : src.ColumnSet.Name))
                .ForMember(dst => dst.GenerationRequestId, opts => opts.MapFrom(src => src.Id))
                .ForMember(dst => dst.RowsToGenerate, opts => opts.MapFrom(src => src.RowsToGenerate))
                .ForMember(dst => dst.ExportFileExtension, opts => opts.MapFrom(src => src.FileExtension))
                .ForMember(dst => dst.ResultReady, opts => opts.MapFrom(src => src.WorkerResponseReceived))
                .ForMember(dst => dst.RequestFailedToProcess, opts => opts.MapFrom(src => src.ErrorText != null))
                .ForMember(dst => dst.ErrorText, opts => opts.MapFrom(src => src.ErrorText))
                .ForMember(dst => dst.RequestProcessedDate, opts => opts.MapFrom(src => src.WorkerResponseReceivedDate));
        }
    }

    public class RowDtoValuesResolver : IValueResolver<Row, RowDto, string[]>
    {
        public string[] Resolve(Row source, RowDto destination, string[] destMember, ResolutionContext context)
        {
            string[] result = source.Select((v, i) => source.GetStringRepresentation(i)).ToArray();
            return result;
        }
    }
}
