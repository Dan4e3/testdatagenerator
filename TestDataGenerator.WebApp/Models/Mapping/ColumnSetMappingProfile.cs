﻿using AutoMapper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;
using TestDataGenerator.Core.Columns;
using TestDataGenerator.Web.Models.AdditionalColumnParams;
using TestDataGenerator.Web.Models.DomainModels;
using TestDataGenerator.WebApp.Models.DTO.ColumnSet;
using TestDataGenerator.WebApp.Models.DTO.ValueGenerator;

namespace TestDataGenerator.WebApp.Models.Mapping
{
    public class ColumnSetMappingProfile: Profile
    {
        public ColumnSetMappingProfile()
        {
            CreateMap<ColumnSetDm, GetColumnSetDto>()
                .ForMember(dst => dst.Id, opts => opts.MapFrom(src => src.Id))
                .ForMember(dst => dst.ColumnSetCustomJson, opts => opts.MapFrom(src => src.ColumnSetJson))
                .ForMember(dst => dst.CreationDate, opts => opts.MapFrom(src => src.CreationDate))
                .ForMember(dst => dst.Name, opts => opts.MapFrom(src => src.Name))
                .ForMember(dst => dst.Columns, opts => opts.MapFrom<ColumnSetColumnsResolver>());

            CreateMap<CreateNewColumnSetDto, ColumnSetDm>()
                .ForMember(dst => dst.Id, opts => opts.MapFrom(src => src.ColumnSetId.HasValue ? src.ColumnSetId.Value : 0))
                .ForMember(dst => dst.Name, opts => opts.MapFrom(src => src.ColumnSetName))
                .ForMember(dst => dst.ColumnSetJson, opts => opts.MapFrom<ColumnSetCustomJsonResolver>())
                .ForMember(dst => dst.AdditionalColumnParamsJson, opts => opts.MapFrom<ColumnSetAdditionalParametersResolver>())
                .ForMember(dst => dst.CreationDate, opts => opts.Ignore());

            CreateMap<UpdateColumnSetDto, ColumnSetDm>()
                .ForMember(dst => dst.Id, opts => opts.MapFrom(src => src.Id))
                .ForMember(dst => dst.Name, opts => opts.MapFrom(src => src.Name))
                .ForMember(dst => dst.AdditionalColumnParamsJson, opts => opts.MapFrom<ColumnSetAdditionalParametersResolver>())
                .ForMember(dst => dst.ColumnSetJson, opts => opts.MapFrom<ColumnSetCustomJsonResolver>());
        }
    }

    public class ColumnSetColumnsResolver : IValueResolver<ColumnSetDm, GetColumnSetDto, ColumnDto[]>
    {
        public ColumnDto[] Resolve(ColumnSetDm source, GetColumnSetDto destination, ColumnDto[] destMember, ResolutionContext context)
        {
            ColumnSet colSet = ColumnSet.DeserializeFromJson(source.ColumnSetJson);
            ColumnDto[] mappedColumnsDto = context.Mapper.Map<ColumnDto[]>(colSet.Cast<Column>());
            
            if (source.AdditionalColumnParamsJson == null)
                return mappedColumnsDto;

            AdditionalColumnParameters[] additionalParams = JsonConvert.DeserializeObject<AdditionalColumnParameters[]>(source.AdditionalColumnParamsJson);
            if (additionalParams == null || additionalParams.Length == 0)
                return mappedColumnsDto;

            foreach (AdditionalColumnParameters colParams in additionalParams)
            {
                ColumnDto matchedColumnDto = mappedColumnsDto.FirstOrDefault(c => c.ColumnSettings.ColumnName == colParams.ColumnName);
                if (matchedColumnDto == null)
                    continue;
                switch (matchedColumnDto.ColumnSettings.ValueSupplier.GeneratorType)
                {
                    case ValueGeneratorTypeEnum.SelectFromList:
                        SelectFromListAdditionalColumnParams selectFromListparams = 
                            JsonConvert.DeserializeObject<SelectFromListAdditionalColumnParams>(colParams.ParamsJson);
                        if (selectFromListparams.PredefinedCategoriesIds == null || selectFromListparams.PredefinedCategoriesIds.Length == 0)
                            continue;
                        SelectFromListGeneratorParametersDto generatorParams = (SelectFromListGeneratorParametersDto)matchedColumnDto.ColumnSettings.ValueSupplier.GeneratorParameters;
                        generatorParams.PredefinedCategoriesIds = selectFromListparams.PredefinedCategoriesIds;
                        break;
                }
            }

            return mappedColumnsDto;
        }
    }

    public class ColumnSetAdditionalParametersResolver : IValueResolver<CreateNewColumnSetDto, ColumnSetDm, string>,
        IValueResolver<UpdateColumnSetDto, ColumnSetDm, string>
    {
        public string Resolve(CreateNewColumnSetDto source, ColumnSetDm destination, string destMember, ResolutionContext context)
        {
            string result = GetAdditionalParamsFromColumnsDtoArray(source.Columns);
            return result;
        }

        public string Resolve(UpdateColumnSetDto source, ColumnSetDm destination, string destMember, ResolutionContext context)
        {
            string result = GetAdditionalParamsFromColumnsDtoArray(source.Columns);
            return result;
        }

        private string GetAdditionalParamsFromColumnsDtoArray(ColumnDto[] columns)
        {
            List<AdditionalColumnParameters> additionalParams = new List<AdditionalColumnParameters>();

            foreach (ColumnDto col in columns)
            {
                string generatorParamsAsString = ((JObject)col.ColumnSettings.ValueSupplier.GeneratorParameters).ToString();
                switch (col.ColumnSettings.ValueSupplier.GeneratorType)
                {
                    case ValueGeneratorTypeEnum.SelectFromList:
                        SelectFromListGeneratorParametersDto existingGeneratorParams =
                            JsonConvert.DeserializeObject<SelectFromListGeneratorParametersDto>(generatorParamsAsString);
                        if (existingGeneratorParams.PredefinedCategoriesIds == null || existingGeneratorParams.PredefinedCategoriesIds.Length == 0)
                            continue;
                        SelectFromListAdditionalColumnParams selectFromListAdditionalParams = new SelectFromListAdditionalColumnParams()
                        {
                            PredefinedCategoriesIds = existingGeneratorParams.PredefinedCategoriesIds
                        };
                        string selectFromListAdditionalParamsAsJsonString = JsonConvert.SerializeObject(selectFromListAdditionalParams);
                        additionalParams.Add(new AdditionalColumnParameters()
                        {
                            ColumnName = col.ColumnSettings.ColumnName,
                            ParamsJson = selectFromListAdditionalParamsAsJsonString
                        });
                        break;
                }
            }

            if (additionalParams.Count > 0)
                return JsonConvert.SerializeObject(additionalParams);

            return "[]";
        }
    }

    public class ColumnSetCustomJsonResolver : IValueResolver<CreateNewColumnSetDto, ColumnSetDm, string>, 
        IValueResolver<UpdateColumnSetDto, ColumnSetDm, string>
    {
        public string Resolve(CreateNewColumnSetDto source, ColumnSetDm destination, string destMember, ResolutionContext context)
        {
            Column[] columns = context.Mapper.Map<Column[]>(source.Columns);
            ColumnSet colset = new ColumnSet(columns);
            return colset.SerializeAsJson();
        }

        public string Resolve(UpdateColumnSetDto source, ColumnSetDm destination, string destMember, ResolutionContext context)
        {
            Column[] columns = context.Mapper.Map<Column[]>(source.Columns);
            ColumnSet colset = new ColumnSet(columns);
            return colset.SerializeAsJson();
        }
    }
}
