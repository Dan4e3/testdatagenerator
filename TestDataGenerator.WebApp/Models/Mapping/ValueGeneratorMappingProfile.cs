﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using TestDataGenerator.Core.ValueGenerators;
using TestDataGenerator.Core.ValueGenerators.IncrementalGenerator;
using TestDataGenerator.Core.ValueGenerators.ValuePatternParser;
using TestDataGenerator.Core.ValueGenerators.ValueRangeGenerator;
using TestDataGenerator.Web.Models.DomainModels.Enums;
using TestDataGenerator.WebApp.Models.DTO.ValueGenerator;

namespace TestDataGenerator.WebApp.Models.Mapping
{
    public class ValueGeneratorMappingProfile: Profile
    {
        public ValueGeneratorMappingProfile()
        {
            CreateMap<ValuePatternParser, ValueGeneratorDto>()
                .ForMember(dest => dest.GeneratorType, opts => opts.MapFrom(src => ValueGeneratorTypeEnum.PatternParser))
                .ForMember(dest => dest.GeneratorParameters, opts => opts.MapFrom<ValuePatternParserResolver>());

            CreateMap<SelectFromListValueGenerator, ValueGeneratorDto>()
                .ForMember(dest => dest.GeneratorType, opts => opts.MapFrom(src => ValueGeneratorTypeEnum.SelectFromList))
                .ForMember(dest => dest.GeneratorParameters, opts => opts.MapFrom<SelectFromLstResolver>());

            CreateMap<ValueRangeValueGenerator, ValueGeneratorDto>()
                .ForMember(dest => dest.GeneratorType, opts => opts.MapFrom(src => ValueGeneratorTypeEnum.ValueRange))
                .ForMember(dest => dest.GeneratorParameters, opts => opts.MapFrom<ValueRangeGeneratorResolver>());

            CreateMap<IncrementalValueGenerator, ValueGeneratorDto>()
                .ForMember(dest => dest.GeneratorType, opts => opts.MapFrom(src => ValueGeneratorTypeEnum.Incremental))
                .ForMember(dest => dest.GeneratorParameters, opts => opts.MapFrom<IncrementalValueGeneratorResolver>());
        }
    }

    public class ValuePatternParserResolver : IValueResolver<ValuePatternParser, ValueGeneratorDto, object>
    {
        public object Resolve(ValuePatternParser source, ValueGeneratorDto destination, object destMember, ResolutionContext context)
        {
            return new PatternParserGeneratorParametersDto()
            {
                ValuePattern = source.ValuePattern
            };
        }
    }

    public class SelectFromLstResolver : IValueResolver<SelectFromListValueGenerator, ValueGeneratorDto, object>
    {
        public object Resolve(SelectFromListValueGenerator source, ValueGeneratorDto destination, object destMember, ResolutionContext context)
        {
            SelectFromListGeneratorParametersDto generatorParamsDto = new SelectFromListGeneratorParametersDto() {
                Values = source.ArrayOfValuesToSelectFrom.Select(x => new SelectFromListValueDto() { 
                   DataType = ValuesConverterHelper.GetDataTypeEnumFromSystemType(x.GetType()),
                   Value = ValuesConverterHelper.UnboxAndConvertValueToString(x, ValuesConverterHelper.GetDataTypeEnumFromSystemType(x.GetType()))
                }).ToArray()
            };

            return generatorParamsDto;
        }
    }

    public class ValueRangeGeneratorResolver : IValueResolver<ValueRangeValueGenerator, ValueGeneratorDto, object>
    {
        public object Resolve(ValueRangeValueGenerator source, ValueGeneratorDto destination, object destMember, ResolutionContext context)
        {
            ValueDataTypeEnum rangeDataType = ValuesConverterHelper.GetDataTypeEnumFromSystemType(source.ValuesRange.GetValueTargetType());
            ValueRangeGeneratorParametersDto generatorParamsDto = new ValueRangeGeneratorParametersDto() { 
                DataType = rangeDataType,
                MinValue = ValuesConverterHelper.UnboxAndConvertValueToString(source.ValuesRange.GetMinRangeValue<object>(), rangeDataType),
                MaxValue = ValuesConverterHelper.UnboxAndConvertValueToString(source.ValuesRange.GetMaxRangeValue<object>(), rangeDataType)
            };

            return generatorParamsDto;
        }
    }

    public class IncrementalValueGeneratorResolver : IValueResolver<IncrementalValueGenerator, ValueGeneratorDto, object>
    {
        public object Resolve(IncrementalValueGenerator source, ValueGeneratorDto destination, object destMember, ResolutionContext context)
        {
            ValueDataTypeEnum dataType = ValuesConverterHelper.GetDataTypeEnumFromSystemType(source.ValueIncrement.GetValueTargetType());
            string valueStep = null;
            if (dataType == ValueDataTypeEnum.DateTime)
                valueStep = source.ValueIncrement.GetValueStep<TimeSpan>().ToString(null, ValuesConverterHelper.Culture);
            else
                valueStep = ValuesConverterHelper.UnboxAndConvertValueToString(source.ValueIncrement.GetValueStep<object>(), dataType);

            IncrementalGeneratorParametersDto generatorParamsDto = new IncrementalGeneratorParametersDto() {
                DataType = dataType,
                StartValue = ValuesConverterHelper.UnboxAndConvertValueToString(source.ValueIncrement.GetStartValue<object>(), dataType),
                EndValue = ValuesConverterHelper.UnboxAndConvertValueToString(source.ValueIncrement.GetEndValue<object>(), dataType),
                ValueStep = valueStep
            };

            return generatorParamsDto;
        }
    }
}
