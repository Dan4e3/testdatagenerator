﻿using AutoMapper;
using TestDataGenerator.Web.BusinessLogic;
using TestDataGenerator.Web.Models.DomainModels;
using TestDataGenerator.WebApp.Models.DTO.PredefinedCategories;
using TestDataGenerator.WebApp.Models.DTO.PredefinedValues;

namespace TestDataGenerator.WebApp.Models.Mapping
{
    public class PredefinedDataMappingProfile: Profile
    {
        public PredefinedDataMappingProfile()
        {
            CreateMap<PredefinedCategoryDm, GetPredefinedCategoryDto>()
                .ForMember(dst => dst.Name, opts => opts.MapFrom(src => src.Name))
                .ForMember(dst => dst.CreatedBy, opts => opts.MapFrom(src => src.ContributorUser.Name))
                .ForMember(dst => dst.Id, opts => opts.MapFrom(src => src.Id))
                .ForMember(dst => dst.IsPublic, opts => opts.MapFrom(src => src.IsPublic))
                .ForMember(dst => dst.CreationDate, opts => opts.MapFrom(src => src.CreationDate));

            CreateMap<AddPredefinedCategoryDto, PredefinedCategoryDm>()
                .ForMember(dst => dst.Name, opts => opts.MapFrom(src => src.Name))
                .ForMember(dst => dst.IsPublic, opts => opts.MapFrom(src => src.IsPublic));

            CreateMap<DeletePredefinedCategoryDto, PredefinedCategoryDm>()
                .ForMember(dst => dst.Id, opts => opts.MapFrom(src => src.CategoryId))
                .ForMember(dst => dst.Name, opts => opts.MapFrom(src => src.CategoryName));

            CreateMap<PredefinedValueDm, GetPredefinedValueDto>()
                .ForMember(dst => dst.Id, opts => opts.MapFrom(src => src.Id))
                .ForMember(dst => dst.DataType, opts => opts.MapFrom(src => src.DataType))
                .ForMember(dst => dst.CreationDate, opts => opts.MapFrom(src => src.CreationDate))
                .ForMember(dst => dst.Value, opts => opts.MapFrom<GetPredefinedValueDtoValueResolver>());

            CreateMap<AddSinglePredefinedValueDto, PredefinedValueDm>()
                .ForPath(dst => dst.Category.Name, opts => opts.MapFrom(src => src.CategoryName))
                .ForMember(dst => dst.DataType, opts => opts.MapFrom(src => src.ValueInfo.DataType))
                .BeforeMap<TryUnboxIncomingPredefinedValueAction>()
                .ForMember(dst => dst.Value, opts => opts.MapFrom<PredefinedValueDmValueResolver>());

            CreateMap<PredefinedValueDto, PredefinedValueDm>()
                .ForMember(dst => dst.DataType, opts => opts.MapFrom(src => src.DataType))
                .ForMember(dst => dst.Value, opts => opts.MapFrom<PredefinedValueDtoValueResolver>());
        }
    }

    public class GetPredefinedValueDtoValueResolver : IValueResolver<PredefinedValueDm, GetPredefinedValueDto, object>
    {
        public object Resolve(PredefinedValueDm source, GetPredefinedValueDto destination, object destMember, ResolutionContext context)
        {
            object convertedResult = ValuesConverterHelper.ConvertAndBoxStringValue(source.Value, source.DataType);
            return convertedResult;
        }
    }

    public class PredefinedValueDmValueResolver : IValueResolver<AddSinglePredefinedValueDto, PredefinedValueDm, string>
    {
        public string Resolve(AddSinglePredefinedValueDto source, PredefinedValueDm destination, string destMember, ResolutionContext context)
        {
            string convertedValue = ValuesConverterHelper.UnboxAndConvertValueToString(source.ValueInfo.Value, source.ValueInfo.DataType);
            return convertedValue;
        }
    }

    public class TryUnboxIncomingPredefinedValueAction : IMappingAction<AddSinglePredefinedValueDto, PredefinedValueDm>
    {
        public void Process(AddSinglePredefinedValueDto source, PredefinedValueDm destination, ResolutionContext context)
        {
            ValuesConverterHelper.UnboxAndConvertValueToString(source.ValueInfo.Value, source.ValueInfo.DataType);
        }
    }

    public class PredefinedValueDtoValueResolver : IValueResolver<PredefinedValueDto, PredefinedValueDm, string>
    {
        public string Resolve(PredefinedValueDto source, PredefinedValueDm destination, string destMember, ResolutionContext context)
        {
            string convertedValue = ValuesConverterHelper.UnboxAndConvertValueToString(source.Value, source.DataType);
            return convertedValue;
        }
    }
}
