﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.WebApp.Models.DTO.PredefinedCategories;

namespace TestDataGenerator.WebApp.Models.ActionResults
{
    public class GetPredefinedCategoriesResult: CommonResult
    {
        public PredefinedCategoriesCollectionDto Data { get; set; }
    }
}
