﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.WebApp.Models.ActionResults
{
    public  class GetDatasetGenerationReadinessResult: CommonResult
    {
        public bool ResultReady { get; set; }

        public bool RequestFailedToProcess { get; set; }

        public string ErrorText { get; set; }

        public DateTime? RequestProcessedDate { get; set; }
    }
}
