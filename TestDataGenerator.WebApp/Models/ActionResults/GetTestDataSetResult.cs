﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.WebApp.Models.DTO.Dataset;

namespace TestDataGenerator.WebApp.Models.ActionResults
{
    public class GetTestDataSetResult: CommonResult
    {
        public RowDto[] GeneratedRows { get; set; }

        public string ErrorText { get; set; }
    }
}
