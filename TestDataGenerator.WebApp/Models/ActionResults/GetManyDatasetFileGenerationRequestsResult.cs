﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.WebApp.Models.DTO.Dataset;

namespace TestDataGenerator.WebApp.Models.ActionResults
{
    public class GetManyDatasetFileGenerationRequestsResult: CommonResult
    {
        public DatasetFileGenerationRequestDto[] DatasetRequests { get; set; }
    }
}
