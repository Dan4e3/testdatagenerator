﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.WebApp.Models.ActionResults
{
    public class UserLoginResult: CommonResult
    {
        public string Name { get; set; }
        public string JwtToken { get; set; }
    }
}
