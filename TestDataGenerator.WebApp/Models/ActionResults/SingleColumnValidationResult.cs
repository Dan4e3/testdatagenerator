﻿using TestDataGenerator.WebApp.Models.DTO.ColumnSet;

namespace TestDataGenerator.WebApp.Models.ActionResults
{
    public class SingleColumnValidationResult: CommonResult
    {
        public ColumnDto CheckedColumn { get; set; }

        public bool IsValid { get; set; }

        public string ValidationError { get; set; }
    }
}
