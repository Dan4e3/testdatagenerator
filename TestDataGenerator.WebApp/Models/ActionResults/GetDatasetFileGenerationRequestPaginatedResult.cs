﻿using TestDataGenerator.WebApp.Models.DTO.Dataset;

namespace TestDataGenerator.WebApp.Models.ActionResults
{
    public class GetDatasetFileGenerationRequestPaginatedResult: CommonResult
    {
        public DatasetFileGenerationRequestDto[] DatasetRequests { get; set; }

        public int PageNumber { get; set; }

        public int ValuesPerPage { get; set; }

        public long TotalValuesCount { get; set; }
    }
}
