﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.WebApp.Models.DTO.PredefinedValues;

namespace TestDataGenerator.WebApp.Models.ActionResults
{
    public class AddSinglePredefinedValueResult: CommonResult
    {
        public GetPredefinedValueDto CreatedValue { get; set; }
    }
}
