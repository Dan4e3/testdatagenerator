﻿using TestDataGenerator.WebApp.Models.DTO.PredefinedValues;

namespace TestDataGenerator.WebApp.Models.ActionResults
{
    public class GetPrdefinedValuesPaginatedResult: CommonResult
    {
        public GetPredefinedValuesCollectionDto ValuesCollection { get; set; }

        public int PageNumber { get; set; }

        public int ValuesPerPage { get; set; }

        public long TotalValuesCount { get; set; }
    }
}