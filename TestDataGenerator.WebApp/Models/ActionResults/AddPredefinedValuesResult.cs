﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.WebApp.Models.ActionResults
{
    public class AddPredefinedValuesResult: CommonResult
    {
        public int ValuesAdded { get; set; }
    }
}
