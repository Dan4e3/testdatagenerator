﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.WebApp.Models.ActionResults
{
    public class CreateDatasetRequestResult: CommonResult
    {
        public long RequestId { get; set; }
    }
}
