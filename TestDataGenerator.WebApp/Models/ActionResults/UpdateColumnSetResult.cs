﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.WebApp.Models.DTO.ColumnSet;

namespace TestDataGenerator.WebApp.Models.ActionResults
{
    public class UpdateColumnSetResult: CommonResult
    {
        public long ColumnSetId { get; set; }

        public ColumnDto[] UpdatedColumns { get; set; }
    }
}
