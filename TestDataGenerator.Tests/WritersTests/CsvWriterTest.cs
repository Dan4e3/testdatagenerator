﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces;
using TestDataGenerator.Abstractions.Interfaces.Columns;
using TestDataGenerator.Core;
using TestDataGenerator.Core.Columns;
using TestDataGenerator.Core.ValueGenerators;
using TestDataGenerator.Core.ValueGenerators.ValuePatternParser;
using TestDataGenerator.IO;
using TestDataGenerator.IO.DatasetWriters;

namespace TestDataGenerator.Tests.WritersTests
{
    public class CsvWriterTest
    {
        private class TestCsvObject
        {
            public int Col1 { get; set; }
            public double Col2 { get; set; }
            public string Col3 { get; set; }
            public bool Col4 { get; set; }
            public DateTime Col5 { get; set; }
        }

        [Test]
        public async Task CsvWriter_WritesDataSetToFile()
        {
            CsvDataSetWriter writer = new CsvDataSetWriter();

            var colSet = new ColumnSet(new List<IColumn>() {
                new Column(new ColumnSettings() {
                    ColumnName = nameof(TestCsvObject.Col1),
                    TargetValueType = typeof(int),
                    ValueSupplier = new ValuePatternParser("[1-9]{1-6}")
                }),
                new Column(new ColumnSettings() {
                    ColumnName = nameof(TestCsvObject.Col2),
                    TargetValueType = typeof(double),
                    ValueSupplier = new ValuePatternParser("[1-9]{1-3}[,][0-9]{1-2}"),
                    Culture = CultureInfo.GetCultureInfo("ru-RU")
                }),
                new Column(new ColumnSettings() {
                    ColumnName = nameof(TestCsvObject.Col3),
                    TargetValueType = typeof(string),
                    ValueSupplier = new ValuePatternParser("[A-z]{5-12}")
                }),
                new Column(new ColumnSettings() {
                    ColumnName = nameof(TestCsvObject.Col4),
                    TargetValueType = typeof(bool),
                    ValueSupplier = new SelectFromListValueGenerator(new object[] { true, false })
                }),
                new Column(new ColumnSettings() {
                    ColumnName = nameof(TestCsvObject.Col5),
                    TargetValueType = typeof(DateTime),
                    ValueSupplier = new ValuePatternParser("[1-2][1-2][/][0-1][1-2][/][1][9][0-9]{2}"),
                    Culture = CultureInfo.GetCultureInfo("ru-RU")
                }),
            });

            TestDataSet dataset = new TestDataSet("Test", colSet);
            int rowsToGenerate = 10;
            dataset.GenerateNewDataSet(rowsToGenerate);
            string testDirPath = TestContext.CurrentContext.WorkDirectory;
            string fullTestFilePath = Path.Combine(testDirPath, "test.csv");

            await writer.WriteToFileAsync(fullTestFilePath, dataset);

            Assert.IsTrue(File.Exists(fullTestFilePath));

            string[] fileContents = File.ReadAllLines(fullTestFilePath);

            Assert.AreEqual(rowsToGenerate + 1, fileContents.Length);
        }

        [Test]
        public async Task CsvWriter_WritesDataSetToStream()
        {
            CsvDataSetWriter writer = new CsvDataSetWriter();

            var colSet = new ColumnSet(new List<IColumn>() {
                new Column(new ColumnSettings() {
                    ColumnName = nameof(TestCsvObject.Col1),
                    TargetValueType = typeof(int),
                    ValueSupplier = new ValuePatternParser("[1-9]{1-6}")
                }),
                new Column(new ColumnSettings() {
                    ColumnName = nameof(TestCsvObject.Col2),
                    TargetValueType = typeof(double),
                    ValueSupplier = new ValuePatternParser("[1-9]{1-3}[.][0-9]{1-2}"),
                    Culture = CultureInfo.InvariantCulture
                }),
                new Column(new ColumnSettings() {
                    ColumnName = nameof(TestCsvObject.Col3),
                    TargetValueType = typeof(string),
                    ValueSupplier = new ValuePatternParser("[A-z]{5-12}")
                }),
            });

            TestDataSet dataset = new TestDataSet("Test", colSet);
            int rowsToGenerate = 10;
            dataset.GenerateNewDataSet(rowsToGenerate);

            using (MemoryStream memstream = new MemoryStream())
            {
                await writer.WriteToStreamAsync(memstream, dataset);

                Assert.NotZero(memstream.Length);

                string resultingCsvAsString = Encoding.UTF8.GetString(memstream.ToArray());
                string[] splittedToRowsCsv = resultingCsvAsString.Split(Environment.NewLine);

                Assert.AreEqual(rowsToGenerate + 1, splittedToRowsCsv.Length);
            }
        }

        [Test]
        public async Task CsvWriterRequestedToGenerateOnTheFly_GeneratesAndWritesRowByRowToFile()
        {
            CsvDataSetWriter writer = new CsvDataSetWriter();

            var colSet = new ColumnSet(new List<IColumn>() {
                new Column(new ColumnSettings() {
                    ColumnName = nameof(TestCsvObject.Col1),
                    TargetValueType = typeof(int),
                    ValueSupplier = new ValuePatternParser("[1-9]{1-6}")
                }),
                new Column(new ColumnSettings() {
                    ColumnName = nameof(TestCsvObject.Col2),
                    TargetValueType = typeof(double),
                    ValueSupplier = new ValuePatternParser("[1-9]{1-3}[.][0-9]{1-2}"),
                    Culture = CultureInfo.InvariantCulture
                }),
                new Column(new ColumnSettings() {
                    ColumnName = nameof(TestCsvObject.Col3),
                    TargetValueType = typeof(string),
                    ValueSupplier = new ValuePatternParser("[A-z]{5-12}")
                }),
            });

            TestDataSet dataset = new TestDataSet("Test", colSet);
            int rowsToGenerate = 25;

            string testDirPath = TestContext.CurrentContext.WorkDirectory;
            string fullTestFilePath = Path.Combine(testDirPath, "on_the_fly_test.csv");
            await writer.WriteToFileUsingInPlaceRowGenerationAsync(fullTestFilePath, dataset, rowsToGenerate);

            Assert.IsTrue(File.Exists(fullTestFilePath));

            string[] fileContents = File.ReadAllLines(fullTestFilePath);

            Assert.AreEqual(rowsToGenerate + 1, fileContents.Length);
        }

        [Test]
        public async Task CsvWriterRequestedToGenerateOnTheFly_GeneratesAndWritesRowByRowToStream()
        {
            CsvDataSetWriter writer = new CsvDataSetWriter();

            var colSet = new ColumnSet(new List<IColumn>() {
                new Column(new ColumnSettings() {
                    ColumnName = nameof(TestCsvObject.Col1),
                    TargetValueType = typeof(int),
                    ValueSupplier = new ValuePatternParser("[1-9]{1-6}")
                }),
                new Column(new ColumnSettings() {
                    ColumnName = nameof(TestCsvObject.Col2),
                    TargetValueType = typeof(double),
                    ValueSupplier = new ValuePatternParser("[1-9]{1-3}[.][0-9]{1-2}"),
                    Culture = CultureInfo.InvariantCulture
                }),
                new Column(new ColumnSettings() {
                    ColumnName = nameof(TestCsvObject.Col3),
                    TargetValueType = typeof(string),
                    ValueSupplier = new ValuePatternParser("[A-z]{5-12}")
                }),
            });

            TestDataSet dataset = new TestDataSet("Test", colSet);
            int rowsToGenerate = 25;

            using (MemoryStream memstream = new MemoryStream())
            {
                await writer.WriteToStreamUsingInPlaceRowGenerationAsync(memstream, dataset, rowsToGenerate);

                Assert.NotZero(memstream.Length);

                string resultingCsvAsString = Encoding.UTF8.GetString(memstream.ToArray());
                string[] splittedToRowsCsv = resultingCsvAsString.Split(Environment.NewLine);

                Assert.AreEqual(rowsToGenerate + 1, splittedToRowsCsv.Length);
            }
        }
    }
}
