﻿using Moq;
using Npgsql;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces;
using TestDataGenerator.Abstractions.Interfaces.Columns;
using TestDataGenerator.Abstractions.Interfaces.Rows;
using TestDataGenerator.Abstractions.Interfaces.Writers;
using TestDataGenerator.Core;
using TestDataGenerator.Core.Columns;
using TestDataGenerator.Core.ValueGenerators;
using TestDataGenerator.Core.ValueGenerators.ValuePatternParser;
using TestDataGenerator.IO;
using TestDataGenerator.IO.DatabaseExportExecutors;
using TestDataGenerator.IO.DatasetWriters;

namespace TestDataGenerator.Tests.WritersTests
{
    public class DbWriterTest
    {
        private static int _expectedInsertedRows;

        private class TestDbObject
        {
            public int Col1 { get; set; }
            public double Col2 { get; set; }
            public string Col3 { get; set; }
            public bool Col4 { get; set; }
            public DateTime Col5 { get; set; }
        }

        [Test]
        public async Task DbDatasetWriterUsesSqlServerExportExecutor_GetsNumberOfRowsInserted()
        {
            DbDataSetWriter writer = new DbDataSetWriter();
            string connString = "Server=PREDATOR-HELIOS;Database=TestDb;Integrated Security=true;MultipleActiveResultSets=true;";
            string tableName = "[dbo].[Table_1]";
            SqlServerExportExecutor executor = new SqlServerExportExecutor(connString, tableName);

            var colSet = new ColumnSet(new List<IColumn>() {
                new Column(new ColumnSettings() {
                    ColumnName = nameof(TestDbObject.Col1),
                    TargetValueType = typeof(int),
                    ValueSupplier = new ValuePatternParser("[1-9]{1-6}")
                }),
                new Column(new ColumnSettings() {
                    ColumnName = nameof(TestDbObject.Col2),
                    TargetValueType = typeof(double),
                    ValueSupplier = new ValuePatternParser("[1-9]{1-3}[.][0-9]{1-2}"),
                    Culture = CultureInfo.InvariantCulture
                }),
                new Column(new ColumnSettings() {
                    ColumnName = nameof(TestDbObject.Col3),
                    TargetValueType = typeof(string),
                    ValueSupplier = new ValuePatternParser("[A-z]{5-12}")
                }),
                new Column(new ColumnSettings() {
                    ColumnName = nameof(TestDbObject.Col4),
                    TargetValueType = typeof(bool),
                    ValueSupplier = new SelectFromListValueGenerator(new object[] { true, false })
                }),
                new Column(new ColumnSettings() {
                    ColumnName = nameof(TestDbObject.Col5),
                    TargetValueType = typeof(DateTime),
                    ValueSupplier = new ValuePatternParser("[0-1][1-2][/][1-2][1-2][/][1][9][0-9]{2}"),
                    Culture = CultureInfo.InvariantCulture
                }),
            });

            TestDataSet dataset = new TestDataSet("Test", colSet);
            int rowsToGenerate = 10;
            _expectedInsertedRows = rowsToGenerate;
            dataset.GenerateNewDataSet(rowsToGenerate);

            if (Environment.GetEnvironmentVariable("TESTING_ENVIRONMENT") == "GITLAB")
            {
                FakeDbExportExecutor fakeExecutor = new FakeDbExportExecutor();
                int insertedRowsCount = writer.WriteToDatabase(fakeExecutor, dataset);
                Assert.AreEqual(_expectedInsertedRows, insertedRowsCount);
            }
            else
            {
                using (SqlConnection conn = new SqlConnection(connString))
                {
                    await conn.OpenAsync();
                    var truncateCmd = conn.CreateCommand();
                    truncateCmd.CommandText = $"TRUNCATE TABLE {tableName}";
                    await truncateCmd.ExecuteNonQueryAsync();
                    int insertedRowsCount = writer.WriteToDatabase(executor, dataset);

                    var countInsertedRowsCmd = conn.CreateCommand();
                    countInsertedRowsCmd.CommandText = $"SELECT COUNT(1) FROM {tableName}";
                    int insertedRowsCountCmdResult = (int)countInsertedRowsCmd.ExecuteScalar();

                    Assert.AreEqual(rowsToGenerate, insertedRowsCount);
                    Assert.AreEqual(rowsToGenerate, insertedRowsCountCmdResult);
                }
            }
        }

        [Test]
        public async Task DbDatasetWriterUsesPostgresExportExecutor_GetsNumberOfRowsInserted()
        {
            DbDataSetWriter writer = new DbDataSetWriter();
            string connString = "User ID=TstUser;Password=1234567890;Host=localhost;Port=5432;Database=TestDb";
            string tableName = "table_1";
            PostgresExportExecutor executor = new PostgresExportExecutor(connString, tableName);

            var colSet = new ColumnSet(new List<IColumn>() {
                new Column(new ColumnSettings() {
                    ColumnName = "col_1",
                    TargetValueType = typeof(int),
                    ValueSupplier = new ValuePatternParser("[1-9]{1-6}")
                }),
                new Column(new ColumnSettings() {
                    ColumnName = "col_2",
                    TargetValueType = typeof(double),
                    ValueSupplier = new ValuePatternParser("[1-9]{1-3}[.][0-9]{1-2}"),
                    Culture = CultureInfo.InvariantCulture
                }),
                new Column(new ColumnSettings() {
                    ColumnName = "col_3",
                    TargetValueType = typeof(string),
                    ValueSupplier = new ValuePatternParser("[A-z]{5-12}")
                }),
                new Column(new ColumnSettings() {
                    ColumnName = "col_4",
                    TargetValueType = typeof(bool),
                    ValueSupplier = new SelectFromListValueGenerator(new object[] { true, false })
                }),
                new Column(new ColumnSettings() {
                    ColumnName = "col_5",
                    TargetValueType = typeof(DateTime),
                    ValueSupplier = new ValuePatternParser("[0-1][1-2][/][1-2][1-2][/][1][9][0-9]{2}"),
                    Culture = CultureInfo.InvariantCulture
                }),
            });

            TestDataSet dataset = new TestDataSet("Test", colSet);
            int rowsToGenerate = 10;
            _expectedInsertedRows = rowsToGenerate;
            dataset.GenerateNewDataSet(rowsToGenerate);

            if (Environment.GetEnvironmentVariable("TESTING_ENVIRONMENT") == "GITLAB")
            {
                FakeDbExportExecutor fakeExecutor = new FakeDbExportExecutor();
                int insertedRowsCount = writer.WriteToDatabase(fakeExecutor, dataset);
                Assert.AreEqual(_expectedInsertedRows, insertedRowsCount);
            }
            else
            {
                using (NpgsqlConnection conn = new NpgsqlConnection(connString))
                {
                    await conn.OpenAsync();
                    var truncateCmd = conn.CreateCommand();
                    truncateCmd.CommandText = $"TRUNCATE TABLE {tableName}";
                    await truncateCmd.ExecuteNonQueryAsync();
                    int insertedRowsCount = writer.WriteToDatabase(executor, dataset);

                    var countInsertedRowsCmd = conn.CreateCommand();
                    countInsertedRowsCmd.CommandText = $"SELECT COUNT(1) FROM {tableName}";
                    long insertedRowsCountCmdResult = (long)countInsertedRowsCmd.ExecuteScalar();

                    Assert.AreEqual(rowsToGenerate, insertedRowsCount);
                    Assert.AreEqual(rowsToGenerate, insertedRowsCountCmdResult);
                }
            }
        }

        [Test]
        public async Task DbWriterUsesSqlServerExecutorAndUsesOnTheFlyRowGeneration_GeneratesRowsAndWritesThemInBatches()
        {
            DbDataSetWriter writer = new DbDataSetWriter();
            string connString = "Server=PREDATOR-HELIOS;Database=TestDb;Integrated Security=true;MultipleActiveResultSets=true;";
            string tableName = "[dbo].[Table_1]";
            SqlServerExportExecutor executor = new SqlServerExportExecutor(connString, tableName);

            var colSet = new ColumnSet(new List<IColumn>() {
                new Column(new ColumnSettings() {
                    ColumnName = nameof(TestDbObject.Col1),
                    TargetValueType = typeof(int),
                    ValueSupplier = new ValuePatternParser("[1-9]{1-6}")
                }),
                new Column(new ColumnSettings() {
                    ColumnName = nameof(TestDbObject.Col2),
                    TargetValueType = typeof(double),
                    ValueSupplier = new ValuePatternParser("[1-9]{1-3}[.][0-9]{1-2}"),
                    Culture = CultureInfo.InvariantCulture
                }),
                new Column(new ColumnSettings() {
                    ColumnName = nameof(TestDbObject.Col3),
                    TargetValueType = typeof(string),
                    ValueSupplier = new ValuePatternParser("[A-z]{5-12}")
                }),
                new Column(new ColumnSettings() {
                    ColumnName = nameof(TestDbObject.Col4),
                    TargetValueType = typeof(bool),
                    ValueSupplier = new SelectFromListValueGenerator(new object[] { true, false })
                }),
                new Column(new ColumnSettings() {
                    ColumnName = nameof(TestDbObject.Col5),
                    TargetValueType = typeof(DateTime),
                    ValueSupplier = new ValuePatternParser("[0-1][1-2][/][1-2][1-2][/][1][9][0-9]{2}"),
                    Culture = CultureInfo.InvariantCulture
                }),
            });

            TestDataSet dataset = new TestDataSet("Test", colSet);
            foreach (int rowsToGenerate in new int[] { 10, 1124, 4775 })
            {
                _expectedInsertedRows = rowsToGenerate;

                if (Environment.GetEnvironmentVariable("TESTING_ENVIRONMENT") == "GITLAB")
                {
                    FakeDbExportExecutor fakeExecutor = new FakeDbExportExecutor();
                    int insertedRowsCount = writer.WriteToDatabaseUsingInPlaceRowGeneration(fakeExecutor, dataset, rowsToGenerate);
                    int loopsPassed = (int)Math.Ceiling(rowsToGenerate / 1000.0);
                    Assert.AreEqual(_expectedInsertedRows, insertedRowsCount / loopsPassed);
                }
                else
                {
                    using (SqlConnection conn = new SqlConnection(connString))
                    {
                        await conn.OpenAsync();
                        var truncateCmd = conn.CreateCommand();
                        truncateCmd.CommandText = $"TRUNCATE TABLE {tableName}";
                        await truncateCmd.ExecuteNonQueryAsync();
                        int insertedRowsCount = writer.WriteToDatabaseUsingInPlaceRowGeneration(executor, dataset, rowsToGenerate);

                        var countInsertedRowsCmd = conn.CreateCommand();
                        countInsertedRowsCmd.CommandText = $"SELECT COUNT(1) FROM {tableName}";
                        int insertedRowsCountCmdResult = (int)countInsertedRowsCmd.ExecuteScalar();

                        Assert.AreEqual(rowsToGenerate, insertedRowsCount);
                        Assert.AreEqual(rowsToGenerate, insertedRowsCountCmdResult);
                    }
                }
            }
        }

        private class FakeDbExportExecutor : IDbExportExecutor
        {
            public string ConnectionString { get; set; }
            public string TargetTableName { get; set; }

            public IDbConnection CreateConnection()
            {
                return new FakeDbConnection();
            }

            public string CreateInsertSqlCommand(IEnumerable<IColumn> columns, IEnumerable<IRow> rowsToInsert)
            {
                return "insert something into somewhere";
            }
        }

        private class FakeDbTransaction : IDbTransaction
        {
            public IDbConnection Connection => throw new NotImplementedException();

            public IsolationLevel IsolationLevel => throw new NotImplementedException();

            public void Commit()
            {
                return;
            }

            public void Dispose()
            {
                return;
            }

            public void Rollback()
            {
                throw new NotImplementedException();
            }
        }

        private class FakeDbCommand : IDbCommand
        {
            public string CommandText { get; set ; }
            public int CommandTimeout { get; set; }
            public CommandType CommandType { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
            public IDbConnection Connection { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

            public IDataParameterCollection Parameters => throw new NotImplementedException();

            public IDbTransaction Transaction { get; set; }
            public UpdateRowSource UpdatedRowSource { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

            public void Cancel()
            {
                throw new NotImplementedException();
            }

            public IDbDataParameter CreateParameter()
            {
                throw new NotImplementedException();
            }

            public void Dispose()
            {
                throw new NotImplementedException();
            }

            public int ExecuteNonQuery()
            {
                throw new NotImplementedException();
            }

            public IDataReader ExecuteReader()
            {
                throw new NotImplementedException();
            }

            public IDataReader ExecuteReader(CommandBehavior behavior)
            {
                throw new NotImplementedException();
            }

            public object ExecuteScalar()
            {
                throw new NotImplementedException();
            }

            public void Prepare()
            {
                throw new NotImplementedException();
            }
        }

        private class FakeDbConnection : IDbConnection
        {
            public string ConnectionString { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

            public int ConnectionTimeout => throw new NotImplementedException();

            public string Database => throw new NotImplementedException();

            public ConnectionState State => throw new NotImplementedException();

            public IDbTransaction BeginTransaction()
            {
                throw new NotImplementedException();
            }

            public IDbTransaction BeginTransaction(IsolationLevel il)
            {
                return new FakeDbTransaction();
            }

            public void ChangeDatabase(string databaseName)
            {
                throw new NotImplementedException();
            }

            public void Close()
            {
                throw new NotImplementedException();
            }

            public IDbCommand CreateCommand()
            {
                Mock<IDbCommand> cmdMock = new Mock<IDbCommand>();

                cmdMock.Setup(cmd => cmd.ExecuteNonQuery()).Returns(_expectedInsertedRows);
                return cmdMock.Object;
            }

            public void Dispose()
            {
                return;
            }

            public void Open()
            {
                return;
            }
        }
    }
}
