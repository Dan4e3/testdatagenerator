﻿using Newtonsoft.Json;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces;
using TestDataGenerator.Abstractions.Interfaces.Columns;
using TestDataGenerator.Core;
using TestDataGenerator.Core.Columns;
using TestDataGenerator.Core.ValueGenerators.ValuePatternParser;
using TestDataGenerator.Core.ValueGenerators.ValueRangeGenerator;
using TestDataGenerator.IO;
using TestDataGenerator.IO.DatasetWriters;
using TestDataGenerator.IO.DataSetWriters;
using TestDataGenerator.IO.DataSetWriters.Enums;

namespace TestDataGenerator.Tests.WritersTests
{
    public class JsonWriterTest
    {
        private class TestJsonObject
        {
            public int Col1 { get; set; }
            public double Col2 { get; set; }
            public string Col3 { get; set; }
            public DateTime Col4 { get; set; }
        }

        private class TestJsonObjectWithArraysPerColumn
        {
            public int[] Col1 { get; set; }
            public double[] Col2 { get; set; }
            public string[] Col3 { get; set; }
            public DateTime[] Col4 { get; set; }
        }

        [Test]
        public async Task JsonFileWriter_WritesDatasetToFile()
        {
            JsonDataSetWriter writer = new JsonDataSetWriter();

            var colSet = new ColumnSet(new List<IColumn>() {
                new Column(new ColumnSettings() {
                    ColumnName = nameof(TestJsonObject.Col1),
                    TargetValueType = typeof(int),
                    ValueSupplier = new ValuePatternParser("[1-9]{1-6}")
                }),
                new Column(new ColumnSettings() {
                    ColumnName = nameof(TestJsonObject.Col2),
                    TargetValueType = typeof(double),
                    ValueSupplier = new ValuePatternParser("[1-9]{1-3}[.][0-9]{1-2}"),
                    Culture = CultureInfo.InvariantCulture
                }),
                new Column(new ColumnSettings() {
                    ColumnName = nameof(TestJsonObject.Col3),
                    TargetValueType = typeof(string),
                    ValueSupplier = new ValuePatternParser("[A-z]{5-12}")
                }),
            });

            TestDataSet dataset = new TestDataSet("Test", colSet);
            int rowsToGenerate = 10;
            dataset.GenerateNewDataSet(rowsToGenerate);
            string testDirPath = TestContext.CurrentContext.WorkDirectory;
            string fullTestFilePath = Path.Combine(testDirPath, "test.json");
            await writer.WriteToFileAsync(fullTestFilePath, dataset);

            Assert.IsTrue(File.Exists(fullTestFilePath));

            string fileContents = File.ReadAllText(fullTestFilePath);
            TestJsonObject[] testObjects = JsonConvert.DeserializeObject<TestJsonObject[]>(fileContents);

            Assert.AreEqual(rowsToGenerate, testObjects.Length);
            Assert.LessOrEqual(testObjects[0].Col1, 999999);
            Assert.LessOrEqual(testObjects[0].Col2, 999.99);
            Assert.LessOrEqual(testObjects[0].Col3.Length, 12);
            Assert.GreaterOrEqual(testObjects[0].Col3.Length, 5);
        }

        [Test]
        public async Task JsonStreamWriter_WritesDatasetToStream()
        {
            JsonDataSetWriter writer = new JsonDataSetWriter();

            var colSet = new ColumnSet(new List<IColumn>() {
                new Column(new ColumnSettings() {
                    ColumnName = nameof(TestJsonObject.Col1),
                    TargetValueType = typeof(int),
                    ValueSupplier = new ValuePatternParser("[1-9]{1-6}")
                }),
                new Column(new ColumnSettings() {
                    ColumnName = nameof(TestJsonObject.Col2),
                    TargetValueType = typeof(double),
                    ValueSupplier = new ValuePatternParser("[1-9]{1-3}[.][0-9]{1-2}"),
                    Culture = CultureInfo.InvariantCulture
                }),
                new Column(new ColumnSettings() {
                    ColumnName = nameof(TestJsonObject.Col3),
                    TargetValueType = typeof(string),
                    ValueSupplier = new ValuePatternParser("[A-z]{5-12}")
                }),
            });

            TestDataSet dataset = new TestDataSet("Test", colSet);
            int rowsToGenerate = 10;
            dataset.GenerateNewDataSet(rowsToGenerate);

            using (MemoryStream memstream = new MemoryStream())
            {
                await writer.WriteToStreamAsync(memstream, dataset);

                Assert.NotZero(memstream.Length);

                string resultingJsonAsString = Encoding.UTF8.GetString(memstream.ToArray());
                TestJsonObject[] testObjects = JsonConvert.DeserializeObject<TestJsonObject[]>(resultingJsonAsString);

                Assert.AreEqual(rowsToGenerate, testObjects.Length);
                Assert.LessOrEqual(testObjects[0].Col1, 999999);
                Assert.LessOrEqual(testObjects[0].Col2, 999.99);
                Assert.LessOrEqual(testObjects[0].Col3.Length, 12);
                Assert.GreaterOrEqual(testObjects[0].Col3.Length, 5);
            }
        }

        [Test]
        public async Task JsonFileWriterUsesOnTheFlyRowGeneration_GeneratesAndWritesRowByRow()
        {
            JsonDataSetWriter writer = new JsonDataSetWriter();

            var colSet = new ColumnSet(new List<IColumn>() {
                new Column(new ColumnSettings() {
                    ColumnName = nameof(TestJsonObject.Col1),
                    TargetValueType = typeof(int),
                    ValueSupplier = new ValuePatternParser("[1-9]{1-6}")
                }),
                new Column(new ColumnSettings() {
                    ColumnName = nameof(TestJsonObject.Col2),
                    TargetValueType = typeof(double),
                    ValueSupplier = new ValuePatternParser("[1-9]{1-3}[.][0-9]{1-2}"),
                    Culture = CultureInfo.InvariantCulture
                }),
                new Column(new ColumnSettings() {
                    ColumnName = nameof(TestJsonObject.Col3),
                    TargetValueType = typeof(string),
                    ValueSupplier = new ValuePatternParser("[A-z]{5-12}")
                }),
            });

            TestDataSet dataset = new TestDataSet("Test", colSet);
            int rowsToGenerate = 10;

            string testDirPath = TestContext.CurrentContext.WorkDirectory;
            string fullTestFilePath = Path.Combine(testDirPath, "test.json");
            await writer.WriteToFileUsingInPlaceRowGenerationAsync(fullTestFilePath, dataset, rowsToGenerate);

            Assert.IsTrue(File.Exists(fullTestFilePath));

            string fileContents = File.ReadAllText(fullTestFilePath);
            TestJsonObject[] testObjects = JsonConvert.DeserializeObject<TestJsonObject[]>(fileContents);

            Assert.AreEqual(rowsToGenerate, testObjects.Length);
            Assert.LessOrEqual(testObjects[0].Col1, 999999);
            Assert.LessOrEqual(testObjects[0].Col2, 999.99);
            Assert.LessOrEqual(testObjects[0].Col3.Length, 12);
        }

        [Test]
        public async Task JsonStreamWriterUsesOnTheFlyGeneration_GeneratesAndWritesRowByRow()
        {
            JsonDataSetWriter writer = new JsonDataSetWriter();

            var colSet = new ColumnSet(new List<IColumn>() {
                new Column(new ColumnSettings() {
                    ColumnName = nameof(TestJsonObject.Col1),
                    TargetValueType = typeof(int),
                    ValueSupplier = new ValuePatternParser("[1-9]{1-6}")
                }),
                new Column(new ColumnSettings() {
                    ColumnName = nameof(TestJsonObject.Col2),
                    TargetValueType = typeof(double),
                    ValueSupplier = new ValuePatternParser("[1-9]{1-3}[.][0-9]{1-2}"),
                    Culture = CultureInfo.InvariantCulture
                }),
                new Column(new ColumnSettings() {
                    ColumnName = nameof(TestJsonObject.Col3),
                    TargetValueType = typeof(string),
                    ValueSupplier = new ValuePatternParser("[A-z]{5-12}")
                }),
            });

            TestDataSet dataset = new TestDataSet("Test", colSet);
            int rowsToGenerate = 10;

            using (MemoryStream memstream = new MemoryStream())
            {
                await writer.WriteToStreamUsingInPlaceRowGenerationAsync(memstream, dataset, rowsToGenerate);

                Assert.NotZero(memstream.Length);

                string resultingJsonAsString = Encoding.UTF8.GetString(memstream.ToArray());
                TestJsonObject[] testObjects = JsonConvert.DeserializeObject<TestJsonObject[]>(resultingJsonAsString);

                Assert.AreEqual(rowsToGenerate, testObjects.Length);
                Assert.LessOrEqual(testObjects[0].Col1, 999999);
                Assert.LessOrEqual(testObjects[0].Col2, 999.99);
                Assert.LessOrEqual(testObjects[0].Col3.Length, 12);
                Assert.GreaterOrEqual(testObjects[0].Col3.Length, 5);
            }
        }

        [Test]
        public async Task JsonWriterUsesCustomSettings_CorrectlyConsumesThemAndUsesInDataSetWriting()
        {
            JsonDataSetWriter writer = new JsonDataSetWriter(writerSettings: new JsonDataSetWriterSettings() {
                Culture = CultureInfo.InvariantCulture,
                DateFormatString = "dd-MM-yyyy HH:mm:ss"
            }); ;

            var colSet = new ColumnSet(new List<IColumn>() {
                new Column(new ColumnSettings() {
                    ColumnName = nameof(TestJsonObject.Col1),
                    TargetValueType = typeof(int),
                    ValueSupplier = new ValuePatternParser("[1-9]{1-6}")
                }),
                new Column(new ColumnSettings() {
                    ColumnName = nameof(TestJsonObject.Col2),
                    TargetValueType = typeof(double),
                    ValueSupplier = new ValuePatternParser("[1-9]{1-3}[.][0-9]{1-2}"),
                    Culture = CultureInfo.InvariantCulture
                }),
                new Column(new ColumnSettings() {
                    ColumnName = nameof(TestJsonObject.Col3),
                    TargetValueType = typeof(string),
                    ValueSupplier = new ValuePatternParser("[A-z]{5-12}")
                }),
                new Column(new ColumnSettings() {
                    ColumnName = nameof(TestJsonObject.Col4),
                    TargetValueType = typeof(DateTime),
                    ValueSupplier = new ValueRangeValueGenerator(new ValueRange(
                            new DateTime(2000, 1, 1),
                            new DateTime(2001, 1, 1)
                        ))
                }),
            });

            TestDataSet dataset = new TestDataSet("Test", colSet);
            int rowsToGenerate = 10;

            string testDirPath = TestContext.CurrentContext.WorkDirectory;
            string fullTestFilePath = Path.Combine(testDirPath, "test_jsonwriter_settings.json");
            await writer.WriteToFileUsingInPlaceRowGenerationAsync(fullTestFilePath, dataset, rowsToGenerate);

            Assert.IsTrue(File.Exists(fullTestFilePath));

            string fileContents = File.ReadAllText(fullTestFilePath);
            TestJsonObject[] testObjects = JsonConvert.DeserializeObject<TestJsonObject[]>(fileContents, new JsonSerializerSettings() {
                Culture = CultureInfo.InvariantCulture,
                DateFormatString = "dd-MM-yyyy HH:mm:ss"
            });

            Assert.AreEqual(rowsToGenerate, testObjects.Length);
            Assert.LessOrEqual(testObjects[0].Col1, 999999);
            Assert.LessOrEqual(testObjects[0].Col2, 999.99);
            Assert.LessOrEqual(testObjects[0].Col3.Length, 12);
            Assert.LessOrEqual(testObjects[0].Col4, new DateTime(2001, 1, 1));
        }

        [Test]
        public async Task JsonWriter_CorrectlyWorksWithDifferentDatasetExportTypes()
        {
            JsonDataSetWriter writer = new JsonDataSetWriter() {
                DatasetExportStyle = DatasetJsonExportStyle.SingleObjectWithArrayOfValuesPerProperty
            };

            var colSet = new ColumnSet(new List<IColumn>() {
                new Column(new ColumnSettings() {
                    ColumnName = nameof(TestJsonObject.Col1),
                    TargetValueType = typeof(int),
                    ValueSupplier = new ValuePatternParser("[1-9]{1-6}")
                }),
                new Column(new ColumnSettings() {
                    ColumnName = nameof(TestJsonObject.Col2),
                    TargetValueType = typeof(double),
                    ValueSupplier = new ValuePatternParser("[1-9]{1-3}[.][0-9]{1-2}"),
                    Culture = CultureInfo.InvariantCulture
                }),
                new Column(new ColumnSettings() {
                    ColumnName = nameof(TestJsonObject.Col3),
                    TargetValueType = typeof(string),
                    ValueSupplier = new ValuePatternParser("[A-z]{5-12}")
                }),
                new Column(new ColumnSettings() {
                    ColumnName = nameof(TestJsonObject.Col4),
                    TargetValueType = typeof(DateTime),
                    ValueSupplier = new ValueRangeValueGenerator(new ValueRange(
                            new DateTime(2000, 1, 1),
                            new DateTime(2001, 1, 1)
                        ))
                }),
            });

            TestDataSet dataset = new TestDataSet("Test", colSet);
            int rowsToGenerate = 10;
            dataset.GenerateNewDataSet(rowsToGenerate);

            string testDirPath = TestContext.CurrentContext.WorkDirectory;
            string fullTestFilePath = Path.Combine(testDirPath, "test_dataset_export_types.json");

            // in-place generation
            await writer.WriteToFileUsingInPlaceRowGenerationAsync(fullTestFilePath, dataset, rowsToGenerate);

            Assert.IsTrue(File.Exists(fullTestFilePath));

            string fileContents = File.ReadAllText(fullTestFilePath);
            TestJsonObjectWithArraysPerColumn testObject = JsonConvert.DeserializeObject<TestJsonObjectWithArraysPerColumn>(fileContents);

            DateTime testDateTime = new DateTime(2001, 1, 1);
            Assert.AreEqual(rowsToGenerate, testObject.Col1.Length);
            Assert.AreEqual(rowsToGenerate, testObject.Col2.Length);
            Assert.AreEqual(rowsToGenerate, testObject.Col3.Length);
            Assert.AreEqual(rowsToGenerate, testObject.Col4.Length);
            Assert.True(testObject.Col1.All(n => n <= 999999));
            Assert.True(testObject.Col2.All(n => n <= 999.99));
            Assert.True(testObject.Col3.All(n => n.Length <= 12));
            Assert.True(testObject.Col4.All(n => n <= testDateTime));

            // pregenerated dataset
            await writer.WriteToFileAsync(fullTestFilePath, dataset);

            Assert.IsTrue(File.Exists(fullTestFilePath));

            string fileContents2 = File.ReadAllText(fullTestFilePath);
            TestJsonObjectWithArraysPerColumn testObject2 = JsonConvert.DeserializeObject<TestJsonObjectWithArraysPerColumn>(fileContents2);

            DateTime testDateTime2 = new DateTime(2001, 1, 1);
            Assert.AreEqual(rowsToGenerate, testObject2.Col1.Length);
            Assert.AreEqual(rowsToGenerate, testObject2.Col2.Length);
            Assert.AreEqual(rowsToGenerate, testObject2.Col3.Length);
            Assert.AreEqual(rowsToGenerate, testObject2.Col4.Length);
            Assert.True(testObject2.Col1.All(n => n <= 999999));
            Assert.True(testObject2.Col2.All(n => n <= 999.99));
            Assert.True(testObject2.Col3.All(n => n.Length <= 12));
            Assert.True(testObject2.Col4.All(n => n <= testDateTime2));
        }

        [Test]
        public void JsonWriterReceivesUnexpectedExportStle_ThrowsAnException()
        {
            JsonDataSetWriter writer = new JsonDataSetWriter()
            {
                DatasetExportStyle = (DatasetJsonExportStyle)(-1)
            };

            var colSet = new ColumnSet(new List<IColumn>() {
                new Column(new ColumnSettings() {
                    ColumnName = nameof(TestJsonObject.Col1),
                    TargetValueType = typeof(int),
                    ValueSupplier = new ValuePatternParser("[1-9]{1-6}")
                }),
                new Column(new ColumnSettings() {
                    ColumnName = nameof(TestJsonObject.Col2),
                    TargetValueType = typeof(double),
                    ValueSupplier = new ValuePatternParser("[1-9]{1-3}[.][0-9]{1-2}"),
                    Culture = CultureInfo.InvariantCulture
                }),
                new Column(new ColumnSettings() {
                    ColumnName = nameof(TestJsonObject.Col3),
                    TargetValueType = typeof(string),
                    ValueSupplier = new ValuePatternParser("[A-z]{5-12}")
                }),
                new Column(new ColumnSettings() {
                    ColumnName = nameof(TestJsonObject.Col4),
                    TargetValueType = typeof(DateTime),
                    ValueSupplier = new ValueRangeValueGenerator(new ValueRange(
                            new DateTime(2000, 1, 1),
                            new DateTime(2001, 1, 1)
                        ))
                }),
            });

            TestDataSet dataset = new TestDataSet("Test", colSet);
            int rowsToGenerate = 10;
            dataset.GenerateNewDataSet(rowsToGenerate);

            string testDirPath = TestContext.CurrentContext.WorkDirectory;
            string fullTestFilePath = Path.Combine(testDirPath, "test_dataset_export_types.json");

            Assert.ThrowsAsync<Exception>(async () => await writer.WriteToFileUsingInPlaceRowGenerationAsync(fullTestFilePath, dataset, rowsToGenerate));
        }
    }
}
