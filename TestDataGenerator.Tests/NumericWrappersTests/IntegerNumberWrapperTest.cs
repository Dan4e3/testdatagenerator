﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Core.NumericWrappers;

namespace TestDataGenerator.Tests.NumericWrappersTests
{
    public class IntegerNumberWrapperTest
    {
        [Test]
        public void IntegerNumberWrapper_CorrectlyUsesArithmetics()
        {
            Assert.True(new IntegerNumberWrapper<int>(5) <= new IntegerNumberWrapper<int>(300));
            Assert.True(new IntegerNumberWrapper<int>(512) > new IntegerNumberWrapper<int>(300));
            Assert.True(5 < new IntegerNumberWrapper<int>(300));
            Assert.True(new IntegerNumberWrapper<int>(5) < 300);
            Assert.AreEqual(305, new IntegerNumberWrapper<int>(5) + new IntegerNumberWrapper<int>(300));
            Assert.AreEqual(-295, new IntegerNumberWrapper<int>(5) - 300);
        }

        [Test]
        public void IntegerNumberWrapper_CorrectlyOverridesToString()
        {
            IntegerNumberWrapper<short> wrap = new IntegerNumberWrapper<short>(419);
            Assert.AreEqual("419", wrap.ToString());
        }
    }
}
