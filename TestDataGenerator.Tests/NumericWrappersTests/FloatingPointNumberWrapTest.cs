﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Core.NumericWrappers;

namespace TestDataGenerator.Tests.NumericWrappersTests
{
    public class FloatingPointNumberWrapTest
    {
        [Test]
        public void FloatinPointWrap_CorrectlyUsesArithmetics()
        {
            Assert.True(new FloatingPointNumberWrapper<float>(1.21f) <= new FloatingPointNumberWrapper<float>(4.5123f));
            Assert.True(new FloatingPointNumberWrapper<double>(1.21d) <= new FloatingPointNumberWrapper<double>(4.5123d));
            Assert.True(new FloatingPointNumberWrapper<decimal>(1.21m) <= new FloatingPointNumberWrapper<decimal>(4.5123m));
            Assert.True(new FloatingPointNumberWrapper<decimal>(1.21m) < new FloatingPointNumberWrapper<decimal>(4.5123m));
            Assert.True(new FloatingPointNumberWrapper<float>(1.21f) < new FloatingPointNumberWrapper<float>(4.5123f));
        }
        
        [Test]
        public void FloatingPointWrap_CorrectlyOverridesToString()
        {
            Assert.AreEqual(1.25d.ToString(), new FloatingPointNumberWrapper<double>(1.25d).ToString());
        }
    }
}
