﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Core.Columns;
using TestDataGenerator.Core.ValueGenerators.ValuePatternParser;

namespace TestDataGenerator.Tests
{
    public class ColumnTest
    {
        [Test]
        public void ColumnWithSetValueGenerator_GeneratesValue()
        {
            Column col = new Column(new ColumnSettings() { 
                ColumnName = "test1",
                Culture = CultureInfo.InvariantCulture,
                TargetValueType = typeof(int),
                ValueSupplier = new ValuePatternParser("[1-9]{1-4}")
            });

            Assert.DoesNotThrow(() => col.GenerateValue());
        }

        [Test]
        public void ColumnWIthSetValueGenerator_GeneratesMultipleValues()
        {
            Column col = new Column(new ColumnSettings()
            {
                ColumnName = "test1",
                Culture = CultureInfo.InvariantCulture,
                TargetValueType = typeof(int),
                ValueSupplier = new ValuePatternParser("[1-9]{1-4}")
            });

            const int valuesToGenerate = 300;
            object[] result = col.GenerateValues(valuesToGenerate);

            Assert.AreEqual(valuesToGenerate, result.Length);
        }

        [Test]
        public void ColumnGeneratesValueAndGetsStringRepresentation_CorrectlyReturnsCorrespondingResult()
        {
            Column col = new Column(new ColumnSettings()
            {
                ColumnName = "test1",
                Culture = CultureInfo.GetCultureInfo("ru-RU"),
                TargetValueType = typeof(double),
                ValueSupplier = new ValuePatternParser("[1]{1}[,][2]{2}")
            });

            object generatedValue = col.GenerateValue();

            string invariantString = col.GetInvariantStringValueRepresentation(generatedValue);
            string cultureDependentString = col.GetStringValueRepresentation(generatedValue);

            Assert.AreEqual("1.22", invariantString);
            Assert.AreEqual("1,22", cultureDependentString);
        }

        [Test]
        public void ColumnIsValid_ReturnsTrueAndEmptyOutputMessage()
        {
            Column col = new Column(new ColumnSettings()
            {
                ColumnName = "test1",
                Culture = CultureInfo.GetCultureInfo("ru-RU"),
                TargetValueType = typeof(double),
                ValueSupplier = new ValuePatternParser("[1]{1}[,][2]{2}")
            });

            Assert.IsTrue(col.IsValid(out string err));
            Assert.IsTrue(err == null || err == "");
        }

        [Test]
        public void ColumnIsNotValid_ReturnsFalseAndErrorMessage()
        {
            Column col = new Column(new ColumnSettings()
            {
                ColumnName = "test1",
                Culture = CultureInfo.GetCultureInfo("ru-RU"),
                TargetValueType = typeof(double)
            });

            Assert.IsFalse(col.IsValid(out string err));
            Assert.IsTrue(err.Length > 0);

            Column col2 = new Column(null);

            Assert.IsFalse(col2.IsValid(out string err2));
            Assert.IsTrue(err2.Length > 0);

            Column col3 = new Column(new ColumnSettings() { 
                Culture = null
            });

            Assert.IsFalse(col3.IsValid(out string err3));
            Assert.IsTrue(err3.Length > 0);
        }

        [Test]
        public void ColumnWithUnsetValueGenerator_ThrowsExceptionWhenTriesToGenerateValue()
        {
            Column col = new Column(new ColumnSettings()
            {
                ColumnName = "test1",
                Culture = CultureInfo.GetCultureInfo("ru-RU"),
                TargetValueType = typeof(double)
            });

            Assert.Null(col.GetValueGenerator());
            Assert.Throws<ArgumentNullException>(() => col.GenerateValue());
            Assert.Throws<ArgumentNullException>(() => col.GenerateValues(100));
        }

        [Test]
        public void ColumnSetsValueGenerator()
        {
            ValuePatternParser generator = new ValuePatternParser("[a-z]");
            Column col = new Column(new ColumnSettings());

            Assert.Null(col.ColumnSettings.ValueSupplier);

            col.SetValueGenerator(generator);
            Assert.AreEqual(generator, col.ColumnSettings.ValueSupplier);
        }
    }
}
