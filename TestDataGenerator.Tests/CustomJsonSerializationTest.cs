﻿using Newtonsoft.Json;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Core.JsonSerialization;

namespace TestDataGenerator.Tests
{
    public class CustomJsonSerializationTest
    {
        [Test]
        public void JsonSerializer_SuccessfullyParsesExistingsCultureInfo()
        {
            CultureInfo existingCulture = CultureInfo.GetCultureInfo("ru-RU");

            string serializedCulture = JsonConvert.SerializeObject(existingCulture, new CultureInfoJsonConverter());
            CultureInfo deserializedCulture = JsonConvert.DeserializeObject<CultureInfo>(serializedCulture, new CultureInfoJsonConverter());

            Assert.True(existingCulture.Equals(deserializedCulture));
        }

        [Test]
        public void JsonSerializer_SuccesfullyParsesCustomCultureInfo()
        {
            CultureInfo customCulture = new CultureInfo(CultureInfoJsonConverter.customCultureName, true);

            customCulture.NumberFormat.NumberDecimalSeparator = "DEC";
            customCulture.TextInfo.ListSeparator = "LISTSEP";
            customCulture.DateTimeFormat.FullDateTimePattern = "1234";

            string serializedCulture = JsonConvert.SerializeObject(customCulture, new CultureInfoJsonConverter());
            CultureInfo deserializedCulture = JsonConvert.DeserializeObject<CultureInfo>(serializedCulture, new CultureInfoJsonConverter());

            Assert.True(customCulture.Equals(deserializedCulture));
        }
    }
}
