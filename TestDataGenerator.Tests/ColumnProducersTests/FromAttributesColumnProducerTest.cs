﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Core.Attributes;
using TestDataGenerator.Core.ColumnProducers;
using TestDataGenerator.Core.Columns;
using TestDataGenerator.Core.ValueGenerators;
using TestDataGenerator.Core.ValueGenerators.Providers.Common;
using TestDataGenerator.Core.ValueGenerators.ValuePatternParser;
using TestDataGenerator.Core.ValueGenerators.ValueRangeGenerator;

namespace TestDataGenerator.Tests.ColumnProducersTests
{
    public class FromAttributesColumnProducerTest
    {
        class SampleClass
        {
            [ColumnName("OverridedName")]
            [ValueGenerator(ValueGeneratorTypeEnum.Pattern)]
            [TargetValueType(typeof(int))]
            public string TestStrProp { get; set; }

            [ValueGenerator(ValueGeneratorTypeEnum.Range)]
            [TargetValueType(typeof(short))]
            public byte TestByteProp { get; set; }

            [TargetValueType(typeof(string))]
            public short TestShortIntProp { get; set; }

            public bool TestBoolProp { get; set; }

            public ComplexObject ColumnToIgnore { get; set; }
        }

        class ValueGeneratorsTestClass
        {
            [ValueGenerator(ValueGeneratorTypeEnum.SelectFromList)]
            [TargetValueType(typeof(string))]
            public string TestProp1 { get; set; }

            [ValueGenerator(ValueGeneratorTypeEnum.SelectFromList)]
            [TargetValueType(typeof(int))]
            public string TestProp2 { get; set; }

            [ValueGenerator(ValueGeneratorTypeEnum.SelectFromList)]
            [TargetValueType(typeof(decimal))]
            public string TestProp3 { get; set; }

            [ValueGenerator(ValueGeneratorTypeEnum.SelectFromList)]
            [TargetValueType(typeof(DateTime))]
            public string TestProp4 { get; set; }

            [ValueGenerator(ValueGeneratorTypeEnum.Incremental)]
            [TargetValueType(typeof(uint))]
            public string TestProp5 { get; set; }

            [ValueGenerator(ValueGeneratorTypeEnum.Incremental)]
            [TargetValueType(typeof(double))]
            public string TestProp6 { get; set; }

            [ValueGenerator(ValueGeneratorTypeEnum.Incremental)]
            [TargetValueType(typeof(DateTime))]
            public string TestProp7 { get; set; }

            [ValueGenerator(ValueGeneratorTypeEnum.Range)]
            [TargetValueType(typeof(DateTime))]
            public string TestProp8 { get; set; }
        }

        class ComplexObject
        {

        }

        [Test]
        public void ColumnProducer_CorrectlyProducesColumnsBasedOnAttributes()
        {
            FromAttributesColumnProducer<SampleClass> producer = new FromAttributesColumnProducer<SampleClass>();
            Column[] producedColumns = producer.GetColumnsCollection().Cast<Column>().ToArray();
            Column[] expectedColumns = new Column[] {
                new Column(new ColumnSettings() {
                    ColumnName = "OverridedName",
                    TargetValueType = typeof(int),
                    ValueSupplier = new ValuePatternParser()
                }),
                new Column(new ColumnSettings() {
                    ColumnName = "TestByteProp",
                    TargetValueType = typeof(short),
                    ValueSupplier = new ValueRangeValueGenerator(new ValueRange(0, 100))
                }),
                new Column(new ColumnSettings() {
                    ColumnName = "TestShortIntProp",
                    TargetValueType = typeof(string),
                    ValueSupplier = new ValuePatternParser()
                }),
                new Column(new ColumnSettings() {
                    ColumnName = "TestBoolProp",
                    TargetValueType = typeof(bool),
                    ValueSupplier = new SelectFromListValueGenerator(new object[] { true, false })
                }),
            };

            Assert.AreEqual(expectedColumns.Length, producedColumns.Length);

            for (int i = 0; i < expectedColumns.Length; i++)
            {
                Assert.AreEqual(expectedColumns[i].ColumnSettings.ColumnName, producedColumns[i].ColumnSettings.ColumnName);
                Assert.AreEqual(expectedColumns[i].ColumnSettings.ValueSupplier.GetType(), producedColumns[i].ColumnSettings.ValueSupplier.GetType());
                Assert.AreEqual(expectedColumns[i].ColumnSettings.TargetValueType, producedColumns[i].ColumnSettings.TargetValueType);
                Assert.DoesNotThrow(() => producedColumns[i].GenerateValue());
            }
        }

        [Test]
        public void ColumnProducer_WorksWithAllDefaultValueGenerators()
        {
            FromAttributesColumnProducer<ValueGeneratorsTestClass> producer = new FromAttributesColumnProducer<ValueGeneratorsTestClass>();
            Column[] producedColumns = producer.GetColumnsCollection().Cast<Column>().ToArray();

            foreach (Column col in producedColumns)
            {
                Assert.DoesNotThrow(() => col.GenerateValue());
            }
        }
    }
}
