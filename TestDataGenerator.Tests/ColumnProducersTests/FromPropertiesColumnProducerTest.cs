﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces.Columns;
using TestDataGenerator.Core.ColumnProducers;

namespace TestDataGenerator.Tests.ColumnProducersTests
{
    public class FromPropertiesColumnProducerTest
    {
        class TestClassWithBasicTypes
        {
            public int IntProp { get; set; }
            public string StringProp { get; set; }
            public double DoubleProp { get; set; }
            public DateTime DateTimeProp { get; set; }
            public bool BoolProp { get; set; }
        }

        class TestClassWithNonBasicTypes
        {
            public int IntProp { get; set; }
            public string StringProp { get; set; }
            public TestClassWithBasicTypes ComplexProp { get; set; }
        }

        [Test]
        public void FromPropertiesColumnProducer_ProducesListOfColumns()
        {
            FromPropertiesColumnProducer<TestClassWithBasicTypes> producer = new FromPropertiesColumnProducer<TestClassWithBasicTypes>();

            IColumn[] resultColumns = producer.GetColumnsCollection().ToArray();
            var classProps = typeof(TestClassWithBasicTypes).GetProperties();

            Assert.AreEqual(resultColumns.Length, classProps.Length);

            foreach (var col in resultColumns)
            {
                Assert.NotNull(classProps.FirstOrDefault(prop => prop.Name == col.GetColumnName()));
            }
        }

        [Test]
        public void FromPropertiesColumnProducer_IgnoresUnknownTypes()
        {
            FromPropertiesColumnProducer<TestClassWithNonBasicTypes> producer = new FromPropertiesColumnProducer<TestClassWithNonBasicTypes>();

            IColumn[] resultColumns = producer.GetColumnsCollection().ToArray();
            var classProps = typeof(TestClassWithNonBasicTypes).GetProperties();

            Assert.AreNotEqual(resultColumns.Length, classProps.Length);
            Assert.Null(resultColumns.FirstOrDefault(x => x.GetColumnName() == nameof(TestClassWithNonBasicTypes.ComplexProp)));
        }
    }
}
