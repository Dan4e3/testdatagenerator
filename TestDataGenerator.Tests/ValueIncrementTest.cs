﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Core.ValueGenerators.IncrementalGenerator;

namespace TestDataGenerator.Tests
{
    public class ValueIncrementTest
    {
        [Test]
        public void ConstructorReceivesParamsOfDifferentTypes_ThrowsException()
        {
            Assert.Throws<ArgumentException>(() => new ValueIncrement(1, 10d, 1));
            Assert.Throws<ArgumentException>(() => new ValueIncrement(1d, 10, 1));
            //Assert.Throws<ArgumentException>(() => new ValueIncrement(1, 10, 1d));
        }

        [Test]
        public void ValueIncrementIsAskedToGetItsParameters_SuccessfullyReturnsThem()
        {
            ValueIncrement increment = new ValueIncrement(1, 100, 1);

            Assert.AreEqual(increment.StartValue, increment.GetStartValue<int>());
            Assert.AreEqual(increment.EndValue, increment.GetEndValue<int>());
            Assert.AreEqual(increment.ValueStep, increment.GetValueStep<int>());
            Assert.AreEqual(increment.TargetType, increment.GetValueTargetType());
        }
    }
}
