﻿using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces.Rows;
using TestDataGenerator.Core;
using TestDataGenerator.Core.Columns;
using TestDataGenerator.Core.ValueGenerators.ValueRangeGenerator;

namespace TestDataGenerator.Tests
{
    public class RowTest
    {
        [Test]
        public void RowRequestFullDataRowAndItemAtSpecificIndex_ReturnsCorrespondingData()
        {
            Column[] colsArr = new Column[] { 
                new Column(new ColumnSettings(){ 
                    ColumnName = "Col1",
                    Culture = CultureInfo.InvariantCulture,
                    TargetValueType = typeof(int),
                    ValueSupplier = new ValueRangeValueGenerator(new ValueRange(0, 1000))
                }),
                new Column(new ColumnSettings(){
                    ColumnName = "Col2",
                    Culture = CultureInfo.GetCultureInfo("ru-RU"),
                    TargetValueType = typeof(double),
                    ValueSupplier = new ValueRangeValueGenerator(new ValueRange(0d, 1d))
                }),
            };

            ColumnSet colset = new ColumnSet(colsArr);
            TestDataSet dset = new TestDataSet("dset", colset);

            object[] testRowData = new object[] { 10, 0.5 };
            Row rw = new Row(testRowData, dset);

            Assert.AreEqual(testRowData[0], rw[0]);
            Assert.AreEqual(testRowData[0], rw[colsArr[0]]);
            CollectionAssert.AreEqual(testRowData, rw.GetRowData());

            object[] anotherTestRowData = new object[] { 101, 0.99 };
            rw.SetRowData(anotherTestRowData);

            Assert.AreEqual(anotherTestRowData[0], rw[0]);
            Assert.AreEqual(anotherTestRowData[0], rw[colsArr[0]]);
            CollectionAssert.AreEqual(anotherTestRowData, rw.GetRowData());
        }

        [Test]
        public void RequestedStringRepresentationOfRowItem_ReturnsCorrectString()
        {
            Column[] colsArr = new Column[] {
                new Column(new ColumnSettings(){
                    ColumnName = "Col1",
                    Culture = CultureInfo.GetCultureInfo("ru-RU"),
                    TargetValueType = typeof(int),
                    ValueSupplier = new ValueRangeValueGenerator(new ValueRange(new DateTime(2000, 1, 1), new DateTime(2001, 1, 1)))
                }),
                new Column(new ColumnSettings(){
                    ColumnName = "Col2",
                    Culture = CultureInfo.GetCultureInfo("ru-RU"),
                    TargetValueType = typeof(double),
                    ValueSupplier = new ValueRangeValueGenerator(new ValueRange(0d, 1d))
                }),
            };

            ColumnSet colset = new ColumnSet(colsArr);
            TestDataSet dset = new TestDataSet("dset", colset);

            object[] testRowData = new object[] { new DateTime(2000, 6, 1), 0.15 };
            Row rw = new Row(testRowData, dset);

            Assert.AreEqual("01.06.2000 00:00:00", rw.GetStringRepresentation(0));
            Assert.AreEqual("01.06.2000 00:00:00", rw.GetStringRepresentation(colsArr[0]));
            Assert.AreEqual("06/01/2000 00:00:00", rw.GetInvariantStringRepresentation(0));
            Assert.AreEqual("06/01/2000 00:00:00", rw.GetInvariantStringRepresentation(colsArr[0]));

            Assert.AreEqual("0,15", rw.GetStringRepresentation(1));
            Assert.AreEqual("0,15", rw.GetStringRepresentation(colsArr[1]));
            Assert.AreEqual("0.15", rw.GetInvariantStringRepresentation(1));
            Assert.AreEqual("0.15", rw.GetInvariantStringRepresentation(colsArr[1]));
        }

        [Test]
        public void RowAsEnumerable_CorrectlyOutputsAtLeastItemCount()
        {
            Column[] colsArr = new Column[] {
                new Column(new ColumnSettings(){
                    ColumnName = "Col1",
                    Culture = CultureInfo.GetCultureInfo("ru-RU"),
                    TargetValueType = typeof(int),
                    ValueSupplier = new ValueRangeValueGenerator(new ValueRange(new DateTime(2000, 1, 1), new DateTime(2001, 1, 1)))
                }),
                new Column(new ColumnSettings(){
                    ColumnName = "Col2",
                    Culture = CultureInfo.GetCultureInfo("ru-RU"),
                    TargetValueType = typeof(double),
                    ValueSupplier = new ValueRangeValueGenerator(new ValueRange(0d, 1d))
                }),
            };

            ColumnSet colset = new ColumnSet(colsArr);
            TestDataSet dset = new TestDataSet("dset", colset);

            object[] testRowData = new object[] { new DateTime(2000, 6, 1), 0.15 };
            Row rw = new Row(testRowData, dset);

            int counter = 0;
            foreach (object item in (IEnumerable<object>)rw)
                counter++;

            Assert.AreEqual(testRowData.Length, counter);

            IEnumerable rwAsNonGenericEnumerable = rw;
            int counter2 = 0;
            foreach (var item in rwAsNonGenericEnumerable)
                counter2++;

            Assert.AreEqual(testRowData.Length, counter2);
        }

        [Test]
        public void RowRequestedValueOfTypeAtSpecificPosition_SuccessfullyReturnsIt()
        {
            Column[] colsArr = new Column[] {
                new Column(new ColumnSettings(){
                    ColumnName = "Col1",
                    Culture = CultureInfo.GetCultureInfo("ru-RU"),
                    TargetValueType = typeof(int),
                    ValueSupplier = new ValueRangeValueGenerator(new ValueRange(1, 100))
                }),
                new Column(new ColumnSettings(){
                    ColumnName = "Col2",
                    Culture = CultureInfo.GetCultureInfo("ru-RU"),
                    TargetValueType = typeof(double),
                    ValueSupplier = new ValueRangeValueGenerator(new ValueRange(0d, 1d))
                }),
            };

            ColumnSet colset = new ColumnSet(colsArr);
            TestDataSet dset = new TestDataSet("dset", colset);

            Row row = new Row(new object[] { 52, 0.5d }, dset);

            Assert.AreEqual(52, row.GetValue<int>(0));
            Assert.AreEqual(0.5d, row.GetValue<double>(1));

            Assert.AreEqual(52, row.GetValue<int>(colsArr[0]));
            Assert.AreEqual(0.5d, row.GetValue<double>(colsArr[1]));
        }
        
        [Test]
        public void Row_ReturnsStringRepresentationsOfAllValuesAtOnce()
        {
            Column[] colsArr = new Column[] {
                new Column(new ColumnSettings(){
                    ColumnName = "Col1",
                    Culture = CultureInfo.GetCultureInfo("ru-RU"),
                    TargetValueType = typeof(int),
                    ValueSupplier = new ValueRangeValueGenerator(new ValueRange(1, 100))
                }),
                new Column(new ColumnSettings(){
                    ColumnName = "Col2",
                    Culture = CultureInfo.GetCultureInfo("ru-RU"),
                    TargetValueType = typeof(double),
                    ValueSupplier = new ValueRangeValueGenerator(new ValueRange(0d, 1d))
                }),
            };

            ColumnSet colset = new ColumnSet(colsArr);
            TestDataSet dset = new TestDataSet("dset", colset);

            Row row = new Row(new object[] { 52, 0.5d }, dset);

            CollectionAssert.AreEqual(new string[] { "52", "0,5" }, row.GetStringRepresentation());
            CollectionAssert.AreEqual(new string[] { "52", "0.5" }, row.GetInvariantStringRepresentation());
        }
    }
}
