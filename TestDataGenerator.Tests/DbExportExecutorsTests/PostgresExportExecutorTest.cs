﻿using Npgsql;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces.Columns;
using TestDataGenerator.Core;
using TestDataGenerator.Core.Columns;
using TestDataGenerator.Core.ValueGenerators;
using TestDataGenerator.Core.ValueGenerators.ValuePatternParser;
using TestDataGenerator.IO.DatabaseExportExecutors;

namespace TestDataGenerator.Tests.DbExportExecutorsTests
{
    public class PostgresExportExecutorTest
    {
        [Test]
        public void PostgresExporterTriesToGetConnection_SuccessfullyReturnsItWithProivdedConnString()
        {
            string connString = "User ID=TstUser;Password=1234567890;Host=localhost;Port=5432;Database=TestDb";
            PostgresExportExecutor executor = new PostgresExportExecutor();
            executor.ConnectionString = connString;
            executor.TargetTableName = "table1";

            NpgsqlConnection sqlConn = executor.CreateConnection() as NpgsqlConnection;

            Assert.NotNull(sqlConn);
            Assert.True(sqlConn.ConnectionString.Contains(connString));
        }

        [Test]
        public void SqlServerExporterTriesToProduceInsertStatement_SuccessfulyReturnsIt()
        {
            string connString = "MyTestConnectionString";
            PostgresExportExecutor executor = new PostgresExportExecutor(connString, "testTable1");

            var colSet = new ColumnSet(new List<IColumn>() {
                new Column(new ColumnSettings() {
                    ColumnName = "aTestColumn",
                    TargetValueType = typeof(int),
                    ValueSupplier = new ValuePatternParser("[1-9]{1-6}")
                }),
                new Column(new ColumnSettings() {
                    ColumnName = "aTestColumn2",
                    TargetValueType = typeof(string),
                    ValueSupplier = new ValuePatternParser("[A-z]{1-32}")
                }),
                new Column(new ColumnSettings() {
                    ColumnName = "aTestColumn3",
                    TargetValueType = typeof(bool),
                    ValueSupplier = new SelectFromListValueGenerator(new object[] { true, false })
                })
            });

            TestDataSet dataset = new TestDataSet("Test", colSet);
            int rowsToGenerate = 10;
            dataset.GenerateNewDataSet(rowsToGenerate);

            string sql = executor.CreateInsertSqlCommand(colSet, dataset.GetExistingDataSet());

            Assert.Greater(sql.Length, 0);
            Assert.True(sql.Contains("testTable1") && 
                sql.Contains("aTestColumn") &&
                sql.Contains("aTestColumn2") &&
                sql.Contains("aTestColumn3"));
        }
    }
}
