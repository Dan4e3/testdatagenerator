﻿using Newtonsoft.Json;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces.Columns;
using TestDataGenerator.Abstractions.Settings;
using TestDataGenerator.Core;
using TestDataGenerator.Core.ColumnProducers;
using TestDataGenerator.Core.Columns;
using TestDataGenerator.Core.JsonSerialization;
using TestDataGenerator.Core.ValueGenerators;
using TestDataGenerator.Core.ValueGenerators.IncrementalGenerator;
using TestDataGenerator.Core.ValueGenerators.ValuePatternParser;
using TestDataGenerator.Core.ValueGenerators.ValueRangeGenerator;

namespace TestDataGenerator.Tests
{
    public class ColumnSetTest
    {
        class SampleClass
        {
            public string StrProp { get; set; }
            public int IntProp { get; set; }
            public double DoubleProp { get; set; }
        }

        [Test]
        public void ColumnSetRowGenerate_GeneratesNewRow()
        {
            ColumnSet colSet = new ColumnSet();
            var patternParser = new ValuePatternParser();
            patternParser.ValidateAndSetPattern("[0-9]{3}");

            colSet.AddColumn(new Column(new ColumnSettings() { 
                ColumnName = "Test",
                TargetValueType = typeof(int),
                ValueSupplier = patternParser
            }));
            colSet.AddColumn(new Column(new ColumnSettings()
            {
                ColumnName = "Test2",
                TargetValueType = typeof(int),
                ValueSupplier = new SelectFromListValueGenerator(new object[] { 100, 200, 300})
            }));

            var resRow = colSet.GetNewRow().Cast<int>().ToArray();

            Assert.AreEqual(2, resRow.Length);
            Assert.LessOrEqual(resRow[0], 999);
            Assert.LessOrEqual(resRow[1], 300);
        }

        [Test]
        public void ColumnSetSerializeToJson_SerializesToJsonAndDeserializesBack()
        {
            Column intPatternColumn = new Column(new ColumnSettings()
            {
                ColumnName = "Test",
                TargetValueType = typeof(int),
                ValueSupplier = new ValuePatternParser("[0-9]{3}")
            });
            Column intFromListColumn = new Column(new ColumnSettings()
            {
                ColumnName = "Test2",
                TargetValueType = typeof(int),
                ValueSupplier = new SelectFromListValueGenerator(new object[] { 100, 200, 300 })
            });
            Column dateTimeFromRangeColumn = new Column(new ColumnSettings()
            {
                ColumnName = "Test3",
                TargetValueType = typeof(DateTime),
                ValueSupplier = new ValueRangeValueGenerator(new ValueRange(new DateTime(2001, 1, 1), new DateTime(2002, 1, 1)))
            });
            Column doubleFromRangeColumn = new Column(new ColumnSettings()
            {
                ColumnName = "Test4",
                TargetValueType = typeof(double),
                ValueSupplier = new ValueRangeValueGenerator(new ValueRange(1.51, 3.02))
            });
            Column decimalFromRangeColumn = new Column(new ColumnSettings()
            {
                ColumnName = "Test4",
                TargetValueType = typeof(decimal),
                ValueSupplier = new ValueRangeValueGenerator(new ValueRange(1.51m, 3.02m))
            });
            Column integerFromRangeColumn = new Column(new ColumnSettings()
            {
                ColumnName = "Test5",
                TargetValueType = typeof(int),
                ValueSupplier = new ValueRangeValueGenerator(new ValueRange(1, 10))
            });
            Column shortIntFromRangeColumn = new Column(new ColumnSettings()
            {
                ColumnName = "Test5",
                TargetValueType = typeof(short),
                ValueSupplier = new ValueRangeValueGenerator(new ValueRange(100, 312))
            });
            Column integerIncrementColumn = new Column(new ColumnSettings() { 
                ColumnName = "Test6",
                TargetValueType = typeof(int),
                ValueSupplier = new IncrementalValueGenerator(new ValueIncrement(1, 100, 1))
            });
            Column doubleIncrementColumn = new Column(new ColumnSettings()
            {
                ColumnName = "Test7",
                TargetValueType = typeof(double),
                ValueSupplier = new IncrementalValueGenerator(new ValueIncrement(0.00, 1.00, 0.25))
            });
            Column dateTimeIncrementColmn = new Column(new ColumnSettings()
            {
                ColumnName = "Test8",
                TargetValueType = typeof(DateTime),
                ValueSupplier = new IncrementalValueGenerator(new ValueIncrement(new DateTime(2000, 01, 01),
                    new DateTime(2001, 01, 01), new TimeSpan(24, 0, 0)))
            });
            Column patternParserWithNullValues = new Column(new ColumnSettings() { 
                ColumnName = "Test9",
                TargetValueType = typeof(string),
                ValueSupplier = new ValuePatternParser("[A-z]{4-10}", new NullValuesSettings() { 
                    GenerateNullValues = true,
                    AverageRowsBeforeNullAppearance = 5
                }),
                Culture = CultureInfo.InvariantCulture
            });

            ColumnSet colSet = new ColumnSet(new Column[] { intPatternColumn, intFromListColumn, 
                dateTimeFromRangeColumn, doubleFromRangeColumn, decimalFromRangeColumn, shortIntFromRangeColumn,
                integerFromRangeColumn, integerIncrementColumn, doubleIncrementColumn, dateTimeIncrementColmn,
                patternParserWithNullValues
            });

            var serialized = colSet.SerializeAsJson();

            Assert.Greater(serialized.Length, 0);

            var deserialized = ColumnSet.DeserializeFromJson(serialized);

            Assert.True(colSet.Count() == deserialized.Count());

            for (int i = 0; i < colSet.Count(); i++)
            {
                Assert.True(colSet[i].GetColumnType() == deserialized[i].GetColumnType());
            }

            Assert.True((colSet[0] as Column)?.ColumnSettings.ValueSupplier as ValuePatternParser != null);
            Assert.True((deserialized[0] as Column)?.ColumnSettings.ValueSupplier as ValuePatternParser != null);

            Assert.True(((colSet[1] as Column)?.ColumnSettings.ValueSupplier as SelectFromListValueGenerator).ArrayOfValuesToSelectFrom.Length == 3);
            Assert.True(((deserialized[1] as Column)?.ColumnSettings.ValueSupplier as SelectFromListValueGenerator).ArrayOfValuesToSelectFrom.Length == 3);
        }

        [Test]
        public void ColumnSetConstructorReceivesColumnProducer_GetColumnListFromProducer()
        {
            FromPropertiesColumnProducer<SampleClass> producer = new FromPropertiesColumnProducer<SampleClass>();
            ColumnSet colset = new ColumnSet(producer);

            Assert.True(colset.Count() == typeof(SampleClass).GetProperties().Length);
        }

        [Test]
        public void ColumnSetCallsClear_ClearsColumnList()
        {
            Column[] cols = new Column[] { 
                new Column(new ColumnSettings()),
                new Column(new ColumnSettings()),
                new Column(new ColumnSettings())
            };
            ColumnSet colset = new ColumnSet(cols);

            Assert.AreEqual(cols.Length, colset.GetColumnsCount());

            colset.Clear();
            Assert.True(colset.Count() == 0);
        }

        [Test]
        public void RemoveColumnFromColumnSet_ColumnIsRemoved()
        {
            Column col1 = new Column(new ColumnSettings());
            Column col2 = new Column(new ColumnSettings());
            ColumnSet colset = new ColumnSet(new Column[] { col1, col2 });

            Assert.AreEqual(2, colset.GetColumnsCount());

            colset.RemoveColumn(col2);

            Assert.AreEqual(1, colset.GetColumnsCount());
            Assert.AreEqual(colset.First(), col1);
        }

        [Test]
        public void ColumnSetRequestedByColumnName_ReturnsCorrespondingColumn()
        {
            Column col1 = new Column(new ColumnSettings()
            {
                ColumnName = "TestCol1"
            });
            Column col2 = new Column(new ColumnSettings()
            {
                ColumnName = "TestCol2"
            });
            Column col3 = new Column(new ColumnSettings()
            {
                ColumnName = "TestCol3"
            });
            ColumnSet colset = new ColumnSet(new Column[] { 
                col1, col2, col3
            });

            IColumn foundCol = colset["TestCol1"];

            Assert.AreEqual(col1, foundCol);

            Assert.IsNull(colset["Nonexistentcolname]"]);
        }
    }
}
