﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Core.ValueGenerators;
using TestDataGenerator.Core.ValueGenerators.Providers;
using TestDataGenerator.Core.ValueGenerators.ValuePatternParser;

namespace TestDataGenerator.Tests
{
    public class ClassInstanceRandomizerTest
    {
        class ComplexClass
        {
            public int IntProp { get; set; }
        }

        class ExampleClass
        {
            public byte ByteProp { get; set; }
            public string StringProp { get; set; }
            public float? FloatProp { get; set; }
            public decimal DecimalProp { get; set; }
            public bool BoolProp { get; set; }
            public int IntProp { get; set; }
            public DateTime? DateTimeProp { get; set; }
            public ComplexClass ComplexProp { get; set; }
            public Point StructProp { get; set; }
        }

        [Test]
        public void DefaultClassInstanceRandomizer_ReturnsDictWithDefaultValueGeneratorsWherePossible()
        {
            DefaultClassInstanceRandomizer<ExampleClass> randomizer = new DefaultClassInstanceRandomizer<ExampleClass>();
            var dict = randomizer.GetGeneratorPerPropertyDict();

            Assert.AreEqual(2, dict.Count);
            Assert.AreEqual(7, dict[typeof(ExampleClass)].Count);
            foreach (var pair in dict[typeof(ExampleClass)])
            {
                Assert.NotNull(pair.Value);
            }
        }

        [Test]
        public void DefaulClassInstanceRandomizerOnDemand_SetsUserDefinedValueGeneratorsPerPropertyOrThrowsExceptionIfPropNotFound()
        {
            DefaultClassInstanceRandomizer<ExampleClass> randomizer = new DefaultClassInstanceRandomizer<ExampleClass>();
            randomizer.SetGeneratorForProperty(nameof(ExampleClass.IntProp), new ValuePatternParser("[8-9]{2}"));
            randomizer.SetGeneratorForProperty(typeof(ComplexClass), nameof(ComplexClass.IntProp), new ValuePatternParser("[6-7]{2}"));

            var dict = randomizer.GetGeneratorPerPropertyDict();

            Assert.True(dict[typeof(ExampleClass)][typeof(ExampleClass).GetProperty(nameof(ExampleClass.IntProp))].GetType() == typeof(ValuePatternParser));
            Assert.True(dict[typeof(ComplexClass)][typeof(ComplexClass).GetProperty(nameof(ComplexClass.IntProp))].GetType() == typeof(ValuePatternParser));
            Assert.AreEqual(2, dict.Count);
            Assert.AreEqual(7, dict[typeof(ExampleClass)].Count);
            foreach (var pair in dict[typeof(ExampleClass)])
            {
                Assert.NotNull(pair.Value);
            }

            Assert.Throws<ArgumentException>(() => randomizer.SetGeneratorForProperty("NonExistentProperty", new ValuePatternParser("[a]")));
            Assert.Throws<ArgumentException>(() => randomizer.SetGeneratorForProperty(typeof(ComplexClass), "NonExistentProperty", new ValuePatternParser("[a]")));
        }
    }
}
