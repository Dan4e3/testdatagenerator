﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Settings;

namespace TestDataGenerator.Tests
{
    public class NullValuesSettingsTest
    {
        [Test]
        public void SettingsReceiveAverageRowsParameterAsLessThanOne_ThrowsException()
        {
            Assert.Throws<ArgumentException>(() => new NullValuesSettings()
            {
                GenerateNullValues = true,
                AverageRowsBeforeNullAppearance = -1
            });
        }
    }
}
