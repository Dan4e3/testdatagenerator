﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Core.ValueGenerators.ValueRangeGenerator;

namespace TestDataGenerator.Tests
{
    public class ValueRangeTest
    {
        [Test]
        public void ValueRangeWithInputOfTwoDifferentObjectTypes_ThrowsException()
        {
            Assert.Throws<ArgumentException>(() => new ValueRange(5, "2"));

            Assert.DoesNotThrow(() => new ValueRange(1, 2));
        }
    }
}
