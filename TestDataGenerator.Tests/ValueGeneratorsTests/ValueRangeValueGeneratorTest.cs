﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Settings;
using TestDataGenerator.Core.ValueGenerators;
using TestDataGenerator.Core.ValueGenerators.ValueRangeGenerator;

namespace TestDataGenerator.Tests.ValueGeneratorsTests
{
    public class ValueRangeValueGeneratorTest
    {
        private const int RAND_GENERATOR_ITERATIONS = 1000;

        private class StubClass
        {

        }

        [Test]
        public void ValueRangeGeneratorOfUnregisteredType_ThrowsException()
        {
            ValueRange range = new ValueRange(new StubClass(), new StubClass());
            Assert.Throws<ArgumentException>(() => new ValueRangeValueGenerator(range));
        }

        [Test]
        public void ValueRangeGeneratorOfDatetimeType_GeneratesValueByParamsSpecified()
        {
            DateTime minDate = DateTime.Parse("01/01/2001");
            DateTime maxDate = DateTime.Parse("01/02/2001");
            ValueRange range = new ValueRange(minDate, maxDate);
            ValueRangeValueGenerator generator = new ValueRangeValueGenerator(range);

            for (int i = 0; i < RAND_GENERATOR_ITERATIONS; i++)
            {
                var result = (DateTime)generator.GenerateValue(typeof(DateTime), CultureInfo.CurrentCulture);

                Assert.GreaterOrEqual(result, minDate);
                Assert.LessOrEqual(result, maxDate);
            }
        }

        [Test]
        public void ValueRangeGeneratorOfDatetimeType_CanConvertOutputDatetimeToString()
        {
            DateTime minDate = DateTime.Parse("01/01/2001");
            DateTime maxDate = DateTime.Parse("01/02/2001");
            ValueRange range = new ValueRange(minDate, maxDate);
            ValueRangeValueGenerator generator = new ValueRangeValueGenerator(range);

            string res = (string)generator.GenerateValue(typeof(string), CultureInfo.GetCultureInfo("ru-RU"));
            DateTime parsedRes = DateTime.ParseExact(res, "dd.MM.yyyy HH:mm:ss", null);
            Assert.True(parsedRes < maxDate);
            Assert.True(parsedRes > minDate);
        }

        [Test]
        public void ValueRangeGeneratorOfDatetimeType_ThrowsExceptionWhenMinDateGreaterOrEqualThanMaxDate()
        {
            DateTime minDate = DateTime.Parse("01/02/2001");
            DateTime maxDate = DateTime.Parse("01/01/2001");
            ValueRange range = new ValueRange(minDate, maxDate);
            ValueRangeValueGenerator generator = new ValueRangeValueGenerator(range);

            Assert.Throws<ArgumentException>(() => generator.GenerateValue(typeof(DateTime), CultureInfo.CurrentCulture));
        }

        [Test]
        public void ValueGeneratorOfDatetimeType_PrintsDatetimeInFriendlyName()
        {
            DateTime minDate = DateTime.Parse("01/01/2001");
            DateTime maxDate = DateTime.Parse("01/02/2001");
            ValueRange range = new ValueRange(minDate, maxDate);
            ValueRangeValueGenerator generator = new ValueRangeValueGenerator(range);

            string fullName = generator.GetFriendlyName();
            Assert.True(fullName.Contains(minDate.ToShortDateString()));
            Assert.True(fullName.Contains(maxDate.ToShortDateString()));

            int charsToReturn = 5;
            string partialName = generator.GetFriendlyName(charsToReturn);
            Assert.AreEqual(charsToReturn, partialName.Length);
        }

        [Test]
        public void ValueGeneratorOfDatetimeType_ReturnsTrueIfValidAndFalseIfIsInvalid()
        {
            DateTime minDate = DateTime.Parse("01/01/2001");
            DateTime maxDate = DateTime.Parse("01/02/2001");
            ValueRange range = new ValueRange(minDate, maxDate);
            ValueRangeValueGenerator generator = new ValueRangeValueGenerator(range);

            bool isValid = generator.IsValid(out string error);
            Assert.True(isValid);
            Assert.True(error == null || error == "");

            minDate = new DateTime(2001, 02, 01);
            maxDate = new DateTime(2001, 01, 01);
            range = new ValueRange(minDate, maxDate);
            generator = new ValueRangeValueGenerator(range);

            bool invalid = generator.IsValid(out string error2);
            Assert.False(invalid);
            Assert.Greater(error2.Length, 0);
        }

        [Test]
        public void ValueRangeGeneratorOfIntegerType_GeneratesValueWithinProvidedRange()
        {
            int minInt = 10;
            int maxInt = 101;
            ValueRange range = new ValueRange(minInt, maxInt);
            ValueRangeValueGenerator generator = new ValueRangeValueGenerator(range);

            for (int i = 0; i < RAND_GENERATOR_ITERATIONS; i++)
            {
                var result = (int)generator.GenerateValue(typeof(int), CultureInfo.CurrentCulture);

                Assert.GreaterOrEqual(result, minInt);
                Assert.LessOrEqual(result, maxInt);

                var newResultAsString = generator.GenerateValue(typeof(string), CultureInfo.CurrentCulture).ToString();

                Assert.GreaterOrEqual(int.Parse(newResultAsString, CultureInfo.CurrentCulture), minInt);
                Assert.LessOrEqual(int.Parse(newResultAsString, CultureInfo.CurrentCulture), maxInt);
            }
        }

        [Test]
        public void ValueRangeGeneratorOfIntegerType_ThrowsExceptionWhenMinValueIsGreaterOrEqualThanMaxValue()
        {
            int minInt = 20;
            int maxInt = 0;
            ValueRange range = new ValueRange(minInt, maxInt);
            ValueRangeValueGenerator generator = new ValueRangeValueGenerator(range);

            Assert.Throws<ArgumentException>(() => generator.GenerateValue(typeof(int), CultureInfo.InvariantCulture));
        }

        [Test]
        public void ValueRangeGeneratorOfIntegerType_IncludesMinAndMaxValueInFriendlyName()
        {
            int minInt = 5;
            int maxInt = 100;
            ValueRange range = new ValueRange(minInt, maxInt);
            ValueRangeValueGenerator generator = new ValueRangeValueGenerator(range);

            string name = generator.GetFriendlyName();
            Assert.True(name.Contains(minInt.ToString()) && name.Contains(maxInt.ToString()));

            int charsToOutput = 2;
            string partialName = generator.GetFriendlyName(charsToOutput);
            Assert.AreEqual(charsToOutput, partialName.Length);
        }

        [Test]
        public void ValueRangeGeneratorOfIntegerType_ReturnsFalseAndErrorMessageIfHasErrors()
        {
            int minInt = 20;
            int maxInt = 0;
            ValueRange range = new ValueRange(minInt, maxInt);
            ValueRangeValueGenerator generator = new ValueRangeValueGenerator(range);

            bool isValid = generator.IsValid(out string err);
            Assert.False(isValid);
            Assert.True(err.Length > 0);
        }

        [Test]
        public void ValueRangeGeneratorOfIntegerType_ReturnsTrueIfHasNoValidationProblems()
        {
            int minInt = 5;
            int maxInt = 100;
            ValueRange range = new ValueRange(minInt, maxInt);
            ValueRangeValueGenerator generator = new ValueRangeValueGenerator(range);

            bool isValid = generator.IsValid(out string err);
            Assert.True(isValid);
            Assert.True(err == null || err == "");
        }

        [Test]
        public void ValueRangeGeneratorOfIntegerType_GeneratesCorrectValuesWithAnyIntegerBasicType()
        {
            sbyte minSbyte = -10;
            sbyte maxSbyte = 5;
            ValueRange range = new ValueRange(minSbyte, maxSbyte);
            ValueRangeValueGenerator generator = new ValueRangeValueGenerator(range);
            for (int i = 0; i < RAND_GENERATOR_ITERATIONS; i++)
            {
                sbyte value = (sbyte)generator.GenerateValue(typeof(sbyte), CultureInfo.InvariantCulture);
                Assert.GreaterOrEqual(value, minSbyte);
                Assert.LessOrEqual(value, maxSbyte);
            }

            short minShort = -2510;
            short maxShort = 32100;
            range = new ValueRange(minShort, maxShort);
            generator = new ValueRangeValueGenerator(range);
            for (int i = 0; i < RAND_GENERATOR_ITERATIONS; i++)
            {
                short value = (short)generator.GenerateValue(typeof(short), CultureInfo.InvariantCulture);
                Assert.GreaterOrEqual(value, minShort);
                Assert.LessOrEqual(value, maxShort);
            }

            uint minUint = 1023023;
            uint maxUint = 2405050;
            range = new ValueRange(minUint, maxUint);
            generator = new ValueRangeValueGenerator(range);
            for (int i = 0; i < RAND_GENERATOR_ITERATIONS; i++)
            {
                string value = generator.GenerateValue(typeof(string), CultureInfo.InvariantCulture).ToString();
                Assert.GreaterOrEqual(Convert.ToUInt32(value), minUint);
                Assert.LessOrEqual(Convert.ToUInt32(value), maxUint);
            }

            byte minByte = 15;
            byte maxByte = 29;
            range = new ValueRange(minByte, maxByte);
            generator = new ValueRangeValueGenerator(range);
            for (int i = 0; i < RAND_GENERATOR_ITERATIONS; i++)
            {
                byte value = (byte)generator.GenerateValue(typeof(byte), CultureInfo.InvariantCulture);
                Assert.GreaterOrEqual(value, minByte);
                Assert.LessOrEqual(value, maxByte);
            }

            ushort minUshort = 12301;
            ushort maxUshort = 35125;
            range = new ValueRange(minUshort, maxUshort);
            generator = new ValueRangeValueGenerator(range);
            for (int i = 0; i < RAND_GENERATOR_ITERATIONS; i++)
            {
                ushort value = (ushort)generator.GenerateValue(typeof(ushort), CultureInfo.InvariantCulture);
                Assert.GreaterOrEqual(value, minUshort);
                Assert.LessOrEqual(value, maxUshort);
            }

            long minLong = 894594949;
            long maxLong = 98349894391;
            range = new ValueRange(minLong, maxLong);
            generator = new ValueRangeValueGenerator(range);
            for (int i = 0; i < RAND_GENERATOR_ITERATIONS; i++)
            {
                long value = (long)generator.GenerateValue(typeof(long), CultureInfo.InvariantCulture);
                Assert.GreaterOrEqual(value, minLong);
                Assert.LessOrEqual(value, maxLong);
            }
        }

        [Test]
        public void ValueRangeGeneratorOfDoubleType_GeneratesValueWithinProvidedRange()
        {
            double minDbl = 105.51;
            double maxDbl = 112.987;
            ValueRange range = new ValueRange(minDbl, maxDbl);
            ValueRangeValueGenerator generator = new ValueRangeValueGenerator(range);

            for (int i = 0; i < RAND_GENERATOR_ITERATIONS; i++)
            {
                var result = (double)generator.GenerateValue(typeof(double), CultureInfo.CurrentCulture);

                Assert.GreaterOrEqual(result, minDbl);
                Assert.LessOrEqual(result, maxDbl);

                Assert.DoesNotThrow(() => generator.GenerateValue(typeof(string), CultureInfo.CurrentCulture));
            }
        }

        [Test]
        public void ValueRangeGeneratorOfDoubleType_ThrowsExceptionIfMinValueGreaterOrEqualThanMaxValue()
        {
            double minDbl = 2.315;
            double maxDbl = 0.78;
            ValueRange range = new ValueRange(minDbl, maxDbl);
            ValueRangeValueGenerator generator = new ValueRangeValueGenerator(range);

            Assert.Throws<ArgumentException>(() => generator.GenerateValue(typeof(double), CultureInfo.CurrentCulture));
        }

        [Test]
        public void ValueGeneratorOfDoubleType_IncludesMinAndMaxValueInFriendlyName()
        {
            double minDbl = 105.51;
            double maxDbl = 112.987;
            ValueRange range = new ValueRange(minDbl, maxDbl);
            ValueRangeValueGenerator generator = new ValueRangeValueGenerator(range);

            string fullName = generator.GetFriendlyName();
            Assert.True(fullName.Contains(minDbl.ToString()));
            Assert.True(fullName.Contains(maxDbl.ToString()));

            int charsToOutput = 5;
            string partialName = generator.GetFriendlyName(charsToOutput);
            Assert.AreEqual(charsToOutput, partialName.Length);
        }

        [Test]
        public void ValueGeneratorOfDoubleType_ReturnsTrueOnValidationRequestIfNoErrors()
        {
            double minDbl = 105.51;
            double maxDbl = 112.987;
            ValueRange range = new ValueRange(minDbl, maxDbl);
            ValueRangeValueGenerator generator = new ValueRangeValueGenerator(range);

            bool isValid = generator.IsValid(out string err);
            Assert.True(isValid);
            Assert.True(err == null || err == "");
        }

        [Test]
        public void ValueRangeGeneratorOfDoubleType_GeneratesCorrectValuesWithAnyFloatingPointBasicType()
        {
            float minF = 10.152f;
            float maxF = 15.52124f;
            ValueRange range = new ValueRange(minF, maxF);
            ValueRangeValueGenerator generator = new ValueRangeValueGenerator(range);
            for (int i = 0; i < RAND_GENERATOR_ITERATIONS; i++)
            {
                float value = (float)generator.GenerateValue(typeof(float), CultureInfo.InvariantCulture);
                Assert.GreaterOrEqual(value, minF);
                Assert.LessOrEqual(value, maxF);
            }

            decimal minDm = 151.23145465599m;
            decimal maxDm= 187.9239492518582359m;
            range = new ValueRange(minDm, maxDm);
            generator = new ValueRangeValueGenerator(range);
            for (int i = 0; i < RAND_GENERATOR_ITERATIONS; i++)
            {
                decimal value = (decimal)generator.GenerateValue(typeof(decimal), CultureInfo.InvariantCulture);
                Assert.GreaterOrEqual(value, minDm);
                Assert.LessOrEqual(value, maxDm);
            }

            double minD = 251.1253259d;
            double maxD = 194241.582823d;
            range = new ValueRange(minD, maxD);
            generator = new ValueRangeValueGenerator(range);
            for (int i = 0; i < RAND_GENERATOR_ITERATIONS; i++)
            {
                string value = generator.GenerateValue(typeof(string), CultureInfo.InvariantCulture).ToString();
                Assert.GreaterOrEqual(Convert.ToDouble(value, CultureInfo.InvariantCulture), minD);
                Assert.LessOrEqual(Convert.ToDouble(value, CultureInfo.InvariantCulture), maxD);
            }
        }

        [Test]
        public void ValueRangeGeneratorUsesNullValues_SuccessfullyReturnsThem()
        {
            NullValuesSettings nullSettings = new NullValuesSettings()
            {
                GenerateNullValues = true,
                AverageRowsBeforeNullAppearance = 10
            };

            uint minInt = 1;
            uint maxInt = 100;
            ValueRangeValueGenerator generatorInt = new ValueRangeValueGenerator(new ValueRange(minInt, maxInt), nullSettings);

            float minF = 1f;
            float maxF = 10f;
            ValueRangeValueGenerator generatorFloat = new ValueRangeValueGenerator(new ValueRange(minF, maxF), nullSettings);

            DateTime minDt = new DateTime(2000, 1, 1);
            DateTime maxDt = new DateTime(2020, 1, 1);
            ValueRangeValueGenerator generatorDt = new ValueRangeValueGenerator(new ValueRange(minDt, maxDt), nullSettings);

            int nullIntCounter = 0;
            int nullFloatCounter = 0;
            int nullDateTimeCounter = 0;
            for (int i = 0; i < RAND_GENERATOR_ITERATIONS; i++)
            {
                uint? generatedIntValue = (uint?)generatorInt.GenerateValue(typeof(uint?), CultureInfo.InvariantCulture);
                if (generatedIntValue == null)
                    nullIntCounter++;

                float? generatedFloatValue = (float?)generatorFloat.GenerateValue(typeof(float?), CultureInfo.InvariantCulture);
                if (generatedFloatValue == null)
                    nullFloatCounter++;

                DateTime? generatedDateTimeValue = (DateTime?)generatorDt.GenerateValue(typeof(DateTime?), CultureInfo.InvariantCulture);
                if (generatedDateTimeValue == null)
                    nullDateTimeCounter++;

                Assert.True((generatedIntValue != null && (generatedIntValue >= minInt && generatedIntValue <= maxInt)) || generatedIntValue == null);
                Assert.True((generatedFloatValue != null && (generatedFloatValue >= minF && generatedFloatValue <= maxF)) || generatedFloatValue == null);
                Assert.True((generatedDateTimeValue != null && (generatedDateTimeValue >= minDt && generatedDateTimeValue <= maxDt)) || generatedDateTimeValue == null);
            }

            Assert.Greater(nullIntCounter, 0);
            Assert.Greater(nullFloatCounter, 0);
            Assert.Greater(nullDateTimeCounter, 0);
        }
    }
}
