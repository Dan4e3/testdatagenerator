﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Settings;
using TestDataGenerator.Core.ValueGenerators.IncrementalGenerator;

namespace TestDataGenerator.Tests.ValueGeneratorsTests
{
    public class IncrementalValueGeneratorTest
    {
        [Test]
        public void IntegerIncrementalGeneratorRequestedValue_ReturnsCorrectValue()
        {
            ValueIncrement increment = new ValueIncrement(0, 100, 1);
            IncrementalValueGenerator generator = new IncrementalValueGenerator(increment);

            for (int i = 0; i < 101; i++)
            {
                int generatedValue = (int)generator.GenerateValue(typeof(int), CultureInfo.InvariantCulture);
                Assert.AreEqual(i, generatedValue);
            }
        }

        [Test]
        public void IntegerIncrementalGeneratorRequestedFriendlyName_ReturnsFullOrPartName()
        {
            ValueIncrement increment = new ValueIncrement(101, 200, 10);
            IncrementalValueGenerator generator = new IncrementalValueGenerator(increment);

            string fullName = generator.GetFriendlyName();
            Assert.True(fullName.Contains("101") &&
                        fullName.Contains("200") &&
                        fullName.Contains("10"));

            const int charsToGet = 5;
            string partName = generator.GetFriendlyName(charsToGet);
            Assert.AreEqual(charsToGet, partName.Length);
        }

        [Test]
        public void IntegerIncrementalGeneratorRequestedValidCheck_ReturnsTrueIfValidAndFalseOtherwise()
        {
            ValueIncrement increment = new ValueIncrement(0, 100, 1);
            IncrementalValueGenerator generator = new IncrementalValueGenerator(increment);
            bool isValid = generator.IsValid(out string errMsg);

            Assert.True(isValid);
            Assert.True(string.IsNullOrEmpty(errMsg));

            increment = new ValueIncrement(100, 0, 1);
            generator = new IncrementalValueGenerator(increment);
            bool isValid2 = generator.IsValid(out string errMsg2);

            Assert.False(isValid2);
            Assert.Greater(errMsg2.Length, 0);

            increment = new ValueIncrement(0, 100, 1000);
            generator = new IncrementalValueGenerator(increment);
            bool isValid3 = generator.IsValid(out string errMsg3);

            Assert.False(isValid3);
            Assert.Greater(errMsg3.Length, 0);
        }

        [Test]
        public void IntegerIncrementalGeneratorRequestedToGenerateValue_GeneratesValueIfSetupIsCorrectAndThrowsExceptionOtherwise()
        {
            ValueIncrement increment = new ValueIncrement(0, 100, 1);
            IncrementalValueGenerator generator = new IncrementalValueGenerator(increment);

            Assert.DoesNotThrow(() => generator.GenerateValue(typeof(int), CultureInfo.InvariantCulture));

            increment = new ValueIncrement(100, 0, 1);
            generator = new IncrementalValueGenerator(increment);
            Assert.Throws<ArgumentException>(() => generator.GenerateValue(typeof(int), CultureInfo.InvariantCulture));

            increment = new ValueIncrement(0, 100, 1000);
            generator = new IncrementalValueGenerator(increment);
            Assert.Throws<ArgumentException>(() => generator.GenerateValue(typeof(int), CultureInfo.InvariantCulture));

            increment = new ValueIncrement(0, 1, 1);
            generator = new IncrementalValueGenerator(increment);
            generator.GenerateValue(typeof(int), CultureInfo.InvariantCulture);
            generator.GenerateValue(typeof(int), CultureInfo.InvariantCulture);
            Assert.Throws<ArithmeticException>(() => generator.GenerateValue(typeof(int), CultureInfo.InvariantCulture));
        }

        [Test]
        public void IntegerIncrementalGeneratorRequestStateReset_ResetsStateOfUnderlyingGenerator()
        {
            int startValue = 0;
            ValueIncrement increment = new ValueIncrement(startValue, 100, 1);
            IncrementalValueGenerator generator = new IncrementalValueGenerator(increment);

            generator.GenerateValue(typeof(int), CultureInfo.InvariantCulture);
            int value = (int)generator.GenerateValue(typeof(int), CultureInfo.InvariantCulture);

            Assert.AreEqual(1, value);

            generator.ResetGeneratorState();
            int valueAfterReset = (int)generator.GenerateValue(typeof(int), CultureInfo.InvariantCulture);

            Assert.AreEqual(0, valueAfterReset);
        }

        [Test]
        public void IntegerIncrementalGenerator_GeneratesCorrectValuesWithAnyBasicIntegerType()
        {
            short startValueShort = 19;
            short endValueShort = 31;
            short incrementValueShort = 2;

            ValueIncrement increment = new ValueIncrement(startValueShort, endValueShort, incrementValueShort);
            IncrementalValueGenerator generator = new IncrementalValueGenerator(increment);

            Assert.AreEqual(startValueShort, generator.GenerateValue(typeof(short), CultureInfo.InvariantCulture));
            Assert.AreEqual(startValueShort + incrementValueShort, generator.GenerateValue(typeof(short), CultureInfo.InvariantCulture));

            sbyte startValueSbyte = -112;
            sbyte endValueSbyte = 14;
            sbyte incrementValueSbyte = 10;

            increment = new ValueIncrement(startValueSbyte, endValueSbyte, incrementValueSbyte);
            generator = new IncrementalValueGenerator(increment);

            Assert.AreEqual(startValueSbyte, generator.GenerateValue(typeof(sbyte), CultureInfo.InvariantCulture));
            Assert.AreEqual(startValueSbyte + incrementValueSbyte, generator.GenerateValue(typeof(sbyte), CultureInfo.InvariantCulture));

            uint startValueUint = 100000;
            uint endValueUint = 12000000;
            uint incrementValueUint = 1523;

            increment = new ValueIncrement(startValueUint, endValueUint, incrementValueUint);
            generator = new IncrementalValueGenerator(increment);

            Assert.AreEqual(startValueUint.ToString(), generator.GenerateValue(typeof(string), CultureInfo.InvariantCulture));
            Assert.AreEqual((startValueUint + incrementValueUint).ToString(), generator.GenerateValue(typeof(string), CultureInfo.InvariantCulture));

            byte startValueByte = 115;
            byte endValueByte = 255;
            byte incrementValueByte = 2;

            increment = new ValueIncrement(startValueByte, endValueByte, incrementValueByte);
            generator = new IncrementalValueGenerator(increment);

            Assert.AreEqual(startValueByte, generator.GenerateValue(typeof(byte), CultureInfo.InvariantCulture));
            Assert.AreEqual(startValueByte + incrementValueByte, generator.GenerateValue(typeof(byte), CultureInfo.InvariantCulture));

            ushort startValueUshort = 5000;
            ushort endValueUshort = 54221;
            ushort incrementValueUshort = 212;

            increment = new ValueIncrement(startValueUshort, endValueUshort, incrementValueUshort);
            generator = new IncrementalValueGenerator(increment);

            Assert.AreEqual(startValueUshort, generator.GenerateValue(typeof(ushort), CultureInfo.InvariantCulture));
            Assert.AreEqual(startValueUshort + incrementValueUshort, generator.GenerateValue(typeof(ushort), CultureInfo.InvariantCulture));

            long startValueLong = 948282391;
            long endValueLong = 102394828238;
            long incrementValueLong = 515253;

            increment = new ValueIncrement(startValueLong, endValueLong, incrementValueLong);
            generator = new IncrementalValueGenerator(increment);

            Assert.AreEqual(startValueLong, generator.GenerateValue(typeof(long), CultureInfo.InvariantCulture));
            Assert.AreEqual(startValueLong + incrementValueLong, generator.GenerateValue(typeof(long), CultureInfo.InvariantCulture));

        }

        [Test]
        public void DoubleIncrementalGeneratorRequestedValue_ReturnsCorrectValue()
        {
            ValueIncrement increment = new ValueIncrement(0.0, 1.0, 0.25);
            IncrementalValueGenerator generator = new IncrementalValueGenerator(increment);

            double total = 0;
            for (int i = 0; i < 4; i++)
            {
                double generatedValue = (double)generator.GenerateValue(typeof(double), CultureInfo.InvariantCulture);
                Assert.AreEqual(total, generatedValue);
                total += 1d / 4;
            }
        }

        [Test]
        public void DoubleIncrementalGeneratorRequestedFriendlyName_ReturnsFullOrPartName()
        {
            ValueIncrement increment = new ValueIncrement(0.0, 1.0, 0.25);
            IncrementalValueGenerator generator = new IncrementalValueGenerator(increment);

            string fullName = generator.GetFriendlyName();
            Assert.True(fullName.Contains(0.0.ToString(CultureInfo.CurrentCulture)) &&
                        fullName.Contains(1.0.ToString(CultureInfo.CurrentCulture)) &&
                        fullName.Contains(0.25.ToString(CultureInfo.CurrentCulture)));

            const int charsToGet = 5;
            string partName = generator.GetFriendlyName(charsToGet);
            Assert.AreEqual(charsToGet, partName.Length);
        }

        [Test]
        public void DoubleIncrementalGeneratorRequestedValidCheck_ReturnsTrueIfValidAndFalseOtherwise()
        {
            ValueIncrement increment = new ValueIncrement(0.0, 1.0, 0.25);
            IncrementalValueGenerator generator = new IncrementalValueGenerator(increment);
            bool isValid = generator.IsValid(out string errMsg);

            Assert.True(isValid);
            Assert.True(string.IsNullOrEmpty(errMsg));

            increment = new ValueIncrement(1.0, 0.0, 0.25);
            generator = new IncrementalValueGenerator(increment);
            bool isValid2 = generator.IsValid(out string errMsg2);

            Assert.False(isValid2);
            Assert.Greater(errMsg2.Length, 0);

            increment = new ValueIncrement(0.0, 1.0, 2.0);
            generator = new IncrementalValueGenerator(increment);
            bool isValid3 = generator.IsValid(out string errMsg3);

            Assert.False(isValid3);
            Assert.Greater(errMsg3.Length, 0);
        }

        [Test]
        public void DoubleIncrementalGeneratorRequestedToGenerateValue_GeneratesValueIfSetupIsCorrectAndThrowsExceptionOtherwise()
        {
            ValueIncrement increment = new ValueIncrement(0.0, 1.0, 0.25);
            IncrementalValueGenerator generator = new IncrementalValueGenerator(increment);

            Assert.DoesNotThrow(() => generator.GenerateValue(typeof(int), CultureInfo.InvariantCulture));

            increment = new ValueIncrement(1.0, 0.0, 1.0);
            generator = new IncrementalValueGenerator(increment);
            Assert.Throws<ArgumentException>(() => generator.GenerateValue(typeof(int), CultureInfo.InvariantCulture));

            increment = new ValueIncrement(0.0, 1.0, 100.0);
            generator = new IncrementalValueGenerator(increment);
            Assert.Throws<ArgumentException>(() => generator.GenerateValue(typeof(int), CultureInfo.InvariantCulture));

            increment = new ValueIncrement(0.0, 1.0, 1.0);
            generator = new IncrementalValueGenerator(increment);
            generator.GenerateValue(typeof(int), CultureInfo.InvariantCulture);
            generator.GenerateValue(typeof(int), CultureInfo.InvariantCulture);
            Assert.Throws<ArithmeticException>(() => generator.GenerateValue(typeof(int), CultureInfo.InvariantCulture));
        }

        [Test]
        public void DoubleIncrementalGeneratorRequestStateReset_ResetsStateOfUnderlyingGenerator()
        {
            double startValue = 0.0;
            double increase = 0.05;
            ValueIncrement increment = new ValueIncrement(startValue, 1.0, increase);
            IncrementalValueGenerator generator = new IncrementalValueGenerator(increment);

            generator.GenerateValue(typeof(double), CultureInfo.InvariantCulture);
            double value = (double)generator.GenerateValue(typeof(double), CultureInfo.InvariantCulture);

            Assert.AreEqual(startValue + increase, value);

            generator.ResetGeneratorState();
            int valueAfterReset = (int)generator.GenerateValue(typeof(int), CultureInfo.InvariantCulture);

            Assert.AreEqual(startValue, valueAfterReset);
        }

        [Test]
        public void DoubleIncrementalGenerator_GeneratesCorrectValuesWithAnyBasicFloatingPointType()
        {
            float startValueF = 11.81f;
            float endValueF = 12.43f;
            float incrementValueF = 0.15f;

            ValueIncrement increment = new ValueIncrement(startValueF, endValueF, incrementValueF);
            IncrementalValueGenerator generator = new IncrementalValueGenerator(increment);

            Assert.AreEqual(startValueF, generator.GenerateValue(typeof(float), CultureInfo.InvariantCulture));
            Assert.AreEqual(startValueF + incrementValueF, generator.GenerateValue(typeof(float), CultureInfo.InvariantCulture));

            decimal startValueDm = 1.491985820193m;
            decimal endValueDm = 1.67491824837101m;
            decimal incrementValueDm = 0.0351534645m;

            increment = new ValueIncrement(startValueDm, endValueDm, incrementValueDm);
            generator = new IncrementalValueGenerator(increment);

            Assert.AreEqual(startValueDm, generator.GenerateValue(typeof(decimal), CultureInfo.InvariantCulture));
            Assert.AreEqual(startValueDm + incrementValueDm, generator.GenerateValue(typeof(decimal), CultureInfo.InvariantCulture));

            double startValueD = 15.04123d;
            double endValueD = 27.5910d;
            double incrementValueD = 1.523d;

            increment = new ValueIncrement(startValueD, endValueD, incrementValueD);
            generator = new IncrementalValueGenerator(increment);

            Assert.AreEqual(startValueD.ToString(CultureInfo.InvariantCulture), generator.GenerateValue(typeof(string), CultureInfo.InvariantCulture));
            Assert.AreEqual((startValueD + incrementValueD).ToString(CultureInfo.InvariantCulture), generator.GenerateValue(typeof(string), CultureInfo.InvariantCulture));
        }

        [Test]
        public void DatetimeIncrementalGeneratorRequestedValue_ReturnsCorrectValue()
        {
            TimeSpan increase = new TimeSpan(24, 0, 0);
            DateTime startDateTime = new DateTime(2000, 01, 01);
            ValueIncrement increment = new ValueIncrement(startDateTime, new DateTime(2001, 01, 01), increase);
            IncrementalValueGenerator generator = new IncrementalValueGenerator(increment);

            DateTime total = new DateTime(2000, 01, 01);
            for (int i = 0; i < 365; i++)
            {
                DateTime generatedValue = (DateTime)generator.GenerateValue(typeof(DateTime), CultureInfo.InvariantCulture);
                Assert.AreEqual(total, generatedValue);
                total += increase;
            }

            generator.ResetGeneratorState();
            string generatedValueAsString = (string)generator.GenerateValue(typeof(string), CultureInfo.InvariantCulture);

            Assert.AreEqual(startDateTime.ToString(CultureInfo.InvariantCulture), generatedValueAsString);
        }

        [Test]
        public void DatetimeIncrementalGeneratorRequestedFriendlyName_ReturnsFullOrPartName()
        {
            TimeSpan increase = new TimeSpan(24, 0, 0);
            DateTime startDate = new DateTime(2000, 01, 01);
            DateTime endDate = new DateTime(2001, 01, 01);
            ValueIncrement increment = new ValueIncrement(startDate, endDate, increase);
            IncrementalValueGenerator generator = new IncrementalValueGenerator(increment);

            string fullName = generator.GetFriendlyName();
            Assert.True(fullName.Contains(startDate.ToString(CultureInfo.CurrentCulture)) &&
                        fullName.Contains(startDate.ToString(CultureInfo.CurrentCulture)) &&
                        fullName.Contains(startDate.ToString(CultureInfo.CurrentCulture)));

            const int charsToGet = 5;
            string partName = generator.GetFriendlyName(charsToGet);
            Assert.AreEqual(charsToGet, partName.Length);
        }

        [Test]
        public void DatetimeIncrementalGeneratorRequestedValidCheck_ReturnsTrueIfValidAndFalseOtherwise()
        {
            ValueIncrement increment = new ValueIncrement(new DateTime(2000, 01, 01), new DateTime(2001, 01, 01), new TimeSpan(24, 0, 0));
            IncrementalValueGenerator generator = new IncrementalValueGenerator(increment);
            bool isValid = generator.IsValid(out string errMsg);

            Assert.True(isValid);
            Assert.True(string.IsNullOrEmpty(errMsg));

            increment = new ValueIncrement(new DateTime(2001, 01, 01), new DateTime(2000, 01, 01), new TimeSpan(24, 0, 0));
            generator = new IncrementalValueGenerator(increment);
            bool isValid2 = generator.IsValid(out string errMsg2);

            Assert.False(isValid2);
            Assert.Greater(errMsg2.Length, 0);

            increment = new ValueIncrement(new DateTime(2000, 01, 01), new DateTime(2001, 01, 01), new TimeSpan(400, 0, 0, 0));
            generator = new IncrementalValueGenerator(increment);
            bool isValid3 = generator.IsValid(out string errMsg3);

            Assert.False(isValid3);
            Assert.Greater(errMsg3.Length, 0);
        }

        [Test]
        public void DatetimeIncrementalGeneratorRequestedToGenerateValue_GeneratesValueIfSetupIsCorrectAndThrowsExceptionOtherwise()
        {
            ValueIncrement increment = new ValueIncrement(new DateTime(2000, 01, 01), new DateTime(2001, 01, 01), new TimeSpan(24, 0, 0));
            IncrementalValueGenerator generator = new IncrementalValueGenerator(increment);

            Assert.DoesNotThrow(() => generator.GenerateValue(typeof(DateTime), CultureInfo.InvariantCulture));

            increment = new ValueIncrement(new DateTime(2001, 01, 01), new DateTime(2000, 01, 01), new TimeSpan(24, 0, 0));
            generator = new IncrementalValueGenerator(increment);
            Assert.Throws<ArgumentException>(() => generator.GenerateValue(typeof(DateTime), CultureInfo.InvariantCulture));

            increment = new ValueIncrement(new DateTime(2000, 01, 01), new DateTime(2001, 01, 01), new TimeSpan(400, 0, 0, 0));
            generator = new IncrementalValueGenerator(increment);
            Assert.Throws<ArgumentException>(() => generator.GenerateValue(typeof(DateTime), CultureInfo.InvariantCulture));

            increment = new ValueIncrement(new DateTime(2000, 01, 01), new DateTime(2001, 01, 01), new TimeSpan(300, 0, 0, 0));
            generator = new IncrementalValueGenerator(increment);
            generator.GenerateValue(typeof(DateTime), CultureInfo.InvariantCulture);
            generator.GenerateValue(typeof(DateTime), CultureInfo.InvariantCulture);
            Assert.Throws<ArithmeticException>(() => generator.GenerateValue(typeof(DateTime), CultureInfo.InvariantCulture));
        }

        [Test]
        public void DatetimeIncrementalGeneratorRequestStateReset_ResetsStateOfUnderlyingGenerator()
        {
            DateTime startValue = new DateTime(2000, 01, 01);
            TimeSpan increase = new TimeSpan(24, 0, 0);
            ValueIncrement increment = new ValueIncrement(startValue, new DateTime(2001, 01, 01), increase);
            IncrementalValueGenerator generator = new IncrementalValueGenerator(increment);

            generator.GenerateValue(typeof(DateTime), CultureInfo.InvariantCulture);
            DateTime value = (DateTime)generator.GenerateValue(typeof(DateTime), CultureInfo.InvariantCulture);

            Assert.AreEqual(startValue + increase, value);

            generator.ResetGeneratorState();
            DateTime valueAfterReset = (DateTime)generator.GenerateValue(typeof(DateTime), CultureInfo.InvariantCulture);

            Assert.AreEqual(startValue, valueAfterReset);
        }

        [Test]
        public void IncrementalGeneratorUsesNullValues_SuccessfullyReturnsThem()
        {
            NullValuesSettings nullSettings = new NullValuesSettings()
            {
                GenerateNullValues = true,
                AverageRowsBeforeNullAppearance = 2
            };

            const int generatorIterations = 50;

            uint minInt = 1;
            uint maxInt = 100;
            uint stepInt = 1;
            IncrementalValueGenerator generatorInt = new IncrementalValueGenerator(new ValueIncrement(minInt, maxInt, stepInt), nullSettings);

            float minF = 1f;
            float maxF = 10f;
            float stepF = 0.1f;
            IncrementalValueGenerator generatorFloat = new IncrementalValueGenerator(new ValueIncrement(minF, maxF, stepF), nullSettings);

            DateTime minDt = new DateTime(2000, 1, 1);
            DateTime maxDt = new DateTime(2020, 1, 1);
            TimeSpan stepDt = new TimeSpan(24, 0, 0);
            IncrementalValueGenerator generatorDt = new IncrementalValueGenerator(new ValueIncrement(minDt, maxDt, stepDt), nullSettings);

            int nullIntCounter = 0;
            int nullFloatCounter = 0;
            int nullDateTimeCounter = 0;
            for (int i = 0; i < generatorIterations; i++)
            {
                uint? generatedIntValue = (uint?)generatorInt.GenerateValue(typeof(uint?), CultureInfo.InvariantCulture);
                if (generatedIntValue == null)
                    nullIntCounter++;

                float? generatedFloatValue = (float?)generatorFloat.GenerateValue(typeof(float?), CultureInfo.InvariantCulture);
                if (generatedFloatValue == null)
                    nullFloatCounter++;

                DateTime? generatedDateTimeValue = (DateTime?)generatorDt.GenerateValue(typeof(DateTime?), CultureInfo.InvariantCulture);
                if (generatedDateTimeValue == null)
                    nullDateTimeCounter++;

                Assert.True((generatedIntValue != null && (generatedIntValue >= minInt && generatedIntValue <= maxInt)) || generatedIntValue == null);
                Assert.True((generatedFloatValue != null && (generatedFloatValue >= minF && generatedFloatValue <= maxF)) || generatedFloatValue == null);
                Assert.True((generatedDateTimeValue != null && (generatedDateTimeValue >= minDt && generatedDateTimeValue <= maxDt)) || generatedDateTimeValue == null);
            }

            Assert.Greater(nullIntCounter, 0);
            Assert.Greater(nullFloatCounter, 0);
            Assert.Greater(nullDateTimeCounter, 0);
        }
    }
}
