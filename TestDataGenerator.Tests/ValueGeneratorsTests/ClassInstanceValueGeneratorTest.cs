﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Settings;
using TestDataGenerator.Core.ValueGenerators;
using TestDataGenerator.Core.ValueGenerators.Providers;

namespace TestDataGenerator.Tests.ValueGeneratorsTests
{
    public class ClassInstanceValueGeneratorTest
    {
        class AnotherComplexClass
        {
            public string Test { get; set; }
        }

        class ComplexClass
        {
            public byte ByteProp { get; set; }
            public string StringProp { get; set; }
            public float? FloatProp { get; set; }
            public decimal DecimalProp { get; set; }
            public bool BoolProp { get; set; }
            public int IntProp { get; set; }
            public uint UIntProp { get; set; }
            public DateTime? DateTimeProp { get; set; }
            public AnotherComplexClass AnotherComplexProp { get; set; }
        }

        class ExampleClass
        {
            public byte ByteProp { get; set; }
            public string StringProp { get; set; }
            public float? FloatProp { get; set; }
            public decimal DecimalProp { get; set; }
            public bool BoolProp { get; set; }
            public int? IntProp { get; set; }
            public DateTime? DateTimeProp { get; set; }
            public ComplexClass ComplexProp { get; set; }
            public Point StructProp { get; set; }
        }

        const int RAND_ITER_NUMBER = 1000;

        [Test]
        public void InstanceGenerator_SuccessfullyGeneratesClassInstanceUsingInstanceRandomizer()
        {
            DefaultClassInstanceRandomizer<ExampleClass> randomizer = new DefaultClassInstanceRandomizer<ExampleClass>();
            ClassInstanceValueGenerator<ExampleClass> generator = new ClassInstanceValueGenerator<ExampleClass>(randomizer);

            ExampleClass generatedResult = (ExampleClass)generator.GenerateValue(typeof(ExampleClass), CultureInfo.InvariantCulture);

            Assert.NotNull(generatedResult);
        }
        
        [Test]
        public void InstanceGeneratorRequestedFriendlyName_ReturnsEitherFullOrShortenedName()
        {
            DefaultClassInstanceRandomizer<ExampleClass> randomizer = new DefaultClassInstanceRandomizer<ExampleClass>();
            ClassInstanceValueGenerator<ExampleClass> generator = new ClassInstanceValueGenerator<ExampleClass>(randomizer);

            string fullName = generator.GetFriendlyName();

            Assert.True(fullName.Contains(nameof(ExampleClass)));

            const int charsToSelect = 5;
            string partialName = generator.GetFriendlyName(charsToSelect);

            Assert.AreEqual(charsToSelect, partialName.Length);
        }

        [Test]
        public void InstanceGeneratorOnValidityCheck_ReturnsTrueIfSetupIsValidAndFalseOtherwise()
        {
            ClassInstanceValueGenerator<ExampleClass> generator = new ClassInstanceValueGenerator<ExampleClass>(null);

            bool isValid = generator.IsValid(out string errMsg);

            Assert.False(isValid);
            Assert.True(errMsg != null && errMsg.Length > 0);

            DefaultClassInstanceRandomizer<ExampleClass> randomizer = new DefaultClassInstanceRandomizer<ExampleClass>();
            generator = new ClassInstanceValueGenerator<ExampleClass>(randomizer);
            isValid = generator.IsValid(out string errMsg2);

            Assert.True(isValid);
            Assert.True(errMsg2 == null || errMsg2.Length == 0);
        }

        [Test]
        public void InstanceGeneratorUsesNullValues_CorrectlyGeneratesThem()
        {
            DefaultClassInstanceRandomizer<ExampleClass> randomizer = new DefaultClassInstanceRandomizer<ExampleClass>();
            ClassInstanceValueGenerator<ExampleClass> generator = new ClassInstanceValueGenerator<ExampleClass>(randomizer, 
                new NullValuesSettings() { 
                    GenerateNullValues = true,
                    AverageRowsBeforeNullAppearance = 3
                });

            int nullValuesCount = 0;
            for (int i = 0; i < RAND_ITER_NUMBER; i++)
            {
                ExampleClass generatedValue = (ExampleClass)generator.GenerateValue(typeof(ExampleClass), CultureInfo.InvariantCulture);
                if (generatedValue == null)
                    nullValuesCount++;
            }

            Assert.Greater(nullValuesCount, 0);
        }
    }
}
