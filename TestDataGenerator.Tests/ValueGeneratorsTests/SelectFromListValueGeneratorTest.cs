﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Settings;
using TestDataGenerator.Core;
using TestDataGenerator.Core.ValueGenerators;

namespace TestDataGenerator.Tests.ValueGeneratorsTests
{
    public class SelectFromListValueGeneratorTest
    {
        CultureInfo _culture = CultureInfo.InvariantCulture;
        const int RANDOM_LOOPS_NUMBER = 1000;

        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void SelectFromListGeneratesValueOfCorrectType_GetRandomValueFromList()
        {
            int[] arr = new int[] { 1, 2, 3 };
            SelectFromListValueGenerator generator = new SelectFromListValueGenerator(arr.Cast<object>());

            for (int i = 0; i < RANDOM_LOOPS_NUMBER; i++)
            {
                var res = (int)generator.GenerateValue(typeof(int), _culture);

                Assert.LessOrEqual(res, 3);

                var res2 = generator.GenerateValue(typeof(string), _culture);

                Assert.Contains(res2, arr.Select(x => x.ToString()).ToArray());
            }
        }

        [Test]
        public void SelectFromListGeneratesValueOfIncorrectType_FailsWithException()
        {
            bool[] arr = new bool[] { false, true, false };
            SelectFromListValueGenerator generator = new SelectFromListValueGenerator(arr.Cast<object>());

            Assert.Throws<NotSupportedException>(() => generator.GenerateValue(typeof(DateTime), _culture));
        }

        [Test]
        public void GeneratorPrintsFriendlyName_IncludesLengthOfArrayOfElements()
        {
            int[] arr = new int[] { 1, 2, 3 };
            SelectFromListValueGenerator generator = new SelectFromListValueGenerator(arr.Cast<object>());

            string name = generator.GetFriendlyName();
            Assert.True(name.Contains(arr.Length.ToString()));

            int charsToOutput = 5;
            string partialName = generator.GetFriendlyName(charsToOutput);
            Assert.AreEqual(charsToOutput, partialName.Length);
        }

        [Test]
        public void OnGeneratorValidation_ReturnsFalseAndErrTextIfHasErrors()
        {
            int[] arr = new int[] { };
            SelectFromListValueGenerator generator = new SelectFromListValueGenerator(arr.Cast<object>());

            bool isValid = generator.IsValid(out string err);
            Assert.False(isValid);
            Assert.True(err.Length > 0);
        }

        [Test]
        public void OnGeneratorValidation_ReturnsTrueIfGeneratorParamsAreValid()
        {
            int[] arr = new int[] { 1, 2, 3 };
            SelectFromListValueGenerator generator = new SelectFromListValueGenerator(arr.Cast<object>());

            bool isValid = generator.IsValid(out string err);
            Assert.True(isValid);
            Assert.True(err == null || err == "");
        }

        [Test]
        public void GeneratorWithNullValuesInArray_SuccessfullyReturnsThem()
        {
            int?[] arr = new int?[] { 1, 2, 3 };
            NullValuesSettings nullValueSettings = new NullValuesSettings()
            {
                GenerateNullValues = true,
                AverageRowsBeforeNullAppearance = 10
            };

            SelectFromListValueGenerator generator = new SelectFromListValueGenerator(arr.Cast<object>(), nullValueSettings);

            int nullCounter = 0;
            for (int i = 0; i < RANDOM_LOOPS_NUMBER; i++)
            {
                int? generatedValue = (int?)generator.GenerateValue(typeof(int?), CultureInfo.InvariantCulture);
                if (generatedValue == null)
                    nullCounter++;
                Assert.True(generatedValue == null || arr.Contains(generatedValue));
            }

            Assert.Greater(nullCounter, 0);

            TestContext.Out.WriteLine($"Null values generated: {nullCounter}; " +
                $"Expected approx. number " +
                $"{RANDOM_LOOPS_NUMBER}/{nullValueSettings.AverageRowsBeforeNullAppearance}={(double)RANDOM_LOOPS_NUMBER / nullValueSettings.AverageRowsBeforeNullAppearance}");
        }
    }
}
