using NUnit.Framework;
using System;
using System.Globalization;
using TestDataGenerator.Abstractions.Settings;
using TestDataGenerator.Core.ValueGenerators.ValuePatternParser;

namespace TestDataGenerator.Tests.ValueGeneratorsTests
{
    public class ValuePatternParserTest
    {
        CultureInfo _culture = CultureInfo.InvariantCulture;
        const int RANDOM_LOOPS_NUMBER = 1000;

        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void ParserReceivesCorrectPattern_ParsesAndReturnsTrue()
        {
            ValuePatternParser parser = new ValuePatternParser();
            bool result = parser.ValidateAndSetPattern("[A-z,1-5,9][�-�]");
            Assert.IsTrue(result);

            bool result2 = parser.ValidateAndSetPattern("[A-z]{3-5}[�-�]{2}");
            Assert.IsTrue(result2);
        }

        [Test]
        public void ParserReceivesIncorrectPattern_TriesToParseAndReturnsFalse()
        {
            ValuePatternParser parser = new ValuePatternParser();

            bool result = parser.ValidateAndSetPattern("[aa-z]");

            Assert.IsFalse(result);
            Assert.IsNotNull(parser.PatternErrorText);

            bool result2 = parser.ValidateAndSetPattern("[A-z]{}");

            Assert.IsFalse(result2);
            Assert.IsNotNull(parser.PatternErrorText);
        }

        [Test]
        public void ParserGeneratesValueWithCorrectPattern_ReturnsDesiredValue()
        {
            ValuePatternParser parser = new ValuePatternParser();

            parser.ValidateAndSetPattern("[G][1]");
            var result = parser.GenerateValue(typeof(string), _culture);

            Assert.AreEqual("G1", result);

            parser.ValidateAndSetPattern("[A-z]{4}[5]");
            var result2 = parser.GenerateValue(typeof(string), _culture).ToString();
            var result3 = parser.GenerateValue(typeof(string), _culture).ToString();

            Assert.AreEqual(5, result2.Length);
            Assert.AreEqual(5, result3.Length);
            Assert.IsTrue(result2.EndsWith("5"));
            Assert.IsTrue(result3.EndsWith("5"));
            Assert.AreNotEqual(result3, result2);

            parser.ValidateAndSetPattern("[0-9]{6}");
            var result4 = (int)parser.GenerateValue(typeof(int), _culture);

            Assert.LessOrEqual(result4, 999999);
        }
        
        [Test]
        public void ParserTriesToGenerateValueWithIncorrectPattern_FailsWithException()
        {
            ValuePatternParser parser = new ValuePatternParser();

            parser.ValidateAndSetPattern("[G-a-a-][1]");
            Assert.Throws(typeof(ArgumentException), () => parser.GenerateValue(typeof(string), _culture));

            parser.ValidateAndSetPattern("[a");
            Assert.Throws(typeof(ArgumentException), () => parser.GenerateValue(typeof(string), _culture));

            parser.ValidateAndSetPattern("[a][b");
            Assert.Throws(typeof(ArgumentException), () => parser.GenerateValue(typeof(string), _culture));

            parser.ValidateAndSetPattern("[a]{1-3");
            Assert.Throws(typeof(ArgumentException), () => parser.GenerateValue(typeof(string), _culture));

            parser.ValidateAndSetPattern("[a]}1-3");
            Assert.Throws(typeof(ArgumentException), () => parser.GenerateValue(typeof(string), _culture));

            parser.ValidateAndSetPattern("a]1-3}");
            Assert.Throws(typeof(ArgumentException), () => parser.GenerateValue(typeof(string), _culture));

            parser.ValidateAndSetPattern("[a-Z]");
            Assert.Throws(typeof(ArgumentException), () => parser.GenerateValue(typeof(string), _culture));

            parser.ValidateAndSetPattern("[aZ]");
            Assert.Throws(typeof(ArgumentException), () => parser.GenerateValue(typeof(string), _culture));

            parser.ValidateAndSetPattern("[a]{1.2-2}");
            Assert.Throws(typeof(ArgumentException), () => parser.GenerateValue(typeof(string), _culture));

            parser.ValidateAndSetPattern("[a]{1--2}");
            Assert.Throws(typeof(ArgumentException), () => parser.GenerateValue(typeof(string), _culture));
        }

        [Test]
        public void ParserTriesToProduceValueWithoutValuePatternSet_FailsWithException()
        {
            ValuePatternParser parser = new ValuePatternParser();
            parser.PatternIsValid = true;

            Assert.Throws<ArgumentNullException>(() => parser.GenerateValue(typeof(int), _culture));
        }

        [Test]
        public void ParserPrintsFriendlyName_NameIncludesPattern()
        {
            string pattern = "[A-z]";
            ValuePatternParser parser = new ValuePatternParser(pattern);

            string fullName = parser.GetFriendlyName();
            Assert.True(fullName.Contains(pattern));

            int charsToOutput = 5;
            string partialName = parser.GetFriendlyName(charsToOutput);
            Assert.AreEqual(charsToOutput, partialName.Length);
        }

        [Test]
        public void ParserUsesNullValues_GeneratesThemCorrectly()
        {
            string pattern = "[A-z]{6}";
            NullValuesSettings nullValueSettings = new NullValuesSettings() { 
                GenerateNullValues = true,
                AverageRowsBeforeNullAppearance = 10
            };

            ValuePatternParser parser = new ValuePatternParser(pattern, nullValueSettings);

            int nullCounter = 0;
            for (int i = 0; i < RANDOM_LOOPS_NUMBER; i++)
            {
                string generatedValue = (string)parser.GenerateValue(typeof(string), CultureInfo.InvariantCulture);
                if (generatedValue == null)
                    nullCounter++;

                Assert.True((generatedValue != null && generatedValue.Length == 6) || generatedValue == null);
            }

            Assert.Greater(nullCounter, 0);

            TestContext.Out.WriteLine($"Null values generated: {nullCounter}; " +
                $"Expected approx. number " +
                $"{RANDOM_LOOPS_NUMBER}/{nullValueSettings.AverageRowsBeforeNullAppearance}={(double)RANDOM_LOOPS_NUMBER / nullValueSettings.AverageRowsBeforeNullAppearance}");
        }
    }
}