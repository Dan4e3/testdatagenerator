﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces.Rows;
using TestDataGenerator.Core;
using TestDataGenerator.Core.Columns;
using TestDataGenerator.Core.ValueGenerators.IncrementalGenerator;
using TestDataGenerator.Core.ValueGenerators.ValuePatternParser;
using TestDataGenerator.Core.ValueGenerators.ValueRangeGenerator;

namespace TestDataGenerator.Tests
{
    public class TestDatasetTest
    {
        [Test]
        public void DatasetWithIncorrectValueGenerators_ReturnsFalseOnReadyCheck()
        {
            List<Column> colsList = new List<Column>() {
                new Column(new ColumnSettings() { 
                    ColumnName = "Col1",
                    Culture = CultureInfo.InvariantCulture,
                    TargetValueType = typeof(int),
                    ValueSupplier = new ValuePatternParser("[1-99]")
                }),
                new Column(new ColumnSettings() {
                    ColumnName = "Col2",
                    Culture = CultureInfo.InvariantCulture,
                    TargetValueType = typeof(double),
                    ValueSupplier = new ValueRangeValueGenerator(new ValueRange(10.15, 1.25))
                }),
            };

            ColumnSet colset = new ColumnSet(colsList);
            TestDataSet dset = new TestDataSet("test", colset);

            bool isDsetReady = dset.CanGenerateDataSet(out string error);

            Assert.IsFalse(isDsetReady);
            Assert.NotNull(error);
            Assert.Greater(error.Length, 0);

            TestDataSet dset2 = new TestDataSet("test2", new ColumnSet());
            bool isDset2Ready = dset2.CanGenerateDataSet(out string error2);

            Assert.IsFalse(isDset2Ready);
            Assert.NotNull(error2);
            Assert.Greater(error2.Length, 0);
        }

        [Test]
        public void DatasetWithCorrectValueGenerators_ReturnsTrueOnReadyCheck()
        {
            TestDataSet dataset = new TestDataSet("test", new ColumnSet(new Column[] { 
                new Column(new ColumnSettings(){ 
                    ColumnName = "Col1",
                    Culture = CultureInfo.InvariantCulture,
                    TargetValueType = typeof(double),
                    ValueSupplier = new ValuePatternParser("[0][.][0-9]{1-3}")
                })
            }));

            bool isReady = dataset.CanGenerateDataSet(out string err);

            Assert.True(isReady);
            Assert.True(err == null || err == "");
        }

        [Test]
        public void DatasetNameCorrectlySetAndReturned()
        {
            string name = "test";
            TestDataSet dset = new TestDataSet(name, new ColumnSet());

            Assert.AreEqual(name, dset.GetDataSetName());
        }

        [Test]
        public void DatasetHasResettableColumn_CheckIfActualStateResetHappens()
        {
            TestDataSet dataset = new TestDataSet("test", new ColumnSet(new Column[] {
                new Column(new ColumnSettings(){
                    ColumnName = "Col1",
                    Culture = CultureInfo.InvariantCulture,
                    TargetValueType = typeof(int),
                    ValueSupplier = new IncrementalValueGenerator(new ValueIncrement(0, 100, 1))
                })
            }));

            var rows = dataset.GenerateNewDataSet(5);

            Assert.AreEqual(4, rows.Max(x => (int)x[0]));

            var anotherSetOfRows = dataset.GenerateNewDataSet(10);

            Assert.AreEqual(9, anotherSetOfRows.Max(x => (int)x[0]));
        }

        [Test]
        public void DatasetRequestedToGenerateSingleRow_SuccessfullyGeneratesAndReturnsIt()
        {
            List<Column> colsList = new List<Column>() {
                new Column(new ColumnSettings() {
                    ColumnName = "Col1",
                    Culture = CultureInfo.InvariantCulture,
                    TargetValueType = typeof(int),
                    ValueSupplier = new ValuePatternParser("[1-9]{2}")
                }),
                new Column(new ColumnSettings() {
                    ColumnName = "Col2",
                    Culture = CultureInfo.InvariantCulture,
                    TargetValueType = typeof(double),
                    ValueSupplier = new ValueRangeValueGenerator(new ValueRange(1.5, 5.1))
                }),
            };

            ColumnSet colset = new ColumnSet(colsList);
            TestDataSet dset = new TestDataSet("test", colset);

            IRow generatedRow = dset.GenerateSingleRow();

            Assert.NotNull(generatedRow);
            Assert.LessOrEqual((int)generatedRow[0], 100);
            Assert.True((double)generatedRow[1] >= 1.5 && (double)generatedRow[1] <= 5.1);
        }
    }
}
