﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Core.ValueGenerators.Providers;
using TestDataGenerator.Core.ValueGenerators.Providers.Common;

namespace TestDataGenerator.Tests.ValueGeneratorProvidersTests
{
    public class DefaultValueGeneratorProviderTest
    {
        class TestClassObject
        {

        }

        [Test]
        public void ProviderIsRequestedForValueGenerator_ThrowsExceptionIfNoRegisteredFound()
        {
            DefaultValueGeneratorProvider provider = new DefaultValueGeneratorProvider();

            Assert.Throws<ArgumentException>(() => provider.GetByTargetValueType(typeof(TestClassObject)));
            Assert.Throws<ArgumentException>(() => provider.GetByTargetValueTypeAndGeneratorType(typeof(TestClassObject), ValueGeneratorTypeEnum.Range));

            Assert.True(provider.ValueGeneratorIsRegistered(typeof(string)));
            Assert.True(provider.ValueGeneratorIsRegistered(typeof(int?)));
            Assert.False(provider.ValueGeneratorIsRegistered(typeof(TestClassObject)));

            Assert.NotNull(provider.GetByTargetValueType(typeof(short?)));
            Assert.NotNull(provider.GetByTargetValueTypeAndGeneratorType(typeof(short?), ValueGeneratorTypeEnum.Range));
        }
    }
}
