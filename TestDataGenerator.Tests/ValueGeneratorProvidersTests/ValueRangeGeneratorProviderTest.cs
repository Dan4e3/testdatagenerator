﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Core.ValueGenerators;
using TestDataGenerator.Core.ValueGenerators.Providers;
using TestDataGenerator.Core.ValueGenerators.ValueRangeGenerator;

namespace TestDataGenerator.Tests.ValueGeneratorProvidersTests
{
    public class ValueRangeGeneratorProviderTest
    {
        class SomeArbitraryClass
        {

        }

        [Test]
        public void ProviderHasRegisteredTargetType_ReturnsTrueOnCheck()
        {
            ValueRangeGeneratorProvider provider = new ValueRangeGeneratorProvider(new ValueRange(1,2));

            bool isIntTypeRegistered = provider.ValueGeneratorIsRegistered(typeof(int));
            Assert.True(isIntTypeRegistered == ValueRangeGeneratorProvider.RegisteredValueRangeGenerators.ContainsKey(typeof(int)));
            Assert.True(
                ValueRangeGeneratorProvider.ValueGeneratorExists(typeof(int)) 
                == ValueRangeGeneratorProvider.RegisteredValueRangeGenerators.ContainsKey(typeof(int))
                );
        }

        [Test]
        public void ProviderDoesntHaveRegisteredType_ReturnsFalseOnCheck()
        {
            ValueRangeGeneratorProvider provider = new ValueRangeGeneratorProvider(new ValueRange(1, 2));

            bool isArbitraryTypeRegistered = provider.ValueGeneratorIsRegistered(typeof(SomeArbitraryClass));
            Assert.True(isArbitraryTypeRegistered == ValueRangeGeneratorProvider.RegisteredValueRangeGenerators.ContainsKey(typeof(SomeArbitraryClass)));
            Assert.True(
                ValueRangeGeneratorProvider.ValueGeneratorExists(typeof(SomeArbitraryClass)) 
                == ValueRangeGeneratorProvider.RegisteredValueRangeGenerators.ContainsKey(typeof(SomeArbitraryClass))
                );
        }
    }
}
