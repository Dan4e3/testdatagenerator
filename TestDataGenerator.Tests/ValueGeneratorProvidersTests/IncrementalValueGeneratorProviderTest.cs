﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Core.ValueGenerators.IncrementalGenerator;
using TestDataGenerator.Core.ValueGenerators.Providers;

namespace TestDataGenerator.Tests.ValueGeneratorProvidersTests
{
    public class IncrementalValueGeneratorProviderTest
    {
        private class SomeClass
        {

        }

        [Test]
        public void GeneratorProviderCheckIfValueGeneratorRegistered_ReturnsTrueIfYesOtherwiseFalse()
        {
            ValueIncrement increment = new ValueIncrement(1, 100, 1);
            IncrementalValueGeneratorProvider provider = new IncrementalValueGeneratorProvider(increment);

            Assert.True(provider.ValueGeneratorIsRegistered(increment.TargetType));
            Assert.True(IncrementalValueGeneratorProvider.ValueGeneratorExists(increment.TargetType));

            Assert.False(provider.ValueGeneratorIsRegistered(typeof(SomeClass)));
            Assert.False(IncrementalValueGeneratorProvider.ValueGeneratorExists(typeof(SomeClass)));
        }

        [Test]
        public void GeneratorProviderRequestedGenerator_ReturnsRegisteredGeneratorOrThrowsExceptionIfNoSuchRegistered()
        {
            ValueIncrement increment = new ValueIncrement(1, 100, 1);
            IncrementalValueGeneratorProvider provider = new IncrementalValueGeneratorProvider(increment);

            Assert.AreEqual(typeof(IntegerIncrementalValueGenerator<int>), provider.GetByTargetValueType(typeof(int)).GetType());

            Assert.Throws<ArgumentException>(() => provider.GetByTargetValueType(typeof(SomeClass)));
        }
    }
}
