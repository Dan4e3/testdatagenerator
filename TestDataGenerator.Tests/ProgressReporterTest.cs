﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Core;

namespace TestDataGenerator.Tests
{
    public class ProgressReporterTest
    {
        [Test]
        public void ProgressReporter_ReportsWithStatusTextAndPercent()
        {
            int percents = 0;
            string status = "empty";

            Action<int> actPercents = (pct) => percents = pct;
            Action<string, int> actStatusAndPercents = (stat, pct) => {
                status = stat;
                percents = pct;
            };

            ProgressReporter reporter = new ProgressReporter(actPercents, actStatusAndPercents);

            reporter.ReportProgress(100);
            Assert.AreEqual(100, percents);

            reporter.ReportProgress("test", 1000);
            Assert.AreEqual("test", status);
            Assert.AreEqual(1000, percents);
        }
    }
}
