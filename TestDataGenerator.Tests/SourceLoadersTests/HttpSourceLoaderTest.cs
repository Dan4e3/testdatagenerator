﻿using Newtonsoft.Json;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.IO.SourceLoaders;

namespace TestDataGenerator.Tests.SourceLoadersTests
{
    public class HttpSourceLoaderTest
    {
        [Test]
        public async Task HttpSourceLoader_LoadsDataFromHttpEndpoint()
        {
            object[] testPayload = new object[] { "test", 1, false, 598.21 };

            using (HttpSourceLoader loader = new HttpSourceLoader(new string[] { "$..json", "$..non_existent_token"}))
            {
                var requestMsg = new HttpRequestMessage()
                {
                    Content = new StringContent(JsonConvert.SerializeObject(testPayload), Encoding.UTF8, "application/json"),
                    Method = HttpMethod.Get,
                    RequestUri = new Uri("https://httpbin.org/anything")
                };

                object[] res = await loader.LoadDataFromHttpSourceAsync(requestMsg);

                CollectionAssert.AreEqual(testPayload, res);
            }
        }
    }
}
