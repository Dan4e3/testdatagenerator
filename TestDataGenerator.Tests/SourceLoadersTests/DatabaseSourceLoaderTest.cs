﻿using Moq;
using Npgsql;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces.ExternalDataLoaders;
using TestDataGenerator.IO;
using TestDataGenerator.IO.SourceLoaders;

namespace TestDataGenerator.Tests.SourceLoadersTests
{
    public class DatabaseSourceLoaderTest
    {
        [Test]
        public async Task DatabaseLoader_LoadsDesiredColumnUsingTablenameAndColumnName()
        {
            string sqlServerConnString = "Server=PREDATOR-HELIOS;Database=TestDb;Integrated Security=true;MultipleActiveResultSets=true;";
            string postgresqlConnString = "User ID=TstUser;Password=1234567890;Host=localhost;Port=5432;Database=TestDb";
            string tableNameSqlServer = "[dbo].[Table_4]";
            string columnNameSqlServer = "Col1";
            string tableNamePostgres = "table_3";
            string columnNamePostgres = "col_1";
            string[] expectedResult = new string[] {"test", "anotherone", "152", "19.04", "simple value" };


            if (Environment.GetEnvironmentVariable("TESTING_ENVIRONMENT") == "GITLAB")
            {
                var loaderMock = new Mock<DatabaseSourceLoader>(sqlServerConnString, SqlProvidersEnum.SqlServer);
                loaderMock.SetupGet(x => x.DbConnection).Returns(new FakeDbConnection());

                object[] result = loaderMock.Object.LoadDataFromDatabaseSource("fakeTable", "fakeColumn");
                CollectionAssert.AreEqual(FakeDbDataReader.TestValues, result);
                loaderMock.Object.Dispose();

                var loaderMock2 = new Mock<DatabaseSourceLoader>(postgresqlConnString, SqlProvidersEnum.PostgreSQL);
                loaderMock2.SetupGet(x => x.DbConnection).Returns(new FakeDbConnection());

                object[] result2 = loaderMock2.Object.LoadDataFromDatabaseSource("some sort of sql query");
                CollectionAssert.AreEqual(FakeDbDataReader.TestValues, result2);
            }
            else
            {
                using (SqlConnection conn = new SqlConnection(sqlServerConnString))
                {
                    await conn.OpenAsync();
                    var truncateCmd = conn.CreateCommand();
                    truncateCmd.CommandText = $"TRUNCATE TABLE {tableNameSqlServer}";
                    await truncateCmd.ExecuteNonQueryAsync();

                    var insertDataCommand = conn.CreateCommand();
                    insertDataCommand.CommandText = $"INSERT INTO {tableNameSqlServer} VALUES {string.Join(",", expectedResult.Select(v => $"('{v}')"))}";
                    await insertDataCommand.ExecuteNonQueryAsync();

                    DatabaseSourceLoader sqlServerLoader = new DatabaseSourceLoader(sqlServerConnString, SqlProvidersEnum.SqlServer);
                    
                    object[] result = sqlServerLoader.LoadDataFromDatabaseSource(tableNameSqlServer, columnNameSqlServer);
                    CollectionAssert.AreEqual(expectedResult, result);
                    sqlServerLoader.Dispose();
                    

                }

                using (NpgsqlConnection postgresConn = new NpgsqlConnection(postgresqlConnString))
                {
                    await postgresConn.OpenAsync();
                    var truncateCmd = postgresConn.CreateCommand();
                    truncateCmd.CommandText = $"TRUNCATE TABLE {tableNamePostgres}";
                    await truncateCmd.ExecuteNonQueryAsync();

                    var insertDataCommand = postgresConn.CreateCommand();
                    insertDataCommand.CommandText = $"INSERT INTO {tableNamePostgres}({columnNamePostgres}) VALUES {string.Join(",", expectedResult.Select(v => $"('{v}')"))}";
                    await insertDataCommand.ExecuteNonQueryAsync();

                    using (DatabaseSourceLoader postgresqlLoader = new DatabaseSourceLoader(postgresqlConnString, SqlProvidersEnum.PostgreSQL))
                    {
                        string sqlQuery = $"SELECT {columnNamePostgres} FROM {tableNamePostgres}";
                        object[] result2 = postgresqlLoader.LoadDataFromDatabaseSource(sqlQuery);
                        CollectionAssert.AreEqual(expectedResult, result2);
                    }
                }
            }
        }

        [Test]
        public void DatabaseLoaderReceivesUnknownDbProvider_ThrowsAnException()
        {
            Assert.Throws<ArgumentException>(() => new DatabaseSourceLoader("conn string", (SqlProvidersEnum)(-1)));
        }

        [Test]
        public void DatabaseLoaderReceivesBadConnString_ThrowsExceptionOnDataLoading()
        {
            string connString = "Server=very-distant-unknown-server;Database=TestDb;Integrated Security=true;MultipleActiveResultSets=true;";
            DatabaseSourceLoader sqlServerLoader = new DatabaseSourceLoader(connString, SqlProvidersEnum.SqlServer);

            Assert.Throws<SqlException>(() => sqlServerLoader.LoadDataFromDatabaseSource(""));
        }

        private class FakeDbConnection : IDbConnection
        {
            public string ConnectionString { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

            public int ConnectionTimeout => throw new NotImplementedException();

            public string Database => throw new NotImplementedException();

            public ConnectionState State => throw new NotImplementedException();

            public IDbTransaction BeginTransaction()
            {
                throw new NotImplementedException();
            }

            public IDbTransaction BeginTransaction(IsolationLevel il)
            {
                throw new NotImplementedException();
            }

            public void ChangeDatabase(string databaseName)
            {
                throw new NotImplementedException();
            }

            public void Close()
            {
                return;
            }

            public IDbCommand CreateCommand()
            {
                return new FakeDbCommand();
            }

            public void Dispose()
            {
                return;
            }

            public void Open()
            {
                return;
            }
        }

        private class FakeDbCommand : IDbCommand
        {
            public string CommandText { get; set; }
            public int CommandTimeout { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
            public CommandType CommandType { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
            public IDbConnection Connection { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

            public IDataParameterCollection Parameters => throw new NotImplementedException();

            public IDbTransaction Transaction { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
            public UpdateRowSource UpdatedRowSource { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

            public void Cancel()
            {
                throw new NotImplementedException();
            }

            public IDbDataParameter CreateParameter()
            {
                throw new NotImplementedException();
            }

            public void Dispose()
            {
                return;
            }

            public int ExecuteNonQuery()
            {
                throw new NotImplementedException();
            }

            public IDataReader ExecuteReader()
            {
                return new FakeDbDataReader();
            }

            public IDataReader ExecuteReader(CommandBehavior behavior)
            {
                throw new NotImplementedException();
            }

            public object ExecuteScalar()
            {
                throw new NotImplementedException();
            }

            public void Prepare()
            {
                throw new NotImplementedException();
            }
        }

        private class FakeDbDataReader : IDataReader
        {
            public static object[] TestValues = new object[] { 
                "test", "1.52", "blahblah", "tdtdt"
            };
            private int _counter = -1;

            public FakeDbDataReader()
            {

            }

            public object this[int i] => throw new NotImplementedException();

            public object this[string name] => throw new NotImplementedException();

            public int Depth => throw new NotImplementedException();

            public bool IsClosed => throw new NotImplementedException();

            public int RecordsAffected => throw new NotImplementedException();

            public int FieldCount => throw new NotImplementedException();

            public void Close()
            {
                return;
            }

            public void Dispose()
            {
                return;
            }

            public bool GetBoolean(int i)
            {
                throw new NotImplementedException();
            }

            public byte GetByte(int i)
            {
                throw new NotImplementedException();
            }

            public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public char GetChar(int i)
            {
                throw new NotImplementedException();
            }

            public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public IDataReader GetData(int i)
            {
                throw new NotImplementedException();
            }

            public string GetDataTypeName(int i)
            {
                throw new NotImplementedException();
            }

            public DateTime GetDateTime(int i)
            {
                throw new NotImplementedException();
            }

            public decimal GetDecimal(int i)
            {
                throw new NotImplementedException();
            }

            public double GetDouble(int i)
            {
                throw new NotImplementedException();
            }

            public Type GetFieldType(int i)
            {
                throw new NotImplementedException();
            }

            public float GetFloat(int i)
            {
                throw new NotImplementedException();
            }

            public Guid GetGuid(int i)
            {
                throw new NotImplementedException();
            }

            public short GetInt16(int i)
            {
                throw new NotImplementedException();
            }

            public int GetInt32(int i)
            {
                throw new NotImplementedException();
            }

            public long GetInt64(int i)
            {
                throw new NotImplementedException();
            }

            public string GetName(int i)
            {
                throw new NotImplementedException();
            }

            public int GetOrdinal(string name)
            {
                throw new NotImplementedException();
            }

            public DataTable GetSchemaTable()
            {
                throw new NotImplementedException();
            }

            public string GetString(int i)
            {
                throw new NotImplementedException();
            }

            public object GetValue(int i)
            {
                return TestValues[_counter];
            }

            public int GetValues(object[] values)
            {
                throw new NotImplementedException();
            }

            public bool IsDBNull(int i)
            {
                throw new NotImplementedException();
            }

            public bool NextResult()
            {
                throw new NotImplementedException();
            }

            public bool Read()
            {
                _counter++;
                if  (_counter < TestValues.Length)
                    return true;

                return false;
            }
        }
    }
}
