﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.IO.SourceLoaders;

namespace TestDataGenerator.Tests.SourceLoadersTests
{
    public class FileSourceLoaderTest
    {
        [Test]
        public async Task FileSourceLoader_LoadsDataFromRawFile()
        {
            string lineTerminator = "\r\n";
            object[] testData = new object[] { 
                "test", "152", "true", "98.113"
            };
            string testDataAsString = string.Join(lineTerminator, testData);
            string testDataFilePath = Path.Combine(TestContext.CurrentContext.TestDirectory, "src_loader_text.txt");
            await File.WriteAllTextAsync(testDataFilePath, testDataAsString);

            FileSourceLoader loader = new FileSourceLoader(lineTerminator);

            object[] readData = await loader.LoadDataFromRawFileSourceAsync(testDataFilePath);

            CollectionAssert.AreEqual(testData, readData);
        }
    }
}
