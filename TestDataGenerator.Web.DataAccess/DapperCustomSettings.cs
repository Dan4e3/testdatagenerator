﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Web.DataAccess.Interfaces;
using TestDataGenerator.Web.Models.Attributes;

namespace TestDataGenerator.Web.DataAccess
{
    public class DapperCustomSettings: IDapperCustomSettings
    {
        private readonly IBasicServiceOptions _serviceOptions;

        public DapperCustomSettings(
            IBasicServiceOptions serviceOptions)
        {
            SqlMapperExtensions.TableNameMapper = (type) => GetTableNameFromAttr(type);
            _serviceOptions = serviceOptions;
        }

        public string GetTableNameFromAttr(Type modelType)
        {
            if (!_serviceOptions.UsePostgreSqlEngine)
            {
                object sqlServerAttr = Attribute.GetCustomAttribute(modelType, typeof(SqlServerTableNameAttribute));

                if (sqlServerAttr != null && sqlServerAttr is SqlServerTableNameAttribute attr)
                    return attr.Name;
            }
            else
            {
                object pgsqlAttr = Attribute.GetCustomAttribute(modelType, typeof(PostgreSqlTableNameAttribute));

                if (pgsqlAttr != null && pgsqlAttr is PostgreSqlTableNameAttribute attr)
                    return attr.Name;
            }

            return modelType.Name;
        }
    }
}
