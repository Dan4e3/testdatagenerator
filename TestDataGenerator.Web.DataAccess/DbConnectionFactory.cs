﻿using Microsoft.Extensions.Configuration;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Web.DataAccess.Interfaces;

namespace TestDataGenerator.Web.DataAccess
{
    public class DbConnectionFactory : IDbConnectionFactory
    {
        private readonly string _defaultConnName;
        private readonly IConfiguration _configuration;
        private readonly IBasicServiceOptions _serviceOptions;

        public DbConnectionFactory(
            IConfiguration configuration,
            IBasicServiceOptions serviceOptions)
        {
            _configuration = configuration;
            _serviceOptions = serviceOptions;

            _defaultConnName = _serviceOptions.DefaultDbConnectionStringName;
        }

        public IDbConnection GetConnectionByName(string appSettingsConnectionName = null)
        {
            string connName = appSettingsConnectionName ?? _defaultConnName;
            string connString = _configuration.GetConnectionString(connName);

            if (connString == null)
                throw new ArgumentException($"Specified connection string name ({connName}) in connection string section was not found!");

            try
            {
                if (!_serviceOptions.UsePostgreSqlEngine)
                {
                    SqlConnection sqlConn = new SqlConnection(connString);
                    return sqlConn;
                }
                else
                {
                    NpgsqlConnection npgsqlConn = new NpgsqlConnection(connString);
                    return npgsqlConn;
                }
            }
            catch (ArgumentException)
            {
                throw;
            }

            throw new Exception("No IDbConnection instance could be produced since no suitable provider found.");
        }
    }
}
