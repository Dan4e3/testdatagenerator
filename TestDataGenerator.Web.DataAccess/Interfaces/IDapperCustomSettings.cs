﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.Web.DataAccess.Interfaces
{
    public interface IDapperCustomSettings
    {
        string GetTableNameFromAttr(Type modelType);
    }
}
