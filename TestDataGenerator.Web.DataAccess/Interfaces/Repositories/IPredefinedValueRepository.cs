﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Web.Models.DomainModels;
using TestDataGenerator.Web.Models.DomainModels.Enums;

namespace TestDataGenerator.Web.DataAccess.Interfaces.Repositories
{
    public interface IPredefinedValueRepository
    {
        Task<PredefinedValueDm> GetSingleByIdAsync(long valueId);

        Task<PredefinedValueDm> GetSingleByValueAndDataTypeAsync(string value, ValueDataTypeEnum dataType);

        Task<PredefinedValueDm[]> GetManyByCategoryIdAsync(long categoryId);

        Task<PredefinedValueDm[]> GetManyByCategoryNameAsync(string categoryName);

        Task<PredefinedValueDm[]> GetManyByCategoryNamePaginatedAsync(string categoryName, int valuesToSkip, int valuesToFetch);

        Task<PredefinedValueDm[]> GetManyByCategoriesIdsAsync(long[] categoriesIds);

        Task<long> GetValuesCountByCategoryNameAsync(string categoryName);

        Task<int> DeleteSingleByIdAsync(long valueId);

        Task<int> DeleteManyByIdsAsync(long[] idsToRemove);

        Task<long> CreateSingleAsync(PredefinedValueDm model);

        Task<int> CreateManyAsync(PredefinedValueDm[] models);

        Task<bool> UpdateSingleAsync(PredefinedValueDm model);

        Task<int> ClearTableAsync();
    }
}
