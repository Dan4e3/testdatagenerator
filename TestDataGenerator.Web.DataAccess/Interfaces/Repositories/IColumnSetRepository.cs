﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Web.Models.DomainModels;

namespace TestDataGenerator.Web.DataAccess.Interfaces.Repositories
{
    public interface IColumnSetRepository
    {
        Task<ColumnSetDm> GetSingleByIdAsync(long columnSetId);

        Task<ColumnSetDm[]> GetManyByOwnerUserIdAsync(long ownerUserId);

        Task<ColumnSetDm[]> GetManyByOwnerUserNameAsync(string ownerUserName);

        Task<int> DeleteSingleByIdAsync(long columnSetId);

        Task<long> CreateSingleAsync(ColumnSetDm model);

        Task<bool> UpdateSingleAsync(ColumnSetDm model);
    }
}
