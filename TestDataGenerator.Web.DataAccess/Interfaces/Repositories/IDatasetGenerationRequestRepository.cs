﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Web.Models.DomainModels;

namespace TestDataGenerator.Web.DataAccess.Interfaces.Repositories
{
    public interface IDatasetGenerationRequestRepository
    {
        public Task<long> CreateSingleAsync(DatasetGenerationRequestDm model);

        public Task<DatasetGenerationRequestDm> GetSingleByIdAsync(long id);

        public Task<DatasetGenerationRequestDm> GetSinglePartialStateByIdAsync(long id);

        /// <summary>
        /// Does not load file contents
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public Task<DatasetGenerationRequestDm[]> GetManyByUserNameAsync(string userName);

        /// <summary>
        /// Does not load file contents
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="valuesToSkip"></param>
        /// <param name="valuesToFetch"></param>
        /// <returns></returns>
        public Task<DatasetGenerationRequestDm[]> GetManyByUserNamePaginatedAsync(string userName,
            int valuesToSkip, int valuesToFetch);

        public Task<int> GetUserRequestsCountAsync(string userName);

        public Task<bool> UpdateSingleAsync(DatasetGenerationRequestDm model);

        public Task<long[]> DeleteFetchedRawJsonDatasetRequestsAsync();

        public Task<long[]> DeleteExpiredFileDatasetReuqestsAsync(TimeSpan requestLifeTime);

        public Task<int> SetGenerationRequestAsFetchedAsync(long id);

        Task<bool> CheckIfWorkerResponseReceivedByRequestIdAsync(long id);
    }
}
