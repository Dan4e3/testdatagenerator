﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Web.Models.DomainModels;

namespace TestDataGenerator.Web.DataAccess.Interfaces.Repositories
{
    public interface IUserRepository
    {
        Task<UserDm> GetSingleByIdAsync(long userId);

        Task<UserDm> GetSingleByNameAsync(string userName);

        Task<int> DeleteSingleByIdAsync(long userId);

        Task<long> CreateSingleAsync(UserDm model);

        Task<bool> UpdateSingleAsync(UserDm model);

        Task<int> ClearTableAsync();
    }
}
