﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Web.Models.DomainModels;

namespace TestDataGenerator.Web.DataAccess.Interfaces.Repositories
{
    public interface IPredefinedCategoryRepository
    {
        Task<PredefinedCategoryDm> GetSingleByIdAsync(long categoryId);

        Task<PredefinedCategoryDm> GetSingleByNameAsync(string categoryName);

        Task<PredefinedCategoryDm[]> GetManyByContributorUserNameAsync(string contributorUserName, bool includePublic);

        Task<PredefinedCategoryDm[]> GetAllAsync();

        Task<PredefinedCategoryDm[]> GetPublicCategoriesAsync();

        Task<int> DeleteSingleByIdAsync(long categoryId);

        Task<int> DeleteManyByIdsAsync(long[] categoriesIds);

        Task<long> CreateSingleAsync(PredefinedCategoryDm model);

        Task<bool> UpdateSingleAsync(PredefinedCategoryDm model);

        Task<int> ClearTableAsync();
    }
}
