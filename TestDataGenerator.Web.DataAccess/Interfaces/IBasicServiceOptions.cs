﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.Web.DataAccess.Interfaces
{
    public interface IBasicServiceOptions
    {


        string DefaultDbConnectionStringName { get; set; }

        bool UsePostgreSqlEngine { get; set; }

        TimeSpan FileDatasetRequestsLifeTime { get; set; }

        MqOptions MqOptions { get; set; }

        ReCaptchaOptions ReCaptchaOptions { get; set; }

        DatasetGenerationRowLimitsOptions DatasetGenerationRowLimitsOptions { get; set; }

        QuartzJobOption[] QuartzJobs { get; set; }
    }

    public class ReCaptchaOptions
    {
        public string SecretKey { get; set; }

        public double AcceptableScore { get; set; }

        public string RecaptchaVerifyAddress { get; set; }
    }

    public class DatasetGenerationRowLimitsOptions
    {
        public int SynchronousRequestsRowLimit { get; set; }

        public int AsyncRawJsonRequestsLimit { get; set; }

        public int AsyncFileRequestsRowLimit { get; set; }
    }

    public class MqOptions
    {
        public string RabbitMqHostname { get; set; }

        public string DatasetInputQueueName { get; set; }
    }

    public class QuartzJobOption
    {
        public string JobName { get; set; }

        public TimeSpan RepeatTimeSpan { get; set; }
    }
}
