﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces.Columns;
using TestDataGenerator.Abstractions.Interfaces.Rows;
using TestDataGenerator.Abstractions.Interfaces.Writers;
using TestDataGenerator.Core.ExtensionMethods;

namespace TestDataGenerator.IO.DatabaseExportExecutors
{
    /// <summary>
    /// PostgreSQL-oriented dataset exporter
    /// </summary>
    public class PostgresExportExecutor : IDbExportExecutor
    {
        /// <inheritdoc/>
        public string ConnectionString { get; set; }

        /// <inheritdoc/>
        public string TargetTableName { get; set; }

        /// <summary>
        /// Create new PostgreSQL export executor instance
        /// </summary>
        /// <param name="connectionString">Connection string to use</param>
        /// <param name="tableName">Database table name for values insertion</param>
        public PostgresExportExecutor(string connectionString, string tableName)
        {
            ConnectionString = connectionString;
            TargetTableName = tableName;
        }

        /// <summary>
        /// Create new PostgreSQL export executor instance
        /// </summary>
        public PostgresExportExecutor()
        {

        }

        /// <inheritdoc/>
        public IDbConnection CreateConnection()
        {
            NpgsqlConnection connection = new NpgsqlConnection(ConnectionString);
            return connection;
        }

        /// <inheritdoc/>
        public string CreateInsertSqlCommand(IEnumerable<IColumn> columns, IEnumerable<IRow> rowsToInsert)
        {
            IColumn[] columnArr = columns.ToArray();
            string colNames = $"({string.Join(", ", columnArr.Select(col => col.GetColumnName()))})";
            string[] valuesArray = rowsToInsert.Select(r => $@"({string.Join(",", r.Select((val, ind) => {
                if (val.IsNumber())
                    return r.GetInvariantStringRepresentation(ind);
                else if (val is bool v)
                    return v == true ? "TRUE" : "FALSE";
                else
                    return $"'{r.GetInvariantStringRepresentation(ind)}'";
            }))})").ToArray();

            string insertSqlCommand = $@"
                INSERT INTO {TargetTableName} {colNames}
                VALUES {string.Join(", ", valuesArray)}
            ";
            return insertSqlCommand;
        }
    }
}
