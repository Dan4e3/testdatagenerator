﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces.Columns;
using TestDataGenerator.Abstractions.Interfaces.Rows;
using TestDataGenerator.Abstractions.Interfaces.Writers;
using TestDataGenerator.Core.ExtensionMethods;

namespace TestDataGenerator.IO.DatabaseExportExecutors
{
    /// <summary>
    /// MS SQL Server-oriented dataset exporter
    /// </summary>
    public class SqlServerExportExecutor : IDbExportExecutor
    {
        /// <inheritdoc/>
        public string ConnectionString { get; set; }

        /// <inheritdoc/>
        public string TargetTableName { get; set; }

        /// <summary>
        /// Create new SQL Server export executor instance
        /// </summary>
        /// <param name="connectionString">Connection string to use</param>
        /// <param name="targetTableName">Database table name for values insertion</param>
        public SqlServerExportExecutor(string connectionString, string targetTableName)
        {
            ConnectionString = connectionString;
            TargetTableName = targetTableName;
        }

        /// <summary>
        /// Create new SQL Server export executor instance
        /// </summary>
        public SqlServerExportExecutor()
        {

        }

        /// <inheritdoc/>
        public IDbConnection CreateConnection()
        {
            SqlConnection conn = new SqlConnection(ConnectionString);
            return conn;
        }

        /// <inheritdoc/>
        public string CreateInsertSqlCommand(IEnumerable<IColumn> columns, IEnumerable<IRow> rowsToInsert)
        {
            IColumn[] columnArr = columns.ToArray();
            string colNames = $"({string.Join(", ", columnArr.Select(col => $"[{col.GetColumnName()}]"))})";
            string[] valuesArray = rowsToInsert.Select(r => $@"({string.Join(",", r.Select((val, ind) => {
                if (val.IsNumber())
                    return r.GetInvariantStringRepresentation(ind);
                else if (val is bool v)
                    return v == true ? "1" : "0";
                else
                    return $"'{r.GetInvariantStringRepresentation(ind)}'";
            }))})").ToArray();

            string insertSqlCommand = $@"
                INSERT INTO {TargetTableName} {colNames}
                VALUES {string.Join(", ", valuesArray)}
            ";
            return insertSqlCommand;
        }
    }
}
