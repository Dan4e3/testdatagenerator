﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces;
using TestDataGenerator.Abstractions.Interfaces.Columns;
using TestDataGenerator.Abstractions.Interfaces.Dataset;
using TestDataGenerator.Abstractions.Interfaces.Rows;
using TestDataGenerator.Abstractions.Interfaces.Writers;
using TestDataGenerator.Core.ExtensionMethods;

namespace TestDataGenerator.IO.DatasetWriters
{
    /// <summary>
    /// Database writer
    /// </summary>
    public class DbDataSetWriter : IDataSetDbWriter
    {
        private IProgressReporter _progressReporter;

        /// <summary>
        /// Create instance of database writer
        /// </summary>
        /// <param name="progressReporter">Progress reporter</param>
        public DbDataSetWriter(IProgressReporter progressReporter = null)
        {
            _progressReporter = progressReporter;
        }

        /// <inheritdoc/>
        public int WriteToDatabase(IDbExportExecutor executor, ITestDataSet dataset, int commandTimeout = 30)
        {
            IColumn[] columns = dataset.GetColumns().ToArray();
            IRow[] datasetRows = dataset.GetExistingDataSet();
            int totalInsertedRows = 0;

            const int batchSize = 1000;
            double loopsToIterate = Math.Ceiling(datasetRows.Length / (double)batchSize);
            Stopwatch watch = new Stopwatch();

            using (IDbConnection connection = executor.CreateConnection())
            {
                connection.Open();
                using (IDbTransaction trans = connection.BeginTransaction(IsolationLevel.ReadCommitted))
                {
                    _progressReporter?.ReportProgress($"Inserting {datasetRows.Length} rows into database table...", 0);
                    watch.Start();
                    for (int i = 0; i < loopsToIterate; i++)
                    {
                        int startingRowIndex = i * batchSize;
                        IEnumerable<IRow> rowsToInsert = datasetRows.Skip(startingRowIndex).Take(batchSize);
                        string sqlInsert = executor.CreateInsertSqlCommand(columns, rowsToInsert);
                        using (IDbCommand command = connection.CreateCommand())
                        {
                            command.Transaction = trans;
                            command.CommandText = sqlInsert;
                            command.CommandTimeout = commandTimeout;
                            int rowsInserted = command.ExecuteNonQuery();
                            totalInsertedRows += rowsInserted;
                        }

                        _progressReporter?.ReportProgress((int)((i + 1) / loopsToIterate * 100));
                    }
                    trans.Commit();
                }
            }

            watch.Stop();
            _progressReporter?.ReportProgress($"Task completed in {watch.Elapsed.Seconds.ToString("F2")} seconds", 0);

            return totalInsertedRows;
        }

        /// <inheritdoc/>
        public int WriteToDatabaseUsingInPlaceRowGeneration(IDbExportExecutor executor, ITestDataSet dataset, int rowsToGenerate, int commandTimeout = 30)
        {
            IColumnSet colset = dataset.GetColumns();
            IColumn[] columns = colset.ToArray();
            int totalInsertedRows = 0;

            const int batchSize = 1000;
            double loopsToIterate = Math.Ceiling(rowsToGenerate / (double)batchSize);
            Stopwatch watch = new Stopwatch();

            using (IDbConnection connection = executor.CreateConnection())
            {
                connection.Open();
                using (IDbTransaction trans = connection.BeginTransaction(IsolationLevel.ReadCommitted))
                {
                    _progressReporter?.ReportProgress($"Inserting {rowsToGenerate} rows into database table...", 0);
                    watch.Start();
                    for (int i = 0; i < loopsToIterate; i++)
                    {
                        int batchRowsToGenerateCount;
                        if (i == 0 && i == loopsToIterate - 1)
                            batchRowsToGenerateCount = rowsToGenerate;
                        else if (i == loopsToIterate - 1)
                            batchRowsToGenerateCount = rowsToGenerate - i * batchSize;
                        else
                            batchRowsToGenerateCount = batchSize;
                        List<IRow> rowsToInsert = new List<IRow>();
                        for (int j = 0; j < batchRowsToGenerateCount; j++)
                            rowsToInsert.Add(colset.GetNewRow(dataset));
                        string sqlInsert = executor.CreateInsertSqlCommand(columns, rowsToInsert);
                        using (IDbCommand command = connection.CreateCommand())
                        {
                            command.Transaction = trans;
                            command.CommandText = sqlInsert;
                            command.CommandTimeout = commandTimeout;
                            int rowsInserted = command.ExecuteNonQuery();
                            totalInsertedRows += rowsInserted;
                        }

                        _progressReporter?.ReportProgress((int)((i + 1) / loopsToIterate * 100));
                    }
                    trans.Commit();
                }
            }

            watch.Stop();
            _progressReporter?.ReportProgress($"Task completed in {watch.Elapsed.Seconds.ToString("F2")} seconds", 0);

            return totalInsertedRows;
        }
    }
}
