﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.IO.DataSetWriters
{
    /// <summary>
    /// Class with JsonWriter settings - indentation, date formatting etc.
    /// </summary>
    public class JsonDataSetWriterSettings
    {
        /// <summary>
        /// Gets or sets the culture used when writing JSON. Defaults to System.Globalization.CultureInfo.InvariantCulture.
        /// </summary>
        public CultureInfo Culture { get; set; } = CultureInfo.InvariantCulture;

        /// <summary>
        /// Gets or sets how special floating point numbers, e.g. System.Double.NaN, System.Double.PositiveInfinity
        /// <br/>and System.Double.NegativeInfinity, are written to JSON text.
        /// </summary>
        public FloatFormatHandling FloatFormatHandling { get; set; } = FloatFormatHandling.DefaultValue;

        /// <summary>
        /// Gets or sets how strings are escaped when writing JSON text.
        /// </summary>
        public StringEscapeHandling StringEscapeHandling { get; set; } = StringEscapeHandling.Default;

        /// <summary>
        /// Gets or sets how System.DateTime time zones are handled when writing JSON text.
        /// </summary>
        public DateTimeZoneHandling DateTimeZoneHandling { get; set; } = DateTimeZoneHandling.Local;

        /// <summary>
        /// Gets or sets how dates are written to JSON text.
        /// </summary>
        public DateFormatHandling DateFormatHandling { get; set; } = DateFormatHandling.IsoDateFormat;

        /// <summary>
        /// Gets or sets a value indicating how JSON text output should be formatted.
        /// </summary>
        public Formatting Formatting { get; set; } = Formatting.Indented;

        /// <summary>
        /// Gets or sets how System.DateTime and System.DateTimeOffset values are formatted when writing JSON text.
        /// </summary>
        public string DateFormatString { get; set; }
    }
}
