﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.IO.DataSetWriters.Enums
{
    public enum DatasetJsonExportStyle
    {
        ArrayOfObjectsWithColumnsAsProperties = 0,
        SingleObjectWithArrayOfValuesPerProperty = 1
    }
}
