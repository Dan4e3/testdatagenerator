﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using TestDataGenerator.Abstractions.Interfaces.Dataset;
using TestDataGenerator.Abstractions.Interfaces.Writers;
using TestDataGenerator.Abstractions.Interfaces.Rows;
using System.Diagnostics;
using TestDataGenerator.IO.DataSetWriters;
using TestDataGenerator.IO.DataSetWriters.Enums;
using TestDataGenerator.Abstractions.Interfaces.Columns;

namespace TestDataGenerator.IO.DatasetWriters
{
    /// <summary>
    /// Export dataset to JSON files
    /// </summary>
    public class JsonDataSetWriter : IDataSetFileWriter, IDataSetStreamWriter
    {
        private IProgressReporter _progressReporter;

        public DatasetJsonExportStyle DatasetExportStyle { get; set; }

        /// <summary>
        /// Underlying json writer settings
        /// </summary>
        public JsonDataSetWriterSettings WriterSettings { get; set; }

        /// <summary>
        /// Create instance of JSON exporter
        /// </summary>
        /// <param name="progressReporter">Progress reporter</param>
        /// <param name="writerSettings">Writer settings with JSON formatting options</param>
        public JsonDataSetWriter(IProgressReporter progressReporter = null, JsonDataSetWriterSettings writerSettings = null)
        {
            _progressReporter = progressReporter;
            WriterSettings = writerSettings ?? new JsonDataSetWriterSettings();
        }

        /// <inheritdoc/>
        public async Task WriteToFileAsync(string filePath, ITestDataSet dataSet)
        {
            using (StreamWriter sw = new StreamWriter(filePath))
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                MapJsonWriterSettings(writer);
                await WriteDataSet(writer, dataSet);
            }
        }

        /// <inheritdoc/>
        public async Task WriteToFileUsingInPlaceRowGenerationAsync(string filePath, ITestDataSet dataSet, int rowsToGenerate)
        {
            using (StreamWriter sw = new StreamWriter(filePath))
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                MapJsonWriterSettings(writer);
                await WriteDataSet(writer, dataSet, rowsToGenerate);
            }
        }

        /// <inheritdoc/>
        public async Task WriteToStreamAsync(Stream stream, ITestDataSet dataSet)
        {
            using (StreamWriter sw = new StreamWriter(stream, null, -1, leaveOpen: true))
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                MapJsonWriterSettings(writer);
                await WriteDataSet(writer, dataSet);
            }
        }

        /// <inheritdoc/>
        public async Task WriteToStreamUsingInPlaceRowGenerationAsync(Stream stream, ITestDataSet dataSet, int rowsToGenerate)
        {
            using (StreamWriter sw = new StreamWriter(stream, null, -1, leaveOpen: true))
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                MapJsonWriterSettings(writer);
                await WriteDataSet(writer, dataSet, rowsToGenerate);
            }
        }

        private void MapJsonWriterSettings(JsonWriter existingWriter)
        {
            existingWriter.Culture = WriterSettings.Culture;
            existingWriter.DateFormatHandling = WriterSettings.DateFormatHandling;
            existingWriter.DateFormatString = WriterSettings.DateFormatString;
            existingWriter.DateTimeZoneHandling = WriterSettings.DateTimeZoneHandling;
            existingWriter.FloatFormatHandling = WriterSettings.FloatFormatHandling;
            existingWriter.StringEscapeHandling = WriterSettings.StringEscapeHandling;
            existingWriter.Formatting = WriterSettings.Formatting;
        }

        private async Task WriteDataSet(JsonWriter writer, ITestDataSet dataSet, int rowsToGenerate = -1)
        {
            var colNames = dataSet.GetColumns().Select(x => x.GetColumnName()).ToArray();
            IRow[] datasetRows = dataSet.GetExistingDataSet();
            IColumnSet colset = dataSet.GetColumns();
            int rowCount = 0;
            bool generateOnTheFly = false;
            if (rowsToGenerate == -1)
            {
                rowCount = datasetRows.Length;
                generateOnTheFly = false;
            }
            else
            {
                rowCount = rowsToGenerate;
                generateOnTheFly = true;
            }

            Stopwatch watch = new Stopwatch();
            _progressReporter?.ReportProgress($"Writing {rowCount} rows to JSON file...", 0);
            watch.Start();

            switch (this.DatasetExportStyle)
            {
                case DatasetJsonExportStyle.ArrayOfObjectsWithColumnsAsProperties:
                    await writer.WriteStartArrayAsync();
                    for (int i = 0; i < rowCount; i++)
                    {
                        IRow row = generateOnTheFly ? colset.GetNewRow(dataSet) : datasetRows[i];
                        await writer.WriteStartObjectAsync();
                        object[] rowData = row.GetRowData();
                        for (int j = 0; j < rowData.Length; j++)
                        {
                            await writer.WritePropertyNameAsync(colNames[j]);
                            await writer.WriteValueAsync(rowData[j]);
                        }
                        await writer.WriteEndObjectAsync();

                        _progressReporter?.ReportProgress((int)((double)i / rowCount * 100));
                    }
                    await writer.WriteEndArrayAsync();
                    break;

                case DatasetJsonExportStyle.SingleObjectWithArrayOfValuesPerProperty:
                    await writer.WriteStartObjectAsync();
                    for (int j = 0; j < colNames.Length; j++)
                    {
                        await writer.WritePropertyNameAsync(colNames[j]);
                        await writer.WriteStartArrayAsync();

                        if (generateOnTheFly)
                        {
                            for (int i = 0; i < rowsToGenerate; i++)
                            {
                                await writer.WriteValueAsync(colset[j].GenerateValue());
                                _progressReporter?.ReportProgress((int)((double)(j + 1) * i / (rowCount * (colNames.Length + 1)) * 100));
                            }
                        }
                        else
                        {
                            object[] columnData = dataSet.GetExistingDataSet().Select(r => r[j]).ToArray();
                            for (int i = 0; i < columnData.Length; i++)
                            {
                                await writer.WriteValueAsync(columnData[i]);
                                _progressReporter?.ReportProgress((int)((double)(j + 1) * i / (rowCount * (colNames.Length + 1)) * 100));
                            }
                        }

                        await writer.WriteEndArrayAsync();
                    }
                    await writer.WriteEndObjectAsync();
                    break;

                default:
                    throw new Exception($"Provided {nameof(DatasetExportStyle)} option is not supported");
            }


            watch.Stop();
            _progressReporter?.ReportProgress($"Task completed in {watch.Elapsed.Seconds.ToString("F2")} seconds", 0);
        }
    }
}
