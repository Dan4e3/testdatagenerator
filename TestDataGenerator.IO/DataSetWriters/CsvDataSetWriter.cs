﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces;
using TestDataGenerator.Abstractions.Interfaces.Columns;
using TestDataGenerator.Abstractions.Interfaces.Dataset;
using TestDataGenerator.Abstractions.Interfaces.Rows;
using TestDataGenerator.Abstractions.Interfaces.Writers;
using TestDataGenerator.Core.ExtensionMethods;

namespace TestDataGenerator.IO.DatasetWriters
{
    /// <summary>
    /// Writes datasets to CSV files
    /// </summary>
    public class CsvDataSetWriter : IDataSetFileWriter, IDataSetStreamWriter
    {
        private CultureInfo _culture;
        private IProgressReporter _progressReporter;

        /// <summary>
        /// Separator between column titles
        /// </summary>
        public string ColumnSeparator { get; set; }

        /// <summary>
        /// New line symbol combination
        /// </summary>
        public string NewLineSequence { get; set; }

        /// <summary>
        /// Create CSV writer instance
        /// </summary>
        /// <param name="inputCulture">Cultural options for writer</param>
        /// <param name="progressReporter">Progress reporter</param>
        public CsvDataSetWriter(CultureInfo inputCulture = null, IProgressReporter progressReporter = null)
        {
            _culture = inputCulture ?? CultureInfo.CurrentCulture;
            _progressReporter = progressReporter;
            ColumnSeparator = _culture.TextInfo.ListSeparator;
            NewLineSequence = Environment.NewLine;
        }

        /// <inheritdoc/>
        public async Task WriteToFileAsync(string filePath, ITestDataSet dataSet)
        {
            using (StreamWriter sw = new StreamWriter(filePath, false))
            {
                await WriteDataSet(sw, dataSet);
            }
        }

        /// <inheritdoc/>
        public async Task WriteToStreamAsync(Stream stream, ITestDataSet dataSet)
        {
            using (StreamWriter sw = new StreamWriter(stream, null, bufferSize: -1, leaveOpen: true))
            {
                await WriteDataSet(sw, dataSet);
            }
        }

        private async Task WriteDataSet(StreamWriter sw, ITestDataSet dataSet, int rowsToGenerate = -1)
        {
            IColumn[] columns = dataSet.GetColumns().ToArray();
            string[] colNames = columns.Select(x => x.GetColumnName()).ToArray();
            IRow[] datasetRows = dataSet.GetExistingDataSet();
            int rowsCount = 0;
            bool generateOnTheFly = false;
            if (rowsToGenerate == -1)
            {
                rowsCount = datasetRows.Length;
                generateOnTheFly = false;
            }
            else
            {
                rowsCount = rowsToGenerate;
                generateOnTheFly = true;
            }

            sw.NewLine = NewLineSequence;

            string colNamesStringRepr = string.Join(ColumnSeparator, colNames);
            await sw.WriteLineAsync(colNamesStringRepr);

            Stopwatch watch = new Stopwatch();
            _progressReporter?.ReportProgress($"Writing {rowsCount} rows to CSV file...", 0);
            watch.Start();

            for (int i = 0; i < rowsCount; i++)
            {
                IRow row = generateOnTheFly ? dataSet.GetColumns().GetNewRow(dataSet) : datasetRows[i];
                string rowStringRepr = string.Join(ColumnSeparator, row.Select((val, ind) => {
                    return row.GetStringRepresentation(ind);
                }));

                if (i != rowsCount - 1)
                    await sw.WriteLineAsync(rowStringRepr);
                else
                    await sw.WriteAsync(rowStringRepr);

                _progressReporter?.ReportProgress((int)((double)i / rowsCount * 100));
            }

            watch.Stop();
            _progressReporter?.ReportProgress($"Task completed in {watch.Elapsed.Seconds.ToString("F2")} seconds", 0);
        }

        /// <inheritdoc/>
        public async Task WriteToFileUsingInPlaceRowGenerationAsync(string filePath, ITestDataSet dataSet, int rowsToGenerate)
        {
            using (StreamWriter sw = new StreamWriter(filePath, false))
            {
                await WriteDataSet(sw, dataSet, rowsToGenerate);
            }
        }

        /// <inheritdoc/>
        public async Task WriteToStreamUsingInPlaceRowGenerationAsync(Stream stream, ITestDataSet dataSet, int rowsToGenerate)
        {
            using (StreamWriter sw = new StreamWriter(stream, null, bufferSize: -1, leaveOpen: true))
            {
                await WriteDataSet(sw, dataSet, rowsToGenerate);
            }
        }
    }
}
