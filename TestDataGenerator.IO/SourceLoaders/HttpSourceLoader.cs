﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces.ExternalDataLoaders;

namespace TestDataGenerator.IO.SourceLoaders
{
    /// <summary>
    /// Basic HTTP external data loader
    /// </summary>
    public class HttpSourceLoader : IHttpSourceLoader, IDisposable
    {
        /// <summary>
        /// Underlying htpp client
        /// </summary>
        public HttpClient HttpClient { get; set; }

        /// <summary>
        /// Tokens to search in response JSON. Must be presented in JSONPath format
        /// </summary>
        public string[] JsonPathsSearchTokens { get; set; }

        /// <summary>
        /// Create new instance of HTTP loader
        /// </summary>
        /// <param name="jsonPathsSearchTokens">JSONPath tokens to search in JSON response</param>
        /// <param name="httpClient">Existing http client to use. New one is created if none specified</param>
        public HttpSourceLoader(string[] jsonPathsSearchTokens, HttpClient httpClient = null)
        {
            HttpClient = httpClient ?? new HttpClient();
            JsonPathsSearchTokens = jsonPathsSearchTokens;
        }

        public void Dispose()
        {
            HttpClient?.Dispose();
        }

        /// <inheritdoc/>
        public async Task<object[]> LoadDataFromHttpSourceAsync(HttpRequestMessage requestMessage)
        {
            HttpResponseMessage responseMessage =  await HttpClient.SendAsync(requestMessage);
            responseMessage.EnsureSuccessStatusCode();

            List<object> resultList = new List<object>();
            string responseBody = await responseMessage.Content.ReadAsStringAsync();
            JToken root = JToken.Parse(responseBody);
            
            foreach(string jsonPath in JsonPathsSearchTokens)
            {
                JToken foundToken = root.SelectToken(jsonPath);
                if (foundToken == null)
                    continue;

                resultList.AddRange(foundToken.ToObject<object[]>().Distinct());
            }

            return resultList.ToArray();
        }
    }
}
