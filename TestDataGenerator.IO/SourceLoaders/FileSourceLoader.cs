﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces.ExternalDataLoaders;

namespace TestDataGenerator.IO.SourceLoaders
{
    /// <summary>
    /// Basic file data extractor
    /// </summary>
    public class FileSourceLoader : IFileSourceLoader
    {
        /// <summary>
        /// Line termination sequence of symbols (\r\n by default)
        /// </summary>
        public string NewLineSequence { get; set; }

        /// <summary>
        /// Encoding of target file (defaults to UTF-8)
        /// </summary>
        public Encoding FileEncoding { get; set; }

        /// <summary>
        /// Create new instance of file data loader
        /// </summary>
        /// <param name="newLineSequence">Line termination sequence. \r\n by default</param>
        /// <param name="fileEncoding">File encoding to use. UTF8 by default</param>
        public FileSourceLoader(string newLineSequence = "\r\n", Encoding fileEncoding = null)
        {
            NewLineSequence = newLineSequence;
            FileEncoding = fileEncoding ?? Encoding.UTF8;
        }

        /// <inheritdoc/>
        public async Task<object[]> LoadDataFromRawFileSourceAsync(string pathToFile)
        {
            using (FileStream stream = File.OpenRead(pathToFile))
            {
                StringBuilder sb = new StringBuilder();

                byte[] buffer = new byte[1024];
                int numRead;
                while ((numRead = await stream.ReadAsync(buffer, 0, buffer.Length)) != 0)
                {
                    string text = FileEncoding.GetString(buffer, 0, numRead);
                    sb.Append(text);
                }

                return sb.ToString()
                    .Split(new string[] { NewLineSequence }, StringSplitOptions.RemoveEmptyEntries)
                    .Distinct()
                    .Cast<object>()
                    .ToArray();
            }
        }
    }
}
