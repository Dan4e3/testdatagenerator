﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces.ExternalDataLoaders;

namespace TestDataGenerator.IO.SourceLoaders
{
    /// <summary>
    /// Class used to load columns from database sources as simple 1D array
    /// </summary>
    public class DatabaseSourceLoader : IDatabaseSourceLoader, IDisposable
    {
        /// <summary>
        /// Underlying connection used to communicate with database
        /// </summary>
        public virtual IDbConnection DbConnection { get; private set; }

        /// <summary>
        /// Connection string associated with loader instance
        /// </summary>
        public string ConnectionString { get; private set; }

        /// <summary>
        /// Create instance of database data loader
        /// </summary>
        /// <param name="connectionString">Connection string to target database</param>
        /// <param name="provider">SQL provider to use</param>
        public DatabaseSourceLoader(string connectionString, SqlProvidersEnum provider)
        {
            ConnectionString = connectionString;
            switch (provider)
            {
                case SqlProvidersEnum.PostgreSQL:
                    DbConnection = new NpgsqlConnection(connectionString);
                    break;
                case SqlProvidersEnum.SqlServer:
                    DbConnection = new SqlConnection(connectionString);
                    break;
                default:
                    throw new ArgumentException("Can't create connection with specified provider");
            }
        }

        /// <inheritdoc/>
        public object[] LoadDataFromDatabaseSource(string tableName, string columnName)
        {
            string query = $@"SELECT {columnName} FROM {tableName}";
            return LoadDataFromDatabaseSourceImpementation(query);
        }

        /// <inheritdoc/>
        public object[] LoadDataFromDatabaseSource(string sqlQuery)
        {
            return LoadDataFromDatabaseSourceImpementation(sqlQuery);
        }

        private object[] LoadDataFromDatabaseSourceImpementation(string query)
        {
            try
            {
                List<object> resultList = new List<object>();
                DbConnection.Open();
                using (IDbCommand command = DbConnection.CreateCommand())
                {
                    command.CommandText = query;
                    using (IDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                            resultList.Add(reader.GetValue(0));
                    }
                }

                return resultList.Distinct().ToArray();
            }
            catch
            {
                throw;
            }
            finally
            {
                DbConnection.Close();
            }
        }

        public void Dispose()
        {
            DbConnection?.Dispose();
        }
    }
}
