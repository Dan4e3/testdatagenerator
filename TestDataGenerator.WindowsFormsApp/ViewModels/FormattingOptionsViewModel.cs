﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Core.JsonSerialization;

namespace TestDataGenerator.WindowsFormsApp.ViewModels
{
    public class FormattingOptionsViewModel : INotifyPropertyChanged
    {
        public CultureInfo ResultCulture { get; private set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public FormattingOptionsViewModel(CultureInfo existingCustomCulture = null)
        {
            ResultCulture = new CultureInfo(CultureInfoJsonConverter.customCultureName, true);
            if (existingCustomCulture != null)
            {
                ResultCulture.TextInfo.ListSeparator = existingCustomCulture.TextInfo.ListSeparator;
                ResultCulture.NumberFormat.NumberDecimalSeparator = existingCustomCulture.NumberFormat.NumberDecimalSeparator;
                ResultCulture.DateTimeFormat.DateSeparator = existingCustomCulture.DateTimeFormat.DateSeparator;
                ResultCulture.DateTimeFormat.ShortDatePattern = existingCustomCulture.DateTimeFormat.ShortDatePattern;
            }
        }

        public string ListSeparator
        {
            get => ResultCulture.TextInfo.ListSeparator;
            set
            {
                if (ResultCulture.TextInfo.ListSeparator != value)
                {
                    ResultCulture.TextInfo.ListSeparator = value;
                    OnPropertyChanged();
                }
            }
        }

        public string DecimalSeparator
        {
            get => ResultCulture.NumberFormat.NumberDecimalSeparator;
            set
            {
                if (ResultCulture.NumberFormat.NumberDecimalSeparator != value)
                {
                    ResultCulture.NumberFormat.NumberDecimalSeparator = value;
                    OnPropertyChanged();
                }
            }
        }

        public string DateTimeShortPattern
        {
            get => ResultCulture.DateTimeFormat.ShortDatePattern;
            set
            {
                if (ResultCulture.DateTimeFormat.ShortDatePattern != value)
                {
                    ResultCulture.DateTimeFormat.ShortDatePattern = value;
                    OnPropertyChanged();
                }
            }
        }

        public string DateSeparator
        {
            get => ResultCulture.DateTimeFormat.DateSeparator;
            set
            {
                if (ResultCulture.DateTimeFormat.DateSeparator != value)
                {
                    ResultCulture.DateTimeFormat.DateSeparator = value;
                    OnPropertyChanged();
                }
            }
        }

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
