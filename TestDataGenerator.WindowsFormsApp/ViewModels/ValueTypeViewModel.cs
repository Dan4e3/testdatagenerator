﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.WinFormApp.ViewModels
{
    public class ValueTypeViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public Type Type { get; private set; }
        public string DisplayName { get; private set; }

        public ValueTypeViewModel(Type type, string displayName)
        {
            Type = type;
            DisplayName = displayName;
        }

        public void OnPropertyChanged([CallerMemberName] string propName="")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
    }
}
