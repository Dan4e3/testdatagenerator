﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Settings;

namespace TestDataGenerator.WindowsFormsApp.ViewModels
{
    public class NullValuesSettingsViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public NullValuesSettings NullValuesSettings { get; private set; }

        public NullValuesSettingsViewModel(NullValuesSettings nullValuesSettings = null)
        {
            NullValuesSettings = nullValuesSettings ?? new NullValuesSettings();
        }

        public bool GenerateNullValues
        {
            get => NullValuesSettings.GenerateNullValues;
            set
            {
                if (NullValuesSettings.GenerateNullValues != value)
                {
                    NullValuesSettings.GenerateNullValues = value;
                    OnPropertyChanged();
                }
            }
        }

        public int AverageRowsBeforeNullAppearance
        {
            get => NullValuesSettings.AverageRowsBeforeNullAppearance;
            set
            {
                if (NullValuesSettings.AverageRowsBeforeNullAppearance != value)
                {
                    NullValuesSettings.AverageRowsBeforeNullAppearance = value;
                    OnPropertyChanged();
                }
            }
        }

        private void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
