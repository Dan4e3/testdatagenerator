﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.WindowsFormsApp.ViewModels
{
    public class HttpMethodViewModel
    {
        public string DisplayText { get; set;}
        public HttpMethod HttpMethod { get; set; }

        public HttpMethodViewModel(string displayText, HttpMethod httpMethod)
        {
            DisplayText = displayText;
            HttpMethod = httpMethod;
        }
    }
}
