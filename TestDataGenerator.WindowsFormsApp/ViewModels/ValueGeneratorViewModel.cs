﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestDataGenerator.Abstractions.Settings;
using TestDataGenerator.WindowsFormsApp.ViewModels;

namespace TestDataGenerator.WinFormApp.ViewModels
{
    public class ValueGeneratorViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public Control UserControl { get; private set;}
        public string GeneratorName { get; private set; }
        public Type GeneratorBoundType { get; private set; }

        public ValueGeneratorViewModel(string generatorName, Control userControl, 
            Type generatorType)
        {
            UserControl = userControl;
            GeneratorName = generatorName;
            GeneratorBoundType = generatorType;
        }

        public void OnPropertyChanged([CallerMemberName] string propName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
    }
}
