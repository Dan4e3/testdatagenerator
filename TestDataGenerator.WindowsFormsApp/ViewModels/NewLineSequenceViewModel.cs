﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.WindowsFormsApp.ViewModels
{
    public class NewLineSequenceViewModel
    {
        public string DisplayText { get; set; }
        public string NewLineSequence { get; set; }

        public NewLineSequenceViewModel(string displayText, string newlineSequence)
        {
            DisplayText = displayText;
            NewLineSequence = newlineSequence;
        }
    }
}
