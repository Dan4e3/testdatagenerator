﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.IO.DataSetWriters;
using TestDataGenerator.IO.DataSetWriters.Enums;

namespace TestDataGenerator.WindowsFormsApp.ViewModels.JsonExportParameters
{
    public class JsonExportParametersViewModel : INotifyPropertyChanged
    {
        private string _dateTimeStringFormat = null;
        private Formatting _formatting = Formatting.Indented;
        private DataSetJsonExportStyleViewModel _dataSetJsonExportStyleViewModel;

        public event PropertyChangedEventHandler PropertyChanged;

        public JsonExportParametersViewModel(DataSetJsonExportStyleViewModel exportStyleVm)
        {
            _dataSetJsonExportStyleViewModel = exportStyleVm;
        }

        public DataSetJsonExportStyleViewModel DatasetJsonExportStyle
        {
            get => _dataSetJsonExportStyleViewModel;
            set
            {
                if (_dataSetJsonExportStyleViewModel != value)
                {
                    _dataSetJsonExportStyleViewModel = value;
                    OnPropertyChanged();
                }
            }
        }

        public string DateTimeStringFormat
        {
            get { return _dateTimeStringFormat; }
            set
            {
                if (_dateTimeStringFormat != value)
                {
                    _dateTimeStringFormat = string.IsNullOrWhiteSpace(value) ? null : value;
                    OnPropertyChanged();
                }
            }
        }

        public Formatting Formatting
        {
            get { return _formatting; }
            set
            {
                if (_formatting != value)
                {
                    _formatting = value;
                    OnPropertyChanged();
                }
            }
        }

        public JsonDataSetWriterSettings GetJsonWriterSettings()
        {
            return new JsonDataSetWriterSettings() { 
                Formatting = this.Formatting,
                DateFormatString = this.DateTimeStringFormat
            };
        }

        private void OnPropertyChanged([CallerMemberName] string prop="")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }

    public class DataSetJsonExportStyleViewModel
    {
        public DatasetJsonExportStyle DatasetJsonExportStyle { get; private set; }
        public string DisplayName { get; private set; }

        public DataSetJsonExportStyleViewModel(string displayName, DatasetJsonExportStyle datasetJsonExportStyle)
        {
            DatasetJsonExportStyle = datasetJsonExportStyle;
            DisplayName = displayName;
        }
    }
}
