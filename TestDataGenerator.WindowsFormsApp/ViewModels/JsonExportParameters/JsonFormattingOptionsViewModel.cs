﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.WindowsFormsApp.ViewModels.JsonExportParameters
{
    public class JsonFormattingOptionsViewModel
    {
        public string DisplayName { get; private set; }
        public Formatting Formatting { get; private set; }

        public JsonFormattingOptionsViewModel(string displayName, Formatting formatting)
        {
            DisplayName = displayName;
            Formatting = formatting;
        }
    }
}
