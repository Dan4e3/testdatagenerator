﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces;
using TestDataGenerator.Abstractions.Interfaces.ValuesHandling;
using TestDataGenerator.Abstractions.Settings;
using TestDataGenerator.Core.Columns;
using TestDataGenerator.WinFormApp.Forms;

namespace TestDataGenerator.WinFormApp.ViewModels
{
    public class ColumnViewModel : INotifyPropertyChanged
    {
        private string _valueSupplierDisplayName = "Select...";

        public event PropertyChangedEventHandler PropertyChanged;
        public Column Column { get; private set; }

        public ColumnViewModel(Column column = null)
        {
            Column = column ?? new Column(new ColumnSettings());
            if (column?.ColumnSettings.ValueSupplier != null)
                ValueSupplierDisplayName = column.ColumnSettings.ValueSupplier.GetFriendlyName();
        }

        public string ColumnName
        {
            get { return Column.ColumnSettings.ColumnName; }
            set
            {
                if (Column.ColumnSettings.ColumnName != value)
                {
                    Column.ColumnSettings.ColumnName = value;
                    OnPropertyChanged();
                }
            }
        }

        public ValueTypeViewModel TargetValueType
        {
            get { return MainForm.AvailableColumnTypes.FirstOrDefault(x => x.Type == Column.ColumnSettings.TargetValueType); }
            set
            {
                if (Column.ColumnSettings.TargetValueType != value.Type)
                {
                    Column.ColumnSettings.TargetValueType = value.Type;
                    OnPropertyChanged();
                }
            }
        }

        public IValueGenerator ValueSupplier
        {
            get { return Column.ColumnSettings.ValueSupplier; }
            set
            {
                if (Column.ColumnSettings.ValueSupplier != value)
                {
                    Column.ColumnSettings.ValueSupplier = value;
                    ValueSupplierDisplayName = value.GetFriendlyName();
                    OnPropertyChanged();
                }
            }
        }

        public string ValueSupplierDisplayName
        {
            get { return _valueSupplierDisplayName; }
            set
            {
                if (_valueSupplierDisplayName != value)
                {
                    _valueSupplierDisplayName = value;
                    OnPropertyChanged();
                }
            }
        }

        private void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
