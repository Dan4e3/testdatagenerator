﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestDataGenerator.WindowsFormsApp.ViewModels
{
    public class ExternalSourceLoaderViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public Control UserControl { get; private set; }
        public string DisplayText { get; private set; }
        public Type BoundSourceLoaderType { get; private set; }

        public ExternalSourceLoaderViewModel(string displayText, Control userControl, Type boundSourceLoaderType)
        {
            DisplayText = displayText;
            UserControl = userControl;
            BoundSourceLoaderType = boundSourceLoaderType;
        }

        private void OnPropertyChanged([CallerMemberName] string prop="")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
