﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.IO;

namespace TestDataGenerator.WindowsFormsApp.ViewModels
{
    public class DatabaseProviderViewModel
    {
        public string DisplayName { get; set; }
        public SqlProvidersEnum Provider { get; set; }

        public DatabaseProviderViewModel(string displayName, SqlProvidersEnum provider)
        {
            DisplayName = displayName;
            Provider = provider;
        }
    }
}
