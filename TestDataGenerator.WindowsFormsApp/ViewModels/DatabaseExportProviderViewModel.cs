﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces.Writers;

namespace TestDataGenerator.WindowsFormsApp.ViewModels
{
    public class DatabaseExportProviderViewModel : INotifyPropertyChanged
    {
        private string _displayName;
        private IDbExportExecutor _executor;
        
        public event PropertyChangedEventHandler PropertyChanged;

        public DatabaseExportProviderViewModel(string providerDisplayName, IDbExportExecutor executor)
        {
            _displayName = providerDisplayName;
            _executor = executor;
        }

        public string DisplayName
        {
            get { return _displayName; }
            set
            {
                if (_displayName != value)
                {
                    _displayName = value;
                    OnPropertyChanged();
                }
            }
        }

        public IDbExportExecutor Executor
        {
            get { return _executor; }
            set
            {
                if (_executor != value)
                {
                    _executor = value;
                    OnPropertyChanged();
                }
            }
        }

        public string ConnectionString
        {
            get { return _executor.ConnectionString; }
            set
            {
                if (_executor.ConnectionString != value)
                {
                    _executor.ConnectionString = value;
                    OnPropertyChanged();
                }
            }
        }

        public string TableName
        {
            get { return _executor.TargetTableName; }
            set
            {
                if (_executor.TargetTableName != value)
                {
                    _executor.TargetTableName = value;
                    OnPropertyChanged();
                }
            }
        }

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
