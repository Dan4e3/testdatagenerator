﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces;
using TestDataGenerator.Core;
using TestDataGenerator.Core.Columns;

namespace TestDataGenerator.WinFormApp.ViewModels
{
    public class TestDatasetViewModel : INotifyPropertyChanged
    {
        private int _rowsToGenerate = 10;
        private BindingList<ColumnViewModel> _columnsViewModels;
        private ColumnViewModel _selectedColumn;
        private TestDataSet _dataSet;

        public event PropertyChangedEventHandler PropertyChanged;
        public TestDataSet DataSet
        {
            get
            {
                _dataSet.ColumnSet.Clear();
                foreach (var colViewModel in ColumnsViewModelsList)
                    _dataSet.ColumnSet.AddColumn(colViewModel.Column);
                return _dataSet;
            }
            set { _dataSet = value; }
        }

        public TestDatasetViewModel(IProgressReporter progressReporter)
        {
            _columnsViewModels = new BindingList<ColumnViewModel>();
            DataSet = new TestDataSet("Test", new ColumnSet(), progressReporter);
        }

        public BindingList<ColumnViewModel> ColumnsViewModelsList
        {
            get { return _columnsViewModels; }
            set
            {
                if (_columnsViewModels != value)
                {
                    _columnsViewModels = value;
                    OnPropertyChanged();
                }
            }
        }

        public int RowsToGenerate
        {
            get { return _rowsToGenerate; }
            set
            {
                if (_rowsToGenerate != value)
                {
                    _rowsToGenerate = value;
                    OnPropertyChanged();
                }
            }
        }

        public string DatasetName
        {
            get { return DataSet.Name; }
            set
            {
                if (DataSet.Name != value)
                {
                    DataSet.Name = value;
                    OnPropertyChanged();
                }
            }
        }

        public ColumnViewModel SelectedColumn
        {
            get { return _selectedColumn; }
            set
            {
                if (_selectedColumn != value)
                {
                    _selectedColumn = value;
                    OnPropertyChanged();
                    OnPropertyChanged(nameof(CanCreateValueGenerator));
                }
            }
        }

        public bool CanCreateValueGenerator
        {
            get { return SelectedColumn == null ? false : true; }
        }

        public void OnPropertyChanged([CallerMemberName] string propName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
    }
}
