﻿
namespace TestDataGenerator.WindowsFormsApp.UserControls
{
    partial class JsonExportParametersPanel
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.GeneralTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.FormattingLabel = new System.Windows.Forms.Label();
            this.FormattingComboBox = new System.Windows.Forms.ComboBox();
            this.DateTimeStringFormatLabel = new System.Windows.Forms.Label();
            this.DateTimeStringFormatTextBox = new System.Windows.Forms.TextBox();
            this.DatasetExportStyleLabel = new System.Windows.Forms.Label();
            this.DatasetExportStyleComboBox = new System.Windows.Forms.ComboBox();
            this.GeneralTableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // GeneralTableLayoutPanel
            // 
            this.GeneralTableLayoutPanel.ColumnCount = 2;
            this.GeneralTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 41.93548F));
            this.GeneralTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 58.06452F));
            this.GeneralTableLayoutPanel.Controls.Add(this.FormattingLabel, 0, 0);
            this.GeneralTableLayoutPanel.Controls.Add(this.FormattingComboBox, 1, 0);
            this.GeneralTableLayoutPanel.Controls.Add(this.DateTimeStringFormatLabel, 0, 1);
            this.GeneralTableLayoutPanel.Controls.Add(this.DateTimeStringFormatTextBox, 1, 1);
            this.GeneralTableLayoutPanel.Controls.Add(this.DatasetExportStyleLabel, 0, 2);
            this.GeneralTableLayoutPanel.Controls.Add(this.DatasetExportStyleComboBox, 1, 2);
            this.GeneralTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GeneralTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.GeneralTableLayoutPanel.Name = "GeneralTableLayoutPanel";
            this.GeneralTableLayoutPanel.RowCount = 4;
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 18.43318F));
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 17.97235F));
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 17.97235F));
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 45.16129F));
            this.GeneralTableLayoutPanel.Size = new System.Drawing.Size(310, 217);
            this.GeneralTableLayoutPanel.TabIndex = 0;
            // 
            // FormattingLabel
            // 
            this.FormattingLabel.AutoSize = true;
            this.FormattingLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FormattingLabel.Location = new System.Drawing.Point(3, 0);
            this.FormattingLabel.Name = "FormattingLabel";
            this.FormattingLabel.Size = new System.Drawing.Size(123, 40);
            this.FormattingLabel.TabIndex = 0;
            this.FormattingLabel.Text = "Formatting:";
            this.FormattingLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FormattingComboBox
            // 
            this.FormattingComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.FormattingComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.FormattingComboBox.FormattingEnabled = true;
            this.FormattingComboBox.Location = new System.Drawing.Point(132, 8);
            this.FormattingComboBox.Name = "FormattingComboBox";
            this.FormattingComboBox.Size = new System.Drawing.Size(175, 23);
            this.FormattingComboBox.TabIndex = 1;
            // 
            // DateTimeStringFormatLabel
            // 
            this.DateTimeStringFormatLabel.AutoSize = true;
            this.DateTimeStringFormatLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DateTimeStringFormatLabel.Location = new System.Drawing.Point(3, 40);
            this.DateTimeStringFormatLabel.Name = "DateTimeStringFormatLabel";
            this.DateTimeStringFormatLabel.Size = new System.Drawing.Size(123, 39);
            this.DateTimeStringFormatLabel.TabIndex = 2;
            this.DateTimeStringFormatLabel.Text = "DateTime string format:";
            this.DateTimeStringFormatLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // DateTimeStringFormatTextBox
            // 
            this.DateTimeStringFormatTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.DateTimeStringFormatTextBox.Location = new System.Drawing.Point(132, 48);
            this.DateTimeStringFormatTextBox.Name = "DateTimeStringFormatTextBox";
            this.DateTimeStringFormatTextBox.Size = new System.Drawing.Size(175, 23);
            this.DateTimeStringFormatTextBox.TabIndex = 3;
            // 
            // DatasetExportStyleLabel
            // 
            this.DatasetExportStyleLabel.AutoSize = true;
            this.DatasetExportStyleLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DatasetExportStyleLabel.Location = new System.Drawing.Point(3, 79);
            this.DatasetExportStyleLabel.Name = "DatasetExportStyleLabel";
            this.DatasetExportStyleLabel.Size = new System.Drawing.Size(123, 39);
            this.DatasetExportStyleLabel.TabIndex = 4;
            this.DatasetExportStyleLabel.Text = "Dataset export style:";
            this.DatasetExportStyleLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // DatasetExportStyleComboBox
            // 
            this.DatasetExportStyleComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.DatasetExportStyleComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.DatasetExportStyleComboBox.FormattingEnabled = true;
            this.DatasetExportStyleComboBox.Location = new System.Drawing.Point(132, 87);
            this.DatasetExportStyleComboBox.Name = "DatasetExportStyleComboBox";
            this.DatasetExportStyleComboBox.Size = new System.Drawing.Size(175, 23);
            this.DatasetExportStyleComboBox.TabIndex = 5;
            // 
            // JsonExportParametersPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.GeneralTableLayoutPanel);
            this.Name = "JsonExportParametersPanel";
            this.Size = new System.Drawing.Size(310, 217);
            this.GeneralTableLayoutPanel.ResumeLayout(false);
            this.GeneralTableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel GeneralTableLayoutPanel;
        private System.Windows.Forms.Label FormattingLabel;
        private System.Windows.Forms.ComboBox FormattingComboBox;
        private System.Windows.Forms.Label DateTimeStringFormatLabel;
        private System.Windows.Forms.TextBox DateTimeStringFormatTextBox;
        private System.Windows.Forms.Label DatasetExportStyleLabel;
        private System.Windows.Forms.ComboBox DatasetExportStyleComboBox;
    }
}
