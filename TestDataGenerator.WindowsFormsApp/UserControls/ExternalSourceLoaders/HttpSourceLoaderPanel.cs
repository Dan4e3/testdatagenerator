﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestDataGenerator.WindowsFormsApp.ViewModels;

namespace TestDataGenerator.WindowsFormsApp.UserControls.ExternalSourceLoaders
{

    public partial class HttpSourceLoaderPanel : UserControl
    {
        private readonly BindingList<HttpMethodViewModel> _httpMethods;

        public string ResourceUrl { get; private set; }
        public HttpMethod HttpMethod { get; private set; }
        public string[] SearchTokens { get; private set; }
        public string RequestJson { get; private set; }

        public HttpSourceLoaderPanel()
        {
            InitializeComponent();

            _httpMethods = new BindingList<HttpMethodViewModel>() { 
                new HttpMethodViewModel("GET", HttpMethod.Get),
                new HttpMethodViewModel("POST", HttpMethod.Post)
            };

            HttpRequestTypeComboBox.DataSource = _httpMethods;
            HttpRequestTypeComboBox.DisplayMember = nameof(HttpMethodViewModel.DisplayText);
        }

        private void ResourceUrlTextBox_TextChanged(object sender, EventArgs e)
        {
            ResourceUrl = ResourceUrlTextBox.Text;
        }

        private void JsonPathSearchTokensTextBox_TextChanged(object sender, EventArgs e)
        {
            SearchTokens = new string[] { JsonPathSearchTokensTextBox.Text };
        }

        private void RequestBodyRichTextBox_TextChanged(object sender, EventArgs e)
        {
            RequestJson = RequestBodyRichTextBox.Text;
        }

        private void HttpRequestTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            HttpMethodViewModel selectedVm = HttpRequestTypeComboBox.SelectedItem as HttpMethodViewModel;

            HttpMethod = selectedVm.HttpMethod;
        }
    }
}
