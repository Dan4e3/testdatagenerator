﻿
namespace TestDataGenerator.WindowsFormsApp.UserControls.ExternalSourceLoaders
{
    partial class HttpSourceLoaderPanel
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.GeneralTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.ResourceUrlTextBox = new System.Windows.Forms.TextBox();
            this.ResourceUrlLabel = new System.Windows.Forms.Label();
            this.JsonPathSearchTokensLabel = new System.Windows.Forms.Label();
            this.JsonPathSearchTokensTextBox = new System.Windows.Forms.TextBox();
            this.HttpRequestTypeLabel = new System.Windows.Forms.Label();
            this.HttpRequestTypeComboBox = new System.Windows.Forms.ComboBox();
            this.RequestBodyLabel = new System.Windows.Forms.Label();
            this.RequestBodyRichTextBox = new System.Windows.Forms.RichTextBox();
            this.GeneralTableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // GeneralTableLayoutPanel
            // 
            this.GeneralTableLayoutPanel.ColumnCount = 2;
            this.GeneralTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35.22168F));
            this.GeneralTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 64.77833F));
            this.GeneralTableLayoutPanel.Controls.Add(this.ResourceUrlTextBox, 1, 0);
            this.GeneralTableLayoutPanel.Controls.Add(this.ResourceUrlLabel, 0, 0);
            this.GeneralTableLayoutPanel.Controls.Add(this.JsonPathSearchTokensLabel, 0, 2);
            this.GeneralTableLayoutPanel.Controls.Add(this.JsonPathSearchTokensTextBox, 1, 2);
            this.GeneralTableLayoutPanel.Controls.Add(this.HttpRequestTypeLabel, 0, 1);
            this.GeneralTableLayoutPanel.Controls.Add(this.HttpRequestTypeComboBox, 1, 1);
            this.GeneralTableLayoutPanel.Controls.Add(this.RequestBodyLabel, 0, 3);
            this.GeneralTableLayoutPanel.Controls.Add(this.RequestBodyRichTextBox, 1, 3);
            this.GeneralTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GeneralTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.GeneralTableLayoutPanel.Name = "GeneralTableLayoutPanel";
            this.GeneralTableLayoutPanel.RowCount = 4;
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54F));
            this.GeneralTableLayoutPanel.Size = new System.Drawing.Size(406, 215);
            this.GeneralTableLayoutPanel.TabIndex = 0;
            // 
            // ResourceUrlTextBox
            // 
            this.ResourceUrlTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ResourceUrlTextBox.Location = new System.Drawing.Point(146, 4);
            this.ResourceUrlTextBox.Name = "ResourceUrlTextBox";
            this.ResourceUrlTextBox.Size = new System.Drawing.Size(257, 23);
            this.ResourceUrlTextBox.TabIndex = 0;
            this.ResourceUrlTextBox.TextChanged += new System.EventHandler(this.ResourceUrlTextBox_TextChanged);
            // 
            // ResourceUrlLabel
            // 
            this.ResourceUrlLabel.AutoSize = true;
            this.ResourceUrlLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ResourceUrlLabel.Location = new System.Drawing.Point(3, 0);
            this.ResourceUrlLabel.Name = "ResourceUrlLabel";
            this.ResourceUrlLabel.Size = new System.Drawing.Size(137, 31);
            this.ResourceUrlLabel.TabIndex = 1;
            this.ResourceUrlLabel.Text = "Resource URL:";
            this.ResourceUrlLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // JsonPathSearchTokensLabel
            // 
            this.JsonPathSearchTokensLabel.AutoSize = true;
            this.JsonPathSearchTokensLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.JsonPathSearchTokensLabel.Location = new System.Drawing.Point(3, 62);
            this.JsonPathSearchTokensLabel.Name = "JsonPathSearchTokensLabel";
            this.JsonPathSearchTokensLabel.Size = new System.Drawing.Size(137, 33);
            this.JsonPathSearchTokensLabel.TabIndex = 2;
            this.JsonPathSearchTokensLabel.Text = "JSONPath search tokens:";
            this.JsonPathSearchTokensLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // JsonPathSearchTokensTextBox
            // 
            this.JsonPathSearchTokensTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.JsonPathSearchTokensTextBox.Location = new System.Drawing.Point(146, 67);
            this.JsonPathSearchTokensTextBox.Name = "JsonPathSearchTokensTextBox";
            this.JsonPathSearchTokensTextBox.Size = new System.Drawing.Size(257, 23);
            this.JsonPathSearchTokensTextBox.TabIndex = 3;
            this.JsonPathSearchTokensTextBox.TextChanged += new System.EventHandler(this.JsonPathSearchTokensTextBox_TextChanged);
            // 
            // HttpRequestTypeLabel
            // 
            this.HttpRequestTypeLabel.AutoSize = true;
            this.HttpRequestTypeLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HttpRequestTypeLabel.Location = new System.Drawing.Point(3, 31);
            this.HttpRequestTypeLabel.Name = "HttpRequestTypeLabel";
            this.HttpRequestTypeLabel.Size = new System.Drawing.Size(137, 31);
            this.HttpRequestTypeLabel.TabIndex = 4;
            this.HttpRequestTypeLabel.Text = "HTTP request type:";
            this.HttpRequestTypeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // HttpRequestTypeComboBox
            // 
            this.HttpRequestTypeComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.HttpRequestTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.HttpRequestTypeComboBox.FormattingEnabled = true;
            this.HttpRequestTypeComboBox.Location = new System.Drawing.Point(146, 35);
            this.HttpRequestTypeComboBox.Name = "HttpRequestTypeComboBox";
            this.HttpRequestTypeComboBox.Size = new System.Drawing.Size(257, 23);
            this.HttpRequestTypeComboBox.TabIndex = 5;
            this.HttpRequestTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.HttpRequestTypeComboBox_SelectedIndexChanged);
            // 
            // RequestBodyLabel
            // 
            this.RequestBodyLabel.AutoSize = true;
            this.RequestBodyLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RequestBodyLabel.Location = new System.Drawing.Point(3, 95);
            this.RequestBodyLabel.Name = "RequestBodyLabel";
            this.RequestBodyLabel.Size = new System.Drawing.Size(137, 120);
            this.RequestBodyLabel.TabIndex = 6;
            this.RequestBodyLabel.Text = "Request JSON body:";
            this.RequestBodyLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // RequestBodyRichTextBox
            // 
            this.RequestBodyRichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RequestBodyRichTextBox.Location = new System.Drawing.Point(146, 98);
            this.RequestBodyRichTextBox.Name = "RequestBodyRichTextBox";
            this.RequestBodyRichTextBox.Size = new System.Drawing.Size(257, 114);
            this.RequestBodyRichTextBox.TabIndex = 7;
            this.RequestBodyRichTextBox.Text = "";
            this.RequestBodyRichTextBox.TextChanged += new System.EventHandler(this.RequestBodyRichTextBox_TextChanged);
            // 
            // HttpSourceLoaderPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.GeneralTableLayoutPanel);
            this.Name = "HttpSourceLoaderPanel";
            this.Size = new System.Drawing.Size(406, 215);
            this.GeneralTableLayoutPanel.ResumeLayout(false);
            this.GeneralTableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel GeneralTableLayoutPanel;
        private System.Windows.Forms.TextBox ResourceUrlTextBox;
        private System.Windows.Forms.Label ResourceUrlLabel;
        private System.Windows.Forms.Label JsonPathSearchTokensLabel;
        private System.Windows.Forms.TextBox JsonPathSearchTokensTextBox;
        private System.Windows.Forms.Label HttpRequestTypeLabel;
        private System.Windows.Forms.ComboBox HttpRequestTypeComboBox;
        private System.Windows.Forms.Label RequestBodyLabel;
        private System.Windows.Forms.RichTextBox RequestBodyRichTextBox;
    }
}
