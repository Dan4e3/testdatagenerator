﻿
namespace TestDataGenerator.WindowsFormsApp.UserControls.ExternalSourceLoaders
{
    partial class FileSourceLoaderPanel
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.GeneralTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.FilePathLabel = new System.Windows.Forms.Label();
            this.NewlineSequenceLabel = new System.Windows.Forms.Label();
            this.NewlineSequenceComboBox = new System.Windows.Forms.ComboBox();
            this.FileEncodingLabel = new System.Windows.Forms.Label();
            this.FileEncodingComboBox = new System.Windows.Forms.ComboBox();
            this.FilePathTextBox = new System.Windows.Forms.TextBox();
            this.GeneralTableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // GeneralTableLayoutPanel
            // 
            this.GeneralTableLayoutPanel.ColumnCount = 2;
            this.GeneralTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 36.30573F));
            this.GeneralTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 63.69427F));
            this.GeneralTableLayoutPanel.Controls.Add(this.FilePathLabel, 0, 0);
            this.GeneralTableLayoutPanel.Controls.Add(this.NewlineSequenceLabel, 0, 1);
            this.GeneralTableLayoutPanel.Controls.Add(this.NewlineSequenceComboBox, 1, 1);
            this.GeneralTableLayoutPanel.Controls.Add(this.FileEncodingLabel, 0, 2);
            this.GeneralTableLayoutPanel.Controls.Add(this.FileEncodingComboBox, 1, 2);
            this.GeneralTableLayoutPanel.Controls.Add(this.FilePathTextBox, 1, 0);
            this.GeneralTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GeneralTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.GeneralTableLayoutPanel.Name = "GeneralTableLayoutPanel";
            this.GeneralTableLayoutPanel.RowCount = 4;
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 13F));
            this.GeneralTableLayoutPanel.Size = new System.Drawing.Size(314, 115);
            this.GeneralTableLayoutPanel.TabIndex = 0;
            // 
            // FilePathLabel
            // 
            this.FilePathLabel.AutoSize = true;
            this.FilePathLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FilePathLabel.Location = new System.Drawing.Point(3, 0);
            this.FilePathLabel.Name = "FilePathLabel";
            this.FilePathLabel.Size = new System.Drawing.Size(107, 31);
            this.FilePathLabel.TabIndex = 0;
            this.FilePathLabel.Text = "Path to file:";
            this.FilePathLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // NewlineSequenceLabel
            // 
            this.NewlineSequenceLabel.AutoSize = true;
            this.NewlineSequenceLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.NewlineSequenceLabel.Location = new System.Drawing.Point(3, 31);
            this.NewlineSequenceLabel.Name = "NewlineSequenceLabel";
            this.NewlineSequenceLabel.Size = new System.Drawing.Size(107, 30);
            this.NewlineSequenceLabel.TabIndex = 2;
            this.NewlineSequenceLabel.Text = "Newline sequence:";
            this.NewlineSequenceLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // NewlineSequenceComboBox
            // 
            this.NewlineSequenceComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.NewlineSequenceComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.NewlineSequenceComboBox.FormattingEnabled = true;
            this.NewlineSequenceComboBox.Location = new System.Drawing.Point(116, 34);
            this.NewlineSequenceComboBox.Name = "NewlineSequenceComboBox";
            this.NewlineSequenceComboBox.Size = new System.Drawing.Size(195, 23);
            this.NewlineSequenceComboBox.TabIndex = 3;
            this.NewlineSequenceComboBox.SelectedIndexChanged += new System.EventHandler(this.NewlineSequenceComboBox_SelectedIndexChanged);
            // 
            // FileEncodingLabel
            // 
            this.FileEncodingLabel.AutoSize = true;
            this.FileEncodingLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FileEncodingLabel.Location = new System.Drawing.Point(3, 61);
            this.FileEncodingLabel.Name = "FileEncodingLabel";
            this.FileEncodingLabel.Size = new System.Drawing.Size(107, 28);
            this.FileEncodingLabel.TabIndex = 4;
            this.FileEncodingLabel.Text = "File encoding:";
            this.FileEncodingLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FileEncodingComboBox
            // 
            this.FileEncodingComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.FileEncodingComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.FileEncodingComboBox.FormattingEnabled = true;
            this.FileEncodingComboBox.Location = new System.Drawing.Point(116, 64);
            this.FileEncodingComboBox.Name = "FileEncodingComboBox";
            this.FileEncodingComboBox.Size = new System.Drawing.Size(195, 23);
            this.FileEncodingComboBox.TabIndex = 5;
            this.FileEncodingComboBox.SelectedIndexChanged += new System.EventHandler(this.FileEncodingComboBox_SelectedIndexChanged);
            // 
            // FilePathTextBox
            // 
            this.FilePathTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.FilePathTextBox.Location = new System.Drawing.Point(116, 4);
            this.FilePathTextBox.Name = "FilePathTextBox";
            this.FilePathTextBox.PlaceholderText = "Click to browse...";
            this.FilePathTextBox.ReadOnly = true;
            this.FilePathTextBox.Size = new System.Drawing.Size(195, 23);
            this.FilePathTextBox.TabIndex = 1;
            this.FilePathTextBox.Click += new System.EventHandler(this.FilePathTextBox_Click);
            // 
            // FileSourceLoaderPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.GeneralTableLayoutPanel);
            this.Name = "FileSourceLoaderPanel";
            this.Size = new System.Drawing.Size(314, 115);
            this.GeneralTableLayoutPanel.ResumeLayout(false);
            this.GeneralTableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel GeneralTableLayoutPanel;
        private System.Windows.Forms.Label FilePathLabel;
        private System.Windows.Forms.TextBox FilePathTextBox;
        private System.Windows.Forms.Label NewlineSequenceLabel;
        private System.Windows.Forms.ComboBox NewlineSequenceComboBox;
        private System.Windows.Forms.Label FileEncodingLabel;
        private System.Windows.Forms.ComboBox FileEncodingComboBox;
    }
}
