﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestDataGenerator.IO;
using TestDataGenerator.WindowsFormsApp.ViewModels;

namespace TestDataGenerator.WindowsFormsApp.UserControls.ExternalSourceLoaders
{
    public partial class DatabaseSourceLoaderPanel : UserControl
    {
        private BindingList<DatabaseProviderViewModel> _availableProviders;

        public SqlProvidersEnum Provider { get; set; }
        public string ConnectionString { get; set; }
        public string TableName { get; set; }
        public string TableColumn { get; set; }

        public DatabaseSourceLoaderPanel()
        {
            InitializeComponent();

            _availableProviders = new BindingList<DatabaseProviderViewModel>() { 
                new DatabaseProviderViewModel("SQL Server", SqlProvidersEnum.SqlServer),
                new DatabaseProviderViewModel("PostgreSQL", SqlProvidersEnum.PostgreSQL)
            };
            SqlProviderComboBox.DataSource = _availableProviders;
            SqlProviderComboBox.DisplayMember = nameof(DatabaseProviderViewModel.DisplayName);
        }

        private void SqlProviderComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            DatabaseProviderViewModel selectedVm = SqlProviderComboBox.SelectedItem as DatabaseProviderViewModel;
            Provider = selectedVm.Provider;
        }

        private void ConnectionStringTextBox_TextChanged(object sender, EventArgs e)
        {
            ConnectionString = ConnectionStringTextBox.Text;
        }

        private void TableNameTextBox_TextChanged(object sender, EventArgs e)
        {
            TableName = TableNameTextBox.Text;
        }

        private void TableColumnTextBox_TextChanged(object sender, EventArgs e)
        {
            TableColumn = TableColumnTextBox.Text;
        }
    }
}
