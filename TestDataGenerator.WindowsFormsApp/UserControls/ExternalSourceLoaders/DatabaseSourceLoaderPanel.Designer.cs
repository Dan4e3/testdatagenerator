﻿
namespace TestDataGenerator.WindowsFormsApp.UserControls.ExternalSourceLoaders
{
    partial class DatabaseSourceLoaderPanel
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.GeneralTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.SqlProviderComboBox = new System.Windows.Forms.ComboBox();
            this.SqlProviderLabel = new System.Windows.Forms.Label();
            this.ConnectionStringLabel = new System.Windows.Forms.Label();
            this.ConnectionStringTextBox = new System.Windows.Forms.TextBox();
            this.TableNameLabel = new System.Windows.Forms.Label();
            this.TableNameTextBox = new System.Windows.Forms.TextBox();
            this.TableColumnLabel = new System.Windows.Forms.Label();
            this.TableColumnTextBox = new System.Windows.Forms.TextBox();
            this.GeneralTableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // GeneralTableLayoutPanel
            // 
            this.GeneralTableLayoutPanel.ColumnCount = 2;
            this.GeneralTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 31.17978F));
            this.GeneralTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 68.82022F));
            this.GeneralTableLayoutPanel.Controls.Add(this.SqlProviderComboBox, 1, 0);
            this.GeneralTableLayoutPanel.Controls.Add(this.SqlProviderLabel, 0, 0);
            this.GeneralTableLayoutPanel.Controls.Add(this.ConnectionStringLabel, 0, 1);
            this.GeneralTableLayoutPanel.Controls.Add(this.ConnectionStringTextBox, 1, 1);
            this.GeneralTableLayoutPanel.Controls.Add(this.TableNameLabel, 0, 2);
            this.GeneralTableLayoutPanel.Controls.Add(this.TableNameTextBox, 1, 2);
            this.GeneralTableLayoutPanel.Controls.Add(this.TableColumnLabel, 0, 3);
            this.GeneralTableLayoutPanel.Controls.Add(this.TableColumnTextBox, 1, 3);
            this.GeneralTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GeneralTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.GeneralTableLayoutPanel.Name = "GeneralTableLayoutPanel";
            this.GeneralTableLayoutPanel.RowCount = 4;
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37F));
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 11F));
            this.GeneralTableLayoutPanel.Size = new System.Drawing.Size(356, 142);
            this.GeneralTableLayoutPanel.TabIndex = 0;
            // 
            // SqlProviderComboBox
            // 
            this.SqlProviderComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.SqlProviderComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SqlProviderComboBox.FormattingEnabled = true;
            this.SqlProviderComboBox.Location = new System.Drawing.Point(114, 7);
            this.SqlProviderComboBox.Name = "SqlProviderComboBox";
            this.SqlProviderComboBox.Size = new System.Drawing.Size(239, 23);
            this.SqlProviderComboBox.TabIndex = 0;
            this.SqlProviderComboBox.SelectedIndexChanged += new System.EventHandler(this.SqlProviderComboBox_SelectedIndexChanged);
            // 
            // SqlProviderLabel
            // 
            this.SqlProviderLabel.AutoSize = true;
            this.SqlProviderLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SqlProviderLabel.Location = new System.Drawing.Point(3, 0);
            this.SqlProviderLabel.Name = "SqlProviderLabel";
            this.SqlProviderLabel.Size = new System.Drawing.Size(105, 37);
            this.SqlProviderLabel.TabIndex = 1;
            this.SqlProviderLabel.Text = "SQL provider:";
            this.SqlProviderLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ConnectionStringLabel
            // 
            this.ConnectionStringLabel.AutoSize = true;
            this.ConnectionStringLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ConnectionStringLabel.Location = new System.Drawing.Point(3, 37);
            this.ConnectionStringLabel.Name = "ConnectionStringLabel";
            this.ConnectionStringLabel.Size = new System.Drawing.Size(105, 38);
            this.ConnectionStringLabel.TabIndex = 2;
            this.ConnectionStringLabel.Text = "Connection string:";
            this.ConnectionStringLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ConnectionStringTextBox
            // 
            this.ConnectionStringTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ConnectionStringTextBox.Location = new System.Drawing.Point(114, 44);
            this.ConnectionStringTextBox.Name = "ConnectionStringTextBox";
            this.ConnectionStringTextBox.Size = new System.Drawing.Size(239, 23);
            this.ConnectionStringTextBox.TabIndex = 3;
            this.ConnectionStringTextBox.TextChanged += new System.EventHandler(this.ConnectionStringTextBox_TextChanged);
            // 
            // TableNameLabel
            // 
            this.TableNameLabel.AutoSize = true;
            this.TableNameLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TableNameLabel.Location = new System.Drawing.Point(3, 75);
            this.TableNameLabel.Name = "TableNameLabel";
            this.TableNameLabel.Size = new System.Drawing.Size(105, 34);
            this.TableNameLabel.TabIndex = 4;
            this.TableNameLabel.Text = "Table name:";
            this.TableNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // TableNameTextBox
            // 
            this.TableNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.TableNameTextBox.Location = new System.Drawing.Point(114, 80);
            this.TableNameTextBox.Name = "TableNameTextBox";
            this.TableNameTextBox.Size = new System.Drawing.Size(239, 23);
            this.TableNameTextBox.TabIndex = 5;
            this.TableNameTextBox.TextChanged += new System.EventHandler(this.TableNameTextBox_TextChanged);
            // 
            // TableColumnLabel
            // 
            this.TableColumnLabel.AutoSize = true;
            this.TableColumnLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TableColumnLabel.Location = new System.Drawing.Point(3, 109);
            this.TableColumnLabel.Name = "TableColumnLabel";
            this.TableColumnLabel.Size = new System.Drawing.Size(105, 33);
            this.TableColumnLabel.TabIndex = 6;
            this.TableColumnLabel.Text = "Table column:";
            this.TableColumnLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // TableColumnTextBox
            // 
            this.TableColumnTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.TableColumnTextBox.Location = new System.Drawing.Point(114, 114);
            this.TableColumnTextBox.Name = "TableColumnTextBox";
            this.TableColumnTextBox.Size = new System.Drawing.Size(239, 23);
            this.TableColumnTextBox.TabIndex = 7;
            this.TableColumnTextBox.TextChanged += new System.EventHandler(this.TableColumnTextBox_TextChanged);
            // 
            // DatabaseSourceLoaderPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.GeneralTableLayoutPanel);
            this.Name = "DatabaseSourceLoaderPanel";
            this.Size = new System.Drawing.Size(356, 142);
            this.GeneralTableLayoutPanel.ResumeLayout(false);
            this.GeneralTableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel GeneralTableLayoutPanel;
        private System.Windows.Forms.ComboBox SqlProviderComboBox;
        private System.Windows.Forms.Label SqlProviderLabel;
        private System.Windows.Forms.Label ConnectionStringLabel;
        private System.Windows.Forms.TextBox ConnectionStringTextBox;
        private System.Windows.Forms.Label TableNameLabel;
        private System.Windows.Forms.TextBox TableNameTextBox;
        private System.Windows.Forms.Label TableColumnLabel;
        private System.Windows.Forms.TextBox TableColumnTextBox;
    }
}
