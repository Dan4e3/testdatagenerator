﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestDataGenerator.WindowsFormsApp.ViewModels;

namespace TestDataGenerator.WindowsFormsApp.UserControls.ExternalSourceLoaders
{
    public partial class FileSourceLoaderPanel : UserControl
    {
        private readonly BindingList<NewLineSequenceViewModel> _newlineSequences;
        private readonly BindingList<EncodingInfo> _avilableEncodingInfos;

        public string PathToFile { get; set; }
        public string NewLineSequence { get; set; }
        public Encoding FileEncoding { get; set; }

        public FileSourceLoaderPanel()
        {
            InitializeComponent();

            _newlineSequences = new BindingList<NewLineSequenceViewModel>() { 
                new NewLineSequenceViewModel("CRLF", "\r\n"),
                new NewLineSequenceViewModel("LF", "\n")
            };

            _avilableEncodingInfos = new BindingList<EncodingInfo>(Encoding.GetEncodings());

            NewlineSequenceComboBox.DataSource = _newlineSequences;
            NewlineSequenceComboBox.DisplayMember = nameof(NewLineSequenceViewModel.DisplayText);

            FileEncodingComboBox.DataSource = _avilableEncodingInfos;
            FileEncodingComboBox.DisplayMember = nameof(EncodingInfo.DisplayName);
            FileEncodingComboBox.SelectedItem = _avilableEncodingInfos.First(enc => enc.DisplayName == Encoding.UTF8.EncodingName);
        }

        private void FilePathTextBox_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog ofd = new OpenFileDialog())
            {
                ofd.Filter = "Text|*.txt|All|*.*";
                DialogResult res = ofd.ShowDialog(this);

                if (res == DialogResult.Cancel)
                    return;

                PathToFile = ofd.FileName;
                FilePathTextBox.Text = ofd.FileName;
                return;
            }
        }

        private void NewlineSequenceComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedVm = NewlineSequenceComboBox.SelectedItem as NewLineSequenceViewModel;
            NewLineSequence = selectedVm.NewLineSequence;
        }

        private void FileEncodingComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedVm = FileEncodingComboBox.SelectedItem as EncodingInfo;
            FileEncoding = selectedVm.GetEncoding();
        }
    }
}
