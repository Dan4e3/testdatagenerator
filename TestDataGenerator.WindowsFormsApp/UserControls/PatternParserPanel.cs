﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestDataGenerator.Abstractions.Interfaces;
using TestDataGenerator.Abstractions.Interfaces.ValuesHandling;
using TestDataGenerator.Core.ValueGenerators.ValuePatternParser;

namespace TestDataGenerator.WinFormApp.UserControls
{
    public partial class PatternParserPanel : UserControl
    {
        public string ValuePattern { get; private set; }

        public PatternParserPanel(IValueGenerator existingGenerator)
        {
            InitializeComponent();

            if (existingGenerator is ValuePatternParser patternParser && patternParser != null)
            {
                ValuePatternTextBox.Text = patternParser.ValuePattern;
            }
        }

        private void ValuePatternTextBox_TextChanged(object sender, EventArgs e)
        {
            ValuePattern = ValuePatternTextBox.Text;
        }
    }
}
