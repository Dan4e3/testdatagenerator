﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestDataGenerator.IO.DataSetWriters.Enums;
using TestDataGenerator.WindowsFormsApp.ViewModels.JsonExportParameters;

namespace TestDataGenerator.WindowsFormsApp.UserControls
{
    public partial class JsonExportParametersPanel : UserControl
    {
        private BindingList<JsonFormattingOptionsViewModel> _availableFormattingOptions;
        private BindingList<DataSetJsonExportStyleViewModel> _availableExportStyleOptions;

        public JsonExportParametersViewModel ExportParameters { get; private set; }

        public JsonExportParametersPanel()
        {
            InitializeComponent();

            _availableFormattingOptions = new BindingList<JsonFormattingOptionsViewModel>() { 
                new JsonFormattingOptionsViewModel("Indented", Formatting.Indented),
                new JsonFormattingOptionsViewModel("Unindented", Formatting.None)
            };

            _availableExportStyleOptions = new BindingList<DataSetJsonExportStyleViewModel>() {
                new DataSetJsonExportStyleViewModel("Array of objects, one value per column-property", DatasetJsonExportStyle.ArrayOfObjectsWithColumnsAsProperties),
                new DataSetJsonExportStyleViewModel("One object, array of values per column-property", DatasetJsonExportStyle.SingleObjectWithArrayOfValuesPerProperty)
            };

            ExportParameters = new JsonExportParametersViewModel(_availableExportStyleOptions.First());

            FormattingComboBox.DataSource = _availableFormattingOptions;
            FormattingComboBox.DisplayMember = nameof(JsonFormattingOptionsViewModel.DisplayName);
            FormattingComboBox.ValueMember = nameof(JsonFormattingOptionsViewModel.Formatting);
            FormattingComboBox.DataBindings.Add(nameof(FormattingComboBox.SelectedValue), ExportParameters, nameof(ExportParameters.Formatting));

            DateTimeStringFormatTextBox.DataBindings.Add("Text", ExportParameters, nameof(ExportParameters.DateTimeStringFormat));

            DatasetExportStyleComboBox.DataSource = _availableExportStyleOptions;
            DatasetExportStyleComboBox.DisplayMember = nameof(DataSetJsonExportStyleViewModel.DisplayName);
            DatasetExportStyleComboBox.DataBindings.Add("SelectedItem", ExportParameters, nameof(ExportParameters.DatasetJsonExportStyle));
        }
    }
}
