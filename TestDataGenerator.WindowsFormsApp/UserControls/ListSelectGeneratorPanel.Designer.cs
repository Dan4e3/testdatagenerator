﻿
namespace TestDataGenerator.WinFormApp.UserControls
{
    partial class ListSelectGeneratorPanel
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.GeneralTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.InsertValueTextBox = new System.Windows.Forms.TextBox();
            this.AddValueButton = new System.Windows.Forms.Button();
            this.RemoveValueButton = new System.Windows.Forms.Button();
            this.ValuesListBox = new System.Windows.Forms.ListBox();
            this.LoadFromSourceButton = new System.Windows.Forms.Button();
            this.ClearAllItemsButton = new System.Windows.Forms.Button();
            this.GeneralTableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // GeneralTableLayoutPanel
            // 
            this.GeneralTableLayoutPanel.ColumnCount = 2;
            this.GeneralTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 232F));
            this.GeneralTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 143F));
            this.GeneralTableLayoutPanel.Controls.Add(this.InsertValueTextBox, 0, 0);
            this.GeneralTableLayoutPanel.Controls.Add(this.AddValueButton, 1, 0);
            this.GeneralTableLayoutPanel.Controls.Add(this.RemoveValueButton, 1, 1);
            this.GeneralTableLayoutPanel.Controls.Add(this.ValuesListBox, 0, 1);
            this.GeneralTableLayoutPanel.Controls.Add(this.LoadFromSourceButton, 1, 3);
            this.GeneralTableLayoutPanel.Controls.Add(this.ClearAllItemsButton, 1, 2);
            this.GeneralTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GeneralTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.GeneralTableLayoutPanel.Name = "GeneralTableLayoutPanel";
            this.GeneralTableLayoutPanel.RowCount = 4;
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.GeneralTableLayoutPanel.Size = new System.Drawing.Size(375, 201);
            this.GeneralTableLayoutPanel.TabIndex = 0;
            // 
            // InsertValueTextBox
            // 
            this.InsertValueTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.InsertValueTextBox.Location = new System.Drawing.Point(3, 6);
            this.InsertValueTextBox.Name = "InsertValueTextBox";
            this.InsertValueTextBox.Size = new System.Drawing.Size(226, 23);
            this.InsertValueTextBox.TabIndex = 1;
            this.InsertValueTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.InsertValueTextBox_KeyDown);
            // 
            // AddValueButton
            // 
            this.AddValueButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AddValueButton.Location = new System.Drawing.Point(235, 3);
            this.AddValueButton.Name = "AddValueButton";
            this.AddValueButton.Size = new System.Drawing.Size(137, 30);
            this.AddValueButton.TabIndex = 3;
            this.AddValueButton.Text = "Add value";
            this.AddValueButton.UseVisualStyleBackColor = true;
            this.AddValueButton.Click += new System.EventHandler(this.AddValueButton_Click);
            // 
            // RemoveValueButton
            // 
            this.RemoveValueButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RemoveValueButton.Location = new System.Drawing.Point(235, 39);
            this.RemoveValueButton.Name = "RemoveValueButton";
            this.RemoveValueButton.Size = new System.Drawing.Size(137, 29);
            this.RemoveValueButton.TabIndex = 4;
            this.RemoveValueButton.Text = "Remove value";
            this.RemoveValueButton.UseVisualStyleBackColor = true;
            this.RemoveValueButton.Click += new System.EventHandler(this.RemoveValueButton_Click);
            // 
            // ValuesListBox
            // 
            this.ValuesListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ValuesListBox.FormattingEnabled = true;
            this.ValuesListBox.ItemHeight = 15;
            this.ValuesListBox.Location = new System.Drawing.Point(3, 39);
            this.ValuesListBox.Name = "ValuesListBox";
            this.GeneralTableLayoutPanel.SetRowSpan(this.ValuesListBox, 3);
            this.ValuesListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.ValuesListBox.Size = new System.Drawing.Size(226, 159);
            this.ValuesListBox.TabIndex = 2;
            // 
            // LoadFromSourceButton
            // 
            this.LoadFromSourceButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.LoadFromSourceButton.Location = new System.Drawing.Point(235, 166);
            this.LoadFromSourceButton.Name = "LoadFromSourceButton";
            this.LoadFromSourceButton.Size = new System.Drawing.Size(137, 32);
            this.LoadFromSourceButton.TabIndex = 5;
            this.LoadFromSourceButton.Text = "Load from source...";
            this.LoadFromSourceButton.UseVisualStyleBackColor = true;
            this.LoadFromSourceButton.Click += new System.EventHandler(this.LoadFromSourceButton_Click);
            // 
            // ClearAllItemsButton
            // 
            this.ClearAllItemsButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ClearAllItemsButton.Location = new System.Drawing.Point(235, 74);
            this.ClearAllItemsButton.Name = "ClearAllItemsButton";
            this.ClearAllItemsButton.Size = new System.Drawing.Size(137, 29);
            this.ClearAllItemsButton.TabIndex = 6;
            this.ClearAllItemsButton.Text = "Clear all";
            this.ClearAllItemsButton.UseVisualStyleBackColor = true;
            this.ClearAllItemsButton.Click += new System.EventHandler(this.ClearAllItemsButton_Click);
            // 
            // ListSelectGeneratorPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.GeneralTableLayoutPanel);
            this.Name = "ListSelectGeneratorPanel";
            this.Size = new System.Drawing.Size(375, 201);
            this.GeneralTableLayoutPanel.ResumeLayout(false);
            this.GeneralTableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel GeneralTableLayoutPanel;
        private System.Windows.Forms.TextBox InsertValueTextBox;
        private System.Windows.Forms.ListBox ValuesListBox;
        private System.Windows.Forms.Button AddValueButton;
        private System.Windows.Forms.Button RemoveValueButton;
        private System.Windows.Forms.Button LoadFromSourceButton;
        private System.Windows.Forms.Button ClearAllItemsButton;
    }
}
