﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestDataGenerator.Core.ValueGenerators.IncrementalGenerator;
using TestDataGenerator.WinFormApp.ViewModels;

namespace TestDataGenerator.WindowsFormsApp.UserControls
{
    public partial class IncrementalGeneratorPanel : UserControl, INotifyPropertyChanged
    {
        private ColumnViewModel _colViewModel;
        private string _errorText;
        private TypeConverter _typeConverter;
        private bool _isStartValueValid = true;
        private bool _isEndValueValid = true;
        private bool _isValueStepValid = true;

        public event PropertyChangedEventHandler PropertyChanged;
        public object StartValue { get; private set; }
        public object EndValue { get; private set; }
        public object ValueStep { get; private set; }

        public string ErrorText
        {
            get { return _errorText; }
            set
            {
                if (_errorText != value)
                {
                    _errorText = value;
                    OnPropertyChanged();
                }
            }
        }

        public IncrementalGeneratorPanel(ColumnViewModel colViewModel)
        {
            InitializeComponent();

            _colViewModel = colViewModel;
            _typeConverter = TypeDescriptor.GetConverter(_colViewModel.TargetValueType.Type);
            if (_colViewModel?.ValueSupplier is IncrementalValueGenerator generator && generator != null)
            {
                StartValueTextBox.Text = _colViewModel.Column.GetStringValueRepresentation(generator.ValueIncrement.GetStartValue<object>());
                EndValueTextBox.Text = _colViewModel.Column.GetStringValueRepresentation(generator.ValueIncrement.GetEndValue<object>());
                ValueStepTextBox.Text = _colViewModel.Column.GetStringValueRepresentation(generator.ValueIncrement.GetValueStep<object>());
            }
        }

        private void OnPropertyChanged([CallerMemberName] string prop="")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }

        private void StartValueTextBox_TextChanged(object sender, EventArgs e)
        {
            string startValueText = StartValueTextBox.Text;
            try
            {
                object convertedRes = _typeConverter.ConvertFromString(null, _colViewModel.Column.ColumnSettings.Culture, startValueText);
                StartValue = convertedRes;
                _isStartValueValid = true;
            }
            catch
            {
                _isStartValueValid = false;
            }
            finally
            {
                UpdateErrorText();
            }
        }

        private void EndValueTextBox_TextChanged(object sender, EventArgs e)
        {
            string endValueText = EndValueTextBox.Text;
            try
            {
                object convertedRes = _typeConverter.ConvertFromString(null, _colViewModel.Column.ColumnSettings.Culture, endValueText);
                EndValue = convertedRes;
                _isEndValueValid = true;
            }
            catch
            {
                _isEndValueValid = false;
            }
            finally
            {
                UpdateErrorText();
            }
        }

        private void ValueStepTextBox_TextChanged(object sender, EventArgs e)
        {
            string valueStepText = ValueStepTextBox.Text;
            try
            {
                object convertedRes;
                if (_colViewModel.TargetValueType.Type == typeof(DateTime))
                    convertedRes = TypeDescriptor.GetConverter(typeof(TimeSpan)).ConvertFromString(null, _colViewModel.Column.ColumnSettings.Culture, valueStepText);
                else
                    convertedRes = _typeConverter.ConvertFromString(null, _colViewModel.Column.ColumnSettings.Culture, valueStepText);
                ValueStep = convertedRes;
                _isValueStepValid = true;
            }
            catch
            {
                _isValueStepValid = false;
            }
            finally
            {
                UpdateErrorText();
            }
        }

        private void UpdateErrorText()
        {
            StringBuilder sb = new StringBuilder();

            if (!_isStartValueValid)
                sb.Append($"Start value can't be converted to target type {_colViewModel.TargetValueType.Type}");

            if (!_isEndValueValid)
                sb.Append($"End value can't be converted to target type {_colViewModel.TargetValueType.Type}");

            if (!_isValueStepValid)
                sb.Append($"Value step can't be converted to target type {_colViewModel.TargetValueType.Type}");

            ErrorText = sb.ToString();
        }

        public bool AllInputsAreCorrect()
        {
            return _isStartValueValid && _isEndValueValid && _isValueStepValid;
        }
    }
}
