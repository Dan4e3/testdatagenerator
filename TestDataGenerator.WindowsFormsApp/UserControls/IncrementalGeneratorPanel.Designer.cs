﻿
namespace TestDataGenerator.WindowsFormsApp.UserControls
{
    partial class IncrementalGeneratorPanel
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.GeneralTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.StartValueLabel = new System.Windows.Forms.Label();
            this.StartValueTextBox = new System.Windows.Forms.TextBox();
            this.EndValueLabel = new System.Windows.Forms.Label();
            this.EndValueTextBox = new System.Windows.Forms.TextBox();
            this.ValueStepLabel = new System.Windows.Forms.Label();
            this.ValueStepTextBox = new System.Windows.Forms.TextBox();
            this.GeneralTableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // GeneralTableLayoutPanel
            // 
            this.GeneralTableLayoutPanel.ColumnCount = 2;
            this.GeneralTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 111F));
            this.GeneralTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 111F));
            this.GeneralTableLayoutPanel.Controls.Add(this.EndValueLabel, 0, 1);
            this.GeneralTableLayoutPanel.Controls.Add(this.EndValueTextBox, 1, 1);
            this.GeneralTableLayoutPanel.Controls.Add(this.StartValueLabel, 0, 0);
            this.GeneralTableLayoutPanel.Controls.Add(this.StartValueTextBox, 1, 0);
            this.GeneralTableLayoutPanel.Controls.Add(this.ValueStepTextBox, 1, 2);
            this.GeneralTableLayoutPanel.Controls.Add(this.ValueStepLabel, 0, 2);
            this.GeneralTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GeneralTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.GeneralTableLayoutPanel.Name = "GeneralTableLayoutPanel";
            this.GeneralTableLayoutPanel.RowCount = 4;
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.GeneralTableLayoutPanel.Size = new System.Drawing.Size(448, 109);
            this.GeneralTableLayoutPanel.TabIndex = 0;
            // 
            // StartValueLabel
            // 
            this.StartValueLabel.AutoSize = true;
            this.StartValueLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.StartValueLabel.Location = new System.Drawing.Point(3, 0);
            this.StartValueLabel.Name = "StartValueLabel";
            this.StartValueLabel.Size = new System.Drawing.Size(105, 29);
            this.StartValueLabel.TabIndex = 2;
            this.StartValueLabel.Text = "Start value:";
            this.StartValueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // StartValueTextBox
            // 
            this.StartValueTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.StartValueTextBox.Location = new System.Drawing.Point(114, 3);
            this.StartValueTextBox.Name = "StartValueTextBox";
            this.StartValueTextBox.Size = new System.Drawing.Size(331, 23);
            this.StartValueTextBox.TabIndex = 3;
            this.StartValueTextBox.TextChanged += new System.EventHandler(this.StartValueTextBox_TextChanged);
            // 
            // EndValueLabel
            // 
            this.EndValueLabel.AutoSize = true;
            this.EndValueLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.EndValueLabel.Location = new System.Drawing.Point(3, 29);
            this.EndValueLabel.Name = "EndValueLabel";
            this.EndValueLabel.Size = new System.Drawing.Size(105, 31);
            this.EndValueLabel.TabIndex = 4;
            this.EndValueLabel.Text = "End value:";
            this.EndValueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // EndValueTextBox
            // 
            this.EndValueTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.EndValueTextBox.Location = new System.Drawing.Point(114, 33);
            this.EndValueTextBox.Name = "EndValueTextBox";
            this.EndValueTextBox.Size = new System.Drawing.Size(331, 23);
            this.EndValueTextBox.TabIndex = 5;
            this.EndValueTextBox.TextChanged += new System.EventHandler(this.EndValueTextBox_TextChanged);
            // 
            // ValueStepLabel
            // 
            this.ValueStepLabel.AutoSize = true;
            this.ValueStepLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ValueStepLabel.Location = new System.Drawing.Point(3, 60);
            this.ValueStepLabel.Name = "ValueStepLabel";
            this.ValueStepLabel.Size = new System.Drawing.Size(105, 30);
            this.ValueStepLabel.TabIndex = 6;
            this.ValueStepLabel.Text = "Value step:";
            this.ValueStepLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ValueStepTextBox
            // 
            this.ValueStepTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ValueStepTextBox.Location = new System.Drawing.Point(114, 63);
            this.ValueStepTextBox.Name = "ValueStepTextBox";
            this.ValueStepTextBox.Size = new System.Drawing.Size(331, 23);
            this.ValueStepTextBox.TabIndex = 7;
            this.ValueStepTextBox.TextChanged += new System.EventHandler(this.ValueStepTextBox_TextChanged);
            // 
            // IncrementalGeneratorPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.GeneralTableLayoutPanel);
            this.Name = "IncrementalGeneratorPanel";
            this.Size = new System.Drawing.Size(448, 109);
            this.GeneralTableLayoutPanel.ResumeLayout(false);
            this.GeneralTableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel GeneralTableLayoutPanel;
        private System.Windows.Forms.Label EndValueLabel;
        private System.Windows.Forms.TextBox EndValueTextBox;
        private System.Windows.Forms.Label StartValueLabel;
        private System.Windows.Forms.TextBox StartValueTextBox;
        private System.Windows.Forms.TextBox ValueStepTextBox;
        private System.Windows.Forms.Label ValueStepLabel;
    }
}
