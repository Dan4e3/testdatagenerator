﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestDataGenerator.Core.ValueGenerators.ValueRangeGenerator;
using TestDataGenerator.WinFormApp.ViewModels;

namespace TestDataGenerator.WinFormApp.UserControls
{
    public partial class ValueRangeGeneratorPanel : UserControl, INotifyPropertyChanged
    {
        private string _controlError;
        private bool _minValueCorrect = true;
        private bool _maxValueCorrect = true;
        private string _minValueErrMessage;
        private string _maxValueErrMessage;
        private ColumnViewModel _colViewModel;
        private TypeConverter _typeConverter;

        public event PropertyChangedEventHandler PropertyChanged;
        public object MinValue { get; private set; }
        public object MaxValue { get; private set; }
        public string ErrorText
        {
            get { return _controlError; }
            set
            {
                if (_controlError != value)
                {
                    _controlError = value;
                    OnPropertyChanged();
                }
            }
        }

        public ValueRangeGeneratorPanel(ColumnViewModel colViewModel)
        {
            InitializeComponent();

            _colViewModel = colViewModel;
            _typeConverter = TypeDescriptor.GetConverter(_colViewModel.TargetValueType.Type);
            _minValueErrMessage = $"Minimum value can't be convereted to column target type {_colViewModel.TargetValueType.Type}. ";
            _maxValueErrMessage = $"Maximum value can't be convereted to column target type {_colViewModel.TargetValueType.Type}. ";

            if (colViewModel.ValueSupplier != null && colViewModel.ValueSupplier is ValueRangeValueGenerator generator)
            {
                MinValueTextBox.Text = _colViewModel.Column.GetStringValueRepresentation(generator.ValuesRange.GetMinRangeValue<object>());
                MaxValueTextBox.Text = _colViewModel.Column.GetStringValueRepresentation(generator.ValuesRange.GetMaxRangeValue<object>());
            }
        }

        private void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }

        private void MinValueTextBox_TextChanged(object sender, EventArgs e)
        {
            string minValueText = MinValueTextBox.Text;
            try
            {
                var convertedRes = _typeConverter.ConvertFromString(null, _colViewModel.Column.ColumnSettings.Culture, minValueText);
                _minValueCorrect = true;
                MinValue = convertedRes;
                ErrorText = $"{(_minValueCorrect ? null : _minValueErrMessage)}{(_maxValueCorrect ? null : _maxValueErrMessage)}";
            }
            catch
            {
                _minValueCorrect = false;
                MinValue = null;
                ErrorText = $"{_minValueErrMessage}{(_maxValueCorrect ? null : _maxValueErrMessage)}";
            }
        }

        private void MaxValueTextBox_TextChanged(object sender, EventArgs e)
        {
            string maxValueText = MaxValueTextBox.Text;
            try
            {
                var convertedRes = _typeConverter.ConvertFromString(null, _colViewModel.Column.ColumnSettings.Culture, maxValueText);
                _maxValueCorrect = true;
                MaxValue = convertedRes;
                ErrorText = $"{(_minValueCorrect ? null : _minValueErrMessage)}{(_maxValueCorrect ? null : _maxValueErrMessage)}";
            }
            catch
            {
                _maxValueCorrect = false;
                MaxValue = null;
                ErrorText = $"{_maxValueErrMessage}{(_minValueCorrect ? null : _minValueErrMessage)}";
            }
        }

        public bool BothInputsAreCorrect()
        {
            return _minValueCorrect && _maxValueCorrect;
        }
    }
}
