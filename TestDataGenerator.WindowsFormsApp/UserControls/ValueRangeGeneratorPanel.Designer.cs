﻿
namespace TestDataGenerator.WinFormApp.UserControls
{
    partial class ValueRangeGeneratorPanel
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.GeneralTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.MinValueLabel = new System.Windows.Forms.Label();
            this.MinValueTextBox = new System.Windows.Forms.TextBox();
            this.MaximumValueLabel = new System.Windows.Forms.Label();
            this.MaxValueTextBox = new System.Windows.Forms.TextBox();
            this.GeneralTableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // GeneralTableLayoutPanel
            // 
            this.GeneralTableLayoutPanel.ColumnCount = 2;
            this.GeneralTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 109F));
            this.GeneralTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 57F));
            this.GeneralTableLayoutPanel.Controls.Add(this.MinValueLabel, 0, 0);
            this.GeneralTableLayoutPanel.Controls.Add(this.MinValueTextBox, 1, 0);
            this.GeneralTableLayoutPanel.Controls.Add(this.MaximumValueLabel, 0, 1);
            this.GeneralTableLayoutPanel.Controls.Add(this.MaxValueTextBox, 1, 1);
            this.GeneralTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GeneralTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.GeneralTableLayoutPanel.Name = "GeneralTableLayoutPanel";
            this.GeneralTableLayoutPanel.RowCount = 3;
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.GeneralTableLayoutPanel.Size = new System.Drawing.Size(351, 85);
            this.GeneralTableLayoutPanel.TabIndex = 0;
            // 
            // MinValueLabel
            // 
            this.MinValueLabel.AutoSize = true;
            this.MinValueLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MinValueLabel.Location = new System.Drawing.Point(3, 0);
            this.MinValueLabel.Name = "MinValueLabel";
            this.MinValueLabel.Size = new System.Drawing.Size(103, 30);
            this.MinValueLabel.TabIndex = 0;
            this.MinValueLabel.Text = "Minimum value:";
            this.MinValueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // MinValueTextBox
            // 
            this.MinValueTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.MinValueTextBox.Location = new System.Drawing.Point(112, 3);
            this.MinValueTextBox.Name = "MinValueTextBox";
            this.MinValueTextBox.Size = new System.Drawing.Size(236, 23);
            this.MinValueTextBox.TabIndex = 1;
            this.MinValueTextBox.TextChanged += new System.EventHandler(this.MinValueTextBox_TextChanged);
            // 
            // MaximumValueLabel
            // 
            this.MaximumValueLabel.AutoSize = true;
            this.MaximumValueLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MaximumValueLabel.Location = new System.Drawing.Point(3, 30);
            this.MaximumValueLabel.Name = "MaximumValueLabel";
            this.MaximumValueLabel.Size = new System.Drawing.Size(103, 29);
            this.MaximumValueLabel.TabIndex = 2;
            this.MaximumValueLabel.Text = "Maximum value:";
            this.MaximumValueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // MaxValueTextBox
            // 
            this.MaxValueTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.MaxValueTextBox.Location = new System.Drawing.Point(112, 33);
            this.MaxValueTextBox.Name = "MaxValueTextBox";
            this.MaxValueTextBox.Size = new System.Drawing.Size(236, 23);
            this.MaxValueTextBox.TabIndex = 3;
            this.MaxValueTextBox.TextChanged += new System.EventHandler(this.MaxValueTextBox_TextChanged);
            // 
            // ValueRangeGeneratorPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.GeneralTableLayoutPanel);
            this.Name = "ValueRangeGeneratorPanel";
            this.Size = new System.Drawing.Size(351, 85);
            this.GeneralTableLayoutPanel.ResumeLayout(false);
            this.GeneralTableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel GeneralTableLayoutPanel;
        private System.Windows.Forms.Label MinValueLabel;
        private System.Windows.Forms.TextBox MinValueTextBox;
        private System.Windows.Forms.Label MaximumValueLabel;
        private System.Windows.Forms.TextBox MaxValueTextBox;
    }
}
