﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestDataGenerator.Core;
using TestDataGenerator.Core.ValueGenerators;
using TestDataGenerator.WindowsFormsApp.Forms;
using TestDataGenerator.WinFormApp.ViewModels;

namespace TestDataGenerator.WinFormApp.UserControls
{
    public partial class ListSelectGeneratorPanel : UserControl, INotifyPropertyChanged
    {
        private string _controlError;
        private ColumnViewModel _columnViewModel;

        public event PropertyChangedEventHandler PropertyChanged;
        public BindingList<object> ListOfValues { get; private set; }
        public string ErrorText
        {
            get { return _controlError; }
            set
            {
                if (_controlError != value)
                {
                    _controlError = value;
                    OnPropertyChanged();
                }
            }
        }

        public ListSelectGeneratorPanel(ColumnViewModel colViewModel)
        {
            InitializeComponent();

            ListOfValues = new BindingList<object>();
            if (colViewModel.ValueSupplier is SelectFromListValueGenerator generator && generator != null)
            {
                foreach (var val in generator.ArrayOfValuesToSelectFrom)
                    ListOfValues.Add(val);
            }
                

            _columnViewModel = colViewModel;

            ValuesListBox.DataSource = ListOfValues;
        }

        private void AddValueButton_Click(object sender, EventArgs e)
        {
            string valueToInsert = InsertValueTextBox.Text;
            if (valueToInsert != null && valueToInsert.Trim() != "")
            {
                if (ListOfValues.Contains(valueToInsert))
                {
                    ErrorText = $"Supplied value already exists in list ({valueToInsert})!";
                    return;
                }

                var typeConverter = TypeDescriptor.GetConverter(_columnViewModel.TargetValueType.Type);
                object result = null;
                try
                {
                    result = typeConverter.ConvertFromString(null, _columnViewModel.Column.ColumnSettings.Culture, valueToInsert);
                }
                catch
                {
                    ErrorText = $"Can't convert inserted value to column type ({_columnViewModel.TargetValueType.DisplayName})";
                    return;
                }

                ListOfValues.Add(result);
                ErrorText = "";
                InsertValueTextBox.Text = "";
            }
            else
                ErrorText = "Can't insert empty value!";
        }

        private void RemoveValueButton_Click(object sender, EventArgs e)
        {
            List<object> valuesToRemove = new List<object>();
            foreach (var item in ValuesListBox.SelectedItems)
                valuesToRemove.Add(item);

            foreach (var item in valuesToRemove)
                ListOfValues.Remove(item);
        }

        private void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }

        private void LoadFromSourceButton_Click(object sender, EventArgs e)
        {
            using (LoadFromExternalSourceForm sourceLoaderForm = new LoadFromExternalSourceForm())
            {
                DialogResult dialogResult = sourceLoaderForm.ShowDialog(this);

                if (dialogResult == DialogResult.OK && sourceLoaderForm.ResultArray != null)
                {
                    foreach (object newItem in sourceLoaderForm.ResultArray)
                        ListOfValues.Add(newItem);
                }
            }
        }

        private void ClearAllItemsButton_Click(object sender, EventArgs e)
        {
            ListOfValues.Clear();
        }

        private void InsertValueTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = true;
                e.SuppressKeyPress = true;
                AddValueButton_Click(this, new EventArgs());
            }
        }
    }
}
