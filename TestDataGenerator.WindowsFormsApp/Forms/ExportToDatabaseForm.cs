﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestDataGenerator.Abstractions.Interfaces;
using TestDataGenerator.Abstractions.Interfaces.Writers;
using TestDataGenerator.Core;
using TestDataGenerator.IO;
using TestDataGenerator.IO.DatabaseExportExecutors;
using TestDataGenerator.IO.DatasetWriters;
using TestDataGenerator.WindowsFormsApp;
using TestDataGenerator.WindowsFormsApp.ViewModels;
using TestDataGenerator.WinFormApp.ViewModels;

namespace TestDataGenerator.WinFormApp.Forms
{
    public partial class ExportToDatabaseForm : Form
    {
        private TestDatasetViewModel _testDatasetViewModel;
        private TestDataSet _testDataset;
        private DbDataSetWriter _dbWriter;
        private BindingList<DatabaseExportProviderViewModel> _dbProvidersModelViews = new BindingList<DatabaseExportProviderViewModel>() { 
            new DatabaseExportProviderViewModel("MS SQL Server", new SqlServerExportExecutor()),
            new DatabaseExportProviderViewModel("PostgreSQL", new PostgresExportExecutor())
        };

        public ExportToDatabaseForm(TestDatasetViewModel datasetViewModel, IProgressReporter progressReporter = null)
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterParent;

            if (AppSettings.Default.ConnectionStringsArray == null)
                AppSettings.Default.ConnectionStringsArray = new StringCollection();
            AutoCompleteStringCollection connStringAutoComplete = new AutoCompleteStringCollection();
            connStringAutoComplete.AddRange(AppSettings.Default.ConnectionStringsArray.Cast<string>().ToArray());
            ConnectionStringTextBox.AutoCompleteCustomSource = connStringAutoComplete;


            if (AppSettings.Default.DataTableNamesArray == null)
                AppSettings.Default.DataTableNamesArray = new StringCollection();
            AutoCompleteStringCollection tableNameAutoComplete = new AutoCompleteStringCollection();
            tableNameAutoComplete.AddRange(AppSettings.Default.DataTableNamesArray.Cast<string>().ToArray());
            DataTableNameTextBox.AutoCompleteCustomSource = tableNameAutoComplete;
            

            _testDatasetViewModel = datasetViewModel;
            _testDataset = datasetViewModel.DataSet;
            _dbWriter = new DbDataSetWriter(progressReporter);

            DatabaseProviderComboBox.DataSource = _dbProvidersModelViews;
            DatabaseProviderComboBox.DisplayMember = nameof(DatabaseExportProviderViewModel.DisplayName);
            DatabaseProviderComboBox.ValueMember = nameof(DatabaseExportProviderViewModel.Executor);

            ConnectionStringTextBox.DataBindings.Add("Text", _dbProvidersModelViews, nameof(DatabaseExportProviderViewModel.ConnectionString));
            DataTableNameTextBox.DataBindings.Add("Text", _dbProvidersModelViews, nameof(DatabaseExportProviderViewModel.TableName));
        }

        private async void OkButton_Click(object sender, EventArgs e)
        {
            if (!_testDataset.CanGenerateDataSet(out string errors))
            {
                MessageBox.Show(errors, "Unable to create dataset", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (ConnectionStringTextBox.Text == "")
            {
                MessageBox.Show("Connection string can't be empty!", "Invalid connection string", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (DataTableNameTextBox.Text == "")
            {
                MessageBox.Show("Table name can't be empty!", "Invalid table name", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            IDbExportExecutor executor = DatabaseProviderComboBox.SelectedValue as IDbExportExecutor;

            if (executor == null)
            {
                MessageBox.Show("Something went wrong!", "Unexpected problem", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            try
            {
                ToggleControlsUsability();
                int insertedRowsCount = await Task.Run((
                    ) => _dbWriter.WriteToDatabaseUsingInPlaceRowGeneration(executor, _testDataset, _testDatasetViewModel.RowsToGenerate)
                );
                MessageBox.Show($"Successfully inserted {insertedRowsCount} rows!", "Successful insert", MessageBoxButtons.OK, MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;

                if (!AppSettings.Default.ConnectionStringsArray.Contains(ConnectionStringTextBox.Text))
                    AppSettings.Default.ConnectionStringsArray.Add(ConnectionStringTextBox.Text);

                if (!AppSettings.Default.DataTableNamesArray.Contains(DataTableNameTextBox.Text))
                    AppSettings.Default.DataTableNamesArray.Add(DataTableNameTextBox.Text);

                AppSettings.Default.Save();

                Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show($"An error has occured: {ex}", "Export error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            finally
            {
                ToggleControlsUsability();
            }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void ToggleControlsUsability()
        {
            OkButton.Enabled = !OkButton.Enabled;
            CancelButton.Enabled = !CancelButton.Enabled;
            DatabaseProviderComboBox.Enabled = !DatabaseProviderComboBox.Enabled;
            DataTableNameTextBox.Enabled = !DataTableNameTextBox.Enabled;
            ConnectionStringTextBox.Enabled = !ConnectionStringTextBox.Enabled;
        }
    }
}
