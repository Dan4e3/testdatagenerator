﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestDataGenerator.IO.SourceLoaders;
using TestDataGenerator.WindowsFormsApp.UserControls.ExternalSourceLoaders;
using TestDataGenerator.WindowsFormsApp.ViewModels;

namespace TestDataGenerator.WindowsFormsApp.Forms
{
    public partial class LoadFromExternalSourceForm : Form
    {
        private readonly BindingList<ExternalSourceLoaderViewModel> _availableSourceLoaders;

        public object[] ResultArray { get; set; }

        public LoadFromExternalSourceForm()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterParent;

            _availableSourceLoaders = new BindingList<ExternalSourceLoaderViewModel>() { 
                new ExternalSourceLoaderViewModel("HTTP resource (JSON format)", new HttpSourceLoaderPanel(), typeof(HttpSourceLoader)),
                new ExternalSourceLoaderViewModel("Plain text file", new FileSourceLoaderPanel(), typeof(FileSourceLoader)),
                new ExternalSourceLoaderViewModel("Database", new DatabaseSourceLoaderPanel(), typeof(DatabaseSourceLoader))
            };

            SourceTypeComboBox.DataSource = _availableSourceLoaders;
            SourceTypeComboBox.DisplayMember = nameof(ExternalSourceLoaderViewModel.DisplayText);
        }

        private void SourceTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ExternalSourceLoaderViewModel selectedVm = SourceTypeComboBox.SelectedItem as ExternalSourceLoaderViewModel;
            if (selectedVm == null)
                return;

            SourceParametersGroupBox.Controls.Clear();
            selectedVm.UserControl.Dock = DockStyle.Fill;
            SourceParametersGroupBox.Controls.Add(selectedVm.UserControl);
        }

        private async void LoadDataButton_Click(object sender, EventArgs e)
        {
            ExternalSourceLoaderViewModel selectedVm = SourceTypeComboBox.SelectedItem as ExternalSourceLoaderViewModel;

            try
            {
                if (selectedVm.UserControl is HttpSourceLoaderPanel httpSrcLoaderPanel)
                {
                    using (HttpSourceLoader httpLoader = new HttpSourceLoader(httpSrcLoaderPanel.SearchTokens))
                    {
                        HttpRequestMessage requestMsg = new HttpRequestMessage()
                        {
                            RequestUri = new Uri(httpSrcLoaderPanel.ResourceUrl),
                            Method = httpSrcLoaderPanel.HttpMethod,
                            Content = httpSrcLoaderPanel.RequestJson == "" ? null :
                                new StringContent(httpSrcLoaderPanel.RequestJson, Encoding.UTF8, "application/json")
                        };
                        ResultArray = await httpLoader.LoadDataFromHttpSourceAsync(requestMsg);
                    }
                }
                else if (selectedVm.UserControl is FileSourceLoaderPanel fileSrcLoaderPanel)
                {
                    FileSourceLoader fileLoader = new FileSourceLoader(fileSrcLoaderPanel.NewLineSequence, fileSrcLoaderPanel.FileEncoding);
                    ResultArray = await fileLoader.LoadDataFromRawFileSourceAsync(fileSrcLoaderPanel.PathToFile);
                }
                else if (selectedVm.UserControl is DatabaseSourceLoaderPanel dbSrcLoaderPanel)
                {
                    DatabaseSourceLoader dbLoader = new DatabaseSourceLoader(dbSrcLoaderPanel.ConnectionString, dbSrcLoaderPanel.Provider);
                    ResultArray = await Task.Run(() => dbLoader.LoadDataFromDatabaseSource(dbSrcLoaderPanel.TableName, dbSrcLoaderPanel.TableColumn));
                }
                else
                    return;

                DialogResult = DialogResult.OK;
                Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show($"Error occured: {ex}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
