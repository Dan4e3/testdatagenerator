﻿
namespace TestDataGenerator.WinFormApp.Forms
{
    partial class ValueGeneratorSetupForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GeneralTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.ValueGeneratorSelectionGroupBox = new System.Windows.Forms.GroupBox();
            this.ValueGeneratorComboBox = new System.Windows.Forms.ComboBox();
            this.ValueGeneratorParamsGroupBox = new System.Windows.Forms.GroupBox();
            this.OkButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.ErrorsRichTextBox = new System.Windows.Forms.RichTextBox();
            this.GenerateNullValuesLabel = new System.Windows.Forms.Label();
            this.AverageRowsBeforeNullLabel = new System.Windows.Forms.Label();
            this.GenerateNullValuesCheckBox = new System.Windows.Forms.CheckBox();
            this.NullEveryNRowTextBox = new System.Windows.Forms.TextBox();
            this.GeneralTableLayoutPanel.SuspendLayout();
            this.ValueGeneratorSelectionGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // GeneralTableLayoutPanel
            // 
            this.GeneralTableLayoutPanel.ColumnCount = 2;
            this.GeneralTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 188F));
            this.GeneralTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 206F));
            this.GeneralTableLayoutPanel.Controls.Add(this.ValueGeneratorSelectionGroupBox, 0, 0);
            this.GeneralTableLayoutPanel.Controls.Add(this.ValueGeneratorParamsGroupBox, 0, 3);
            this.GeneralTableLayoutPanel.Controls.Add(this.OkButton, 0, 5);
            this.GeneralTableLayoutPanel.Controls.Add(this.CancelButton, 1, 5);
            this.GeneralTableLayoutPanel.Controls.Add(this.ErrorsRichTextBox, 0, 4);
            this.GeneralTableLayoutPanel.Controls.Add(this.GenerateNullValuesLabel, 0, 1);
            this.GeneralTableLayoutPanel.Controls.Add(this.AverageRowsBeforeNullLabel, 0, 2);
            this.GeneralTableLayoutPanel.Controls.Add(this.GenerateNullValuesCheckBox, 1, 1);
            this.GeneralTableLayoutPanel.Controls.Add(this.NullEveryNRowTextBox, 1, 2);
            this.GeneralTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GeneralTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.GeneralTableLayoutPanel.Name = "GeneralTableLayoutPanel";
            this.GeneralTableLayoutPanel.RowCount = 6;
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 63F));
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 247F));
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 103F));
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.GeneralTableLayoutPanel.Size = new System.Drawing.Size(394, 513);
            this.GeneralTableLayoutPanel.TabIndex = 0;
            // 
            // ValueGeneratorSelectionGroupBox
            // 
            this.GeneralTableLayoutPanel.SetColumnSpan(this.ValueGeneratorSelectionGroupBox, 2);
            this.ValueGeneratorSelectionGroupBox.Controls.Add(this.ValueGeneratorComboBox);
            this.ValueGeneratorSelectionGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ValueGeneratorSelectionGroupBox.Location = new System.Drawing.Point(3, 3);
            this.ValueGeneratorSelectionGroupBox.Name = "ValueGeneratorSelectionGroupBox";
            this.ValueGeneratorSelectionGroupBox.Size = new System.Drawing.Size(388, 57);
            this.ValueGeneratorSelectionGroupBox.TabIndex = 0;
            this.ValueGeneratorSelectionGroupBox.TabStop = false;
            this.ValueGeneratorSelectionGroupBox.Text = "Value generator type";
            // 
            // ValueGeneratorComboBox
            // 
            this.ValueGeneratorComboBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ValueGeneratorComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ValueGeneratorComboBox.FormattingEnabled = true;
            this.ValueGeneratorComboBox.Location = new System.Drawing.Point(3, 19);
            this.ValueGeneratorComboBox.Name = "ValueGeneratorComboBox";
            this.ValueGeneratorComboBox.Size = new System.Drawing.Size(382, 23);
            this.ValueGeneratorComboBox.TabIndex = 0;
            this.ValueGeneratorComboBox.SelectedValueChanged += new System.EventHandler(this.ValueGeneratorComboBox_SelectedValueChanged);
            // 
            // ValueGeneratorParamsGroupBox
            // 
            this.GeneralTableLayoutPanel.SetColumnSpan(this.ValueGeneratorParamsGroupBox, 2);
            this.ValueGeneratorParamsGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ValueGeneratorParamsGroupBox.Location = new System.Drawing.Point(3, 121);
            this.ValueGeneratorParamsGroupBox.Name = "ValueGeneratorParamsGroupBox";
            this.ValueGeneratorParamsGroupBox.Size = new System.Drawing.Size(388, 241);
            this.ValueGeneratorParamsGroupBox.TabIndex = 1;
            this.ValueGeneratorParamsGroupBox.TabStop = false;
            this.ValueGeneratorParamsGroupBox.Text = "Value generator parameters";
            // 
            // OkButton
            // 
            this.OkButton.AutoEllipsis = true;
            this.OkButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.OkButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OkButton.Location = new System.Drawing.Point(3, 471);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(182, 39);
            this.OkButton.TabIndex = 2;
            this.OkButton.Text = "OK";
            this.OkButton.UseVisualStyleBackColor = true;
            this.OkButton.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CancelButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CancelButton.Location = new System.Drawing.Point(191, 471);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(200, 39);
            this.CancelButton.TabIndex = 3;
            this.CancelButton.Text = "Cancel";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // ErrorsRichTextBox
            // 
            this.ErrorsRichTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.GeneralTableLayoutPanel.SetColumnSpan(this.ErrorsRichTextBox, 2);
            this.ErrorsRichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ErrorsRichTextBox.ForeColor = System.Drawing.Color.Red;
            this.ErrorsRichTextBox.Location = new System.Drawing.Point(3, 368);
            this.ErrorsRichTextBox.Name = "ErrorsRichTextBox";
            this.ErrorsRichTextBox.ReadOnly = true;
            this.ErrorsRichTextBox.Size = new System.Drawing.Size(388, 97);
            this.ErrorsRichTextBox.TabIndex = 4;
            this.ErrorsRichTextBox.Text = "";
            // 
            // GenerateNullValuesLabel
            // 
            this.GenerateNullValuesLabel.AutoSize = true;
            this.GenerateNullValuesLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GenerateNullValuesLabel.Location = new System.Drawing.Point(3, 63);
            this.GenerateNullValuesLabel.Name = "GenerateNullValuesLabel";
            this.GenerateNullValuesLabel.Size = new System.Drawing.Size(182, 27);
            this.GenerateNullValuesLabel.TabIndex = 5;
            this.GenerateNullValuesLabel.Text = "Generate null values:";
            this.GenerateNullValuesLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // AverageRowsBeforeNullLabel
            // 
            this.AverageRowsBeforeNullLabel.AutoSize = true;
            this.AverageRowsBeforeNullLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AverageRowsBeforeNullLabel.Location = new System.Drawing.Point(3, 90);
            this.AverageRowsBeforeNullLabel.Name = "AverageRowsBeforeNullLabel";
            this.AverageRowsBeforeNullLabel.Size = new System.Drawing.Size(182, 28);
            this.AverageRowsBeforeNullLabel.TabIndex = 6;
            this.AverageRowsBeforeNullLabel.Text = "Null every N row:";
            this.AverageRowsBeforeNullLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // GenerateNullValuesCheckBox
            // 
            this.GenerateNullValuesCheckBox.AutoSize = true;
            this.GenerateNullValuesCheckBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GenerateNullValuesCheckBox.Location = new System.Drawing.Point(191, 66);
            this.GenerateNullValuesCheckBox.Name = "GenerateNullValuesCheckBox";
            this.GenerateNullValuesCheckBox.Size = new System.Drawing.Size(200, 21);
            this.GenerateNullValuesCheckBox.TabIndex = 7;
            this.GenerateNullValuesCheckBox.UseVisualStyleBackColor = true;
            // 
            // NullEveryNRowTextBox
            // 
            this.NullEveryNRowTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.NullEveryNRowTextBox.Location = new System.Drawing.Point(191, 93);
            this.NullEveryNRowTextBox.Name = "NullEveryNRowTextBox";
            this.NullEveryNRowTextBox.Size = new System.Drawing.Size(200, 23);
            this.NullEveryNRowTextBox.TabIndex = 8;
            // 
            // ValueGeneratorSetupForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(394, 513);
            this.ControlBox = false;
            this.Controls.Add(this.GeneralTableLayoutPanel);
            this.MaximizeBox = false;
            this.Name = "ValueGeneratorSetupForm";
            this.ShowIcon = false;
            this.Text = "Value generator selection";
            this.GeneralTableLayoutPanel.ResumeLayout(false);
            this.GeneralTableLayoutPanel.PerformLayout();
            this.ValueGeneratorSelectionGroupBox.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel GeneralTableLayoutPanel;
        private System.Windows.Forms.GroupBox ValueGeneratorSelectionGroupBox;
        private System.Windows.Forms.ComboBox ValueGeneratorComboBox;
        private System.Windows.Forms.GroupBox ValueGeneratorParamsGroupBox;
        private System.Windows.Forms.Button OkButton;
        private new System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.RichTextBox ErrorsRichTextBox;
        private System.Windows.Forms.Label GenerateNullValuesLabel;
        private System.Windows.Forms.Label AverageRowsBeforeNullLabel;
        private System.Windows.Forms.CheckBox GenerateNullValuesCheckBox;
        private System.Windows.Forms.TextBox NullEveryNRowTextBox;
    }
}