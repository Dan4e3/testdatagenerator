﻿
namespace TestDataGenerator.WinFormApp.Forms
{
    partial class ExportToJsonForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GeneralTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.PathToFileTextBox = new System.Windows.Forms.TextBox();
            this.PathToSaveFileButton = new System.Windows.Forms.Button();
            this.JsonParametersGroupBox = new System.Windows.Forms.GroupBox();
            this.JsonExportParametersPanel = new TestDataGenerator.WindowsFormsApp.UserControls.JsonExportParametersPanel();
            this.JsonPreviewGroupBox = new System.Windows.Forms.GroupBox();
            this.JsonPreviewRichTextBox = new System.Windows.Forms.RichTextBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.OkButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.RefreshJsonPreviewButton = new System.Windows.Forms.Button();
            this.GeneralTableLayoutPanel.SuspendLayout();
            this.JsonParametersGroupBox.SuspendLayout();
            this.JsonPreviewGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // GeneralTableLayoutPanel
            // 
            this.GeneralTableLayoutPanel.ColumnCount = 3;
            this.GeneralTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 267F));
            this.GeneralTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 173F));
            this.GeneralTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 105F));
            this.GeneralTableLayoutPanel.Controls.Add(this.PathToFileTextBox, 0, 0);
            this.GeneralTableLayoutPanel.Controls.Add(this.PathToSaveFileButton, 2, 0);
            this.GeneralTableLayoutPanel.Controls.Add(this.JsonParametersGroupBox, 0, 1);
            this.GeneralTableLayoutPanel.Controls.Add(this.JsonPreviewGroupBox, 1, 1);
            this.GeneralTableLayoutPanel.Controls.Add(this.splitContainer1, 0, 3);
            this.GeneralTableLayoutPanel.Controls.Add(this.RefreshJsonPreviewButton, 2, 2);
            this.GeneralTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GeneralTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.GeneralTableLayoutPanel.Name = "GeneralTableLayoutPanel";
            this.GeneralTableLayoutPanel.RowCount = 4;
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 215F));
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.GeneralTableLayoutPanel.Size = new System.Drawing.Size(545, 317);
            this.GeneralTableLayoutPanel.TabIndex = 0;
            // 
            // PathToFileTextBox
            // 
            this.PathToFileTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.GeneralTableLayoutPanel.SetColumnSpan(this.PathToFileTextBox, 2);
            this.PathToFileTextBox.Enabled = false;
            this.PathToFileTextBox.Location = new System.Drawing.Point(3, 5);
            this.PathToFileTextBox.Name = "PathToFileTextBox";
            this.PathToFileTextBox.PlaceholderText = "Specify path to file...";
            this.PathToFileTextBox.ReadOnly = true;
            this.PathToFileTextBox.Size = new System.Drawing.Size(434, 23);
            this.PathToFileTextBox.TabIndex = 1;
            // 
            // PathToSaveFileButton
            // 
            this.PathToSaveFileButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PathToSaveFileButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PathToSaveFileButton.Location = new System.Drawing.Point(443, 3);
            this.PathToSaveFileButton.Name = "PathToSaveFileButton";
            this.PathToSaveFileButton.Size = new System.Drawing.Size(99, 28);
            this.PathToSaveFileButton.TabIndex = 0;
            this.PathToSaveFileButton.Text = "Browse...";
            this.PathToSaveFileButton.UseVisualStyleBackColor = true;
            this.PathToSaveFileButton.Click += new System.EventHandler(this.PathToSaveFileButton_Click);
            // 
            // JsonParametersGroupBox
            // 
            this.JsonParametersGroupBox.Controls.Add(this.JsonExportParametersPanel);
            this.JsonParametersGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.JsonParametersGroupBox.Location = new System.Drawing.Point(3, 37);
            this.JsonParametersGroupBox.Name = "JsonParametersGroupBox";
            this.JsonParametersGroupBox.Size = new System.Drawing.Size(261, 209);
            this.JsonParametersGroupBox.TabIndex = 2;
            this.JsonParametersGroupBox.TabStop = false;
            this.JsonParametersGroupBox.Text = "Json parameters";
            // 
            // JsonExportParametersPanel
            // 
            this.JsonExportParametersPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.JsonExportParametersPanel.Location = new System.Drawing.Point(3, 19);
            this.JsonExportParametersPanel.Name = "JsonExportParametersPanel";
            this.JsonExportParametersPanel.Size = new System.Drawing.Size(255, 187);
            this.JsonExportParametersPanel.TabIndex = 0;
            // 
            // JsonPreviewGroupBox
            // 
            this.GeneralTableLayoutPanel.SetColumnSpan(this.JsonPreviewGroupBox, 2);
            this.JsonPreviewGroupBox.Controls.Add(this.JsonPreviewRichTextBox);
            this.JsonPreviewGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.JsonPreviewGroupBox.Location = new System.Drawing.Point(270, 37);
            this.JsonPreviewGroupBox.Name = "JsonPreviewGroupBox";
            this.JsonPreviewGroupBox.Size = new System.Drawing.Size(272, 209);
            this.JsonPreviewGroupBox.TabIndex = 3;
            this.JsonPreviewGroupBox.TabStop = false;
            this.JsonPreviewGroupBox.Text = "Json preview";
            // 
            // JsonPreviewRichTextBox
            // 
            this.JsonPreviewRichTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.JsonPreviewRichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.JsonPreviewRichTextBox.Location = new System.Drawing.Point(3, 19);
            this.JsonPreviewRichTextBox.Name = "JsonPreviewRichTextBox";
            this.JsonPreviewRichTextBox.ReadOnly = true;
            this.JsonPreviewRichTextBox.Size = new System.Drawing.Size(266, 187);
            this.JsonPreviewRichTextBox.TabIndex = 0;
            this.JsonPreviewRichTextBox.Text = "";
            this.JsonPreviewRichTextBox.WordWrap = false;
            // 
            // splitContainer1
            // 
            this.GeneralTableLayoutPanel.SetColumnSpan(this.splitContainer1, 3);
            this.splitContainer1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(3, 279);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.OkButton);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.CancelButton);
            this.splitContainer1.Size = new System.Drawing.Size(539, 35);
            this.splitContainer1.SplitterDistance = 263;
            this.splitContainer1.TabIndex = 4;
            // 
            // OkButton
            // 
            this.OkButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.OkButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OkButton.Location = new System.Drawing.Point(0, 0);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(263, 35);
            this.OkButton.TabIndex = 0;
            this.OkButton.Text = "Save file";
            this.OkButton.UseVisualStyleBackColor = true;
            this.OkButton.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CancelButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CancelButton.Location = new System.Drawing.Point(0, 0);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(272, 35);
            this.CancelButton.TabIndex = 0;
            this.CancelButton.Text = "Cancel";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // RefreshJsonPreviewButton
            // 
            this.RefreshJsonPreviewButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RefreshJsonPreviewButton.Location = new System.Drawing.Point(443, 252);
            this.RefreshJsonPreviewButton.Name = "RefreshJsonPreviewButton";
            this.RefreshJsonPreviewButton.Size = new System.Drawing.Size(99, 21);
            this.RefreshJsonPreviewButton.TabIndex = 5;
            this.RefreshJsonPreviewButton.Text = "Refresh preview";
            this.RefreshJsonPreviewButton.UseVisualStyleBackColor = true;
            this.RefreshJsonPreviewButton.Click += new System.EventHandler(this.RefreshJsonPreviewButton_Click);
            // 
            // ExportToJsonForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(545, 317);
            this.ControlBox = false;
            this.Controls.Add(this.GeneralTableLayoutPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "ExportToJsonForm";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Export dataset to JSON";
            this.Shown += new System.EventHandler(this.ExportToJsonForm_Shown);
            this.GeneralTableLayoutPanel.ResumeLayout(false);
            this.GeneralTableLayoutPanel.PerformLayout();
            this.JsonParametersGroupBox.ResumeLayout(false);
            this.JsonPreviewGroupBox.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel GeneralTableLayoutPanel;
        private System.Windows.Forms.TextBox PathToFileTextBox;
        private System.Windows.Forms.Button PathToSaveFileButton;
        private System.Windows.Forms.GroupBox JsonParametersGroupBox;
        private System.Windows.Forms.GroupBox JsonPreviewGroupBox;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button OkButton;
        private new System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.RichTextBox JsonPreviewRichTextBox;
        private WindowsFormsApp.UserControls.JsonExportParametersPanel JsonExportParametersPanel;
        private System.Windows.Forms.Button RefreshJsonPreviewButton;
    }
}