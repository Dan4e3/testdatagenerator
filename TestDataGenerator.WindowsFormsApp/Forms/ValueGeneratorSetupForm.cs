﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestDataGenerator.Abstractions.Interfaces;
using TestDataGenerator.Abstractions.Interfaces.ValuesHandling;
using TestDataGenerator.Core;
using TestDataGenerator.Core.ValueGenerators;
using TestDataGenerator.Core.ValueGenerators.IncrementalGenerator;
using TestDataGenerator.Core.ValueGenerators.Providers;
using TestDataGenerator.Core.ValueGenerators.ValuePatternParser;
using TestDataGenerator.Core.ValueGenerators.ValueRangeGenerator;
using TestDataGenerator.WindowsFormsApp.UserControls;
using TestDataGenerator.WindowsFormsApp.ViewModels;
using TestDataGenerator.WinFormApp.UserControls;
using TestDataGenerator.WinFormApp.ViewModels;

namespace TestDataGenerator.WinFormApp.Forms
{
    public partial class ValueGeneratorSetupForm : Form
    {
        private List<ValueGeneratorViewModel> _availableValueGenerators;
        private ColumnViewModel _columnViewModel;
        private NullValuesSettingsViewModel _nullValuesSettingsViewModel;

        public IValueGenerator ValueGenerator { get; private set; }

        public ValueGeneratorSetupForm(ColumnViewModel colViewModel)
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterParent;

            _columnViewModel = colViewModel;

            _nullValuesSettingsViewModel = new NullValuesSettingsViewModel(_columnViewModel.ValueSupplier?.NullValuesSettings);

            _availableValueGenerators = new List<ValueGeneratorViewModel>() { 
                new ValueGeneratorViewModel("Value pattern", new PatternParserPanel(_columnViewModel.ValueSupplier), typeof(ValuePatternParser)),
                new ValueGeneratorViewModel("List select", new ListSelectGeneratorPanel(_columnViewModel), typeof(SelectFromListValueGenerator)),
                new ValueGeneratorViewModel("From range selection", new ValueRangeGeneratorPanel(_columnViewModel), typeof(ValueRangeValueGenerator)),
                new ValueGeneratorViewModel("Incremental", new IncrementalGeneratorPanel(_columnViewModel), typeof(IncrementalValueGenerator))
            };

            GenerateNullValuesCheckBox.DataBindings.Add("Checked", _nullValuesSettingsViewModel, nameof(_nullValuesSettingsViewModel.GenerateNullValues), false, DataSourceUpdateMode.OnPropertyChanged);

            NullEveryNRowTextBox.DataBindings.Add("Text", _nullValuesSettingsViewModel, nameof(_nullValuesSettingsViewModel.AverageRowsBeforeNullAppearance));
            NullEveryNRowTextBox.DataBindings.Add("Enabled", _nullValuesSettingsViewModel, nameof(_nullValuesSettingsViewModel.GenerateNullValues));

            ValueGeneratorComboBox.DataSource = _availableValueGenerators;
            ValueGeneratorComboBox.DisplayMember = "GeneratorName";
            if (_columnViewModel.ValueSupplier != null)
                ValueGeneratorComboBox.SelectedItem = _availableValueGenerators.First(x => x.GeneratorBoundType == _columnViewModel.ValueSupplier.GetType());
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            var selectedValue = ValueGeneratorComboBox.SelectedItem as ValueGeneratorViewModel;
            if (selectedValue.UserControl is PatternParserPanel patternParserPanel)
            {
                var patternParser = new ValuePatternParser(patternParserPanel.ValuePattern);
                if (!patternParser.PatternIsValid)
                {
                    ErrorsRichTextBox.Text = patternParser.PatternErrorText;
                    MessageBox.Show("Supplied pattern is invalid!", "Invalid pattern", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                ValueGenerator = patternParser;
            }
            else if (selectedValue.UserControl is ListSelectGeneratorPanel listSelectPanel)
            {
                if (listSelectPanel.ListOfValues.Count == 0)
                {
                    ErrorsRichTextBox.Text = "Provide at least one value in list to select from!";
                    return;
                }

                ValueGenerator = new SelectFromListValueGenerator(listSelectPanel.ListOfValues);
            }
            else if (selectedValue.UserControl is ValueRangeGeneratorPanel valueRangePanel)
            {
                if (valueRangePanel.MinValue == null || valueRangePanel.MaxValue == null)
                {
                    ErrorsRichTextBox.Text = "Either minimum or maximum value is not set!";
                    return;
                }

                if (!valueRangePanel.BothInputsAreCorrect())
                    return;

                ValueGenerator = new ValueRangeValueGenerator(new ValueRange(valueRangePanel.MinValue, valueRangePanel.MaxValue));
            }
            else if (selectedValue.UserControl is IncrementalGeneratorPanel incrementalPanel)
            {
                if (incrementalPanel.StartValue == null || incrementalPanel.EndValue == null || incrementalPanel.ValueStep == null)
                {
                    ErrorsRichTextBox.Text = "Some of the parameters were not set!";
                    return;
                }

                if (!incrementalPanel.AllInputsAreCorrect())
                    return;

                ValueGenerator = new IncrementalValueGenerator(new ValueIncrement(
                    incrementalPanel.StartValue, incrementalPanel.EndValue, incrementalPanel.ValueStep));
            }
            else
            {
                DialogResult = DialogResult.Abort;
                Close();
            }

            ValueGenerator.NullValuesSettings = _nullValuesSettingsViewModel.NullValuesSettings;
            DialogResult = DialogResult.OK;
            Close();
        }

        private void ValueGeneratorComboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            var selectedValue = ValueGeneratorComboBox.SelectedItem as ValueGeneratorViewModel;
            if (selectedValue != null)
            {
                ErrorsRichTextBox.Clear();

                // unsupported type for ValueRangeValueGenerator
                if (selectedValue.GeneratorBoundType == typeof(ValueRangeValueGenerator) &&
                    !ValueRangeGeneratorProvider.ValueGeneratorExists(_columnViewModel.TargetValueType.Type))
                {
                    ErrorsRichTextBox.Text = $"Range value generator is not available for this column type ({_columnViewModel.TargetValueType.Type}). " +
                        $"Supported types: {string.Join(", ", ValueRangeGeneratorProvider.RegisteredValueRangeGenerators.Select(x => x.Key))}";
                    ValueGeneratorParamsGroupBox.Controls.Clear();
                    return;
                }

                // unsupported type for IncrementalValueGenerator
                if (selectedValue.GeneratorBoundType == typeof(IncrementalValueGenerator) &&
                    !IncrementalValueGeneratorProvider.ValueGeneratorExists(_columnViewModel.TargetValueType.Type))
                {
                    ErrorsRichTextBox.Text = $"Incremental value generator is not available for this column type ({_columnViewModel.TargetValueType.Type}). " +
                        $"Supported types: {string.Join(", ", IncrementalValueGeneratorProvider.RegisteredIncrementalValueGenerators.Select(x => x.Key))}";
                    ValueGeneratorParamsGroupBox.Controls.Clear();
                    return;
                }

                // adding binding for errors produced by some panels
                if (selectedValue.UserControl is ListSelectGeneratorPanel ||
                    selectedValue.UserControl is ValueRangeGeneratorPanel ||
                    selectedValue.UserControl is IncrementalGeneratorPanel)
                {
                    ErrorsRichTextBox.DataBindings.Clear();
                    ErrorsRichTextBox.DataBindings.Add("Text", selectedValue.UserControl, "ErrorText");
                }
                ValueGeneratorParamsGroupBox.Controls.Clear();
                selectedValue.UserControl.Dock = DockStyle.Top;
                ValueGeneratorParamsGroupBox.Controls.Add(selectedValue.UserControl);
            }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
