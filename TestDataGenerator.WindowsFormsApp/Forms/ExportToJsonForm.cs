﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestDataGenerator.Abstractions.Interfaces;
using TestDataGenerator.Core;
using TestDataGenerator.IO;
using TestDataGenerator.IO.DatasetWriters;
using TestDataGenerator.IO.DataSetWriters;
using TestDataGenerator.WinFormApp.ViewModels;

namespace TestDataGenerator.WinFormApp.Forms
{
    public partial class ExportToJsonForm : Form
    {
        private TestDataSet _testDataSet;
        private JsonDataSetWriter _datasetWriter;
        private int _rowsToGenerate;

        public ExportToJsonForm(TestDatasetViewModel dataSetViewModel, IProgressReporter progressReporter = null)
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterParent;

            _testDataSet = dataSetViewModel.DataSet;
            _rowsToGenerate = dataSetViewModel.RowsToGenerate;
            _datasetWriter = new JsonDataSetWriter(progressReporter);

            if (!_testDataSet.CanGenerateDataSet(out string error))
            {
                MessageBox.Show($"Unable to generate a preview due to following problems. {error}", "Unable to generate JSON preview", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void SetPathToFileWithSaveFileDialog()
        {
            using (SaveFileDialog sfd = new SaveFileDialog())
            {
                sfd.Filter = "JSON file (*.json)|*.json";
                sfd.RestoreDirectory = true;
                sfd.InitialDirectory = PathToFileTextBox.Text != "" ? PathToFileTextBox.Text : null;
                sfd.FileName = $"{_testDataSet.Name}.json";

                var dialogResult = sfd.ShowDialog(this);

                if (dialogResult == DialogResult.OK)
                {
                    PathToFileTextBox.Text = sfd.FileName;
                }
            }
        }

        private void PathToSaveFileButton_Click(object sender, EventArgs e)
        {
            SetPathToFileWithSaveFileDialog();
        }

        private async void OkButton_Click(object sender, EventArgs e)
        {
            if (!_testDataSet.CanGenerateDataSet(out string errors))
            {
                MessageBox.Show(errors, "Unable to create dataset", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (PathToFileTextBox.Text == "")
            {
                MessageBox.Show("Please specify path to save file!", "Path unspecified", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            try
            {
                ToggleControlsUsability();
                _datasetWriter.WriterSettings = JsonExportParametersPanel.ExportParameters.GetJsonWriterSettings();
                await _datasetWriter.WriteToFileUsingInPlaceRowGenerationAsync(PathToFileTextBox.Text, _testDataSet, _rowsToGenerate);
                MessageBox.Show($"File successfully saved to: {PathToFileTextBox.Text}!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
                Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            finally
            {
                ToggleControlsUsability();
            }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private async void ExportToJsonForm_Shown(object sender, EventArgs e)
        {
            using (MemoryStream memStream = new MemoryStream())
            {
                await Task.Run(() => _testDataSet.GenerateNewDataSet(3));
                await _datasetWriter.WriteToStreamAsync(memStream, _testDataSet);

                JsonPreviewRichTextBox.Text = Encoding.UTF8.GetString(memStream.ToArray());
            }
        }

        private void ToggleControlsUsability()
        {
            OkButton.Enabled = !OkButton.Enabled;
            CancelButton.Enabled = !CancelButton.Enabled;
            PathToSaveFileButton.Enabled = !PathToSaveFileButton.Enabled;
        }

        private async void RefreshJsonPreviewButton_Click(object sender, EventArgs e)
        {
            _datasetWriter.WriterSettings = JsonExportParametersPanel.ExportParameters.GetJsonWriterSettings();
            _datasetWriter.DatasetExportStyle = JsonExportParametersPanel.ExportParameters.DatasetJsonExportStyle.DatasetJsonExportStyle;

            using (MemoryStream memStream = new MemoryStream())
            {
                await Task.Run(() => _testDataSet.GenerateNewDataSet(3));
                await _datasetWriter.WriteToStreamAsync(memStream, _testDataSet);

                JsonPreviewRichTextBox.Text = Encoding.UTF8.GetString(memStream.ToArray());
            }
        }
    }
}
