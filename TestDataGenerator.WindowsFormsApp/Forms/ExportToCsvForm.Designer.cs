﻿
namespace TestDataGenerator.WinFormApp.Forms
{
    partial class ExportToCsvForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GeneralTableLayouPanel = new System.Windows.Forms.TableLayoutPanel();
            this.ControlButtonsSplitContainer = new System.Windows.Forms.SplitContainer();
            this.SaveFileButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.FilePathSplitContainer = new System.Windows.Forms.SplitContainer();
            this.FilePathTextBox = new System.Windows.Forms.TextBox();
            this.OpenFileDialogButton = new System.Windows.Forms.Button();
            this.CsvPreviewRichTextBox = new System.Windows.Forms.RichTextBox();
            this.CsvOptionsGroupBox = new System.Windows.Forms.GroupBox();
            this.GeneralTableLayouPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ControlButtonsSplitContainer)).BeginInit();
            this.ControlButtonsSplitContainer.Panel1.SuspendLayout();
            this.ControlButtonsSplitContainer.Panel2.SuspendLayout();
            this.ControlButtonsSplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FilePathSplitContainer)).BeginInit();
            this.FilePathSplitContainer.Panel1.SuspendLayout();
            this.FilePathSplitContainer.Panel2.SuspendLayout();
            this.FilePathSplitContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // GeneralTableLayouPanel
            // 
            this.GeneralTableLayouPanel.ColumnCount = 2;
            this.GeneralTableLayouPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 378F));
            this.GeneralTableLayouPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 350F));
            this.GeneralTableLayouPanel.Controls.Add(this.ControlButtonsSplitContainer, 0, 2);
            this.GeneralTableLayouPanel.Controls.Add(this.FilePathSplitContainer, 0, 0);
            this.GeneralTableLayouPanel.Controls.Add(this.CsvPreviewRichTextBox, 1, 1);
            this.GeneralTableLayouPanel.Controls.Add(this.CsvOptionsGroupBox, 0, 1);
            this.GeneralTableLayouPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GeneralTableLayouPanel.Location = new System.Drawing.Point(0, 0);
            this.GeneralTableLayouPanel.Name = "GeneralTableLayouPanel";
            this.GeneralTableLayouPanel.RowCount = 3;
            this.GeneralTableLayouPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.GeneralTableLayouPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 302F));
            this.GeneralTableLayouPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 9F));
            this.GeneralTableLayouPanel.Size = new System.Drawing.Size(738, 370);
            this.GeneralTableLayouPanel.TabIndex = 0;
            // 
            // ControlButtonsSplitContainer
            // 
            this.GeneralTableLayouPanel.SetColumnSpan(this.ControlButtonsSplitContainer, 2);
            this.ControlButtonsSplitContainer.Cursor = System.Windows.Forms.Cursors.VSplit;
            this.ControlButtonsSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ControlButtonsSplitContainer.Location = new System.Drawing.Point(3, 337);
            this.ControlButtonsSplitContainer.Name = "ControlButtonsSplitContainer";
            // 
            // ControlButtonsSplitContainer.Panel1
            // 
            this.ControlButtonsSplitContainer.Panel1.Controls.Add(this.SaveFileButton);
            // 
            // ControlButtonsSplitContainer.Panel2
            // 
            this.ControlButtonsSplitContainer.Panel2.Controls.Add(this.CancelButton);
            this.ControlButtonsSplitContainer.Size = new System.Drawing.Size(732, 30);
            this.ControlButtonsSplitContainer.SplitterDistance = 352;
            this.ControlButtonsSplitContainer.TabIndex = 2;
            // 
            // SaveFileButton
            // 
            this.SaveFileButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.SaveFileButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SaveFileButton.Location = new System.Drawing.Point(0, 0);
            this.SaveFileButton.Name = "SaveFileButton";
            this.SaveFileButton.Size = new System.Drawing.Size(352, 30);
            this.SaveFileButton.TabIndex = 0;
            this.SaveFileButton.Text = "Save CSV file";
            this.SaveFileButton.UseVisualStyleBackColor = true;
            this.SaveFileButton.Click += new System.EventHandler(this.SaveFileButton_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CancelButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CancelButton.Location = new System.Drawing.Point(0, 0);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(376, 30);
            this.CancelButton.TabIndex = 0;
            this.CancelButton.Text = "Cancel";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // FilePathSplitContainer
            // 
            this.GeneralTableLayouPanel.SetColumnSpan(this.FilePathSplitContainer, 2);
            this.FilePathSplitContainer.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.FilePathSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FilePathSplitContainer.Location = new System.Drawing.Point(3, 3);
            this.FilePathSplitContainer.Name = "FilePathSplitContainer";
            // 
            // FilePathSplitContainer.Panel1
            // 
            this.FilePathSplitContainer.Panel1.Controls.Add(this.FilePathTextBox);
            // 
            // FilePathSplitContainer.Panel2
            // 
            this.FilePathSplitContainer.Panel2.Controls.Add(this.OpenFileDialogButton);
            this.FilePathSplitContainer.Size = new System.Drawing.Size(732, 26);
            this.FilePathSplitContainer.SplitterDistance = 596;
            this.FilePathSplitContainer.TabIndex = 3;
            // 
            // FilePathTextBox
            // 
            this.FilePathTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.FilePathTextBox.BackColor = System.Drawing.SystemColors.Control;
            this.FilePathTextBox.Location = new System.Drawing.Point(3, 0);
            this.FilePathTextBox.Name = "FilePathTextBox";
            this.FilePathTextBox.PlaceholderText = "Specify path to file...";
            this.FilePathTextBox.ReadOnly = true;
            this.FilePathTextBox.Size = new System.Drawing.Size(590, 23);
            this.FilePathTextBox.TabIndex = 0;
            // 
            // OpenFileDialogButton
            // 
            this.OpenFileDialogButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.OpenFileDialogButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OpenFileDialogButton.Location = new System.Drawing.Point(0, 0);
            this.OpenFileDialogButton.Name = "OpenFileDialogButton";
            this.OpenFileDialogButton.Size = new System.Drawing.Size(132, 26);
            this.OpenFileDialogButton.TabIndex = 0;
            this.OpenFileDialogButton.Text = "Browse...";
            this.OpenFileDialogButton.UseVisualStyleBackColor = true;
            this.OpenFileDialogButton.Click += new System.EventHandler(this.OpenFileDialogButton_Click);
            // 
            // CsvPreviewRichTextBox
            // 
            this.CsvPreviewRichTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.CsvPreviewRichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CsvPreviewRichTextBox.Location = new System.Drawing.Point(381, 35);
            this.CsvPreviewRichTextBox.Name = "CsvPreviewRichTextBox";
            this.CsvPreviewRichTextBox.ReadOnly = true;
            this.CsvPreviewRichTextBox.Size = new System.Drawing.Size(354, 296);
            this.CsvPreviewRichTextBox.TabIndex = 4;
            this.CsvPreviewRichTextBox.Text = "";
            // 
            // CsvOptionsGroupBox
            // 
            this.CsvOptionsGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CsvOptionsGroupBox.Location = new System.Drawing.Point(3, 35);
            this.CsvOptionsGroupBox.Name = "CsvOptionsGroupBox";
            this.CsvOptionsGroupBox.Size = new System.Drawing.Size(372, 296);
            this.CsvOptionsGroupBox.TabIndex = 5;
            this.CsvOptionsGroupBox.TabStop = false;
            this.CsvOptionsGroupBox.Text = "CSV Parameters";
            // 
            // ExportToCsvForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(738, 370);
            this.ControlBox = false;
            this.Controls.Add(this.GeneralTableLayouPanel);
            this.Name = "ExportToCsvForm";
            this.ShowIcon = false;
            this.Text = "Export dataset to CSV file";
            this.Shown += new System.EventHandler(this.ExportToCsvForm_Shown);
            this.GeneralTableLayouPanel.ResumeLayout(false);
            this.ControlButtonsSplitContainer.Panel1.ResumeLayout(false);
            this.ControlButtonsSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ControlButtonsSplitContainer)).EndInit();
            this.ControlButtonsSplitContainer.ResumeLayout(false);
            this.FilePathSplitContainer.Panel1.ResumeLayout(false);
            this.FilePathSplitContainer.Panel1.PerformLayout();
            this.FilePathSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.FilePathSplitContainer)).EndInit();
            this.FilePathSplitContainer.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel GeneralTableLayouPanel;
        private System.Windows.Forms.SplitContainer ControlButtonsSplitContainer;
        private System.Windows.Forms.Button SaveFileButton;
        private new System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.SplitContainer FilePathSplitContainer;
        private System.Windows.Forms.TextBox FilePathTextBox;
        private System.Windows.Forms.Button OpenFileDialogButton;
        private System.Windows.Forms.RichTextBox CsvPreviewRichTextBox;
        private System.Windows.Forms.GroupBox CsvOptionsGroupBox;
    }
}