﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestDataGenerator.WindowsFormsApp.Forms
{
    public partial class AboutForm : Form
    {
        public AboutForm()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterParent;

            this.DescriptionRichTextBox.Text = "• GitLab code repository: https://gitlab.com/Dan4e3/testdatagenerator \n" +
                "• Sourceforge: https://sourceforge.net/projects/testdatagenerator/ \n" +
                "• Project help and wiki: https://gitlab.com/Dan4e3/testdatagenerator/-/wikis/home \n\n" +
                "\n" +
                "If you have any questions/issues, please address to the following page: https://gitlab.com/Dan4e3/testdatagenerator/-/issues \n\n" +
                "Or send an email: mailto:contact-project+dan4e3-testdatagenerator-36631650-issue-@incoming.gitlab.com";
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void DescriptionRichTextBox_LinkClicked(object sender, LinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("explorer.exe", e.LinkText);
        }
    }
}
