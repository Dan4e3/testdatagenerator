﻿
namespace TestDataGenerator.WindowsFormsApp.Forms
{
    partial class LoadFromExternalSourceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GeneralTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.SourceTypeComboBox = new System.Windows.Forms.ComboBox();
            this.SourceParametersGroupBox = new System.Windows.Forms.GroupBox();
            this.LoadDataButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.GeneralTableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // GeneralTableLayoutPanel
            // 
            this.GeneralTableLayoutPanel.ColumnCount = 2;
            this.GeneralTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 186F));
            this.GeneralTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 147F));
            this.GeneralTableLayoutPanel.Controls.Add(this.SourceTypeComboBox, 0, 0);
            this.GeneralTableLayoutPanel.Controls.Add(this.SourceParametersGroupBox, 0, 1);
            this.GeneralTableLayoutPanel.Controls.Add(this.LoadDataButton, 0, 2);
            this.GeneralTableLayoutPanel.Controls.Add(this.CancelButton, 1, 2);
            this.GeneralTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GeneralTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.GeneralTableLayoutPanel.Name = "GeneralTableLayoutPanel";
            this.GeneralTableLayoutPanel.RowCount = 3;
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 231F));
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.GeneralTableLayoutPanel.Size = new System.Drawing.Size(385, 306);
            this.GeneralTableLayoutPanel.TabIndex = 0;
            // 
            // SourceTypeComboBox
            // 
            this.SourceTypeComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.GeneralTableLayoutPanel.SetColumnSpan(this.SourceTypeComboBox, 2);
            this.SourceTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SourceTypeComboBox.FormattingEnabled = true;
            this.SourceTypeComboBox.Location = new System.Drawing.Point(3, 5);
            this.SourceTypeComboBox.Name = "SourceTypeComboBox";
            this.SourceTypeComboBox.Size = new System.Drawing.Size(379, 23);
            this.SourceTypeComboBox.TabIndex = 0;
            this.SourceTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.SourceTypeComboBox_SelectedIndexChanged);
            // 
            // SourceParametersGroupBox
            // 
            this.GeneralTableLayoutPanel.SetColumnSpan(this.SourceParametersGroupBox, 2);
            this.SourceParametersGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SourceParametersGroupBox.Location = new System.Drawing.Point(3, 37);
            this.SourceParametersGroupBox.Name = "SourceParametersGroupBox";
            this.SourceParametersGroupBox.Size = new System.Drawing.Size(379, 225);
            this.SourceParametersGroupBox.TabIndex = 1;
            this.SourceParametersGroupBox.TabStop = false;
            this.SourceParametersGroupBox.Text = "Source loader parameters";
            // 
            // LoadDataButton
            // 
            this.LoadDataButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LoadDataButton.Location = new System.Drawing.Point(3, 268);
            this.LoadDataButton.Name = "LoadDataButton";
            this.LoadDataButton.Size = new System.Drawing.Size(180, 35);
            this.LoadDataButton.TabIndex = 2;
            this.LoadDataButton.Text = "Load data";
            this.LoadDataButton.UseVisualStyleBackColor = true;
            this.LoadDataButton.Click += new System.EventHandler(this.LoadDataButton_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CancelButton.Location = new System.Drawing.Point(189, 268);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(193, 35);
            this.CancelButton.TabIndex = 3;
            this.CancelButton.Text = "Cancel";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // LoadFromExternalSourceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(385, 306);
            this.ControlBox = false;
            this.Controls.Add(this.GeneralTableLayoutPanel);
            this.Name = "LoadFromExternalSourceForm";
            this.Text = "Load data from source";
            this.GeneralTableLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel GeneralTableLayoutPanel;
        private System.Windows.Forms.ComboBox SourceTypeComboBox;
        private System.Windows.Forms.GroupBox SourceParametersGroupBox;
        private System.Windows.Forms.Button LoadDataButton;
        private new System.Windows.Forms.Button CancelButton;
    }
}