﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestDataGenerator.WindowsFormsApp.ViewModels;

namespace TestDataGenerator.WindowsFormsApp.Forms
{
    public partial class FormatOptionsForm : Form
    {
        private FormattingOptionsViewModel _formattingOptsViewModel;

        public CultureInfo ResultingCultureInfo { get; private set; }

        public FormatOptionsForm(CultureInfo existingCustomCulture = null)
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterParent;

            _formattingOptsViewModel = new FormattingOptionsViewModel(existingCustomCulture);

            DecimalSeparatorTexBox.DataBindings.Add("Text", _formattingOptsViewModel, nameof(_formattingOptsViewModel.DecimalSeparator));
            ListSeparatorTextBox.DataBindings.Add("Text", _formattingOptsViewModel, nameof(_formattingOptsViewModel.ListSeparator));
            DateSeparatorTextBox.DataBindings.Add("Text", _formattingOptsViewModel, nameof(_formattingOptsViewModel.DateSeparator));
            DateTimeShortPatternTextBox.DataBindings.Add("Text", _formattingOptsViewModel, nameof(_formattingOptsViewModel.DateTimeShortPattern));
        }

        private void SaveOptionButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            ResultingCultureInfo = _formattingOptsViewModel.ResultCulture;
            Close();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
