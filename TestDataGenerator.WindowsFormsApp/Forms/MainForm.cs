﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestDataGenerator.Abstractions.Interfaces;
using TestDataGenerator.Abstractions.Interfaces.Columns;
using TestDataGenerator.Core;
using TestDataGenerator.Core.Columns;
using TestDataGenerator.Core.JsonSerialization;
using TestDataGenerator.WindowsFormsApp;
using TestDataGenerator.WindowsFormsApp.Forms;
using TestDataGenerator.WinFormApp.ViewModels;

namespace TestDataGenerator.WinFormApp.Forms
{
    public partial class MainForm : Form
    {
        private TestDatasetViewModel _testDatasetViewModel;
        private DataTable _previewDataset;
        private CultureInfo _commonCultureInfo;
        private const string _columnSetBackupFileName = "columnset_backup.bak";
        private const string _formattingOptionsBackupFileName = "formatting_opts_backup.bak";

        public static readonly BindingList<ValueTypeViewModel> AvailableColumnTypes;
        public IProgressReporter ProgressReporter { get; private set; }

        static MainForm()
        {
            AvailableColumnTypes = new BindingList<ValueTypeViewModel>( new ValueTypeViewModel[] {
                new ValueTypeViewModel(typeof(float), "Float (Single)"),
                new ValueTypeViewModel(typeof(double), "Double"),
                new ValueTypeViewModel(typeof(decimal), "Decimal"),
                new ValueTypeViewModel(typeof(sbyte), "Signed Byte"),
                new ValueTypeViewModel(typeof(byte), "Byte (unsigned)"),
                new ValueTypeViewModel(typeof(short), "Short Integer (Int16)"),
                new ValueTypeViewModel(typeof(ushort), "Unsigned Short Integer (UInt16)"),
                new ValueTypeViewModel(typeof(int), "Integer (Int32)"),
                new ValueTypeViewModel(typeof(uint), "Unsigned Integer (UInt32)"),
                new ValueTypeViewModel(typeof(long), "Long Integer (Int64)"),
                new ValueTypeViewModel(typeof(string), "String"),
                new ValueTypeViewModel(typeof(DateTime), "DateTime"),
                new ValueTypeViewModel(typeof(bool), "Boolean")
            }.OrderBy(vm => vm.DisplayName).ToArray());
        }

        public MainForm()
        {
            InitializeComponent();
            TopMenuStrip.CausesValidation = true;

            ProgressReporter = new ProgressReporter(
                (percent) =>  this.Invoke(new Action(() => ToolStringProgressBar.Value = percent)),
                (status, percent) =>
                    this.Invoke(new Action(() => {
                        this.ToolStringProgressBar.Value = percent;
                        this.TolStripStatusLabel.Text = status;
                    }))
            );
            _testDatasetViewModel = new TestDatasetViewModel(ProgressReporter);
            _commonCultureInfo = new CultureInfo(CultureInfoJsonConverter.customCultureName, true);

            RestorePreviousFormValues();
            BindDataToControls();
        }

        private void BindDataToControls()
        {
            ColumnsListBox.DataSource = _testDatasetViewModel.ColumnsViewModelsList;
            ColumnsListBox.DisplayMember = "ColumnName";

            ColumnNameTextBox.DataBindings.Add("Text", _testDatasetViewModel.ColumnsViewModelsList, "ColumnName");

            ColumnDataTypeComboBox.DataSource = AvailableColumnTypes;
            ColumnDataTypeComboBox.DataBindings.Add("SelectedItem", _testDatasetViewModel.ColumnsViewModelsList, "TargetValueType");
            ColumnDataTypeComboBox.DisplayMember = "DisplayName";

            RowsToGenerateTextBox.DataBindings.Add("Text", _testDatasetViewModel, "RowsToGenerate");

            DataSetNameTextBox.DataBindings.Add("Text", _testDatasetViewModel, "DatasetName");

            SelectValueGeneratorButton.DataBindings.Add("Text", _testDatasetViewModel.ColumnsViewModelsList, "ValueSupplierDisplayName");
            SelectValueGeneratorButton.DataBindings.Add("Enabled", _testDatasetViewModel, "CanCreateValueGenerator");

            DatasetPreviewDataGridView.DataSource = _previewDataset;
        }

        private void RestorePreviousFormValues()
        {
            // restoring column set
            if (File.Exists(_columnSetBackupFileName))
            {
                string inputJson = File.ReadAllText(_columnSetBackupFileName);
                ColumnSet colset = ColumnSet.DeserializeFromJson(inputJson);
                foreach (var col in colset)
                    _testDatasetViewModel.ColumnsViewModelsList.Add(new ColumnViewModel(col as Column));
            }

            // restoring culture info formatting
            if (File.Exists(_formattingOptionsBackupFileName))
            {
                string inputJson = File.ReadAllText(_formattingOptionsBackupFileName);
                _commonCultureInfo = JsonConvert.DeserializeObject<CultureInfo>(inputJson, new CultureInfoJsonConverter());
            }

            _testDatasetViewModel.DatasetName = AppSettings.Default.DatasetName;
            _testDatasetViewModel.RowsToGenerate = AppSettings.Default.RowsToGenerate;
        }

        private void AddColumnButton_Click(object sender, EventArgs e)
        {
            int columnCount = ColumnsListBox.Items.Count;
            _testDatasetViewModel.ColumnsViewModelsList.Add(new ColumnViewModel(new Column(new ColumnSettings() { 
                ColumnName = $"ColumnName{columnCount}",
                TargetValueType = typeof(string),
                Culture = _commonCultureInfo
            })));
            ColumnsListBox.SelectedIndex = columnCount;
        }

        private void SelectValueGeneratorButton_Click(object sender, EventArgs e)
        {
            if (_testDatasetViewModel.SelectedColumn == null)
                return;

            using (ValueGeneratorSetupForm valueGeneratorForm = new ValueGeneratorSetupForm(_testDatasetViewModel.SelectedColumn))
            {
                var result = valueGeneratorForm.ShowDialog(this);
                switch (result)
                {
                    case DialogResult.OK:
                        _testDatasetViewModel.SelectedColumn.ValueSupplier = valueGeneratorForm.ValueGenerator;
                        break;

                    case DialogResult.Cancel:
                        if (_testDatasetViewModel.SelectedColumn.ValueSupplier == null)
                            MessageBox.Show("Value generator is not set!", "Value generator info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        break;

                    default:
                        MessageBox.Show("Something went wrong with creating value generator", "Value generator error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                }
            }
        }

        private void ColumnsListBox_SelectedValueChanged(object sender, EventArgs e)
        {
            _testDatasetViewModel.SelectedColumn = ColumnsListBox.SelectedItem as ColumnViewModel;
        }

        private void toJSONToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (ExportToJsonForm exportToJsonForm = new ExportToJsonForm(_testDatasetViewModel, ProgressReporter))
            {
                exportToJsonForm.ShowDialog(this);
            }
        }

        private async void generatePreviewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TestDataSet dset = _testDatasetViewModel.DataSet;

            if (!dset.CanGenerateDataSet(out string errors))
            {
                MessageBox.Show(errors, "Unable to create dataset", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            var sampleRows = await Task.Run(() => dset.GenerateNewDataSet(10));

            _previewDataset = new DataTable();
            foreach (var col in dset.ColumnSet)
                _previewDataset.Columns.Add(new DataColumn(col.GetColumnName(), typeof(string)));

            foreach (var sampleRow in sampleRows)
            {
                _previewDataset.Rows.Add(sampleRow.Select((r, i) => sampleRow.GetStringRepresentation(i)).ToArray());
            }

            DatasetPreviewDataGridView.DataSource = null;
            DatasetPreviewDataGridView.DataSource = _previewDataset;
        }

        private void toDatabaseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (ExportToDatabaseForm exportToDatabseForm = new ExportToDatabaseForm(_testDatasetViewModel, ProgressReporter))
            {
                exportToDatabseForm.ShowDialog(this);
            }
        }

        private void RemoveColumnButton_Click(object sender, EventArgs e)
        {
            if (ColumnsListBox.SelectedItem != null)
            {
                _testDatasetViewModel.ColumnsViewModelsList.Remove(ColumnsListBox.SelectedItem as ColumnViewModel);
            }
        }

        private void TopMenuStrip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            TopMenuStrip.Focus();
        }

        private async void saveColumnsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ColumnSet colset = new ColumnSet(_testDatasetViewModel.ColumnsViewModelsList.Select(x => x.Column));
            if (colset.Count() == 0)
            {
                MessageBox.Show("No columns to save. Add at least one column.", "Columns error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            using (SaveFileDialog sfd = new SaveFileDialog())
            {
                sfd.Filter = "JSON file (*.json)|*.json";
                sfd.RestoreDirectory = true;

                var dialogResult = sfd.ShowDialog(this);

                if (dialogResult == DialogResult.OK)
                {
                    try
                    {
                        string colsetJson = colset.SerializeAsJson();
                        await File.WriteAllTextAsync(sfd.FileName, colsetJson);
                        MessageBox.Show("File was successfully saved!");
                    }
                    catch(Exception ex)
                    {
                        MessageBox.Show($"File was not saved! Error:{ex}", "File save error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
            }
        }

        private void loadColumnsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog ofd = new OpenFileDialog())
            {
                ofd.Filter = "JSON file (*.json)|*.json";
                ofd.RestoreDirectory = true;

                var dialogResult = ofd.ShowDialog(this);

                if (dialogResult == DialogResult.OK)
                {
                    try
                    {
                        string inputJson = File.ReadAllText(ofd.FileName);
                        ColumnSet colset = ColumnSet.DeserializeFromJson(inputJson);
                        _testDatasetViewModel.ColumnsViewModelsList.Clear();
                        foreach (IColumn col in colset)
                        {
                            Column colInstance = col as Column;
                            colInstance.ColumnSettings.Culture = _commonCultureInfo;
                            _testDatasetViewModel.ColumnsViewModelsList.Add(new ColumnViewModel(colInstance));
                        }
                            
                    }
                    catch(Exception ex)
                    {
                        MessageBox.Show($"File failed to process! Error:{ex}", "File process error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
            }
        }

        private void toCSVFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (ExportToCsvForm exportCsvForm = new ExportToCsvForm(_testDatasetViewModel, _commonCultureInfo, ProgressReporter))
            {
                exportCsvForm.ShowDialog(this);
            }
        }

        private void optionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (FormatOptionsForm formatOptsForm = new FormatOptionsForm(_commonCultureInfo))
            {
                DialogResult dialogRes = formatOptsForm.ShowDialog(this);

                if (dialogRes == DialogResult.Cancel)
                    return;

                _commonCultureInfo = formatOptsForm.ResultingCultureInfo;

                foreach (var colViewModel in _testDatasetViewModel.ColumnsViewModelsList)
                    colViewModel.Column.ColumnSettings.Culture = _commonCultureInfo;
            }
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (AboutForm about = new AboutForm())
            {
                about.ShowDialog(this);
            }
        }

        private void newColumnSetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _testDatasetViewModel.ColumnsViewModelsList.Clear();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            // backing up column set
            string colsetAsJson = _testDatasetViewModel.DataSet.ColumnSet.SerializeAsJson();
            File.WriteAllText(_columnSetBackupFileName, colsetAsJson);

            // backing up culture info formatting
            string formattingOptsAsJson = JsonConvert.SerializeObject(_commonCultureInfo, new CultureInfoJsonConverter());
            File.WriteAllText(_formattingOptionsBackupFileName, formattingOptsAsJson);

            // saving mainform fields
            AppSettings.Default.DatasetName = _testDatasetViewModel.DatasetName;
            AppSettings.Default.RowsToGenerate = _testDatasetViewModel.RowsToGenerate;

            AppSettings.Default.Save();
        }
    }
}
