﻿
namespace TestDataGenerator.WinFormApp.Forms
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.GeneralTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.AddColumnButton = new System.Windows.Forms.Button();
            this.RemoveColumnButton = new System.Windows.Forms.Button();
            this.StatusStrip = new System.Windows.Forms.StatusStrip();
            this.TolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.ToolStringProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.ColumnsListBox = new System.Windows.Forms.ListBox();
            this.TopMenuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newColumnSetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.saveColumnsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadColumnsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.formatToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.datasetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toJSONToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toCSVFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toDatabaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generatePreviewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DatasetPreviewGroupBox = new System.Windows.Forms.GroupBox();
            this.DatasetPreviewDataGridView = new System.Windows.Forms.DataGridView();
            this.ColumnParametersGroupBox = new System.Windows.Forms.GroupBox();
            this.ColumnParametersTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.ColumnNameLabel = new System.Windows.Forms.Label();
            this.ColumnNameTextBox = new System.Windows.Forms.TextBox();
            this.ColumnDataTypeLabel = new System.Windows.Forms.Label();
            this.ColumnDataTypeComboBox = new System.Windows.Forms.ComboBox();
            this.ValueGeneratorLabel = new System.Windows.Forms.Label();
            this.SelectValueGeneratorButton = new System.Windows.Forms.Button();
            this.DatasetParametersGroupBox = new System.Windows.Forms.GroupBox();
            this.DatasetParametersTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.RowsToGenerateTextBox = new System.Windows.Forms.TextBox();
            this.RowsToGenerateLabel = new System.Windows.Forms.Label();
            this.DataSetNameLabel = new System.Windows.Forms.Label();
            this.DataSetNameTextBox = new System.Windows.Forms.TextBox();
            this.GeneralTableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.StatusStrip.SuspendLayout();
            this.TopMenuStrip.SuspendLayout();
            this.DatasetPreviewGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DatasetPreviewDataGridView)).BeginInit();
            this.ColumnParametersGroupBox.SuspendLayout();
            this.ColumnParametersTableLayoutPanel.SuspendLayout();
            this.DatasetParametersGroupBox.SuspendLayout();
            this.DatasetParametersTableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // GeneralTableLayoutPanel
            // 
            this.GeneralTableLayoutPanel.ColumnCount = 3;
            this.GeneralTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 207F));
            this.GeneralTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 361F));
            this.GeneralTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 371F));
            this.GeneralTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.GeneralTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.GeneralTableLayoutPanel.Controls.Add(this.splitContainer1, 0, 3);
            this.GeneralTableLayoutPanel.Controls.Add(this.StatusStrip, 0, 4);
            this.GeneralTableLayoutPanel.Controls.Add(this.ColumnsListBox, 0, 1);
            this.GeneralTableLayoutPanel.Controls.Add(this.TopMenuStrip, 0, 0);
            this.GeneralTableLayoutPanel.Controls.Add(this.DatasetPreviewGroupBox, 1, 1);
            this.GeneralTableLayoutPanel.Controls.Add(this.ColumnParametersGroupBox, 1, 2);
            this.GeneralTableLayoutPanel.Controls.Add(this.DatasetParametersGroupBox, 2, 2);
            this.GeneralTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GeneralTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.GeneralTableLayoutPanel.Name = "GeneralTableLayoutPanel";
            this.GeneralTableLayoutPanel.RowCount = 5;
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 234F));
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 169F));
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 39F));
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 14F));
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.GeneralTableLayoutPanel.Size = new System.Drawing.Size(939, 496);
            this.GeneralTableLayoutPanel.TabIndex = 0;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Cursor = System.Windows.Forms.Cursors.VSplit;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(3, 428);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.AddColumnButton);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.RemoveColumnButton);
            this.splitContainer1.Size = new System.Drawing.Size(201, 33);
            this.splitContainer1.SplitterDistance = 94;
            this.splitContainer1.TabIndex = 1;
            // 
            // AddColumnButton
            // 
            this.AddColumnButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.AddColumnButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AddColumnButton.Location = new System.Drawing.Point(0, 0);
            this.AddColumnButton.Name = "AddColumnButton";
            this.AddColumnButton.Size = new System.Drawing.Size(94, 33);
            this.AddColumnButton.TabIndex = 2;
            this.AddColumnButton.Text = "Add column";
            this.AddColumnButton.UseVisualStyleBackColor = true;
            this.AddColumnButton.Click += new System.EventHandler(this.AddColumnButton_Click);
            // 
            // RemoveColumnButton
            // 
            this.RemoveColumnButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.RemoveColumnButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RemoveColumnButton.Location = new System.Drawing.Point(0, 0);
            this.RemoveColumnButton.Name = "RemoveColumnButton";
            this.RemoveColumnButton.Size = new System.Drawing.Size(103, 33);
            this.RemoveColumnButton.TabIndex = 3;
            this.RemoveColumnButton.Text = "Remove column";
            this.RemoveColumnButton.UseVisualStyleBackColor = true;
            this.RemoveColumnButton.Click += new System.EventHandler(this.RemoveColumnButton_Click);
            // 
            // StatusStrip
            // 
            this.GeneralTableLayoutPanel.SetColumnSpan(this.StatusStrip, 3);
            this.StatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TolStripStatusLabel,
            this.ToolStringProgressBar});
            this.StatusStrip.Location = new System.Drawing.Point(0, 474);
            this.StatusStrip.Name = "StatusStrip";
            this.StatusStrip.Size = new System.Drawing.Size(939, 22);
            this.StatusStrip.TabIndex = 6;
            // 
            // TolStripStatusLabel
            // 
            this.TolStripStatusLabel.Name = "TolStripStatusLabel";
            this.TolStripStatusLabel.Size = new System.Drawing.Size(622, 17);
            this.TolStripStatusLabel.Spring = true;
            this.TolStripStatusLabel.Text = "No active task...";
            this.TolStripStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ToolStringProgressBar
            // 
            this.ToolStringProgressBar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.ToolStringProgressBar.AutoSize = false;
            this.ToolStringProgressBar.Name = "ToolStringProgressBar";
            this.ToolStringProgressBar.Size = new System.Drawing.Size(300, 16);
            // 
            // ColumnsListBox
            // 
            this.ColumnsListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ColumnsListBox.FormattingEnabled = true;
            this.ColumnsListBox.ItemHeight = 15;
            this.ColumnsListBox.Location = new System.Drawing.Point(3, 25);
            this.ColumnsListBox.Name = "ColumnsListBox";
            this.GeneralTableLayoutPanel.SetRowSpan(this.ColumnsListBox, 2);
            this.ColumnsListBox.Size = new System.Drawing.Size(201, 397);
            this.ColumnsListBox.TabIndex = 0;
            this.ColumnsListBox.SelectedValueChanged += new System.EventHandler(this.ColumnsListBox_SelectedValueChanged);
            // 
            // TopMenuStrip
            // 
            this.GeneralTableLayoutPanel.SetColumnSpan(this.TopMenuStrip, 3);
            this.TopMenuStrip.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TopMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.formatToolStripMenuItem,
            this.datasetToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.TopMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.TopMenuStrip.Name = "TopMenuStrip";
            this.TopMenuStrip.Size = new System.Drawing.Size(939, 22);
            this.TopMenuStrip.TabIndex = 2;
            this.TopMenuStrip.Text = "menuStrip1";
            this.TopMenuStrip.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.TopMenuStrip_ItemClicked);
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newColumnSetToolStripMenuItem,
            this.toolStripSeparator1,
            this.saveColumnsToolStripMenuItem,
            this.loadColumnsToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 18);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newColumnSetToolStripMenuItem
            // 
            this.newColumnSetToolStripMenuItem.Name = "newColumnSetToolStripMenuItem";
            this.newColumnSetToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.newColumnSetToolStripMenuItem.Text = "New column set";
            this.newColumnSetToolStripMenuItem.Click += new System.EventHandler(this.newColumnSetToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(157, 6);
            // 
            // saveColumnsToolStripMenuItem
            // 
            this.saveColumnsToolStripMenuItem.Name = "saveColumnsToolStripMenuItem";
            this.saveColumnsToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.saveColumnsToolStripMenuItem.Text = "Save columns...";
            this.saveColumnsToolStripMenuItem.Click += new System.EventHandler(this.saveColumnsToolStripMenuItem_Click);
            // 
            // loadColumnsToolStripMenuItem
            // 
            this.loadColumnsToolStripMenuItem.Name = "loadColumnsToolStripMenuItem";
            this.loadColumnsToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.loadColumnsToolStripMenuItem.Text = "Load columns...";
            this.loadColumnsToolStripMenuItem.Click += new System.EventHandler(this.loadColumnsToolStripMenuItem_Click);
            // 
            // formatToolStripMenuItem
            // 
            this.formatToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.optionsToolStripMenuItem});
            this.formatToolStripMenuItem.Name = "formatToolStripMenuItem";
            this.formatToolStripMenuItem.Size = new System.Drawing.Size(57, 18);
            this.formatToolStripMenuItem.Text = "Format";
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.optionsToolStripMenuItem.Text = "Options...";
            this.optionsToolStripMenuItem.Click += new System.EventHandler(this.optionsToolStripMenuItem_Click);
            // 
            // datasetToolStripMenuItem
            // 
            this.datasetToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exportToolStripMenuItem,
            this.generatePreviewToolStripMenuItem});
            this.datasetToolStripMenuItem.Name = "datasetToolStripMenuItem";
            this.datasetToolStripMenuItem.Size = new System.Drawing.Size(58, 18);
            this.datasetToolStripMenuItem.Text = "Dataset";
            // 
            // exportToolStripMenuItem
            // 
            this.exportToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toJSONToolStripMenuItem,
            this.toCSVFileToolStripMenuItem,
            this.toDatabaseToolStripMenuItem});
            this.exportToolStripMenuItem.Name = "exportToolStripMenuItem";
            this.exportToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.exportToolStripMenuItem.Text = "Export...";
            // 
            // toJSONToolStripMenuItem
            // 
            this.toJSONToolStripMenuItem.Name = "toJSONToolStripMenuItem";
            this.toJSONToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.toJSONToolStripMenuItem.Text = "To JSON file";
            this.toJSONToolStripMenuItem.Click += new System.EventHandler(this.toJSONToolStripMenuItem_Click);
            // 
            // toCSVFileToolStripMenuItem
            // 
            this.toCSVFileToolStripMenuItem.Name = "toCSVFileToolStripMenuItem";
            this.toCSVFileToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.toCSVFileToolStripMenuItem.Text = "To CSV file";
            this.toCSVFileToolStripMenuItem.Click += new System.EventHandler(this.toCSVFileToolStripMenuItem_Click);
            // 
            // toDatabaseToolStripMenuItem
            // 
            this.toDatabaseToolStripMenuItem.Name = "toDatabaseToolStripMenuItem";
            this.toDatabaseToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.toDatabaseToolStripMenuItem.Text = "To database";
            this.toDatabaseToolStripMenuItem.Click += new System.EventHandler(this.toDatabaseToolStripMenuItem_Click);
            // 
            // generatePreviewToolStripMenuItem
            // 
            this.generatePreviewToolStripMenuItem.Name = "generatePreviewToolStripMenuItem";
            this.generatePreviewToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.generatePreviewToolStripMenuItem.Text = "Generate preview";
            this.generatePreviewToolStripMenuItem.Click += new System.EventHandler(this.generatePreviewToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(52, 18);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // DatasetPreviewGroupBox
            // 
            this.GeneralTableLayoutPanel.SetColumnSpan(this.DatasetPreviewGroupBox, 2);
            this.DatasetPreviewGroupBox.Controls.Add(this.DatasetPreviewDataGridView);
            this.DatasetPreviewGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DatasetPreviewGroupBox.Location = new System.Drawing.Point(210, 25);
            this.DatasetPreviewGroupBox.Name = "DatasetPreviewGroupBox";
            this.DatasetPreviewGroupBox.Size = new System.Drawing.Size(726, 228);
            this.DatasetPreviewGroupBox.TabIndex = 3;
            this.DatasetPreviewGroupBox.TabStop = false;
            this.DatasetPreviewGroupBox.Text = "Dataset preview";
            // 
            // DatasetPreviewDataGridView
            // 
            this.DatasetPreviewDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DatasetPreviewDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DatasetPreviewDataGridView.Location = new System.Drawing.Point(3, 19);
            this.DatasetPreviewDataGridView.Name = "DatasetPreviewDataGridView";
            this.DatasetPreviewDataGridView.RowTemplate.Height = 25;
            this.DatasetPreviewDataGridView.Size = new System.Drawing.Size(720, 206);
            this.DatasetPreviewDataGridView.TabIndex = 0;
            // 
            // ColumnParametersGroupBox
            // 
            this.ColumnParametersGroupBox.Controls.Add(this.ColumnParametersTableLayoutPanel);
            this.ColumnParametersGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ColumnParametersGroupBox.Location = new System.Drawing.Point(210, 259);
            this.ColumnParametersGroupBox.Name = "ColumnParametersGroupBox";
            this.GeneralTableLayoutPanel.SetRowSpan(this.ColumnParametersGroupBox, 2);
            this.ColumnParametersGroupBox.Size = new System.Drawing.Size(355, 202);
            this.ColumnParametersGroupBox.TabIndex = 4;
            this.ColumnParametersGroupBox.TabStop = false;
            this.ColumnParametersGroupBox.Text = "Column parameters";
            // 
            // ColumnParametersTableLayoutPanel
            // 
            this.ColumnParametersTableLayoutPanel.ColumnCount = 2;
            this.ColumnParametersTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 132F));
            this.ColumnParametersTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 217F));
            this.ColumnParametersTableLayoutPanel.Controls.Add(this.ColumnNameLabel, 0, 0);
            this.ColumnParametersTableLayoutPanel.Controls.Add(this.ColumnNameTextBox, 1, 0);
            this.ColumnParametersTableLayoutPanel.Controls.Add(this.ColumnDataTypeLabel, 0, 1);
            this.ColumnParametersTableLayoutPanel.Controls.Add(this.ColumnDataTypeComboBox, 1, 1);
            this.ColumnParametersTableLayoutPanel.Controls.Add(this.ValueGeneratorLabel, 0, 2);
            this.ColumnParametersTableLayoutPanel.Controls.Add(this.SelectValueGeneratorButton, 1, 2);
            this.ColumnParametersTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ColumnParametersTableLayoutPanel.Location = new System.Drawing.Point(3, 19);
            this.ColumnParametersTableLayoutPanel.Name = "ColumnParametersTableLayoutPanel";
            this.ColumnParametersTableLayoutPanel.RowCount = 5;
            this.ColumnParametersTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.ColumnParametersTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 39F));
            this.ColumnParametersTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.ColumnParametersTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.ColumnParametersTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 19F));
            this.ColumnParametersTableLayoutPanel.Size = new System.Drawing.Size(349, 180);
            this.ColumnParametersTableLayoutPanel.TabIndex = 0;
            // 
            // ColumnNameLabel
            // 
            this.ColumnNameLabel.AutoSize = true;
            this.ColumnNameLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ColumnNameLabel.Location = new System.Drawing.Point(3, 0);
            this.ColumnNameLabel.Name = "ColumnNameLabel";
            this.ColumnNameLabel.Size = new System.Drawing.Size(126, 40);
            this.ColumnNameLabel.TabIndex = 0;
            this.ColumnNameLabel.Text = "Name:";
            this.ColumnNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ColumnNameTextBox
            // 
            this.ColumnNameTextBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.ColumnNameTextBox.Location = new System.Drawing.Point(135, 8);
            this.ColumnNameTextBox.Name = "ColumnNameTextBox";
            this.ColumnNameTextBox.Size = new System.Drawing.Size(211, 23);
            this.ColumnNameTextBox.TabIndex = 1;
            // 
            // ColumnDataTypeLabel
            // 
            this.ColumnDataTypeLabel.AutoSize = true;
            this.ColumnDataTypeLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ColumnDataTypeLabel.Location = new System.Drawing.Point(3, 40);
            this.ColumnDataTypeLabel.Name = "ColumnDataTypeLabel";
            this.ColumnDataTypeLabel.Size = new System.Drawing.Size(126, 39);
            this.ColumnDataTypeLabel.TabIndex = 2;
            this.ColumnDataTypeLabel.Text = "Data type:";
            this.ColumnDataTypeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ColumnDataTypeComboBox
            // 
            this.ColumnDataTypeComboBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.ColumnDataTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ColumnDataTypeComboBox.FormattingEnabled = true;
            this.ColumnDataTypeComboBox.Location = new System.Drawing.Point(135, 48);
            this.ColumnDataTypeComboBox.Name = "ColumnDataTypeComboBox";
            this.ColumnDataTypeComboBox.Size = new System.Drawing.Size(211, 23);
            this.ColumnDataTypeComboBox.TabIndex = 3;
            // 
            // ValueGeneratorLabel
            // 
            this.ValueGeneratorLabel.AutoSize = true;
            this.ValueGeneratorLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ValueGeneratorLabel.Location = new System.Drawing.Point(3, 79);
            this.ValueGeneratorLabel.Name = "ValueGeneratorLabel";
            this.ValueGeneratorLabel.Size = new System.Drawing.Size(126, 35);
            this.ValueGeneratorLabel.TabIndex = 4;
            this.ValueGeneratorLabel.Text = "Value generator:";
            this.ValueGeneratorLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // SelectValueGeneratorButton
            // 
            this.SelectValueGeneratorButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.SelectValueGeneratorButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SelectValueGeneratorButton.Location = new System.Drawing.Point(135, 82);
            this.SelectValueGeneratorButton.Name = "SelectValueGeneratorButton";
            this.SelectValueGeneratorButton.Size = new System.Drawing.Size(211, 29);
            this.SelectValueGeneratorButton.TabIndex = 5;
            this.SelectValueGeneratorButton.Text = "Select...";
            this.SelectValueGeneratorButton.UseVisualStyleBackColor = true;
            this.SelectValueGeneratorButton.Click += new System.EventHandler(this.SelectValueGeneratorButton_Click);
            // 
            // DatasetParametersGroupBox
            // 
            this.DatasetParametersGroupBox.Controls.Add(this.DatasetParametersTableLayoutPanel);
            this.DatasetParametersGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DatasetParametersGroupBox.Location = new System.Drawing.Point(571, 259);
            this.DatasetParametersGroupBox.Name = "DatasetParametersGroupBox";
            this.GeneralTableLayoutPanel.SetRowSpan(this.DatasetParametersGroupBox, 2);
            this.DatasetParametersGroupBox.Size = new System.Drawing.Size(365, 202);
            this.DatasetParametersGroupBox.TabIndex = 5;
            this.DatasetParametersGroupBox.TabStop = false;
            this.DatasetParametersGroupBox.Text = "Dataset parameters";
            // 
            // DatasetParametersTableLayoutPanel
            // 
            this.DatasetParametersTableLayoutPanel.ColumnCount = 2;
            this.DatasetParametersTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 161F));
            this.DatasetParametersTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 198F));
            this.DatasetParametersTableLayoutPanel.Controls.Add(this.RowsToGenerateTextBox, 1, 1);
            this.DatasetParametersTableLayoutPanel.Controls.Add(this.RowsToGenerateLabel, 0, 1);
            this.DatasetParametersTableLayoutPanel.Controls.Add(this.DataSetNameLabel, 0, 0);
            this.DatasetParametersTableLayoutPanel.Controls.Add(this.DataSetNameTextBox, 1, 0);
            this.DatasetParametersTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DatasetParametersTableLayoutPanel.Location = new System.Drawing.Point(3, 19);
            this.DatasetParametersTableLayoutPanel.Name = "DatasetParametersTableLayoutPanel";
            this.DatasetParametersTableLayoutPanel.RowCount = 4;
            this.DatasetParametersTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.DatasetParametersTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.DatasetParametersTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.DatasetParametersTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.DatasetParametersTableLayoutPanel.Size = new System.Drawing.Size(359, 180);
            this.DatasetParametersTableLayoutPanel.TabIndex = 0;
            // 
            // RowsToGenerateTextBox
            // 
            this.RowsToGenerateTextBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.RowsToGenerateTextBox.Location = new System.Drawing.Point(164, 32);
            this.RowsToGenerateTextBox.Name = "RowsToGenerateTextBox";
            this.RowsToGenerateTextBox.Size = new System.Drawing.Size(192, 23);
            this.RowsToGenerateTextBox.TabIndex = 1;
            // 
            // RowsToGenerateLabel
            // 
            this.RowsToGenerateLabel.AutoSize = true;
            this.RowsToGenerateLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RowsToGenerateLabel.Location = new System.Drawing.Point(3, 29);
            this.RowsToGenerateLabel.Name = "RowsToGenerateLabel";
            this.RowsToGenerateLabel.Size = new System.Drawing.Size(155, 25);
            this.RowsToGenerateLabel.TabIndex = 0;
            this.RowsToGenerateLabel.Text = "Rows to generate:";
            this.RowsToGenerateLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // DataSetNameLabel
            // 
            this.DataSetNameLabel.AutoSize = true;
            this.DataSetNameLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DataSetNameLabel.Location = new System.Drawing.Point(3, 0);
            this.DataSetNameLabel.Name = "DataSetNameLabel";
            this.DataSetNameLabel.Size = new System.Drawing.Size(155, 29);
            this.DataSetNameLabel.TabIndex = 2;
            this.DataSetNameLabel.Text = "Dataset name:";
            this.DataSetNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // DataSetNameTextBox
            // 
            this.DataSetNameTextBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.DataSetNameTextBox.Location = new System.Drawing.Point(164, 3);
            this.DataSetNameTextBox.Name = "DataSetNameTextBox";
            this.DataSetNameTextBox.Size = new System.Drawing.Size(192, 23);
            this.DataSetNameTextBox.TabIndex = 3;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(939, 496);
            this.Controls.Add(this.GeneralTableLayoutPanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "TDG - Test Dataset Generator";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.GeneralTableLayoutPanel.ResumeLayout(false);
            this.GeneralTableLayoutPanel.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.StatusStrip.ResumeLayout(false);
            this.StatusStrip.PerformLayout();
            this.TopMenuStrip.ResumeLayout(false);
            this.TopMenuStrip.PerformLayout();
            this.DatasetPreviewGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DatasetPreviewDataGridView)).EndInit();
            this.ColumnParametersGroupBox.ResumeLayout(false);
            this.ColumnParametersTableLayoutPanel.ResumeLayout(false);
            this.ColumnParametersTableLayoutPanel.PerformLayout();
            this.DatasetParametersGroupBox.ResumeLayout(false);
            this.DatasetParametersTableLayoutPanel.ResumeLayout(false);
            this.DatasetParametersTableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel GeneralTableLayoutPanel;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button AddColumnButton;
        private System.Windows.Forms.Button RemoveColumnButton;
        private System.Windows.Forms.MenuStrip TopMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem datasetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toJSONToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toCSVFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toDatabaseToolStripMenuItem;
        private System.Windows.Forms.GroupBox ColumnParametersGroupBox;
        private System.Windows.Forms.GroupBox DatasetPreviewGroupBox;
        private System.Windows.Forms.ListBox ColumnsListBox;
        private System.Windows.Forms.GroupBox DatasetParametersGroupBox;
        private System.Windows.Forms.DataGridView DatasetPreviewDataGridView;
        private System.Windows.Forms.TableLayoutPanel ColumnParametersTableLayoutPanel;
        private System.Windows.Forms.Label ColumnNameLabel;
        private System.Windows.Forms.TextBox ColumnNameTextBox;
        private System.Windows.Forms.TableLayoutPanel DatasetParametersTableLayoutPanel;
        private System.Windows.Forms.Label ColumnDataTypeLabel;
        private System.Windows.Forms.ComboBox ColumnDataTypeComboBox;
        private System.Windows.Forms.Label ValueGeneratorLabel;
        private System.Windows.Forms.Button SelectValueGeneratorButton;
        private System.Windows.Forms.Label RowsToGenerateLabel;
        private System.Windows.Forms.TextBox RowsToGenerateTextBox;
        private System.Windows.Forms.Label DataSetNameLabel;
        private System.Windows.Forms.TextBox DataSetNameTextBox;
        private System.Windows.Forms.ToolStripMenuItem generatePreviewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveColumnsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadColumnsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem formatToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.StatusStrip StatusStrip;
        private System.Windows.Forms.ToolStripStatusLabel TolStripStatusLabel;
        private System.Windows.Forms.ToolStripProgressBar ToolStringProgressBar;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newColumnSetToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
    }
}

