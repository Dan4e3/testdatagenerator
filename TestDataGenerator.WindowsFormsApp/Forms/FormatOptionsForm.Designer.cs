﻿
namespace TestDataGenerator.WindowsFormsApp.Forms
{
    partial class FormatOptionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GeneralTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.DateTimeShortPatternLabel = new System.Windows.Forms.Label();
            this.DateTimeShortPatternTextBox = new System.Windows.Forms.TextBox();
            this.DateSeparatorLabel = new System.Windows.Forms.Label();
            this.DateSeparatorTextBox = new System.Windows.Forms.TextBox();
            this.ListSeparatorLabel = new System.Windows.Forms.Label();
            this.ListSeparatorTextBox = new System.Windows.Forms.TextBox();
            this.DecimalSeparatorLabel = new System.Windows.Forms.Label();
            this.DecimalSeparatorTexBox = new System.Windows.Forms.TextBox();
            this.ControlButtonsSplitContainer = new System.Windows.Forms.SplitContainer();
            this.SaveOptionButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.GeneralTableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ControlButtonsSplitContainer)).BeginInit();
            this.ControlButtonsSplitContainer.Panel1.SuspendLayout();
            this.ControlButtonsSplitContainer.Panel2.SuspendLayout();
            this.ControlButtonsSplitContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // GeneralTableLayoutPanel
            // 
            this.GeneralTableLayoutPanel.ColumnCount = 2;
            this.GeneralTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.GeneralTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 276F));
            this.GeneralTableLayoutPanel.Controls.Add(this.DateTimeShortPatternLabel, 0, 3);
            this.GeneralTableLayoutPanel.Controls.Add(this.DateTimeShortPatternTextBox, 1, 3);
            this.GeneralTableLayoutPanel.Controls.Add(this.DateSeparatorLabel, 0, 2);
            this.GeneralTableLayoutPanel.Controls.Add(this.DateSeparatorTextBox, 1, 2);
            this.GeneralTableLayoutPanel.Controls.Add(this.ListSeparatorLabel, 0, 1);
            this.GeneralTableLayoutPanel.Controls.Add(this.ListSeparatorTextBox, 1, 1);
            this.GeneralTableLayoutPanel.Controls.Add(this.DecimalSeparatorLabel, 0, 0);
            this.GeneralTableLayoutPanel.Controls.Add(this.DecimalSeparatorTexBox, 1, 0);
            this.GeneralTableLayoutPanel.Controls.Add(this.ControlButtonsSplitContainer, 0, 5);
            this.GeneralTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GeneralTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.GeneralTableLayoutPanel.Name = "GeneralTableLayoutPanel";
            this.GeneralTableLayoutPanel.RowCount = 6;
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 21F));
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.GeneralTableLayoutPanel.Size = new System.Drawing.Size(436, 188);
            this.GeneralTableLayoutPanel.TabIndex = 0;
            // 
            // DateTimeShortPatternLabel
            // 
            this.DateTimeShortPatternLabel.AutoSize = true;
            this.DateTimeShortPatternLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DateTimeShortPatternLabel.Location = new System.Drawing.Point(3, 90);
            this.DateTimeShortPatternLabel.Name = "DateTimeShortPatternLabel";
            this.DateTimeShortPatternLabel.Size = new System.Drawing.Size(154, 30);
            this.DateTimeShortPatternLabel.TabIndex = 7;
            this.DateTimeShortPatternLabel.Text = "Datetime short pattern:";
            this.DateTimeShortPatternLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // DateTimeShortPatternTextBox
            // 
            this.DateTimeShortPatternTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.DateTimeShortPatternTextBox.Location = new System.Drawing.Point(163, 93);
            this.DateTimeShortPatternTextBox.Name = "DateTimeShortPatternTextBox";
            this.DateTimeShortPatternTextBox.Size = new System.Drawing.Size(270, 23);
            this.DateTimeShortPatternTextBox.TabIndex = 8;
            // 
            // DateSeparatorLabel
            // 
            this.DateSeparatorLabel.AutoSize = true;
            this.DateSeparatorLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DateSeparatorLabel.Location = new System.Drawing.Point(3, 60);
            this.DateSeparatorLabel.Name = "DateSeparatorLabel";
            this.DateSeparatorLabel.Size = new System.Drawing.Size(154, 30);
            this.DateSeparatorLabel.TabIndex = 5;
            this.DateSeparatorLabel.Text = "Date separator:";
            this.DateSeparatorLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // DateSeparatorTextBox
            // 
            this.DateSeparatorTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.DateSeparatorTextBox.Location = new System.Drawing.Point(163, 63);
            this.DateSeparatorTextBox.Name = "DateSeparatorTextBox";
            this.DateSeparatorTextBox.Size = new System.Drawing.Size(270, 23);
            this.DateSeparatorTextBox.TabIndex = 6;
            // 
            // ListSeparatorLabel
            // 
            this.ListSeparatorLabel.AutoSize = true;
            this.ListSeparatorLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ListSeparatorLabel.Location = new System.Drawing.Point(3, 30);
            this.ListSeparatorLabel.Name = "ListSeparatorLabel";
            this.ListSeparatorLabel.Size = new System.Drawing.Size(154, 30);
            this.ListSeparatorLabel.TabIndex = 3;
            this.ListSeparatorLabel.Text = "List separator:";
            this.ListSeparatorLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ListSeparatorTextBox
            // 
            this.ListSeparatorTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ListSeparatorTextBox.Location = new System.Drawing.Point(163, 33);
            this.ListSeparatorTextBox.Name = "ListSeparatorTextBox";
            this.ListSeparatorTextBox.Size = new System.Drawing.Size(270, 23);
            this.ListSeparatorTextBox.TabIndex = 4;
            // 
            // DecimalSeparatorLabel
            // 
            this.DecimalSeparatorLabel.AutoSize = true;
            this.DecimalSeparatorLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DecimalSeparatorLabel.Location = new System.Drawing.Point(3, 0);
            this.DecimalSeparatorLabel.Name = "DecimalSeparatorLabel";
            this.DecimalSeparatorLabel.Size = new System.Drawing.Size(154, 30);
            this.DecimalSeparatorLabel.TabIndex = 0;
            this.DecimalSeparatorLabel.Text = "Decimal separator:";
            this.DecimalSeparatorLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // DecimalSeparatorTexBox
            // 
            this.DecimalSeparatorTexBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.DecimalSeparatorTexBox.Location = new System.Drawing.Point(163, 3);
            this.DecimalSeparatorTexBox.Name = "DecimalSeparatorTexBox";
            this.DecimalSeparatorTexBox.Size = new System.Drawing.Size(270, 23);
            this.DecimalSeparatorTexBox.TabIndex = 1;
            // 
            // ControlButtonsSplitContainer
            // 
            this.GeneralTableLayoutPanel.SetColumnSpan(this.ControlButtonsSplitContainer, 2);
            this.ControlButtonsSplitContainer.Cursor = System.Windows.Forms.Cursors.VSplit;
            this.ControlButtonsSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ControlButtonsSplitContainer.IsSplitterFixed = true;
            this.ControlButtonsSplitContainer.Location = new System.Drawing.Point(3, 144);
            this.ControlButtonsSplitContainer.Name = "ControlButtonsSplitContainer";
            // 
            // ControlButtonsSplitContainer.Panel1
            // 
            this.ControlButtonsSplitContainer.Panel1.Controls.Add(this.SaveOptionButton);
            // 
            // ControlButtonsSplitContainer.Panel2
            // 
            this.ControlButtonsSplitContainer.Panel2.Controls.Add(this.CancelButton);
            this.ControlButtonsSplitContainer.Size = new System.Drawing.Size(430, 41);
            this.ControlButtonsSplitContainer.SplitterDistance = 200;
            this.ControlButtonsSplitContainer.TabIndex = 2;
            // 
            // SaveOptionButton
            // 
            this.SaveOptionButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.SaveOptionButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SaveOptionButton.Location = new System.Drawing.Point(0, 0);
            this.SaveOptionButton.Name = "SaveOptionButton";
            this.SaveOptionButton.Size = new System.Drawing.Size(200, 41);
            this.SaveOptionButton.TabIndex = 0;
            this.SaveOptionButton.Text = "Save changes";
            this.SaveOptionButton.UseVisualStyleBackColor = true;
            this.SaveOptionButton.Click += new System.EventHandler(this.SaveOptionButton_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CancelButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CancelButton.Location = new System.Drawing.Point(0, 0);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(226, 41);
            this.CancelButton.TabIndex = 0;
            this.CancelButton.Text = "Cancel";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // FormatOptionsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(436, 188);
            this.Controls.Add(this.GeneralTableLayoutPanel);
            this.Name = "FormatOptionsForm";
            this.ShowIcon = false;
            this.Text = "Data formatting options";
            this.GeneralTableLayoutPanel.ResumeLayout(false);
            this.GeneralTableLayoutPanel.PerformLayout();
            this.ControlButtonsSplitContainer.Panel1.ResumeLayout(false);
            this.ControlButtonsSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ControlButtonsSplitContainer)).EndInit();
            this.ControlButtonsSplitContainer.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel GeneralTableLayoutPanel;
        private System.Windows.Forms.Label DecimalSeparatorLabel;
        private System.Windows.Forms.TextBox DecimalSeparatorTexBox;
        private System.Windows.Forms.Label DateTimeShortPatternLabel;
        private System.Windows.Forms.TextBox DateTimeShortPatternTextBox;
        private System.Windows.Forms.Label DateSeparatorLabel;
        private System.Windows.Forms.TextBox DateSeparatorTextBox;
        private System.Windows.Forms.Label ListSeparatorLabel;
        private System.Windows.Forms.TextBox ListSeparatorTextBox;
        private System.Windows.Forms.SplitContainer ControlButtonsSplitContainer;
        private System.Windows.Forms.Button SaveOptionButton;
        private new System.Windows.Forms.Button CancelButton;
    }
}