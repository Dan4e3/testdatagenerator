﻿
namespace TestDataGenerator.WinFormApp.Forms
{
    partial class ExportToDatabaseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GeneralTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.ConnectionStringGroupBox = new System.Windows.Forms.GroupBox();
            this.ConnectionStringTextBox = new System.Windows.Forms.TextBox();
            this.OkButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.ParametersGroupBox = new System.Windows.Forms.GroupBox();
            this.AdditionalParametersTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.DataTableNameTextBox = new System.Windows.Forms.TextBox();
            this.DataTableNameLabel = new System.Windows.Forms.Label();
            this.DatabaseProviderLabel = new System.Windows.Forms.Label();
            this.DatabaseProviderComboBox = new System.Windows.Forms.ComboBox();
            this.GeneralTableLayoutPanel.SuspendLayout();
            this.ConnectionStringGroupBox.SuspendLayout();
            this.ParametersGroupBox.SuspendLayout();
            this.AdditionalParametersTableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // GeneralTableLayoutPanel
            // 
            this.GeneralTableLayoutPanel.ColumnCount = 2;
            this.GeneralTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 284F));
            this.GeneralTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 221F));
            this.GeneralTableLayoutPanel.Controls.Add(this.ConnectionStringGroupBox, 0, 0);
            this.GeneralTableLayoutPanel.Controls.Add(this.OkButton, 0, 2);
            this.GeneralTableLayoutPanel.Controls.Add(this.CancelButton, 1, 2);
            this.GeneralTableLayoutPanel.Controls.Add(this.ParametersGroupBox, 0, 1);
            this.GeneralTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GeneralTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.GeneralTableLayoutPanel.Name = "GeneralTableLayoutPanel";
            this.GeneralTableLayoutPanel.RowCount = 3;
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 198F));
            this.GeneralTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.GeneralTableLayoutPanel.Size = new System.Drawing.Size(611, 291);
            this.GeneralTableLayoutPanel.TabIndex = 0;
            // 
            // ConnectionStringGroupBox
            // 
            this.GeneralTableLayoutPanel.SetColumnSpan(this.ConnectionStringGroupBox, 2);
            this.ConnectionStringGroupBox.Controls.Add(this.ConnectionStringTextBox);
            this.ConnectionStringGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ConnectionStringGroupBox.Location = new System.Drawing.Point(3, 3);
            this.ConnectionStringGroupBox.Name = "ConnectionStringGroupBox";
            this.ConnectionStringGroupBox.Size = new System.Drawing.Size(605, 54);
            this.ConnectionStringGroupBox.TabIndex = 0;
            this.ConnectionStringGroupBox.TabStop = false;
            this.ConnectionStringGroupBox.Text = "Connection string";
            // 
            // ConnectionStringTextBox
            // 
            this.ConnectionStringTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ConnectionStringTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.ConnectionStringTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ConnectionStringTextBox.Location = new System.Drawing.Point(3, 19);
            this.ConnectionStringTextBox.Name = "ConnectionStringTextBox";
            this.ConnectionStringTextBox.Size = new System.Drawing.Size(599, 23);
            this.ConnectionStringTextBox.TabIndex = 0;
            // 
            // OkButton
            // 
            this.OkButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OkButton.Location = new System.Drawing.Point(3, 261);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(278, 27);
            this.OkButton.TabIndex = 1;
            this.OkButton.Text = "Save data to database";
            this.OkButton.UseVisualStyleBackColor = true;
            this.OkButton.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CancelButton.Location = new System.Drawing.Point(287, 261);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(321, 27);
            this.CancelButton.TabIndex = 2;
            this.CancelButton.Text = "Cancel";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // ParametersGroupBox
            // 
            this.GeneralTableLayoutPanel.SetColumnSpan(this.ParametersGroupBox, 2);
            this.ParametersGroupBox.Controls.Add(this.AdditionalParametersTableLayoutPanel);
            this.ParametersGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ParametersGroupBox.Location = new System.Drawing.Point(3, 63);
            this.ParametersGroupBox.Name = "ParametersGroupBox";
            this.ParametersGroupBox.Size = new System.Drawing.Size(605, 192);
            this.ParametersGroupBox.TabIndex = 3;
            this.ParametersGroupBox.TabStop = false;
            this.ParametersGroupBox.Text = "Additional parameters";
            // 
            // AdditionalParametersTableLayoutPanel
            // 
            this.AdditionalParametersTableLayoutPanel.ColumnCount = 2;
            this.AdditionalParametersTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 188F));
            this.AdditionalParametersTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 411F));
            this.AdditionalParametersTableLayoutPanel.Controls.Add(this.DataTableNameTextBox, 1, 1);
            this.AdditionalParametersTableLayoutPanel.Controls.Add(this.DataTableNameLabel, 0, 1);
            this.AdditionalParametersTableLayoutPanel.Controls.Add(this.DatabaseProviderLabel, 0, 0);
            this.AdditionalParametersTableLayoutPanel.Controls.Add(this.DatabaseProviderComboBox, 1, 0);
            this.AdditionalParametersTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AdditionalParametersTableLayoutPanel.Location = new System.Drawing.Point(3, 19);
            this.AdditionalParametersTableLayoutPanel.Name = "AdditionalParametersTableLayoutPanel";
            this.AdditionalParametersTableLayoutPanel.RowCount = 3;
            this.AdditionalParametersTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.AdditionalParametersTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.AdditionalParametersTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.AdditionalParametersTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.AdditionalParametersTableLayoutPanel.Size = new System.Drawing.Size(599, 170);
            this.AdditionalParametersTableLayoutPanel.TabIndex = 0;
            // 
            // DataTableNameTextBox
            // 
            this.DataTableNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.DataTableNameTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.DataTableNameTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.DataTableNameTextBox.Location = new System.Drawing.Point(191, 35);
            this.DataTableNameTextBox.Name = "DataTableNameTextBox";
            this.DataTableNameTextBox.Size = new System.Drawing.Size(405, 23);
            this.DataTableNameTextBox.TabIndex = 0;
            // 
            // DataTableNameLabel
            // 
            this.DataTableNameLabel.AutoSize = true;
            this.DataTableNameLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DataTableNameLabel.Location = new System.Drawing.Point(3, 30);
            this.DataTableNameLabel.Name = "DataTableNameLabel";
            this.DataTableNameLabel.Size = new System.Drawing.Size(182, 34);
            this.DataTableNameLabel.TabIndex = 1;
            this.DataTableNameLabel.Text = "Table name:";
            this.DataTableNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // DatabaseProviderLabel
            // 
            this.DatabaseProviderLabel.AutoSize = true;
            this.DatabaseProviderLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DatabaseProviderLabel.Location = new System.Drawing.Point(3, 0);
            this.DatabaseProviderLabel.Name = "DatabaseProviderLabel";
            this.DatabaseProviderLabel.Size = new System.Drawing.Size(182, 30);
            this.DatabaseProviderLabel.TabIndex = 2;
            this.DatabaseProviderLabel.Text = "Database provider:";
            this.DatabaseProviderLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // DatabaseProviderComboBox
            // 
            this.DatabaseProviderComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.DatabaseProviderComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.DatabaseProviderComboBox.FormattingEnabled = true;
            this.DatabaseProviderComboBox.Location = new System.Drawing.Point(191, 3);
            this.DatabaseProviderComboBox.Name = "DatabaseProviderComboBox";
            this.DatabaseProviderComboBox.Size = new System.Drawing.Size(405, 23);
            this.DatabaseProviderComboBox.TabIndex = 3;
            // 
            // ExportToDatabaseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(611, 291);
            this.ControlBox = false;
            this.Controls.Add(this.GeneralTableLayoutPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "ExportToDatabaseForm";
            this.Text = "Export dataset to database";
            this.GeneralTableLayoutPanel.ResumeLayout(false);
            this.ConnectionStringGroupBox.ResumeLayout(false);
            this.ConnectionStringGroupBox.PerformLayout();
            this.ParametersGroupBox.ResumeLayout(false);
            this.AdditionalParametersTableLayoutPanel.ResumeLayout(false);
            this.AdditionalParametersTableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel GeneralTableLayoutPanel;
        private System.Windows.Forms.GroupBox ConnectionStringGroupBox;
        private System.Windows.Forms.TextBox ConnectionStringTextBox;
        private System.Windows.Forms.Button OkButton;
        private new System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.GroupBox ParametersGroupBox;
        private System.Windows.Forms.TableLayoutPanel AdditionalParametersTableLayoutPanel;
        private System.Windows.Forms.TextBox DataTableNameTextBox;
        private System.Windows.Forms.Label DataTableNameLabel;
        private System.Windows.Forms.Label DatabaseProviderLabel;
        private System.Windows.Forms.ComboBox DatabaseProviderComboBox;
    }
}