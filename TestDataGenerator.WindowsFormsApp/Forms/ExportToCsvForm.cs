﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestDataGenerator.Abstractions.Interfaces;
using TestDataGenerator.Core;
using TestDataGenerator.IO;
using TestDataGenerator.IO.DatasetWriters;
using TestDataGenerator.WinFormApp.ViewModels;

namespace TestDataGenerator.WinFormApp.Forms
{
    public partial class ExportToCsvForm : Form
    {
        private TestDataSet _testDataSet;
        private CsvDataSetWriter _datasetWriter;
        private int _rowsToGenerate;

        public ExportToCsvForm(TestDatasetViewModel dataSetViewModel, 
            CultureInfo datasetCultureInfo = null, IProgressReporter progressReporter = null)
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterParent;

            _testDataSet = dataSetViewModel.DataSet;
            _rowsToGenerate = dataSetViewModel.RowsToGenerate;
            _datasetWriter = new CsvDataSetWriter(datasetCultureInfo, progressReporter);


            if (!_testDataSet.CanGenerateDataSet(out string error))
            {
                MessageBox.Show($"Unable to generate a preview due to following problems. {error}", "Unable to generate CSV preview", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void OpenFileDialogButton_Click(object sender, EventArgs e)
        {
            using (SaveFileDialog sfd = new SaveFileDialog())
            {
                sfd.Filter = "CSV file (*.csv)|*.csv";
                sfd.RestoreDirectory = true;
                sfd.InitialDirectory = FilePathTextBox.Text != "" ? FilePathTextBox.Text : null;
                sfd.FileName = $"{_testDataSet.Name}.csv";

                var dialogResult = sfd.ShowDialog(this);

                if (dialogResult == DialogResult.OK)
                {
                    FilePathTextBox.Text = sfd.FileName;
                }
            }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private async void SaveFileButton_Click(object sender, EventArgs e)
        {
            if (!_testDataSet.CanGenerateDataSet(out string errors))
            {
                MessageBox.Show(errors, "Unable to create dataset", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (FilePathTextBox.Text == "")
            {
                MessageBox.Show("Please specify path to save file!", "Path unspecified", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            try
            {
                ToggleControlsUsability();
                await _datasetWriter.WriteToFileUsingInPlaceRowGenerationAsync(FilePathTextBox.Text, _testDataSet, _rowsToGenerate);
                MessageBox.Show($"File successfully saved to: {FilePathTextBox.Text}!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            finally
            {
                ToggleControlsUsability();
            }
        }

        private async void ExportToCsvForm_Shown(object sender, EventArgs e)
        {
            using (MemoryStream memStream = new MemoryStream())
            {
                await Task.Run(() => _testDataSet.GenerateNewDataSet(10));
                _datasetWriter.WriteToStreamAsync(memStream, _testDataSet).Wait();

                CsvPreviewRichTextBox.Text = Encoding.UTF8.GetString(memStream.ToArray());
            }
        }

        private void ToggleControlsUsability()
        {
            SaveFileButton.Enabled = !SaveFileButton.Enabled;
            CancelButton.Enabled = !CancelButton.Enabled;
            OpenFileDialogButton.Enabled = !OpenFileDialogButton.Enabled;
        }
    }
}
