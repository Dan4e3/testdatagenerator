﻿CREATE TABLE [tdg].[DatasetGenerationRequests] (
    [Id]                         BIGINT          IDENTITY (1, 1) NOT NULL,
    [RequestingUserId]           BIGINT          NOT NULL,
    [ColumnSetId]                BIGINT          NULL,
    [RowsToGenerate]             INT             NOT NULL,
    [RowsJson]                   NVARCHAR (MAX)  NULL,
    [FileContents]               VARBINARY (MAX) NULL,
    [FileExtension]              NVARCHAR (10)   NULL,
    [FetchedAtLeastOnce]         BIT             NOT NULL,
    [WorkerResponseReceived]     BIT             NOT NULL,
    [CreationDate]               DATETIME        CONSTRAINT [DF_DatasetGenerationRequests_CreationDate] DEFAULT (getdate()) NOT NULL,
    [WorkerResponseReceivedDate] DATETIME        NULL,
    [ErrorText]                  NVARCHAR (MAX)  NULL,
    [SystemExceptionText]        NVARCHAR (MAX)  NULL,
    CONSTRAINT [PK_DatasetGenerationRequests] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_DatasetGenerationRequests_Users] FOREIGN KEY ([RequestingUserId]) REFERENCES [tdg].[Users] ([Id]) ON DELETE CASCADE ON UPDATE CASCADE
);




GO
CREATE NONCLUSTERED INDEX [IX_RequestingUserId]
    ON [tdg].[DatasetGenerationRequests]([RequestingUserId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_FetchedAtLeastOnce_WorkerResponseReceived]
    ON [tdg].[DatasetGenerationRequests]([FetchedAtLeastOnce] ASC, [WorkerResponseReceived] ASC);

