﻿CREATE TABLE [tdg].[PredefinedCategories] (
    [Id]                BIGINT         IDENTITY (1, 1) NOT NULL,
    [Name]              NVARCHAR (200) NOT NULL,
    [ContributorUserId] BIGINT         NOT NULL,
    [IsPublic]          BIT            NOT NULL,
    [CreationDate]      DATETIME       CONSTRAINT [DF_PredefinedCategories_CreationDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_PredefinedCategories] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_PredefinedCategories_Users] FOREIGN KEY ([ContributorUserId]) REFERENCES [tdg].[Users] ([Id]) ON DELETE CASCADE ON UPDATE CASCADE
);








GO



GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Name]
    ON [tdg].[PredefinedCategories]([Name] ASC);




GO
CREATE NONCLUSTERED INDEX [IX_ContributorUserId]
    ON [tdg].[PredefinedCategories]([ContributorUserId] ASC);

