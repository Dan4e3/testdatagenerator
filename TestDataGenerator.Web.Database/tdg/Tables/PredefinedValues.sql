CREATE TABLE [tdg].[PredefinedValues] (
    [Id]                BIGINT         IDENTITY (1, 1) NOT NULL,
    [Value]             NVARCHAR (300) NOT NULL,
    [DataType]          TINYINT        NOT NULL,
    [CategoryId]        BIGINT         NOT NULL,
    [ContributorUserId] BIGINT         NOT NULL,
    [CreationDate]      DATETIME       CONSTRAINT [DF_PredefinedValues_CreationDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_PredefinedValues] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_PredefinedValues_PredefinedCategories] FOREIGN KEY ([CategoryId]) REFERENCES [tdg].[PredefinedCategories] ([Id]),
    CONSTRAINT [FK_PredefinedValues_Users] FOREIGN KEY ([ContributorUserId]) REFERENCES [tdg].[Users] ([Id]) ON DELETE CASCADE ON UPDATE CASCADE
);








GO
CREATE NONCLUSTERED INDEX [IX_ContributorUserId]
    ON [tdg].[PredefinedValues]([ContributorUserId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_CategoryId]
    ON [tdg].[PredefinedValues]([CategoryId] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Value_DataType_CategoryId]
    ON [tdg].[PredefinedValues]([Value] ASC, [DataType] ASC, [CategoryId] ASC);

