CREATE TABLE [tdg].[Users] (
    [Id]               BIGINT         IDENTITY (1, 1) NOT NULL,
    [Name]             NVARCHAR (150) NOT NULL,
    [Password]         NVARCHAR (256) NOT NULL,
    [AccessToken]      NVARCHAR (256) NULL,
    [RegistrationDate] DATETIME       CONSTRAINT [DF_Users_RegistrationDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED ([Id] ASC)
);






GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Username]
    ON [tdg].[Users]([Name] ASC);

