﻿CREATE TABLE [tdg].[ColumnSets] (
    [Id]                         BIGINT         IDENTITY (1, 1) NOT NULL,
    [Name]                       NVARCHAR (256) NULL,
    [ColumnSetJson]              NVARCHAR (MAX) NOT NULL,
    [AdditionalColumnParamsJson] NVARCHAR (MAX) NULL,
    [OwnerUserId]                BIGINT         NOT NULL,
    [CreationDate]               DATETIME       CONSTRAINT [DF_ColumnSets_CreationDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_ColumnSets] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ColumnSets_Users] FOREIGN KEY ([OwnerUserId]) REFERENCES [tdg].[Users] ([Id]) ON DELETE CASCADE ON UPDATE CASCADE
);






GO
CREATE NONCLUSTERED INDEX [IX_OwnerUserId]
    ON [tdg].[ColumnSets]([OwnerUserId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Name]
    ON [tdg].[ColumnSets]([Name] ASC);

