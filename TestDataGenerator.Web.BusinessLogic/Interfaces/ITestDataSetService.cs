﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces.Rows;
using TestDataGenerator.Core;
using TestDataGenerator.Web.Models.DomainModels;

namespace TestDataGenerator.Web.BusinessLogic.Interfaces
{
    public interface ITestDataSetService
    {
        public Task<Row[]> GetDatasetWithPreSavedColumnSetAsync(long columnSetId, string requestingUserName, int rowsToGenerate = 25);

        public Task<long> CreateDatasetGenerationRequestAndPostToMqAsync(long columnSetId, string requestingUserName,
            int rowsToGenerate = 10000, string fileExtension = null);

        public Task<long> CreateDatasetGenerationRequestAndPostToMqAsync(ColumnSetDm colsetDm, string requestingUserName,
            int rowsToGenerate = 10000, string fileExtension = null);

        public Task<DatasetGenerationRequestDm> GetDatasetGenerationRequestAsync(long requestId, string requestingUserName);

        public Task<DatasetGenerationRequestDm[]> GetAllDatasetGenerationRequestsByUser(string requestingUserName);

        public Task<Row[]> GenerateDatasetAsync(ColumnSetDm datasetParameters, int rowsToGenerate);

        public Task<DatasetGenerationRequestDm> GetGenerationRequestStateAsync(long requestId, string requestingUserName);

        public Task<DatasetGenerationRequestDm[]> GetGenerationRequestsPaginatedAsync(string requestingUserName, 
            int pageNumber, int requestsPerPage);
    }
}
