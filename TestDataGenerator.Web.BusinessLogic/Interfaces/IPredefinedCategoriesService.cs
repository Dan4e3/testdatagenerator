﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Web.Models.DomainModels;

namespace TestDataGenerator.Web.BusinessLogic.Interfaces
{
    public interface IPredefinedCategoriesService
    {
        public Task<PredefinedCategoryDm[]> GetPredefinedCategoriesFromDatabaseAsync(string contributorUserName, bool includePublic);

        public Task<PredefinedCategoryDm[]> GetPublicPredefinedCategoriesFromDatabaseAsync();

        public Task<PredefinedCategoryDm> CreatePredefinedCategoryAsync(PredefinedCategoryDm inputModel, string contributorUserName);

        public Task<int> DeletePredefinedCategoriesAsync(PredefinedCategoryDm[] categoriesToDelete, string requestorUserName);
    }
}
