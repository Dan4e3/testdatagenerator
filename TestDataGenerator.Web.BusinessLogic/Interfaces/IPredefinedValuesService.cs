﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Web.Models.DomainModels;
using TestDataGenerator.Web.Models.DomainModels.Enums;

namespace TestDataGenerator.Web.BusinessLogic.Interfaces
{
    public interface IPredefinedValuesService
    {
        public Task<PredefinedValueDm[]> GetPredefinedValuesAsync(string categoryName, string requestingUserName, int? valuesToFetch = 1000);

        public Task<PredefinedValueDm[]> GetPredefinedValuesPaginatedAsync(string categoryName, string requestingUserName,
            int pageNumber, int valuesPerPage);

        public Task<long> GetPredefinedValuesCountInCategoryAsync(string categoryName, string requestingUserName);

        public Task<int> AddPredefinedValuesAsync(PredefinedValueDm[] inputData, string categoryName, string inserterUserName);

        public Task<PredefinedValueDm> AddPredefinedValueAsync(PredefinedValueDm inputData, string inserterUserName);

        public  Task<int> DeletePredefinedValuesAsync(IEnumerable<long> predefinedValuesIdsToDelete);
    }
}
