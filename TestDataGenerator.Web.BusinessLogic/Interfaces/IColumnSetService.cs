﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Core.Columns;
using TestDataGenerator.Web.Models.DomainModels;

namespace TestDataGenerator.Web.BusinessLogic.Interfaces
{
    public interface IColumnSetService
    {
        public Task SaveColumnSetInDataBaseAsync(ColumnSetDm inputColset, string ownerUserName);

        public Task<ColumnSetDm> GetColumnSetFromDataBaseAsync(long colSetId, string requestingUserName);

        public Task<ColumnSetDm[]> GetAllColumnSetsOfUser(string requestingUserName);

        public Task<int> RemoveColumnSetFromDatabaseAsync(long columnSetId, string requestingUserName);

        public Task<bool> UpdateColumnSetAsync(ColumnSetDm colsetUpdateData, string requestingUserName);

        public Task<Dictionary<string, string>> ValidateAllColumnsInColumnSetAsync(string colSetAsCustomJson, string additionalParamsAsJson);
    }
}
