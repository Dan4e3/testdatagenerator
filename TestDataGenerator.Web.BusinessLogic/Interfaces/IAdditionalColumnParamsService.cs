﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Core.Columns;
using TestDataGenerator.Web.Models.AdditionalColumnParams;
using TestDataGenerator.Web.Models.DomainModels;

namespace TestDataGenerator.Web.BusinessLogic.Interfaces
{
    public interface IAdditionalColumnParamsService
    {
        Task ApplyAdditionalParamsAsync(ColumnSet colSet, AdditionalColumnParameters[] colParams);

        Task ApplyAdditionalParamsAsync(Column column, AdditionalColumnParameters singleColParams);
    }
}
