﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.Web.BusinessLogic.Interfaces
{
    public interface IRecaptchaService
    {
        Task<bool> IsCaptchaPassedAsync(string token);
    }
}
