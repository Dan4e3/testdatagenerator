﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Web.Models.DomainModels;

namespace TestDataGenerator.Web.BusinessLogic.Interfaces
{
    public interface IUserHandlingService
    {
        public Task<UserDm> RegisterUserAsync(UserDm userDm);

        public Task<bool> UserCredentialsAreCorrectAsync(string username, string password);
    }
}
