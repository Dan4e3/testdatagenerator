﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Web.BusinessLogic.Interfaces;
using TestDataGenerator.Web.DataAccess.Interfaces;

namespace TestDataGenerator.Web.BusinessLogic.Services
{
    public class RecaptchaService : IRecaptchaService
    {
        private readonly IBasicServiceOptions _serviceOpts;
        private readonly IHttpClientFactory _httpClientFactory;

        public RecaptchaService(
            IHttpClientFactory httpClientFactory,
            IBasicServiceOptions serviceOpts)
        {
            _serviceOpts = serviceOpts;
            _httpClientFactory = httpClientFactory;
        }

        public async Task<bool> IsCaptchaPassedAsync(string token)
        {
            dynamic response = await GetCaptchaResultDataAsync(token);
            if (response.success == "true")
            {
                return Convert.ToDouble(response.score) >= _serviceOpts.ReCaptchaOptions.AcceptableScore;
            }
            return false;
        }

        public async Task<JObject> GetCaptchaResultDataAsync(string token)
        {
            var content = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("secret", _serviceOpts.ReCaptchaOptions.SecretKey),
                new KeyValuePair<string, string>("response", token)
            });
            using var httpClient = _httpClientFactory.CreateClient();
            var res = await httpClient.PostAsync(_serviceOpts.ReCaptchaOptions.RecaptchaVerifyAddress, content);
            if (res.StatusCode != HttpStatusCode.OK)
            {
                throw new HttpRequestException(res.ReasonPhrase);
            }
            var jsonResult = await res.Content.ReadAsStringAsync();
            return JObject.Parse(jsonResult);
        }
    }
}
