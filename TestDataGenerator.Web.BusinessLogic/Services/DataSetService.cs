﻿using AutoMapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces.Rows;
using TestDataGenerator.Core;
using TestDataGenerator.Core.Columns;
using TestDataGenerator.Web.BusinessLogic.Interfaces;
using TestDataGenerator.Web.DataAccess.Interfaces.Repositories;
using TestDataGenerator.Web.Models.AdditionalColumnParams;
using TestDataGenerator.Web.Models.DomainModels;
using TestDataGenerator.Web.Mq.Interfaces;
using TestDataGenerator.Web.Mq.Models;

namespace TestDataGenerator.Web.BusinessLogic.Services
{
    public class DataSetService : ITestDataSetService
    {
        private readonly IColumnSetRepository _colSetRepo;
        private readonly IColumnSetService _colsetService;
        private readonly IUserRepository _userRepo;
        private readonly IDatasetGenerationRequestRepository _datasetReqRepo;
        private readonly IMqClient _mqClient;
        private readonly IAdditionalColumnParamsService _additionalColumnParamsService;

        public DataSetService(
            IColumnSetService colsetService,
            IColumnSetRepository colSetRepo,
            IDatasetGenerationRequestRepository datasetReqRepo,
            IUserRepository userRepo,
            IMqClient mqClient,
            IAdditionalColumnParamsService additionalColumnParamsService)
        {
            _colsetService = colsetService;
            _colSetRepo = colSetRepo;
            _datasetReqRepo = datasetReqRepo;
            _userRepo = userRepo;
            _mqClient = mqClient;
            _additionalColumnParamsService = additionalColumnParamsService;
        }

        public async Task<Row[]> GetDatasetWithPreSavedColumnSetAsync(long columnSetId, string requestingUserName, int rowsToGenerate = 25)
        {
            ColumnSetDm columnSetDm = await _colSetRepo.GetSingleByIdAsync(columnSetId);

            if (columnSetDm == null)
                throw new CustomBusinessLogicException($"Column set with Id={columnSetId} was not found!");

            if (columnSetDm.OwnerUser.Name != requestingUserName)
                throw new CustomBusinessLogicException($"You are unauthorized to use column set with Id={columnSetId}!");

            ColumnSet colSet = ColumnSet.DeserializeFromJson(columnSetDm.ColumnSetJson);
            AdditionalColumnParameters[] additionalColumnParams = 
                JsonConvert.DeserializeObject<AdditionalColumnParameters[]>(columnSetDm.AdditionalColumnParamsJson);
            await _additionalColumnParamsService.ApplyAdditionalParamsAsync(colSet, additionalColumnParams);

            TestDataSet dataSet = new TestDataSet("DataSet", colSet);
            Row[] generatedRows = dataSet
                .GenerateNewDataSet(rowsToGenerate)
                .Cast<Row>()
                .ToArray();

            return generatedRows;
        }

        public async Task<Row[]> GenerateDatasetAsync(ColumnSetDm colsetDm, int rowsToGenerate)
        {
            ColumnSet columnSet = ColumnSet.DeserializeFromJson(colsetDm.ColumnSetJson);
            AdditionalColumnParameters[] additionalColumnParams =
                JsonConvert.DeserializeObject<AdditionalColumnParameters[]>(colsetDm.AdditionalColumnParamsJson);
            await _additionalColumnParamsService.ApplyAdditionalParamsAsync(columnSet, additionalColumnParams);

            Dictionary<string, string> columnsValidationResult = 
                await _colsetService.ValidateAllColumnsInColumnSetAsync(colsetDm.ColumnSetJson, colsetDm.AdditionalColumnParamsJson);
            if (columnsValidationResult.Count > 0)
            {
                throw new CustomBusinessLogicException(string.Join("\r\n",
                    columnsValidationResult.Select(pair => $"Column '{pair.Key}' is not valid due to following reasons: '{pair.Value}'")));
            }

            TestDataSet dataSet = new TestDataSet("DataSet", columnSet);
            Row[] generatedRows = dataSet
                .GenerateNewDataSet(rowsToGenerate)
                .Cast<Row>()
                .ToArray();

            return generatedRows;
        }

        public async Task<long> CreateDatasetGenerationRequestAndPostToMqAsync(long columnSetId, string requestingUserName,
            int rowsToGenerate = 10000, string fileExtension = null)
        {
            ColumnSetDm columnSetDm = await _colSetRepo.GetSingleByIdAsync(columnSetId);

            if (columnSetDm == null)
                throw new CustomBusinessLogicException($"Column set with Id={columnSetId} was not found!");

            if (columnSetDm.OwnerUser.Name != requestingUserName)
                throw new CustomBusinessLogicException($"You are unauthorized to use column set with Id={columnSetId}!");

            DatasetGenerationRequestDm generationRequest = new DatasetGenerationRequestDm() {
                RequestingUserId = columnSetDm.OwnerUserId,
                CreationDate = DateTime.Now,
                RowsToGenerate = rowsToGenerate
            };

            long requestId = await _datasetReqRepo.CreateSingleAsync(generationRequest);

            ColumnSet columnSet = ColumnSet.DeserializeFromJson(columnSetDm.ColumnSetJson);
            AdditionalColumnParameters[] additionalColumnParams =
                JsonConvert.DeserializeObject<AdditionalColumnParameters[]>(columnSetDm.AdditionalColumnParamsJson);
            await _additionalColumnParamsService.ApplyAdditionalParamsAsync(columnSet, additionalColumnParams);

            TestDataSetCreateMqDto datasetSetup = new TestDataSetCreateMqDto() { 
                RequestId = requestId,
                ColumnSetAsJson = columnSet.SerializeAsJson(),
                RowsToGenerate = rowsToGenerate,
                FileExtension = fileExtension
            };
            string mqMessagePayload = JsonConvert.SerializeObject(datasetSetup);

            _mqClient.SendMessage(mqMessagePayload, requestId);

            return requestId;
        }

        public async Task<long> CreateDatasetGenerationRequestAndPostToMqAsync(ColumnSetDm colsetDm, string requestingUserName,
            int rowsToGenerate = 10000, string fileExtension = null)
        {
            UserDm userDm = await _userRepo.GetSingleByNameAsync(requestingUserName);

            if (userDm == null)
                throw new CustomBusinessLogicException($"User with username '{requestingUserName}' is not registered!");

            Dictionary<string, string> columnsValidationResult = 
                await _colsetService.ValidateAllColumnsInColumnSetAsync(colsetDm.ColumnSetJson, colsetDm.AdditionalColumnParamsJson);
            if (columnsValidationResult.Count > 0)
            {
                throw new CustomBusinessLogicException(string.Join("\r\n",
                    columnsValidationResult.Select(pair => $"Column '{pair.Key}' is not valid due to following reasons: '{pair.Value}'")));
            }

            DatasetGenerationRequestDm generationRequest = new DatasetGenerationRequestDm()
            {
                ColumnSetId = colsetDm.Id > 0 ? colsetDm.Id : null,
                RequestingUserId = userDm.Id,
                CreationDate = DateTime.Now,
                RowsToGenerate = rowsToGenerate,
                FileExtension = fileExtension
            };

            long requestId = await _datasetReqRepo.CreateSingleAsync(generationRequest);

            ColumnSet columnSet = ColumnSet.DeserializeFromJson(colsetDm.ColumnSetJson);
            AdditionalColumnParameters[] additionalColumnParams =
                JsonConvert.DeserializeObject<AdditionalColumnParameters[]>(colsetDm.AdditionalColumnParamsJson);
            await _additionalColumnParamsService.ApplyAdditionalParamsAsync(columnSet, additionalColumnParams);

            TestDataSetCreateMqDto datasetSetup = new TestDataSetCreateMqDto()
            {
                RequestId = requestId,
                ColumnSetAsJson = columnSet.SerializeAsJson(),
                RowsToGenerate = rowsToGenerate,
                FileExtension = fileExtension
            };
            string mqMessagePayload = JsonConvert.SerializeObject(datasetSetup);

            _mqClient.SendMessage(mqMessagePayload, requestId);

            return requestId;
        }

        public async Task<DatasetGenerationRequestDm> GetDatasetGenerationRequestAsync(long requestId, string requestingUserName)
        {
            DatasetGenerationRequestDm generationRequest = await _datasetReqRepo.GetSingleByIdAsync(requestId);

            if (generationRequest == null)
                throw new CustomBusinessLogicException($"Generation request {requestId} does not exist or it was " +
                    $"deleted according to file persistence policy.");

            if (generationRequest.RequestingUser.Name != requestingUserName)
                throw new CustomBusinessLogicException("You are not allowed to load this dataset since you don't own it!");

            if (!generationRequest.WorkerResponseReceived)
                throw new CustomBusinessLogicException("Your dataset generation request has not been processed yet. Please wait a few seconds more.");

            return generationRequest;
        }

        /// <summary>
        /// Get dataset generation completion status. 
        /// Does not load file contents in order to reduce RAM usage for large datasets.
        /// </summary>
        /// <param name="requestId">Generation request Id.</param>
        /// <param name="requestingUserName">Authorized user's username.</param>
        /// <returns></returns>
        /// <exception cref="CustomBusinessLogicException"></exception>
        public async Task<DatasetGenerationRequestDm> GetGenerationRequestStateAsync(long requestId, string requestingUserName)
        {
            DatasetGenerationRequestDm generationRequest = await _datasetReqRepo.GetSinglePartialStateByIdAsync(requestId);

            if (generationRequest == null)
                throw new CustomBusinessLogicException($"Provided requestId={requestId} does not exist!");

            if (generationRequest.RequestingUser.Name != requestingUserName)
                throw new CustomBusinessLogicException("You are not allowed to load this dataset since you don't own it!");

            return generationRequest;
        }

        public async Task<DatasetGenerationRequestDm[]> GetAllDatasetGenerationRequestsByUser(string requestingUserName)
        {
            DatasetGenerationRequestDm[] generationRequests = await _datasetReqRepo.GetManyByUserNameAsync(requestingUserName);

            return generationRequests;
        }

        public async Task<DatasetGenerationRequestDm[]> GetGenerationRequestsPaginatedAsync(string requestingUserName,
            int pageNumber, int requestsPerPage)
        {
            int valuesToSkip = pageNumber * requestsPerPage;

            DatasetGenerationRequestDm[] generationRequests = 
                await _datasetReqRepo.GetManyByUserNamePaginatedAsync(requestingUserName, valuesToSkip, requestsPerPage);

            return generationRequests;
        }
    }
}
