﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces.Columns;
using TestDataGenerator.Core.Columns;
using TestDataGenerator.Core.ValueGenerators;
using TestDataGenerator.Web.BusinessLogic.Interfaces;
using TestDataGenerator.Web.DataAccess.Interfaces.Repositories;
using TestDataGenerator.Web.Models.AdditionalColumnParams;
using TestDataGenerator.Web.Models.DomainModels;

namespace TestDataGenerator.Web.BusinessLogic.Services
{
    public class AdditionalColumnParamsService: IAdditionalColumnParamsService
    {
        private readonly IPredefinedValueRepository _predefinedValueRepo;

        public AdditionalColumnParamsService(
            IPredefinedValueRepository predefinedValueRepo)
        {
            _predefinedValueRepo = predefinedValueRepo;
        }

        public async Task ApplyAdditionalParamsAsync(ColumnSet colSet, AdditionalColumnParameters[] colParams)
        {
            foreach (IColumn col in colSet)
            {
                Column castedCol = (Column)col;
                AdditionalColumnParameters additionalColParams = colParams
                    .FirstOrDefault(c => c.ColumnName == castedCol.ColumnSettings.ColumnName);
                if (additionalColParams == null)
                    continue;

                await ApplyAdditionalParamsAsync(castedCol, additionalColParams);
            }
        }

        public async Task ApplyAdditionalParamsAsync(Column column, AdditionalColumnParameters singleColParams)
        {
            if (column.ColumnSettings.ValueSupplier is SelectFromListValueGenerator selFromListValGenerator)
            {
                SelectFromListAdditionalColumnParams selFromListAdditionalParams =
                    JsonConvert.DeserializeObject<SelectFromListAdditionalColumnParams>(singleColParams.ParamsJson);
                PredefinedValueDm[] predefinedValuesInCategories =
                    await _predefinedValueRepo.GetManyByCategoriesIdsAsync(selFromListAdditionalParams.PredefinedCategoriesIds);
                selFromListValGenerator.ArrayOfValuesToSelectFrom =
                    selFromListValGenerator.ArrayOfValuesToSelectFrom.Concat(predefinedValuesInCategories.Select(x => x.Value)).ToArray();
            }
        }
    }
}
