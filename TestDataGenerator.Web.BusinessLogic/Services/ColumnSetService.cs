﻿using AutoMapper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Abstractions.Interfaces.Columns;
using TestDataGenerator.Abstractions.Interfaces.ValuesHandling;
using TestDataGenerator.Core.Columns;
using TestDataGenerator.Core.ValueGenerators;
using TestDataGenerator.Core.ValueGenerators.IncrementalGenerator;
using TestDataGenerator.Core.ValueGenerators.ValuePatternParser;
using TestDataGenerator.Core.ValueGenerators.ValueRangeGenerator;
using TestDataGenerator.Web.BusinessLogic.Interfaces;
using TestDataGenerator.Web.DataAccess.Interfaces.Repositories;
using TestDataGenerator.Web.Models.AdditionalColumnParams;
using TestDataGenerator.Web.Models.DomainModels;
using TestDataGenerator.Web.Models.DomainModels.Enums;

namespace TestDataGenerator.Web.BusinessLogic.Services
{
    public class ColumnSetService: IColumnSetService
    {
        public Dictionary<ValueDataTypeEnum, Type> TargetTypeMapper = new Dictionary<ValueDataTypeEnum, Type>() {
                { ValueDataTypeEnum.String, typeof(string) },
                { ValueDataTypeEnum.IntNumber, typeof(long) },
                { ValueDataTypeEnum.FloatPointNumber, typeof(double) },
                { ValueDataTypeEnum.Boolean, typeof(bool) },
                { ValueDataTypeEnum.DateTime, typeof(DateTime) }
            };

        private readonly IColumnSetRepository _colSetRepo;
        private readonly IUserRepository _userRepo;
        private readonly IPredefinedValuesService _predefinedValuesService;
        private readonly IAdditionalColumnParamsService _additionalColumnParamsService;

        private CultureInfo _culture = CultureInfo.InvariantCulture;

        public ColumnSetService(
            IColumnSetRepository colSetRepo,
            IUserRepository userRepo,
            IPredefinedValuesService predefinedValuesService,
            IAdditionalColumnParamsService additionalColumnParamsService)
        {
            _colSetRepo = colSetRepo;
            _userRepo = userRepo;
            _predefinedValuesService = predefinedValuesService;
            _additionalColumnParamsService = additionalColumnParamsService;
        }

        public async Task SaveColumnSetInDataBaseAsync(ColumnSetDm inputColset, string ownerUserName)
        {
            UserDm existingUser = await _userRepo.GetSingleByNameAsync(ownerUserName);
            if (existingUser == null)
                throw new CustomBusinessLogicException($"Username '{ownerUserName}' was not found in database!");

            Dictionary<string, string> columnsValidationResult = 
                await ValidateAllColumnsInColumnSetAsync(inputColset.ColumnSetJson, inputColset.AdditionalColumnParamsJson);
            if (columnsValidationResult.Count > 0) 
            {
                throw new CustomBusinessLogicException(string.Join("\r\n", 
                    columnsValidationResult.Select(pair => $"Column '{pair.Key}' is not valid due to following reasons: '{pair.Value}'")));
            }

            inputColset.OwnerUserId = existingUser.Id;
            inputColset.CreationDate = DateTime.Now;

            long columnSetId = await _colSetRepo.CreateSingleAsync(inputColset);

            return;
        }

        public async Task<ColumnSetDm> GetColumnSetFromDataBaseAsync(long colSetId, string requestingUserName)
        {
            ColumnSetDm columnSetDm = await _colSetRepo.GetSingleByIdAsync(colSetId);

            if (columnSetDm == null)
                throw new CustomBusinessLogicException($"Column set with Id={colSetId} was not found!");

            if (columnSetDm.OwnerUser.Name != requestingUserName)
                throw new CustomBusinessLogicException($"You are unauthorized to use column set with Id={colSetId}!");

            return columnSetDm;
        }

        public async Task<ColumnSetDm[]> GetAllColumnSetsOfUser(string requestingUserName)
        {
            ColumnSetDm[] columnSetDms = await _colSetRepo.GetManyByOwnerUserNameAsync(requestingUserName);

            return columnSetDms;
        }

        public async Task<int> RemoveColumnSetFromDatabaseAsync(long columnSetId, string requestingUserName)
        {
            ColumnSetDm columnSetDm = await _colSetRepo.GetSingleByIdAsync(columnSetId);

            if (columnSetDm == null)
                throw new CustomBusinessLogicException($"Column set with Id={columnSetId} was not found!");

            if (columnSetDm.OwnerUser.Name != requestingUserName)
                throw new CustomBusinessLogicException($"You are unauthorized to delete column set with Id={columnSetId}!");

            int deletedRows = await _colSetRepo.DeleteSingleByIdAsync(columnSetId);

            return deletedRows;
        }

        public async Task<bool> UpdateColumnSetAsync(ColumnSetDm colsetUpdateData, string requestingUserName)
        {
            ColumnSetDm existingColumnSetDm = await _colSetRepo.GetSingleByIdAsync(colsetUpdateData.Id);

            if (existingColumnSetDm == null)
                throw new CustomBusinessLogicException($"Column set with Id={colsetUpdateData.Id} was not found!");

            if (existingColumnSetDm.OwnerUser.Name != requestingUserName)
                throw new CustomBusinessLogicException($"You are unauthorized to delete column set with Id={colsetUpdateData.Id}!");

            Dictionary<string, string> columnsValidationResult = 
                await ValidateAllColumnsInColumnSetAsync(colsetUpdateData.ColumnSetJson, colsetUpdateData.AdditionalColumnParamsJson);
            if (columnsValidationResult.Count > 0)
            {
                throw new CustomBusinessLogicException(string.Join("\r\n",
                    columnsValidationResult.Select(pair => $"Column '{pair.Key}' is not valid due to following reasons: '{pair.Value}'")));
            }

            // updating properties
            existingColumnSetDm.ColumnSetJson = colsetUpdateData.ColumnSetJson;
            existingColumnSetDm.Name = colsetUpdateData.Name;
            existingColumnSetDm.AdditionalColumnParamsJson = colsetUpdateData.AdditionalColumnParamsJson;

            bool updateResult = await _colSetRepo.UpdateSingleAsync(existingColumnSetDm);

            return updateResult;
        }

        public async Task<Dictionary<string, string>> ValidateAllColumnsInColumnSetAsync(string colSetAsCustomJson, string additionalParamsAsJson)
        {
            AdditionalColumnParameters[] additionalParams = additionalParamsAsJson == null ? 
                new AdditionalColumnParameters[0] : JsonConvert.DeserializeObject<AdditionalColumnParameters[]>(additionalParamsAsJson);
            ColumnSet colSet = ColumnSet.DeserializeFromJson(colSetAsCustomJson);
            await _additionalColumnParamsService.ApplyAdditionalParamsAsync(colSet, additionalParams);
            Dictionary<string, string> result = new Dictionary<string, string>();

            foreach (IColumn col in colSet)
            {
                if (!col.IsValid(out string errMessage))
                    result[col.GetColumnName()] = errMessage;
            }

            return result;
        }
    }
}
