﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Web.BusinessLogic.Interfaces;
using TestDataGenerator.Web.DataAccess.Interfaces.Repositories;
using TestDataGenerator.Web.Models.DomainModels;

namespace TestDataGenerator.Web.BusinessLogic.Services
{
    public class PredefinedCategoriesService : IPredefinedCategoriesService
    {
        private readonly IPredefinedCategoryRepository _predefinedCategoryRepo;
        private readonly IUserRepository _userRepo;

        public PredefinedCategoriesService(
            IPredefinedCategoryRepository predefinedCategoryRepo,
            IUserRepository userRepo,
            IMapper mapper)
        {
            _predefinedCategoryRepo = predefinedCategoryRepo;
            _userRepo = userRepo;
        }

        public async Task<PredefinedCategoryDm[]> GetPredefinedCategoriesFromDatabaseAsync(string contributorUserName, bool includePublic)
        {
            PredefinedCategoryDm[] queriedCategories;

            if (contributorUserName == null)
                queriedCategories = await _predefinedCategoryRepo.GetAllAsync();
            else
            {
                UserDm queriedUser = await _userRepo.GetSingleByNameAsync(contributorUserName);
                if (queriedUser == null)
                    throw new CustomBusinessLogicException($"User '{contributorUserName}' was not found!");
                else
                    queriedCategories = await _predefinedCategoryRepo.GetManyByContributorUserNameAsync(contributorUserName, includePublic);
            }

            return queriedCategories;
        }

        public async Task<PredefinedCategoryDm[]> GetPublicPredefinedCategoriesFromDatabaseAsync()
        {
            PredefinedCategoryDm[] queriedCategories = await _predefinedCategoryRepo.GetPublicCategoriesAsync();

            return queriedCategories;
        }

        public async Task<PredefinedCategoryDm> CreatePredefinedCategoryAsync(PredefinedCategoryDm inputModel, string contributorUserName)
        {
            PredefinedCategoryDm existingCategory = await _predefinedCategoryRepo.GetSingleByNameAsync(inputModel.Name);
            if (existingCategory != null)
                throw new CustomBusinessLogicException($"Category with name '{inputModel.Name}' already exists!");

            UserDm contributorUser = await _userRepo.GetSingleByNameAsync(contributorUserName);
            if (contributorUser == null)
                throw new CustomBusinessLogicException($"User '{contributorUserName}' was not found!");

            inputModel.ContributorUserId = contributorUser.Id;
            inputModel.ContributorUser = contributorUser;
            inputModel.CreationDate = DateTime.Now;

            await _predefinedCategoryRepo.CreateSingleAsync(inputModel);

            return inputModel;
        }

        public async Task<int> DeletePredefinedCategoriesAsync(PredefinedCategoryDm[] categoriesToDelete, string requestorUserName)
        {
            PredefinedCategoryDm[] userCategories = await _predefinedCategoryRepo.GetManyByContributorUserNameAsync(requestorUserName, false);

            if (userCategories == null || userCategories.Length == 0)
                throw new CustomBusinessLogicException("No categories were found!");

            long[] notFoundIdsInDatabase = categoriesToDelete
                .Select(cd => cd.Id)
                .Except(userCategories.Select(c => c.Id))
                .ToArray();

            PredefinedCategoryDm[] notFoundCategoriesDto = categoriesToDelete
                .Where(c => notFoundIdsInDatabase.Contains(c.Id))
                .ToArray();

            if (notFoundIdsInDatabase.Length > 0)
                throw new CustomBusinessLogicException($"Categories {string.Join(", ", notFoundCategoriesDto.Select(c => $"'{c.Name}'"))} " +
                    $"can't be deleted since you are not their owner!");

            int affectedRows = await _predefinedCategoryRepo.DeleteManyByIdsAsync(categoriesToDelete.Select(c => c.Id).ToArray());

            return affectedRows;
        }
    }
}
