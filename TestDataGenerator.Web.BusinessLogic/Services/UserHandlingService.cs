﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Web.BusinessLogic.Interfaces;
using TestDataGenerator.Web.DataAccess.Interfaces.Repositories;
using TestDataGenerator.Web.Models.DomainModels;

namespace TestDataGenerator.Web.BusinessLogic.Services
{
    public class UserHandlingService: IUserHandlingService
    {
        private readonly IUserRepository _userRepo;

        public UserHandlingService(IUserRepository userRepo)
        {
            _userRepo = userRepo;
        }

        public async Task<UserDm> RegisterUserAsync(UserDm userDm)
        {
            UserDm existingUser = await _userRepo.GetSingleByNameAsync(userDm.Name);

            if (existingUser != null)
                throw new CustomBusinessLogicException($"Username '{userDm.Name}' is already taken. Please choose another one.");

            string hashedPassword = BCrypt.Net.BCrypt.HashPassword(userDm.Password);
            userDm.Password = hashedPassword;
            userDm.RegistrationDate = DateTime.Now;
            await _userRepo.CreateSingleAsync(userDm);

            return userDm;
        }

        public async Task<bool> UserCredentialsAreCorrectAsync(string username, string password)
        {
            UserDm userModel = await _userRepo.GetSingleByNameAsync(username);

            if (userModel == null)
                return false;

            bool passwordIsCorrect = BCrypt.Net.BCrypt.Verify(password, userModel.Password);

            if (!passwordIsCorrect)
                return false;

            return true;
        }
    }
}
