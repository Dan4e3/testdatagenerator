﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Web.BusinessLogic.Interfaces;
using TestDataGenerator.Web.DataAccess.Interfaces.Repositories;
using TestDataGenerator.Web.Models.DomainModels;
using TestDataGenerator.Web.Models.DomainModels.Enums;

namespace TestDataGenerator.Web.BusinessLogic.Services
{
    public class PredefinedValuesService: IPredefinedValuesService
    {
        private readonly IPredefinedValueRepository _predefinedValuesRepo;
        private readonly IPredefinedCategoryRepository _predefinedCategoryRepo;
        private readonly IUserRepository _userRepo;

        public PredefinedValuesService(
            IPredefinedValueRepository predefinedValuesRepo,
            IPredefinedCategoryRepository predefinedCategoryRepo,
            IUserRepository userRepo,
            IMapper mapper)
        {
            _predefinedValuesRepo = predefinedValuesRepo;
            _predefinedCategoryRepo = predefinedCategoryRepo;
            _userRepo = userRepo;
        }

        public async Task<PredefinedValueDm[]> GetPredefinedValuesAsync(string categoryName, string requestingUserName, int? valuesToFetch = 1000)
        {
            PredefinedCategoryDm existingCategory = await _predefinedCategoryRepo.GetSingleByNameAsync(categoryName);

            if (existingCategory == null)
                throw new CustomBusinessLogicException($"Category '{categoryName}' was not found!");

            if (existingCategory.ContributorUser.Name != requestingUserName && !existingCategory.IsPublic)
                throw new CustomBusinessLogicException($"Category '{categoryName}' can not be accessed because you are not " +
                    $"owner and category is not considered public!");

            PredefinedValueDm[] predefinedValues = await _predefinedValuesRepo.GetManyByCategoryNameAsync(categoryName);
            PredefinedValueDm[] filteredValues = predefinedValues.Take(valuesToFetch.Value).ToArray();

            return filteredValues;
        }

        public async Task<PredefinedValueDm[]> GetPredefinedValuesPaginatedAsync(string categoryName, string requestingUserName, 
            int pageNumber, int valuesPerPage)
        {
            PredefinedCategoryDm existingCategory = await _predefinedCategoryRepo.GetSingleByNameAsync(categoryName);

            if (existingCategory == null)
                throw new CustomBusinessLogicException($"Category '{categoryName}' was not found!");

            if (existingCategory.ContributorUser.Name != requestingUserName && !existingCategory.IsPublic)
                throw new CustomBusinessLogicException($"Category '{categoryName}' can not be accessed because you are not " +
                    $"owner and category is not considered public!");

            int valuesToSkip = pageNumber * valuesPerPage;

            PredefinedValueDm[] predefinedValues = await _predefinedValuesRepo.GetManyByCategoryNamePaginatedAsync(categoryName, 
                valuesToSkip, valuesPerPage);

            return predefinedValues;
        }

        public async Task<long> GetPredefinedValuesCountInCategoryAsync(string categoryName, string requestingUserName)
        {
            PredefinedCategoryDm existingCategory = await _predefinedCategoryRepo.GetSingleByNameAsync(categoryName);

            if (existingCategory == null)
                throw new CustomBusinessLogicException($"Category '{categoryName}' was not found!");

            if (existingCategory.ContributorUser.Name != requestingUserName && !existingCategory.IsPublic)
                throw new CustomBusinessLogicException($"Category '{categoryName}' can not be accessed because you are not " +
                    $"owner and category is not considered public!");

            long valuesCount = await _predefinedValuesRepo.GetValuesCountByCategoryNameAsync(categoryName);
            return valuesCount;
        }

        public async Task<int> AddPredefinedValuesAsync(PredefinedValueDm[] inputData, string categoryName, string inserterUserName)
        {
            PredefinedCategoryDm queriedCategory = await _predefinedCategoryRepo.GetSingleByNameAsync(categoryName);

            if (queriedCategory == null)
                throw new CustomBusinessLogicException($"Category '{categoryName}' was not found in database!");

            UserDm queriedUser = await _userRepo.GetSingleByNameAsync(inserterUserName);

            if (queriedUser == null)
                throw new CustomBusinessLogicException($"User '{inserterUserName}' was not found in database!");

            DateTime creationDate = DateTime.Now;
            foreach (PredefinedValueDm predefinedValueDm in inputData)
            {
                predefinedValueDm.CategoryId = queriedCategory.Id;
                predefinedValueDm.ContributorUserId = queriedUser.Id;
                predefinedValueDm.CreationDate = creationDate;
            }

            int insertedRows = await _predefinedValuesRepo.CreateManyAsync(inputData);

            return insertedRows;
        }

        public async Task<PredefinedValueDm> AddPredefinedValueAsync(PredefinedValueDm inputData, string inserterUserName)
        {
            PredefinedCategoryDm queriedCategory = await _predefinedCategoryRepo.GetSingleByNameAsync(inputData.Category.Name);
            if (queriedCategory == null)
                throw new CustomBusinessLogicException($"Category '{inputData.Category.Name}' was not found in database!");

            UserDm queriedUser = await _userRepo.GetSingleByNameAsync(inserterUserName);
            if (queriedUser == null)
                throw new CustomBusinessLogicException($"User '{inserterUserName}' was not found in database!");

            PredefinedValueDm existingValue = await _predefinedValuesRepo.GetSingleByValueAndDataTypeAsync(inputData.Value, inputData.DataType);
            if (existingValue != null)
                throw new CustomBusinessLogicException($"Value '{existingValue.Value}' " +
                    $"with type '{Enum.GetName(typeof(ValueDataTypeEnum), existingValue.DataType)}' already exists!");

            inputData.CategoryId = queriedCategory.Id;
            inputData.ContributorUserId = queriedUser.Id;
            inputData.CreationDate = DateTime.Now;

            await _predefinedValuesRepo.CreateSingleAsync(inputData);

            return inputData;
        }

        public async Task<int> DeletePredefinedValuesAsync(IEnumerable<long> predefinedValuesIdsToDelete)
        {
            int deletedRows = await _predefinedValuesRepo.DeleteManyByIdsAsync(predefinedValuesIdsToDelete.ToArray());

            return deletedRows;
        }

        private void TryConvertStringValueToTargetType(PredefinedValueDm valueDm)
        {
            try
            {
                switch (valueDm.DataType)
                {
                    case ValueDataTypeEnum.String:
                        return;

                    case ValueDataTypeEnum.IntNumber:
                        Convert.ToInt64(valueDm.Value);
                        break;

                    case ValueDataTypeEnum.FloatPointNumber:
                        Convert.ToDouble(valueDm.Value);
                        break;

                    case ValueDataTypeEnum.Boolean:
                        Convert.ToBoolean(valueDm.Value);
                        break;

                    case ValueDataTypeEnum.DateTime:
                        Convert.ToDateTime(valueDm.Value);
                        break;

                    default:
                        throw new CustomBusinessLogicException("Provided unknown type for value!");
                }
            }
            catch
            {
                throw new CustomBusinessLogicException($"Can't convert value '{valueDm.Value}' " +
                    $"to target type '{Enum.GetName(typeof(ValueDataTypeEnum), valueDm.DataType)}'!");
            }
        }
    }
}
