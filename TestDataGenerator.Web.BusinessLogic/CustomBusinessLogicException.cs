﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator.Web.BusinessLogic
{
    public class CustomBusinessLogicException: Exception
    {
        public CustomBusinessLogicException(string errorMessage): base(errorMessage)
        {

        }
    }
}
